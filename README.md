# Two lizards individuals

To assess the demographic history between L. viridis and L. bilineata we used
the blockwise composite likelihood approach. 
We have two unphased diploid genome (one individual from Viridis and one from
Bilineata) therefore there are four types of sites: heterozygous sites in
either A or B, shared heterozygous sites and fixed differences. 

# Licence
Everything (that I made in this project) would be under "THE BEER-WARE LICENSE"
(Revision 42) unless we include a license that specifies otherwise. Mathematica
notebooks_S1 are not mine and their licence are the same as paper below:
"Efficient Strategies for Calculating Blockwise Likelihoods under the Coalescent
Konrad Lohse, Martin Chmelik, Simon H. Martin and Nick H. Barton
GENETICS Early online December 29, 2015;"

## Preparation:
To count the number of the four mutation types defined by the joint SFS we used Heffalump query.

`heffalump query -f 'anno filter/Lacerta_Viridis_Intergenic.BED' \`
`--block-length=220 --num-observed=200 --accum '"{".  \`
`{(TGT1[0] != TGT2[0]) && (TGT1[1] == TGT2[1]) && (TGT1[1] != "N")} . "," . \`
`{(TGT1[1] != TGT2[1]) && (TGT1[0] == TGT2[0]) && (TGT1[0] != "N")} . "," . \`
`{(TGT1[0] == TGT2[0]) && (TGT1[1] == TGT2[1]) && (TGT1[0] != TGT1[1]) &&( TGT1[0] != "N") && (TGT1[1] != "N")} . "," . \`
`{(TGT1[0] != TGT2[0]) && (TGT1[1] != TGT2[1]) && (TGT1[0] != "N") && (TGT1[1] != "N")} . \`
`"},"' -o Lacerta_Viridis_bilineata_Intergenic.200.block.config \`
`-r Viridis.2bit viridis_poor-patches_all.hef bilineata_poor-patches_all.hef`

## Generating function and results

All generating functions and their likelihood results given our data are in
`GFs` and `results` folder.

## Name
reverseGame is inspired by "Day of the Hunters" (1950) from Isaac Asimov.
