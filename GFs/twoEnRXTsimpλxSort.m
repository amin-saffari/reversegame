{(-4*M*(2 + M + 2*\[Lambda][{x}] + 4*\[Omega][{{Y}, {X, X, Y}}] - 
     2*\[Omega][{{X, X}, {Y, Y}}])*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{X, X}, {Y, Y}}]))/
   (E^((T*(2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{X, X}, {Y, Y}}]))/2)*(-2 + M + 2*\[Lambda][{x}])*
    (M + 2*\[Lambda][{x}] + 4*\[Omega][{{Y}, {X, X, Y}}] - 
     2*\[Omega][{{X, X}, {Y, Y}}])*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{X, X}, {Y, Y}}])*(-2 - 2*\[Omega][{{Y}, {X, X, Y}}] + 
     \[Omega][{{X, X}, {Y, Y}}])*(1 + 2*\[Omega][{{X, X}, {Y, Y}}])*
    (2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}])) - 
  (8*\[Lambda][{x}]*(1 + \[Omega][{{X, X}, {Y, Y}}])*
    (1 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))/
   (E^((T*(2 + M + 4*\[Omega][{{Y}, {X, X, Y}}] + 
        2*\[Omega][{{X, X}, {Y, Y}}]))/2)*(M + 2*\[Lambda][{x}] + 
     4*\[Omega][{{X}, {X, Y, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
    (1 + 2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
    (3 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
    (1 + 2*\[Omega][{{X, X}, {Y, Y}}])*
    (2 + M + 4*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}])) - 
  (8*\[Lambda][{x}]*\[Omega][{{X, X}, {Y, Y}}])/
   (E^((T*(M + 4*\[Omega][{{X, X}, {Y, Y}}]))/2)*(M + 2*\[Lambda][{x}] + 
     4*\[Omega][{{X}, {X, Y, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
    (-1 - 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
    (1 + 2*\[Omega][{{X, X}, {Y, Y}}])*(M + 4*\[Omega][{{X, X}, {Y, Y}}])) - 
  (4*M*(2*\[Omega][{{Y}, {X, X, Y}}]^2 + \[Omega][{{X}, {X, Y, Y}}]^2*
      (2 + 4*\[Omega][{{X, X}, {Y, Y}}]) + 
     3*(3 + 4*\[Omega][{{X, X}, {Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}]^2) + 
     \[Omega][{{Y}, {X, X, Y}}]*(9 + 8*\[Omega][{{X, X}, {Y, Y}}] + 
       2*\[Omega][{{X, X}, {Y, Y}}]^2) + \[Omega][{{X}, {X, Y, Y}}]*
      (9 + 14*\[Omega][{{X, X}, {Y, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
       4*\[Omega][{{Y}, {X, X, Y}}]*(1 + \[Omega][{{X, X}, {Y, Y}}]))))/
   (E^((T*(6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
        4*\[Omega][{{Y}, {X, X, Y}}]))/2)*(-4 + M + 2*\[Lambda][{x}])*
    (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
    (6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
    (2 + 2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
    (3 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
    (3 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
    (1 + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
  (2*M^2 + 2*M*(6 + M)*\[Omega][{{X}, {X, Y, Y}}] + 
    8*(2 + M)*\[Omega][{{X}, {X, Y, Y}}]^2 - 2*M*\[Omega][{{X, X}, {Y, Y}}] + 
    M^2*\[Omega][{{X, X}, {Y, Y}}] - 4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
    2*M*\[Omega][{{X, X}, {Y, Y}}]^2 + 4*\[Lambda][{x}]^3*
     (2 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}]) + 
    4*\[Lambda][{x}]^2*(2*(-1 + M) + 4*\[Omega][{{X}, {X, Y, Y}}]^2 + 
      (2 + M)*\[Omega][{{X, X}, {Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}]^2 + 
      2*\[Omega][{{X}, {X, Y, Y}}]*(M + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
    \[Lambda][{x}]*(2*M^2 + 8*(-4 + M)*\[Omega][{{X}, {X, Y, Y}}]^2 + 
      (-12 + 6*M + M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
      2*M*\[Omega][{{X, X}, {Y, Y}}]^2 + 2*\[Omega][{{X}, {X, Y, Y}}]*
       (-4 + 2*M + M^2 + 4*(-2 + M)*\[Omega][{{X, X}, {Y, Y}}])))/
   (E^(T*(M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{X, X}, {Y, Y}}]))*(-2 + M + 2*\[Lambda][{x}])*
    (M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] - 
     2*\[Omega][{{X, X}, {Y, Y}}])*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{X, X}, {Y, Y}}])*(M + \[Lambda][{x}] + 
     2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
    (-1 - 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
    (1 + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
  (-(E^(T*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      (24*E^(2*T) + 34*E^(2*T)*M - 12*E^((M*T)/2 + T*\[Lambda][{x}])*M + 
       5*E^(2*T)*M^2 - 12*E^((M*T)/2 + T*\[Lambda][{x}])*M^2 + 
       4*E^(T*(3 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
           2*\[Omega][{{Y}, {X, X, Y}}]))*M^2 - 
       E^(T*(3 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
           2*\[Omega][{{Y}, {X, X, Y}}]))*M^3 + 
       8*(-2*E^((M*T)/2 + T*\[Lambda][{x}])*M + E^(2*T)*(4 + M))*
        \[Omega][{{X}, {X, Y, Y}}]^2 + 
       2*(-4*E^((M*T)/2 + T*\[Lambda][{x}])*M*(4 + M) + 
         E^(2*T)*(32 + 20*M + M^2))*\[Omega][{{Y}, {X, X, Y}}] + 
       8*(-2*E^((M*T)/2 + T*\[Lambda][{x}])*M + E^(2*T)*(4 + M))*
        \[Omega][{{Y}, {X, X, Y}}]^2 + 2*\[Omega][{{X}, {X, Y, Y}}]*
        (-4*E^((M*T)/2 + T*\[Lambda][{x}])*M*(4 + M) + 
         E^(2*T)*(32 + 20*M + M^2) + 8*(-2*E^((M*T)/2 + T*\[Lambda][{x}])*M + 
           E^(2*T)*(4 + M))*\[Omega][{{Y}, {X, X, Y}}]))*
      (M + 4*\[Omega][{{X}, {X, Y, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
      (2 + M + 4*\[Omega][{{Y}, {X, X, Y}}] + 
       2*\[Omega][{{X, X}, {Y, Y}}])) + 
    4*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        4*\[Omega][{{Y}, {X, X, Y}}]))*\[Lambda][{x}]^3*
     (6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
     (12*E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2) - 
      10*E^(T*\[Omega][{{X, X}, {Y, Y}}]) - 
      5*E^(T*\[Omega][{{X, X}, {Y, Y}}])*M + 
      6*E^(T*(1 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M + 
      8*(E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2) - 
        E^(T*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^2 + 
      12*E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2)*
       \[Omega][{{X, X}, {Y, Y}}] - 10*E^(T*\[Omega][{{X, X}, {Y, Y}}])*
       \[Omega][{{X, X}, {Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}]*
       (14*E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2) + 
        E^(T*(1 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M - 
        E^(T*\[Omega][{{X, X}, {Y, Y}}])*(12 + M) + 
        2*(E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2) - 
          E^(T*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]) + 
      2*\[Omega][{{X}, {X, Y, Y}}]*
       (2*E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2) - 
        2*E^(T*\[Omega][{{X, X}, {Y, Y}}]) - E^(T*\[Omega][{{X, X}, {Y, Y}}])*
         M + E^(T*(1 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M + 
        4*(E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2) - 
          E^(T*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}] + 
        2*(E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2) - 
          E^(T*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}])) + 
    4*\[Lambda][{x}]^2*
     (-72*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
           8*\[Omega][{{Y}, {X, X, Y}}]))/2) + 
      60*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])) + 
      96*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
           8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M - 
      20*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M - 
      72*E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M + 
      12*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
           8*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))/2)*
       M + 18*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 
           8*\[Omega][{{X}, {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*
       M^2 - 35*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^2 + 
      26*E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^2 + 
      6*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
           8*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))/2)*
       M^2 - 5*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^3 + 
      7*E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^3 + 
      64*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}]))*
       (E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2) - 
        E^(T*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^4 - 
      72*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
           8*\[Omega][{{Y}, {X, X, Y}}]))/2)*\[Omega][{{X, X}, {Y, Y}}] + 
      48*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
       \[Omega][{{X, X}, {Y, Y}}] + 
      96*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
           8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*\[Omega][{{X, X}, {Y, Y}}] - 
      58*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M*
       \[Omega][{{X, X}, {Y, Y}}] - 
      36*E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M*
       \[Omega][{{X, X}, {Y, Y}}] + 
      12*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
           8*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))/2)*M*
       \[Omega][{{X, X}, {Y, Y}}] + 
      18*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
           8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^2*\[Omega][{{X, X}, {Y, Y}}] - 
      11*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^2*
       \[Omega][{{X, X}, {Y, Y}}] - 
      4*E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^2*
       \[Omega][{{X, X}, {Y, Y}}] - 
      12*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
       \[Omega][{{X, X}, {Y, Y}}]^2 - 
      2*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M*
       \[Omega][{{X, X}, {Y, Y}}]^2 + 
      32*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^3*
       (-2*E^(T*\[Omega][{{X, X}, {Y, Y}}])*(4 + M) + 
        E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2)*
         (9 + 2*M) + (E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][
                {{X}, {X, Y, Y}}]))/2) - E^(T*\[Omega][{{X, X}, {Y, Y}}]))*
         \[Omega][{{X, X}, {Y, Y}}]) + 
      16*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^3*
       (2*E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2) - 
        2*E^(T*\[Omega][{{X, X}, {Y, Y}}]) - E^(T*\[Omega][{{X, X}, {Y, Y}}])*
         M + E^(T*(1 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M + 
        4*(E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2) - 
          E^(T*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}] + 
        2*(E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2) - 
          E^(T*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]) + 
      4*\[Omega][{{Y}, {X, X, Y}}]^2*
       (E^(T*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}]))*
         (4*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X, X}, {Y, Y}}])*M + 
          2*E^(T*(3 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
              2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
           (-2 + M)*M + E^((T*(4 + M + 2*\[Lambda][{x}] + 4*\[Omega][
                 {{X}, {X, Y, Y}}]))/2)*(68 + 72*M + 3*M^2) - 
          E^(T*(2 + \[Omega][{{X, X}, {Y, Y}}]))*(56 + 60*M + 5*M^2)) - 
        2*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}]))*
         (E^(T*(1 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
              2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M - 
          4*E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2)*
           (4 + M) + E^(T*\[Omega][{{X, X}, {Y, Y}}])*(16 + 3*M))*
         \[Omega][{{X, X}, {Y, Y}}]) + 2*\[Omega][{{Y}, {X, X, Y}}]*
       (2*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
             8*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))/2)*
         M*(8 + M) + E^(T*(5 + 2*M + \[Lambda][{x}] + 
            4*\[Omega][{{X}, {X, Y, Y}}] + 6*\[Omega][{{Y}, {X, X, Y}}] + 
            \[Omega][{{X, X}, {Y, Y}}]))*M*(-36 + 18*M + M^2) + 
        E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
             8*\[Omega][{{Y}, {X, X, Y}}]))/2)*(-36 + 160*M + 21*M^2) - 
        E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
         (-32 + 104*M + 32*M^2 + M^3) + 
        E^(T*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}]))*
         (4*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X, X}, {Y, Y}}])*M - 
          E^(T*(3 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
              2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M*
           (18 + M) - 2*E^(T*(2 + \[Omega][{{X, X}, {Y, Y}}]))*
           (22 + 22*M + M^2) + E^((T*(4 + M + 2*\[Lambda][{x}] + 4*
                \[Omega][{{X}, {X, Y, Y}}]))/2)*(36 + 64*M + 3*M^2))*
         \[Omega][{{X, X}, {Y, Y}}] - 
        4*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
         \[Omega][{{X, X}, {Y, Y}}]^2) + 
      4*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
       (32*E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2) - 
        24*E^(T*\[Omega][{{X, X}, {Y, Y}}]) + 
        8*E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2)*M - 
        18*E^(T*\[Omega][{{X, X}, {Y, Y}}])*M + 
        14*E^(T*(1 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M - 
        3*E^(T*\[Omega][{{X, X}, {Y, Y}}])*M^2 + 
        3*E^(T*(1 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^2 + 
        48*(E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2) - 
          E^(T*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^2 + 
        (-2*E^(T*(1 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
              2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M + 
          8*E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2)*
           (4 + M) - 6*E^(T*\[Omega][{{X, X}, {Y, Y}}])*(4 + M))*
         \[Omega][{{X, X}, {Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]*
         (E^(T*(1 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
              2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M - 
          3*E^(T*\[Omega][{{X, X}, {Y, Y}}])*(3 + M) + 
          E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2)*
           (11 + 2*M) + 3*(E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][
                  {{X}, {X, Y, Y}}]))/2) - E^(T*\[Omega][{{X, X}, {Y, Y}}]))*
           \[Omega][{{X, X}, {Y, Y}}])) + 2*\[Omega][{{X}, {X, Y, Y}}]*
       (36*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
             8*\[Omega][{{Y}, {X, X, Y}}]))/2) - 
        16*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])) + 
        64*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
             8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M - 
        46*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M + 
        4*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
             8*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))/2)*
         M + 3*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, 
                {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^2 - 
        21*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^2 + 
        22*E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^2 + 
        2*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
             8*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))/2)*
         M^2 - E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^3 + 
        E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^3 + 
        96*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}]))*
         (E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2) - 
          E^(T*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^3 + 
        E^(T*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}]))*
         (4*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X, X}, {Y, Y}}])*M - 
          E^(T*(3 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
              2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M*
           (18 + M) - 2*E^(T*(2 + \[Omega][{{X, X}, {Y, Y}}]))*
           (10 + 20*M + M^2) + E^((T*(4 + M + 2*\[Lambda][{x}] + 4*
                \[Omega][{{X}, {X, Y, Y}}]))/2)*(36 + 64*M + 3*M^2))*
         \[Omega][{{X, X}, {Y, Y}}] - 
        4*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
         \[Omega][{{X, X}, {Y, Y}}]^2 + 
        8*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^2*
         (E^(T*(1 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
              2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M + 
          E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2)*
           (38 + 8*M) - E^(T*\[Omega][{{X, X}, {Y, Y}}])*(32 + 9*M) + 
          6*(E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/
              2) - E^(T*\[Omega][{{X, X}, {Y, Y}}]))*
           \[Omega][{{X, X}, {Y, Y}}]) + 2*\[Omega][{{Y}, {X, X, Y}}]*
         (E^(T*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][
                {{Y}, {X, X, Y}}]))*(4*E^((M*T)/2 + T*\[Lambda][{x}] + T*
                \[Omega][{{X, X}, {Y, Y}}])*M + 
            5*E^(T*(3 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
                2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M*
             (2 + M) - 4*E^(T*(2 + \[Omega][{{X, X}, {Y, Y}}]))*
             (17 + 19*M + 2*M^2) + E^((T*(4 + M + 2*\[Lambda][{x}] + 
                 4*\[Omega][{{X}, {X, Y, Y}}]))/2)*(100 + 80*M + 3*M^2)) + 
          4*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][
                {{Y}, {X, X, Y}}]))*(-(E^(T*(1 + M + \[Lambda][{x}] + 
                 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, 
                     Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M) + 
            4*E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2)*
             (4 + M) - E^(T*\[Omega][{{X, X}, {Y, Y}}])*(14 + 3*M))*
           \[Omega][{{X, X}, {Y, Y}}]))) + \[Lambda][{x}]*
     (-576*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
           8*\[Omega][{{Y}, {X, X, Y}}]))/2) + 
      480*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])) - 
      528*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
           8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M + 
      320*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M + 
      48*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
           8*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))/2)*
       M + 72*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 
           8*\[Omega][{{X}, {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*
       M^2 - 20*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^2 - 
      160*E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^2 + 
      96*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
           8*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))/2)*
       M^2 + 24*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 
           8*\[Omega][{{X}, {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*
       M^3 - 40*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^3 + 
      12*E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^3 + 
      36*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
           8*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))/2)*
       M^3 - 5*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^4 + 
      10*E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^4 + 
      128*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}]))*
       (E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2) - 
        E^(T*\[Omega][{{X, X}, {Y, Y}}]))*(-4 + M)*\[Omega][{{Y}, {X, X, Y}}]^
        4 - 576*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 
           8*\[Omega][{{X}, {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*
       \[Omega][{{X, X}, {Y, Y}}] + 
      528*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
       \[Omega][{{X, X}, {Y, Y}}] - 
      528*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
           8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*\[Omega][{{X, X}, {Y, Y}}] + 
      88*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M*
       \[Omega][{{X, X}, {Y, Y}}] + 
      288*E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M*
       \[Omega][{{X, X}, {Y, Y}}] + 
      72*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
           8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^2*\[Omega][{{X, X}, {Y, Y}}] - 
      72*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^2*
       \[Omega][{{X, X}, {Y, Y}}] - 
      48*E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^2*
       \[Omega][{{X, X}, {Y, Y}}] + 
      48*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
           8*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))/2)*
       M^2*\[Omega][{{X, X}, {Y, Y}}] + 
      24*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
           8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^3*\[Omega][{{X, X}, {Y, Y}}] - 
      12*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^3*
       \[Omega][{{X, X}, {Y, Y}}] - 
      8*E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^3*
       \[Omega][{{X, X}, {Y, Y}}] + 
      48*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
       \[Omega][{{X, X}, {Y, Y}}]^2 - 
      16*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M*
       \[Omega][{{X, X}, {Y, Y}}]^2 - 
      48*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
           8*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))/2)*M*
       \[Omega][{{X, X}, {Y, Y}}]^2 - 
      4*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^2*
       \[Omega][{{X, X}, {Y, Y}}]^2 - 
      8*E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^2*
       \[Omega][{{X, X}, {Y, Y}}]^2 + 
      32*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^3*
       (-8*E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2) + 
        12*E^(T*\[Omega][{{X, X}, {Y, Y}}]) + 
        2*E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2)*M + 
        4*E^(T*\[Omega][{{X, X}, {Y, Y}}])*M - 
        4*E^(T*(1 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M - 
        E^(T*\[Omega][{{X, X}, {Y, Y}}])*M^2 + 
        E^(T*(1 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^2 + 
        4*(-(E^(T*\[Omega][{{X, X}, {Y, Y}}])*(-6 + M)) + 
          E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2)*
           (-4 + M))*\[Omega][{{Y}, {X, X, Y}}] + 
        2*(-(E^(T*\[Omega][{{X, X}, {Y, Y}}])*(-6 + M)) + 
          E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2)*
           (-4 + M))*\[Omega][{{X, X}, {Y, Y}}]) + 
      32*E^(T*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^3*
       (4*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X, X}, {Y, Y}}])*M + 
        E^(T*(2 + \[Omega][{{X, X}, {Y, Y}}]))*(80 - 10*M - 3*M^2) + 
        E^((T*(4 + M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/2)*
         (-88 + 10*M + 3*M^2) + 
        2*(E^((T*(4 + M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}]))/
             2)*(-4 + M) - E^(T*(2 + \[Omega][{{X, X}, {Y, Y}}]))*(-2 + M))*
         \[Omega][{{X, X}, {Y, Y}}]) + 8*\[Omega][{{Y}, {X, X, Y}}]^2*
       (E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
         (-4 + M)*M^2 + 8*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 
             4*\[Omega][{{X}, {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}] + 
             2*\[Omega][{{X, X}, {Y, Y}}]))/2)*M*(5 + 2*M) + 
        E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
         (512 + 36*M - 42*M^2 - 3*M^3) + 
        2*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
             8*\[Omega][{{Y}, {X, X, Y}}]))/2)*(-296 - 38*M + 24*M^2 + M^3) + 
        2*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}]))*
         (-(E^(T*(1 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*
                \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
            (-4 + M)*M) - 2*E^(T*\[Omega][{{X, X}, {Y, Y}}])*
           (-30 + 9*M + M^2) + E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][
                 {{X}, {X, Y, Y}}]))/2)*(-80 + 8*M + 3*M^2))*
         \[Omega][{{X, X}, {Y, Y}}] - 
        8*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
         \[Omega][{{X, X}, {Y, Y}}]^2) + 2*\[Omega][{{Y}, {X, X, Y}}]*
       (E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^2*
         (-88 + 22*M + M^2) + 4*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 
             4*\[Omega][{{X}, {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}] + 
             2*\[Omega][{{X, X}, {Y, Y}}]))/2)*M*(28 + 32*M + 3*M^2) + 
        4*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
             8*\[Omega][{{Y}, {X, X, Y}}]))/2)*(-360 - 178*M + 39*M^2 + 
          7*M^3) - E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
         (-1216 - 424*M + 124*M^2 + 38*M^3 + M^4) + 
        2*(8*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, 
                  {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}] + 2*
                \[Omega][{{X, X}, {Y, Y}}]))/2)*M^2 - 
          E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
              6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M*
           (-72 + 18*M + M^2) + 2*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*
                \[Omega][{{X}, {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/
             2)*(-216 - 46*M + 21*M^2 + M^3) - 
          E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][
                {{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
           (-376 + 36*M + 32*M^2 + M^3))*\[Omega][{{X, X}, {Y, Y}}] - 
        16*E^(T*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
         (E^((M*T)/2 + T*\[Lambda][{x}])*M + E^(2*T)*(2 + M))*
         \[Omega][{{X, X}, {Y, Y}}]^2) + 16*\[Omega][{{X}, {X, Y, Y}}]^2*
       (-80*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, 
                {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2) + 
        80*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])) + 
        8*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
             8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M + 
        32*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M - 
        36*E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M + 
        8*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
             8*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))/2)*
         M + 3*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, 
                {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^2 - 
        6*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^2 + 
        5*E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^2 + 
        4*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
             8*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))/2)*
         M^2 - E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^3 + 
        E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^3 + 
        8*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}]))*(E^(T*\[Omega][{{X, X}, {Y, Y}}])*
           (16 - 3*M) + 3*E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][
                 {{X}, {X, Y, Y}}]))/2)*(-4 + M))*\[Omega][{{Y}, {X, X, Y}}]^
          2 + E^(T*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}]))*
         (8*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X, X}, {Y, Y}}])*M - 
          E^(T*(3 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
              2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
           (-4 + M)*M - 2*E^(T*(2 + \[Omega][{{X, X}, {Y, Y}}]))*
           (-38 + 5*M + M^2) + E^((T*(4 + M + 2*\[Lambda][{x}] + 4*
                \[Omega][{{X}, {X, Y, Y}}]))/2)*(-80 + 8*M + 3*M^2))*
         \[Omega][{{X, X}, {Y, Y}}] - 
        4*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
         \[Omega][{{X, X}, {Y, Y}}]^2 + 2*\[Omega][{{Y}, {X, X, Y}}]*
         (E^(T*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][
                {{Y}, {X, X, Y}}]))*(8*E^((M*T)/2 + T*\[Lambda][{x}] + T*
                \[Omega][{{X, X}, {Y, Y}}])*M + 
            2*E^(T*(3 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
                2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
             (-4 + M)*M + E^(T*(2 + \[Omega][{{X, X}, {Y, Y}}]))*
             (112 + 2*M - 5*M^2) + E^((T*(4 + M + 2*\[Lambda][{x}] + 
                 4*\[Omega][{{X}, {X, Y, Y}}]))/2)*(-104 + 14*M + 3*M^2)) + 
          2*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][
                {{Y}, {X, X, Y}}]))*(E^(T*\[Omega][{{X, X}, {Y, Y}}])*
             (14 - 3*M) + 3*E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][
                   {{X}, {X, Y, Y}}]))/2)*(-4 + M))*
           \[Omega][{{X, X}, {Y, Y}}])) + 2*\[Omega][{{X}, {X, Y, Y}}]*
       (-864*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, 
                {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2) + 
        688*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])) - 
        184*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, 
                {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M + 
        336*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M - 
        288*E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M + 
        112*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, 
                {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}] + 
             2*\[Omega][{{X, X}, {Y, Y}}]))/2)*M + 
        84*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
             8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^2 - 
        52*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^2 - 
        40*E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^2 + 
        80*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
             8*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))/2)*
         M^2 + 4*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 
             8*\[Omega][{{X}, {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*
         M^3 - 26*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^3 + 
        30*E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^3 + 
        12*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
             8*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))/2)*
         M^3 - E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^4 + 
        E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M^4 + 
        64*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}]))*(E^(T*\[Omega][{{X, X}, {Y, Y}}])*
           (14 - 3*M) + 3*E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][
                 {{X}, {X, Y, Y}}]))/2)*(-4 + M))*\[Omega][{{Y}, {X, X, Y}}]^
          3 + 2*(8*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 4*\[Omega][
                 {{X}, {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}] + 2*
                \[Omega][{{X, X}, {Y, Y}}]))/2)*M*(6 + M) - 
          E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
              6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M*
           (-72 + 10*M + M^2) + 2*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*
                \[Omega][{{X}, {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/
             2)*(-216 - 46*M + 21*M^2 + M^3) - 
          E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][
                {{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
           (-328 + 20*M + 28*M^2 + M^3))*\[Omega][{{X, X}, {Y, Y}}] - 
        16*E^(T*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            4*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
         (E^((M*T)/2 + T*\[Lambda][{x}])*M + E^(2*T)*(2 + M))*
         \[Omega][{{X, X}, {Y, Y}}]^2 + 16*\[Omega][{{Y}, {X, X, Y}}]^2*
         (E^(T*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][
                {{Y}, {X, X, Y}}]))*(12*E^((M*T)/2 + T*\[Lambda][{x}] + T*
                \[Omega][{{X, X}, {Y, Y}}])*M + 
            E^(T*(3 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
                2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
             (-4 + M)*M + E^(T*(2 + \[Omega][{{X, X}, {Y, Y}}]))*
             (180 - 12*M - 7*M^2) + 2*E^((T*(4 + M + 2*\[Lambda][{x}] + 
                 4*\[Omega][{{X}, {X, Y, Y}}]))/2)*(-92 + 11*M + 3*M^2)) + 
          2*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][
                {{Y}, {X, X, Y}}]))*(E^(T*\[Omega][{{X, X}, {Y, Y}}])*
             (10 - 3*M) + 3*E^((T*(M + 2*\[Lambda][{x}] + 4*\[Omega][
                   {{X}, {X, Y, Y}}]))/2)*(-4 + M))*
           \[Omega][{{X, X}, {Y, Y}}]) + 4*\[Omega][{{Y}, {X, X, Y}}]*
         (8*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 4*\[Omega][{{X}, 
                  {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}] + 2*
                \[Omega][{{X, X}, {Y, Y}}]))/2)*M*(10 + 3*M) + 
          E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
              6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*M*
           (-72 + 10*M + 3*M^2) + 
          E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][
                {{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
           (648 + 108*M - 52*M^2 - 5*M^3) + 
          2*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, 
                  {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*
           (-376 - 30*M + 27*M^2 + M^3) + 
          4*E^(T*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][
                {{Y}, {X, X, Y}}]))*(4*E^((M*T)/2 + T*\[Lambda][{x}] + T*
                \[Omega][{{X, X}, {Y, Y}}])*M - 
            E^(T*(3 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
                2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
             (-4 + M)*M - 2*E^(T*(2 + \[Omega][{{X, X}, {Y, Y}}]))*
             (-34 + 7*M + M^2) + E^((T*(4 + M + 2*\[Lambda][{x}] + 
                 4*\[Omega][{{X}, {X, Y, Y}}]))/2)*(-80 + 8*M + 3*M^2))*
           \[Omega][{{X, X}, {Y, Y}}] - 
          16*E^(T*(4 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][
                {{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
           \[Omega][{{X, X}, {Y, Y}}]^2))))/
   (E^(T*(5 + 2*M + \[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
       6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
    (-4 + M + 2*\[Lambda][{x}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{Y}, {X, X, Y}}])*(1 + 2*\[Omega][{{Y}, {X, X, Y}}])*
    (1 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
     2*\[Omega][{{Y}, {X, X, Y}}])*(6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
     4*\[Omega][{{Y}, {X, X, Y}}])*(M + 2*\[Lambda][{x}] + 
     4*\[Omega][{{X}, {X, Y, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
    (3 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
    (2 + M + 4*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
  (-12*M^2 + 12*M^3 + 96*M*\[Omega][{{Y}, {X, X, Y}}] + 
    68*M^2*\[Omega][{{Y}, {X, X, Y}}] + 4*M^3*\[Omega][{{Y}, {X, X, Y}}] + 
    224*M*\[Omega][{{Y}, {X, X, Y}}]^2 + 
    24*M^2*\[Omega][{{Y}, {X, X, Y}}]^2 + 64*M*\[Omega][{{Y}, {X, X, Y}}]^3 - 
    24*M*\[Omega][{{X, X}, {Y, Y}}] - 76*M^2*\[Omega][{{X, X}, {Y, Y}}] + 
    19*M^3*\[Omega][{{X, X}, {Y, Y}}] - 192*\[Omega][{{Y}, {X, X, Y}}]*
     \[Omega][{{X, X}, {Y, Y}}] - 168*M*\[Omega][{{Y}, {X, X, Y}}]*
     \[Omega][{{X, X}, {Y, Y}}] + 60*M^2*\[Omega][{{Y}, {X, X, Y}}]*
     \[Omega][{{X, X}, {Y, Y}}] + 8*M^3*\[Omega][{{Y}, {X, X, Y}}]*
     \[Omega][{{X, X}, {Y, Y}}] - 448*\[Omega][{{Y}, {X, X, Y}}]^2*
     \[Omega][{{X, X}, {Y, Y}}] + 80*M*\[Omega][{{Y}, {X, X, Y}}]^2*
     \[Omega][{{X, X}, {Y, Y}}] + 24*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*
     \[Omega][{{X, X}, {Y, Y}}] - 128*\[Omega][{{Y}, {X, X, Y}}]^3*
     \[Omega][{{X, X}, {Y, Y}}] + 96*\[Omega][{{X, X}, {Y, Y}}]^2 + 
    64*M*\[Omega][{{X, X}, {Y, Y}}]^2 - 84*M^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
    5*M^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 64*\[Omega][{{Y}, {X, X, Y}}]*
     \[Omega][{{X, X}, {Y, Y}}]^2 - 200*M*\[Omega][{{Y}, {X, X, Y}}]*
     \[Omega][{{X, X}, {Y, Y}}]^2 - 8*M^2*\[Omega][{{Y}, {X, X, Y}}]*
     \[Omega][{{X, X}, {Y, Y}}]^2 + 2*M^3*\[Omega][{{Y}, {X, X, Y}}]*
     \[Omega][{{X, X}, {Y, Y}}]^2 - 256*\[Omega][{{Y}, {X, X, Y}}]^2*
     \[Omega][{{X, X}, {Y, Y}}]^2 - 16*M*\[Omega][{{Y}, {X, X, Y}}]^2*
     \[Omega][{{X, X}, {Y, Y}}]^2 + 8*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*
     \[Omega][{{X, X}, {Y, Y}}]^2 + 80*\[Omega][{{X, X}, {Y, Y}}]^3 + 
    84*M*\[Omega][{{X, X}, {Y, Y}}]^3 - 20*M^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
    128*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
    32*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
    8*M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
    64*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
    16*M*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
    16*\[Omega][{{X, X}, {Y, Y}}]^4 + 20*M*\[Omega][{{X, X}, {Y, Y}}]^4 + 
    32*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
    8*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
    16*\[Omega][{{X}, {X, Y, Y}}]^3*((-2 + M)*M + 
      (-8 + M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
      2*(4 + M)*\[Omega][{{X, X}, {Y, Y}}]^2 + 4*\[Omega][{{Y}, {X, X, Y}}]*
       (4 + (4 + M)*\[Omega][{{X, X}, {Y, Y}}])) + 
    8*\[Lambda][{x}]^4*(12 + 19*\[Omega][{{X, X}, {Y, Y}}] + 
      5*\[Omega][{{X, X}, {Y, Y}}]^2 + 4*\[Omega][{{X}, {X, Y, Y}}]^2*
       (1 + \[Omega][{{X, X}, {Y, Y}}]) + 2*\[Omega][{{X}, {X, Y, Y}}]*
       (1 + \[Omega][{{X, X}, {Y, Y}}])*(8 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}]) + 2*\[Omega][{{Y}, {X, X, Y}}]*
       (2 + 4*\[Omega][{{X, X}, {Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}]^2)) + 
    4*\[Omega][{{X}, {X, Y, Y}}]^2*(M*(-28 + 14*M + M^2) + 
      (-112 - 28*M + 16*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}] - 
      2*(40 + 20*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
      16*\[Omega][{{Y}, {X, X, Y}}]^2*
       (8 + (4 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
      4*\[Omega][{{Y}, {X, X, Y}}]*(56 + 8*M + M^2 + 2*(12 + 10*M + M^2)*
         \[Omega][{{X, X}, {Y, Y}}] - 2*(4 + M)*\[Omega][{{X, X}, {Y, Y}}]^
          2)) + 2*\[Omega][{{X}, {X, Y, Y}}]*(2*M*(-12 + 5*M + 4*M^2) + 
      128*\[Omega][{{Y}, {X, X, Y}}]^3 + (-96 - 132*M + 10*M^2 + 9*M^3)*
       \[Omega][{{X, X}, {Y, Y}}] + (32 - 116*M - 24*M^2 + M^3)*
       \[Omega][{{X, X}, {Y, Y}}]^2 + (80 + 4*M - 4*M^2)*
       \[Omega][{{X, X}, {Y, Y}}]^3 + 4*(4 + M)*\[Omega][{{X, X}, {Y, Y}}]^
        4 + 8*\[Omega][{{Y}, {X, X, Y}}]^2*(14*(4 + M) + 
        (16 + 10*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]) + 
      2*\[Omega][{{Y}, {X, X, Y}}]*(96 + 124*M + 8*M^2 + M^3 + 
        (-144 + 92*M + 22*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}] - 
        8*(16 + 3*M)*\[Omega][{{X, X}, {Y, Y}}]^2 - 
        4*(4 + M)*\[Omega][{{X, X}, {Y, Y}}]^3)) + 
    4*\[Lambda][{x}]^3*(16*\[Omega][{{X}, {X, Y, Y}}]^3*
       (1 + \[Omega][{{X, X}, {Y, Y}}]) + 12*\[Omega][{{X}, {X, Y, Y}}]^2*
       (4 + M + 4*\[Omega][{{Y}, {X, X, Y}}])*
       (1 + \[Omega][{{X, X}, {Y, Y}}]) + (3 + \[Omega][{{X, X}, {Y, Y}}])*
       (-8 + 12*M + (-14 + 15*M)*\[Omega][{{X, X}, {Y, Y}}] - 
        8*\[Omega][{{X, X}, {Y, Y}}]^2) + 16*\[Omega][{{Y}, {X, X, Y}}]^2*
       (2 + 4*\[Omega][{{X, X}, {Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}]^2) + 
      2*\[Omega][{{Y}, {X, X, Y}}]*(44 + 6*M + 6*(11 + 2*M)*
         \[Omega][{{X, X}, {Y, Y}}] + (10 + 3*M)*\[Omega][{{X, X}, {Y, Y}}]^
          2 - 2*\[Omega][{{X, X}, {Y, Y}}]^3) + 2*\[Omega][{{X}, {X, Y, Y}}]*
       (-4 + 24*M + (-4 + 27*M)*\[Omega][{{X, X}, {Y, Y}}] + 
        (-10 + 3*M)*\[Omega][{{X, X}, {Y, Y}}]^2 - 
        2*\[Omega][{{X, X}, {Y, Y}}]^3 + 16*\[Omega][{{Y}, {X, X, Y}}]^2*
         (1 + \[Omega][{{X, X}, {Y, Y}}]) + \[Omega][{{Y}, {X, X, Y}}]*
         (64 + 6*M + (80 + 6*M)*\[Omega][{{X, X}, {Y, Y}}] + 
          8*\[Omega][{{X, X}, {Y, Y}}]^2))) + 2*\[Lambda][{x}]^2*
     (32*\[Omega][{{X}, {X, Y, Y}}]^3*(-3 + M + 
        2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
       (1 + \[Omega][{{X, X}, {Y, Y}}]) + 32*\[Omega][{{Y}, {X, X, Y}}]^3*
       (2 + 4*\[Omega][{{X, X}, {Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}]^2) + 
      16*\[Omega][{{Y}, {X, X, Y}}]^2*(6 + 4*M + 
        (7 + 8*M)*\[Omega][{{X, X}, {Y, Y}}] + 2*(-1 + M)*
         \[Omega][{{X, X}, {Y, Y}}]^2 - \[Omega][{{X, X}, {Y, Y}}]^3) - 
      (3 + \[Omega][{{X, X}, {Y, Y}}])*(32 + 8*M - 12*M^2 + 
        (32 + 18*M - 15*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
        8*(-1 + 2*M)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
        4*\[Omega][{{X, X}, {Y, Y}}]^3) + 2*\[Omega][{{Y}, {X, X, Y}}]*
       (-160 + 92*M + 6*M^2 + 4*(-64 + 35*M + 3*M^2)*
         \[Omega][{{X, X}, {Y, Y}}] + (-92 + 22*M + 3*M^2)*
         \[Omega][{{X, X}, {Y, Y}}]^2 - 4*(2 + M)*\[Omega][{{X, X}, {Y, Y}}]^
          3) + 4*\[Omega][{{X}, {X, Y, Y}}]^2*(-92 + 26*M + 3*M^2 + 
        (-116 + 26*M + 3*M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
        36*\[Omega][{{X, X}, {Y, Y}}]^2 - 4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
        32*\[Omega][{{Y}, {X, X, Y}}]^2*(1 + \[Omega][{{X, X}, {Y, Y}}]) + 
        8*\[Omega][{{Y}, {X, X, Y}}]*(3*M + 3*M*\[Omega][{{X, X}, {Y, Y}}] - 
          \[Omega][{{X, X}, {Y, Y}}]^2)) + 2*\[Omega][{{X}, {X, Y, Y}}]*
       (8*(-17 + M + 3*M^2) + (-176 + 10*M + 27*M^2)*
         \[Omega][{{X, X}, {Y, Y}}] + (-52 - 18*M + 3*M^2)*
         \[Omega][{{X, X}, {Y, Y}}]^2 - 4*(3 + M)*\[Omega][{{X, X}, {Y, Y}}]^
          3 + 32*\[Omega][{{Y}, {X, X, Y}}]^3*
         (1 + \[Omega][{{X, X}, {Y, Y}}]) + 16*\[Omega][{{Y}, {X, X, Y}}]^2*
         (5 + 2*M + (9 + 2*M)*\[Omega][{{X, X}, {Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}]^2) + 2*\[Omega][{{Y}, {X, X, Y}}]*
         (-116 + 66*M + 3*M^2 + (-132 + 82*M + 3*M^2)*
           \[Omega][{{X, X}, {Y, Y}}] + 8*(-6 + M)*\[Omega][{{X, X}, {Y, Y}}]^
            2 - 8*\[Omega][{{X, X}, {Y, Y}}]^3))) + 
    \[Lambda][{x}]*(16*\[Omega][{{X}, {X, Y, Y}}]^3*
       (1 + \[Omega][{{X, X}, {Y, Y}}])*(8 - 4*M + M^2 + 
        4*(-6 + M)*\[Omega][{{Y}, {X, X, Y}}] - 2*(-6 + M)*
         \[Omega][{{X, X}, {Y, Y}}]) + 32*\[Omega][{{Y}, {X, X, Y}}]^3*
       (2*(-4 + M) + 2*(-7 + 2*M)*\[Omega][{{X, X}, {Y, Y}}] + 
        (-4 + M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
      16*\[Omega][{{Y}, {X, X, Y}}]^2*(-56 + 9*M + 2*M^2 + 
        2*(-39 + 5*M + 2*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
        (-1 + M)*M*\[Omega][{{X, X}, {Y, Y}}]^2 - 
        (-6 + M)*\[Omega][{{X, X}, {Y, Y}}]^3) + 
      (3 + \[Omega][{{X, X}, {Y, Y}}])*(4*M*(-10 + 2*M + M^2) + 
        (32 - 80*M + 6*M^2 + 5*M^3)*\[Omega][{{X, X}, {Y, Y}}] - 
        8*(-10 + 4*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 - 
        4*(-2 + M)*\[Omega][{{X, X}, {Y, Y}}]^3) + 
      2*\[Omega][{{Y}, {X, X, Y}}]*(2*(-96 - 46*M + 26*M^2 + M^3) + 
        2*(-120 - 98*M + 41*M^2 + 2*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
        (128 - 100*M + 14*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}]^2 - 
        2*(-16 + 8*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]^3 - 
        8*\[Omega][{{X, X}, {Y, Y}}]^4) + 4*\[Omega][{{X}, {X, Y, Y}}]^2*
       (112 - 64*M + 16*M^2 + M^3 + (264 - 84*M + 16*M^2 + M^3)*
         \[Omega][{{X, X}, {Y, Y}}] - 8*(-23 + 5*M)*
         \[Omega][{{X, X}, {Y, Y}}]^2 - 4*(-4 + M)*\[Omega][{{X, X}, {Y, Y}}]^
          3 + 32*\[Omega][{{Y}, {X, X, Y}}]^2*(-6 + M + 
          (-5 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 4*\[Omega][{{Y}, {X, X, Y}}]*
         (-76 + 2*M + 3*M^2 + (-60 + 4*M + 3*M^2)*\[Omega][
            {{X, X}, {Y, Y}}] - 2*(-6 + M)*\[Omega][{{X, X}, {Y, Y}}]^2)) + 
      2*\[Omega][{{X}, {X, Y, Y}}]*(4*(24 - 29*M + 7*M^2 + 2*M^3) + 
        (240 - 156*M + 32*M^2 + 9*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
        (288 - 100*M - 6*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}]^2 - 
        2*(-20 + 10*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]^3 - 
        8*\[Omega][{{X, X}, {Y, Y}}]^4 + 32*\[Omega][{{Y}, {X, X, Y}}]^3*
         (-6 + M + (-4 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
        16*\[Omega][{{Y}, {X, X, Y}}]^2*(-50 + 5*M + M^2 + 
          (-56 + 10*M + M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
          (-4 + M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
        2*\[Omega][{{Y}, {X, X, Y}}]*(-160 - 100*M + 36*M^2 + M^3 + 
          (-104 - 88*M + 44*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
          4*(48 - 12*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 - 
          8*(-5 + M)*\[Omega][{{X, X}, {Y, Y}}]^3))))/
   (E^(T*(1 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
       2*\[Omega][{{Y}, {X, X, Y}}]))*(-4 + M + 2*\[Lambda][{x}])*
    (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
    (1 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
     2*\[Omega][{{Y}, {X, X, Y}}])*(M + 2*\[Lambda][{x}] + 
     4*\[Omega][{{X}, {X, Y, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
    (M + 2*\[Lambda][{x}] + 4*\[Omega][{{Y}, {X, X, Y}}] - 
     2*\[Omega][{{X, X}, {Y, Y}}])*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] - 
     \[Omega][{{X, X}, {Y, Y}}])*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{X, X}, {Y, Y}}])*(3 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
     \[Omega][{{X, X}, {Y, Y}}])*(1 + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
  (M*(\[Lambda][{x}]^2*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{Y}, {X, X, Y}}])*(6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
       4*\[Omega][{{Y}, {X, X, Y}}])*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{X, X}, {Y, Y}}])*(2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
       2*\[Omega][{{X, X}, {Y, Y}}])*(6 + M + 4*\[Omega][{{Y}, {X, X, Y}}] + 
       6*\[Omega][{{X, X}, {Y, Y}}]) + 
     M*(2 + M + 4*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}])*
      (M + 4*\[Omega][{{X, X}, {Y, Y}}])*(8*\[Omega][{{X}, {X, Y, Y}}]^3 + 
       8*\[Omega][{{Y}, {X, X, Y}}]^3 + \[Omega][{{Y}, {X, X, Y}}]^2*
        (6*(8 + M) + 8*\[Omega][{{X, X}, {Y, Y}}]) + 
       \[Omega][{{X}, {X, Y, Y}}]^2*(52 + 6*M + 
         24*\[Omega][{{Y}, {X, X, Y}}] + 24*\[Omega][{{X, X}, {Y, Y}}]) + 
       (3 + \[Omega][{{X, X}, {Y, Y}}])*(18 + 11*M + M^2 + 
         (8 + 3*M)*\[Omega][{{X, X}, {Y, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]^
           2) + \[Omega][{{Y}, {X, X, Y}}]*(90 + 29*M + M^2 + 
         (38 + 6*M)*\[Omega][{{X, X}, {Y, Y}}] + 
         4*\[Omega][{{X, X}, {Y, Y}}]^2) + \[Omega][{{X}, {X, Y, Y}}]*
        (102 + 29*M + M^2 + 24*\[Omega][{{Y}, {X, X, Y}}]^2 + 
         6*(11 + 2*M)*\[Omega][{{X, X}, {Y, Y}}] + 
         12*\[Omega][{{X, X}, {Y, Y}}]^2 + 4*\[Omega][{{Y}, {X, X, Y}}]*
          (25 + 3*M + 6*\[Omega][{{X, X}, {Y, Y}}]))) + 
     \[Lambda][{x}]*(32*\[Omega][{{Y}, {X, X, Y}}]^4*
        (3 + \[Omega][{{X, X}, {Y, Y}}])*(2 + M + 
         2*\[Omega][{{X, X}, {Y, Y}}]) + 64*\[Omega][{{X}, {X, Y, Y}}]^5*
        (6 + M + 4*\[Omega][{{Y}, {X, X, Y}}] + 
         6*\[Omega][{{X, X}, {Y, Y}}]) + 16*\[Omega][{{Y}, {X, X, Y}}]^3*
        (78 + 51*M + 7*M^2 + 2*(61 + 21*M + M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
         (50 + 7*M)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         6*\[Omega][{{X, X}, {Y, Y}}]^3) + (3 + \[Omega][{{X, X}, {Y, Y}}])*
        (216 + 468*M + 286*M^2 + 61*M^3 + 4*M^4 + 
         2*(360 + 524*M + 192*M^2 + 19*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
         (792 + 644*M + 92*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         32*(9 + 2*M)*\[Omega][{{X, X}, {Y, Y}}]^3) + 
       2*\[Omega][{{Y}, {X, X, Y}}]^2*(1368 + 1236*M + 350*M^2 + 19*M^3 + 
         (2640 + 1680*M + 226*M^2 + 5*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
         8*(205 + 74*M + 4*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         20*(20 + 3*M)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         32*\[Omega][{{X, X}, {Y, Y}}]^4) + \[Omega][{{Y}, {X, X, Y}}]*
        (2376 + 3132*M + 1378*M^2 + 175*M^3 + 4*M^4 + 
         (5976 + 6100*M + 1554*M^2 + 91*M^3 + M^4)*
          \[Omega][{{X, X}, {Y, Y}}] + 2*(2700 + 1782*M + 214*M^2 + 5*M^3)*
          \[Omega][{{X, X}, {Y, Y}}]^2 + 12*(174 + 55*M + 2*M^2)*
          \[Omega][{{X, X}, {Y, Y}}]^3 + 16*(18 + M)*
          \[Omega][{{X, X}, {Y, Y}}]^4) + 64*\[Omega][{{X}, {X, Y, Y}}]^4*
        (42 + 14*M + M^2 + 12*\[Omega][{{Y}, {X, X, Y}}]^2 + 
         (52 + 8*M)*\[Omega][{{X, X}, {Y, Y}}] + 
         10*\[Omega][{{X, X}, {Y, Y}}]^2 + \[Omega][{{Y}, {X, X, Y}}]*
          (46 + 7*M + 22*\[Omega][{{X, X}, {Y, Y}}])) + 
       4*\[Omega][{{X}, {X, Y, Y}}]^3*(1680 + 984*M + 138*M^2 + 5*M^3 + 
         192*\[Omega][{{Y}, {X, X, Y}}]^3 + 4*(668 + 233*M + 13*M^2)*
          \[Omega][{{X, X}, {Y, Y}}] + 8*(135 + 17*M)*
          \[Omega][{{X, X}, {Y, Y}}]^2 + 88*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         32*\[Omega][{{Y}, {X, X, Y}}]^2*(41 + 6*M + 
           15*\[Omega][{{X, X}, {Y, Y}}]) + 8*\[Omega][{{Y}, {X, X, Y}}]*
          (332 + 113*M + 7*M^2 + 4*(73 + 11*M)*\[Omega][{{X, X}, {Y, Y}}] + 
           54*\[Omega][{{X, X}, {Y, Y}}]^2)) + 2*\[Omega][{{X}, {X, Y, Y}}]^2*
        (3744 + 3576*M + 832*M^2 + 64*M^3 + M^4 + 
         128*\[Omega][{{Y}, {X, X, Y}}]^4 + (7824 + 4736*M + 630*M^2 + 
           15*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 4*(1256 + 385*M + 17*M^2)*
          \[Omega][{{X, X}, {Y, Y}}]^2 + (976 + 92*M)*
          \[Omega][{{X, X}, {Y, Y}}]^3 + 32*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         32*\[Omega][{{Y}, {X, X, Y}}]^3*(50 + 7*M + 
           18*\[Omega][{{X, X}, {Y, Y}}]) + 16*\[Omega][{{Y}, {X, X, Y}}]^2*
          (364 + 120*M + 7*M^2 + (280 + 41*M)*\[Omega][{{X, X}, {Y, Y}}] + 
           50*\[Omega][{{X, X}, {Y, Y}}]^2) + 4*\[Omega][{{Y}, {X, X, Y}}]*
          (2016 + 1184*M + 164*M^2 + 5*M^3 + (2664 + 922*M + 52*M^2)*
            \[Omega][{{X, X}, {Y, Y}}] + 2*(518 + 71*M)*
            \[Omega][{{X, X}, {Y, Y}}]^2 + 100*\[Omega][{{X, X}, {Y, Y}}]^
             3)) + \[Omega][{{X}, {X, Y, Y}}]*(3672 + 5508*M + 2038*M^2 + 
         265*M^3 + 10*M^4 + (10296 + 9892*M + 2394*M^2 + 151*M^3 + M^4)*
          \[Omega][{{X, X}, {Y, Y}}] + 2*(4788 + 2682*M + 322*M^2 + 5*M^3)*
          \[Omega][{{X, X}, {Y, Y}}]^2 + 12*(270 + 71*M + 2*M^2)*
          \[Omega][{{X, X}, {Y, Y}}]^3 + 16*(18 + M)*
          \[Omega][{{X, X}, {Y, Y}}]^4 + 64*\[Omega][{{Y}, {X, X, Y}}]^4*
          (8 + M + 4*\[Omega][{{X, X}, {Y, Y}}]) + 
         64*\[Omega][{{Y}, {X, X, Y}}]^3*(61 + 19*M + M^2 + 
           (50 + 7*M)*\[Omega][{{X, X}, {Y, Y}}] + 
           9*\[Omega][{{X, X}, {Y, Y}}]^2) + 4*\[Omega][{{Y}, {X, X, Y}}]^2*
          (2520 + 1420*M + 194*M^2 + 5*M^3 + 12*(262 + 90*M + 5*M^2)*
            \[Omega][{{X, X}, {Y, Y}}] + 8*(149 + 22*M)*
            \[Omega][{{X, X}, {Y, Y}}]^2 + 136*\[Omega][{{X, X}, {Y, Y}}]^
             3) + 2*\[Omega][{{Y}, {X, X, Y}}]*(5256 + 4836*M + 1182*M^2 + 
           83*M^3 + M^4 + 4*(2436 + 1502*M + 204*M^2 + 5*M^3)*
            \[Omega][{{X, X}, {Y, Y}}] + 4*(1522 + 517*M + 25*M^2)*
            \[Omega][{{X, X}, {Y, Y}}]^2 + 8*(172 + 19*M)*
            \[Omega][{{X, X}, {Y, Y}}]^3 + 64*\[Omega][{{X, X}, {Y, Y}}]^
             4)))))/((3 + \[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{Y}, {X, X, Y}}])*(1 + M + \[Lambda][{x}] + 
     2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
    (6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
    (3 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
    (M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{X, X}, {Y, Y}}])*(3 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
     \[Omega][{{X, X}, {Y, Y}}])*(1 + 2*\[Omega][{{X, X}, {Y, Y}}])*
    (2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}])*
    (2 + M + 4*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}])*
    (M + 4*\[Omega][{{X, X}, {Y, Y}}])) + 
  ((8*M*\[Omega][{{X}, {X, Y, Y}}]*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{Y}, {X, X, Y}}])*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{X, X}, {Y, Y}}])*(6 + M + 2*\[Lambda][{x}] + 
       4*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))/
     (E^((T*(M + 4*\[Omega][{{X}, {X, Y, Y}}]))/2)*
      (M + 4*\[Omega][{{X}, {X, Y, Y}}])*(3 + 2*\[Omega][{{Y}, {X, X, Y}}])*
      (2 + M + 2*\[Lambda][{x}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
      (1 + \[Omega][{{X, X}, {Y, Y}}])*(M + 2*\[Lambda][{x}] + 
       2*\[Omega][{{X, X}, {Y, Y}}])) + 
    (4*M*(3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
      (2 + M + 2*\[Lambda][{x}] + 4*\[Omega][{{Y}, {X, X, Y}}] - 
       2*\[Omega][{{X, X}, {Y, Y}}])*(1 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
       4*\[Omega][{{X}, {X, Y, Y}}]^2 - \[Omega][{{X, X}, {Y, Y}}]^2))/
     (E^((T*(2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
          2*\[Omega][{{X, X}, {Y, Y}}]))/2)*(-2 + M + 2*\[Lambda][{x}])*
      (M + 2*\[Lambda][{x}] + 4*\[Omega][{{Y}, {X, X, Y}}] - 
       2*\[Omega][{{X, X}, {Y, Y}}])*(1 + \[Omega][{{X, X}, {Y, Y}}])*
      (-2 - 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
      (2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
       2*\[Omega][{{X, X}, {Y, Y}}])) + 
    (4*M*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
      (-3 + 2*\[Omega][{{X}, {X, Y, Y}}]^2 - \[Omega][{{Y}, {X, X, Y}}]*
        (5 + \[Omega][{{X, X}, {Y, Y}}]) + \[Omega][{{X}, {X, Y, Y}}]*
        (3 - 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])))/
     (E^((T*(6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}]))/2)*(-4 + M + 2*\[Lambda][{x}])*
      (3 + 2*\[Omega][{{Y}, {X, X, Y}}])*
      (6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
      (2 + 2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])) - 
    (2*(3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
      (M^2 + 4*\[Lambda][{x}]^3 + 8*M*\[Omega][{{X}, {X, Y, Y}}]^2 - 
       4*\[Omega][{{X, X}, {Y, Y}}]^2 + 4*\[Lambda][{x}]^2*
        (-1 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]) + 4*\[Omega][{{X}, {X, Y, Y}}]*
        (2*M + (-2 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
       \[Lambda][{x}]*(M^2 + 4*(-2 + M)*\[Omega][{{X, X}, {Y, Y}}] + 
         4*\[Omega][{{X, X}, {Y, Y}}]^2 + 4*\[Omega][{{X}, {X, Y, Y}}]*
          (-2 + M + 2*\[Omega][{{X, X}, {Y, Y}}]))))/
     (E^(T*(M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{X, X}, {Y, Y}}]))*(-2 + M + 2*\[Lambda][{x}])*
      (M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{X, X}, {Y, Y}}])*(-1 - 2*\[Omega][{{Y}, {X, X, Y}}] + 
       \[Omega][{{X, X}, {Y, Y}}])*(M + 2*\[Lambda][{x}] + 
       2*\[Omega][{{X, X}, {Y, Y}}])) + 
    (M^2*(108 + 90*M + 20*M^2 + M^3 + 128*\[Omega][{{X}, {X, Y, Y}}]^3 + 
       108*\[Omega][{{Y}, {X, X, Y}}] + 54*M*\[Omega][{{Y}, {X, X, Y}}] + 
       6*M^2*\[Omega][{{Y}, {X, X, Y}}] + 24*\[Omega][{{Y}, {X, X, Y}}]^2 + 
       8*M*\[Omega][{{Y}, {X, X, Y}}]^2 + 72*\[Omega][{{X, X}, {Y, Y}}] + 
       32*M*\[Omega][{{X, X}, {Y, Y}}] + 3*M^2*\[Omega][{{X, X}, {Y, Y}}] + 
       48*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
       10*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
       8*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
       12*\[Omega][{{X, X}, {Y, Y}}]^2 + 2*M*\[Omega][{{X, X}, {Y, Y}}]^2 + 
       4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
       4*\[Omega][{{X}, {X, Y, Y}}]^2*(106 + 21*M + 
         36*\[Omega][{{Y}, {X, X, Y}}] + 18*\[Omega][{{X, X}, {Y, Y}}]) + 
       \[Lambda][{x}]*(36 + 14*M + M^2 + 40*\[Omega][{{X}, {X, Y, Y}}]^2 + 
         12*\[Omega][{{X, X}, {Y, Y}}] + 2*M*\[Omega][{{X, X}, {Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}]*(3 + M + \[Omega][{{X, X}, {Y, Y}}]) + 
         4*\[Omega][{{X}, {X, Y, Y}}]*(23 + 3*M + 
           6*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}])) + 
       2*\[Omega][{{X}, {X, Y, Y}}]*(198 + 99*M + 8*M^2 + 
         24*\[Omega][{{Y}, {X, X, Y}}]^2 + (76 + 15*M)*
          \[Omega][{{X, X}, {Y, Y}}] + 6*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         6*\[Omega][{{Y}, {X, X, Y}}]*(24 + 5*M + 
           4*\[Omega][{{X, X}, {Y, Y}}]))))/
     ((M + 4*\[Omega][{{X}, {X, Y, Y}}])*(1 + M + \[Lambda][{x}] + 
       2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
      (6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
      (M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{X, X}, {Y, Y}}])*(2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
       2*\[Omega][{{X, X}, {Y, Y}}])) - 
    (16*M*\[Omega][{{X}, {X, Y, Y}}]^3*(-2 + M + 
        4*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}]) + 
      8*\[Lambda][{x}]^4*(5 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}]) + 4*\[Lambda][{x}]^3*
       (3*M + 4*\[Omega][{{X}, {X, Y, Y}}] + 12*\[Omega][{{Y}, {X, X, Y}}] - 
        2*\[Omega][{{X, X}, {Y, Y}}])*(5 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}]) + 2*\[Lambda][{x}]^2*
       (5 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
       (-12 + 2*M + 3*M^2 + 48*\[Omega][{{Y}, {X, X, Y}}]^2 + 
        8*\[Omega][{{Y}, {X, X, Y}}]*(-2 + 3*M - 
          2*\[Omega][{{X, X}, {Y, Y}}]) + 8*\[Omega][{{X}, {X, Y, Y}}]*
         (-1 + M + 4*\[Omega][{{Y}, {X, X, Y}}] - 
          \[Omega][{{X, X}, {Y, Y}}]) - 4*M*\[Omega][{{X, X}, {Y, Y}}]) + 
      8*\[Omega][{{X}, {X, Y, Y}}]^2*(4*M*(-5 + 2*M) + 
        8*(-4 + M)*\[Omega][{{Y}, {X, X, Y}}]^2 + (8 - 16*M + M^2)*
         \[Omega][{{X, X}, {Y, Y}}] - 2*M*\[Omega][{{X, X}, {Y, Y}}]^2 + 
        2*\[Omega][{{Y}, {X, X, Y}}]*(-8 + 10*M + M^2 + 
          8*\[Omega][{{X, X}, {Y, Y}}])) + 
      (2 + M + 4*\[Omega][{{Y}, {X, X, Y}}])*(M*(-32 + 5*M) + 
        (40 - 2*M + M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
        2*(-4 + M)*\[Omega][{{X, X}, {Y, Y}}]^2 - 
        32*\[Omega][{{Y}, {X, X, Y}}]^2*(5 + \[Omega][{{X, X}, {Y, Y}}]) - 
        8*\[Omega][{{Y}, {X, X, Y}}]*(10 + 3*M - 
          8*\[Omega][{{X, X}, {Y, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}]^2)) + 
      2*\[Omega][{{X}, {X, Y, Y}}]*(M*(-100 + 24*M + M^2) - 
        128*\[Omega][{{Y}, {X, X, Y}}]^3 + 8*(12 - 7*M + M^2)*
         \[Omega][{{X, X}, {Y, Y}}] - 16*(-1 + M)*\[Omega][{{X, X}, {Y, Y}}]^
          2 + 16*\[Omega][{{Y}, {X, X, Y}}]^2*(-28 + M + 
          M*\[Omega][{{X, X}, {Y, Y}}]) + 4*\[Omega][{{Y}, {X, X, Y}}]*
         (4*(-12 - 2*M + M^2) + (48 + M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
          2*(-4 + M)*\[Omega][{{X, X}, {Y, Y}}]^2)) + 
      \[Lambda][{x}]*(-80 - 104*M + 20*M^2 + 5*M^3 + 
        32*M*\[Omega][{{X}, {X, Y, Y}}]^3 + 104*\[Omega][{{X, X}, {Y, Y}}] - 
        12*M*\[Omega][{{X, X}, {Y, Y}}] - 6*M^2*\[Omega][{{X, X}, {Y, Y}}] + 
        M^3*\[Omega][{{X, X}, {Y, Y}}] + 24*\[Omega][{{X, X}, {Y, Y}}]^2 - 
        4*M*\[Omega][{{X, X}, {Y, Y}}]^2 - 2*M^2*\[Omega][{{X, X}, {Y, Y}}]^
          2 + 64*\[Omega][{{Y}, {X, X, Y}}]^3*
         (5 + \[Omega][{{X, X}, {Y, Y}}]) + 16*\[Omega][{{Y}, {X, X, Y}}]^2*
         (-8 + 3*M - 2*\[Omega][{{X, X}, {Y, Y}}])*
         (5 + \[Omega][{{X, X}, {Y, Y}}]) + 8*\[Omega][{{X}, {X, Y, Y}}]^2*
         (-8 + 14*M + M^2 + 16*\[Omega][{{Y}, {X, X, Y}}]^2 + 
          4*\[Omega][{{Y}, {X, X, Y}}]*(-6 + 3*M - 
            2*\[Omega][{{X, X}, {Y, Y}}]) + 4*\[Omega][{{X, X}, {Y, Y}}]) + 
        4*\[Omega][{{Y}, {X, X, Y}}]*(-140 - 22*M + 15*M^2 + 
          (12 - 22*M + 3*M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
          4*(-2 + M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
        2*\[Omega][{{X}, {X, Y, Y}}]*(-96 + 16*M + 14*M^2 + M^3 + 
          16*(2 + 3*M)*\[Omega][{{Y}, {X, X, Y}}]^2 + 
          64*\[Omega][{{Y}, {X, X, Y}}]^3 - 8*(-6 + M)*
           \[Omega][{{X, X}, {Y, Y}}] - 4*(-2 + M)*\[Omega][{{X, X}, {Y, Y}}]^
            2 + 4*\[Omega][{{Y}, {X, X, Y}}]*(-88 + 24*M + 3*M^2 + 
            2*(-12 + M)*\[Omega][{{X, X}, {Y, Y}}] - 
            4*\[Omega][{{X, X}, {Y, Y}}]^2))))/
     (E^(T*(1 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}]))*(-4 + M + 2*\[Lambda][{x}])*
      (1 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
       2*\[Omega][{{Y}, {X, X, Y}}])*(2 + M + 2*\[Lambda][{x}] + 
       4*\[Omega][{{Y}, {X, X, Y}}])*(M + 2*\[Lambda][{x}] + 
       4*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
      (1 + 2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])))/
   ((1 + 2*\[Omega][{{X}, {X, Y, Y}}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{Y}, {X, X, Y}}])*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{X, X}, {Y, Y}}])), 
 (-2*((4*M*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (1 + \[Omega][{{X, Y}, {X, Y}}]))/
      (E^((T*(6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
           4*\[Omega][{{Y}, {X, X, Y}}]))/2)*(-4 + M + 2*\[Lambda][{x}])*
       (6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
       (2 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] - 
        \[Omega][{{X, Y}, {X, Y}}])) - 
     (4*M*(3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, Y}, {X, Y}}]))/
      (E^((T*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
           2*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
       (M + 2*\[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, Y}, {X, Y}}])*
       (2 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] - 
        \[Omega][{{X, Y}, {X, Y}}])*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}])) - 
     (M^2*(8 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
        4*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/
      ((1 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}])*(6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
        4*\[Omega][{{Y}, {X, X, Y}}])*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}])) + 
     (-8*M - M^2 - 4*\[Lambda][{x}]^3 - 4*(-4 + M)*\[Omega][{{X}, {X, Y, Y}}]^
         2 + 8*\[Omega][{{Y}, {X, X, Y}}] - 10*M*\[Omega][{{Y}, {X, X, Y}}] + 
       16*\[Omega][{{Y}, {X, X, Y}}]^2 - 4*M*\[Omega][{{Y}, {X, X, Y}}]^2 - 
       4*\[Lambda][{x}]^2*(-1 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}]) - 
       8*\[Omega][{{X, Y}, {X, Y}}] - 10*M*\[Omega][{{X, Y}, {X, Y}}] - 
       16*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}] - 
       4*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}] - 
       \[Lambda][{x}]*(-8 + M^2 + 8*\[Omega][{{X}, {X, Y, Y}}]^2 + 
         8*\[Omega][{{Y}, {X, X, Y}}]^2 + \[Omega][{{Y}, {X, X, Y}}]*
          (-20 + 6*M - 8*\[Omega][{{X, Y}, {X, Y}}]) + 
         2*\[Omega][{{X}, {X, Y, Y}}]*(-10 + 3*M + 
           8*\[Omega][{{Y}, {X, X, Y}}] - 4*\[Omega][{{X, Y}, {X, Y}}]) + 
         4*\[Omega][{{X, Y}, {X, Y}}] - 2*M*\[Omega][{{X, Y}, {X, Y}}]) - 
       2*\[Omega][{{X}, {X, Y, Y}}]*(-4 + 5*M + 4*(-4 + M)*
          \[Omega][{{Y}, {X, X, Y}}] + 2*(4 + M)*\[Omega][{{X, Y}, {X, Y}}]))/
      (E^(T*(1 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          2*\[Omega][{{Y}, {X, X, Y}}]))*(-4 + M + 2*\[Lambda][{x}])*
       (1 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}])*(M + 2*\[Lambda][{x}] + 
        2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
        2*\[Omega][{{X, Y}, {X, Y}}]))))/
   ((3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
    (1 + 2*\[Omega][{{Y}, {X, X, Y}}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])) - 
  (2*((-8*M*\[Omega][{{X}, {X, Y, Y}}]*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))/
      (E^((T*(M + 4*\[Omega][{{X}, {X, Y, Y}}]))/2)*
       (M + 4*\[Omega][{{X}, {X, Y, Y}}])*(3 + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (2 + M + 2*\[Lambda][{x}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
       (1 - \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, Y}, {X, Y}}])) + (8*M*(1 + \[Omega][{{X}, {X, Y, Y}}])*
       (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, Y}, {X, Y}}]))/
      (E^((T*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
           2*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
       (M + 2*\[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, Y}, {X, Y}}])*
       (2 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] - 
        \[Omega][{{X, Y}, {X, Y}}])*(1 - \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])*
       (2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        2*\[Omega][{{X, Y}, {X, Y}}])) - 
     (4*M*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (6 + \[Omega][{{X}, {X, Y, Y}}]^2 + \[Omega][{{Y}, {X, X, Y}}]^2 - 
        \[Omega][{{Y}, {X, X, Y}}]*(-4 + \[Omega][{{X, Y}, {X, Y}}]) + 
        \[Omega][{{X}, {X, Y, Y}}]*(6 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
          \[Omega][{{X, Y}, {X, Y}}])))/
      (E^((T*(6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
           4*\[Omega][{{Y}, {X, X, Y}}]))/2)*(-4 + M + 2*\[Lambda][{x}])*
       (3 + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
       (2 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] - 
        \[Omega][{{X, Y}, {X, Y}}])) - 
     (M^2*(36 + 8*M + M^2 + 20*\[Omega][{{X}, {X, Y, Y}}]^2 + 
        4*\[Omega][{{Y}, {X, X, Y}}]^2 + 12*\[Omega][{{X, Y}, {X, Y}}] + 
        2*M*\[Omega][{{X, Y}, {X, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}]*
         (6 + M + \[Omega][{{X, Y}, {X, Y}}]) + 4*\[Omega][{{X}, {X, Y, Y}}]*
         (14 + 2*M + 6*\[Omega][{{Y}, {X, X, Y}}] + 
          3*\[Omega][{{X, Y}, {X, Y}}])))/((M + 4*\[Omega][{{X}, {X, Y, Y}}])*
       (1 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}])*(6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
        4*\[Omega][{{Y}, {X, X, Y}}])*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}])) + 
     (-8*\[Lambda][{x}]^4 + 16*M*\[Omega][{{X}, {X, Y, Y}}]^3 - 
       4*\[Lambda][{x}]^3*(3*M + 6*\[Omega][{{X}, {X, Y, Y}}] + 
         10*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, Y}, {X, Y}}]) - 
       (2 + M + 4*\[Omega][{{Y}, {X, X, Y}}])*((-28 + M)*M - 
         16*\[Omega][{{Y}, {X, X, Y}}]^2 - 2*\[Omega][{{Y}, {X, X, Y}}]*
          (4 + 7*M - 8*\[Omega][{{X, Y}, {X, Y}}]) - 
         2*(-4 + M)*\[Omega][{{X, Y}, {X, Y}}]) - 2*\[Lambda][{x}]^2*
        (-12 + 2*M + 3*M^2 + 8*\[Omega][{{X}, {X, Y, Y}}]^2 + 
         32*\[Omega][{{Y}, {X, X, Y}}]^2 + 4*\[Omega][{{Y}, {X, X, Y}}]*
          (-4 + 5*M - 4*\[Omega][{{X, Y}, {X, Y}}]) + 
         4*\[Omega][{{X}, {X, Y, Y}}]*(-2 + 3*M + 
           10*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, Y}, {X, Y}}]) - 
         4*M*\[Omega][{{X, Y}, {X, Y}}]) + 16*\[Omega][{{X}, {X, Y, Y}}]^2*
        (2 + 7*M + 2*(2 + M)*\[Omega][{{Y}, {X, X, Y}}] + 
         M*\[Omega][{{X, Y}, {X, Y}}]) + 2*\[Omega][{{X}, {X, Y, Y}}]*
        (8 + 90*M + 7*M^2 + 8*(8 + M)*\[Omega][{{Y}, {X, X, Y}}]^2 + 
         16*(-1 + M)*\[Omega][{{X, Y}, {X, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}]*(12 + 23*M + 2*(-4 + M)*
            \[Omega][{{X, Y}, {X, Y}}])) + \[Lambda][{x}]*
        (16 + 64*M - 4*M^2 - M^3 - 32*\[Omega][{{Y}, {X, X, Y}}]^3 - 
         8*\[Omega][{{X}, {X, Y, Y}}]^2*(-2 + M + 
           4*\[Omega][{{Y}, {X, X, Y}}]) - 32*\[Omega][{{Y}, {X, X, Y}}]^2*
          (-3 + M - \[Omega][{{X, Y}, {X, Y}}]) - 
         24*\[Omega][{{X, Y}, {X, Y}}] + 4*M*\[Omega][{{X, Y}, {X, Y}}] + 
         2*M^2*\[Omega][{{X, Y}, {X, Y}}] + \[Omega][{{X}, {X, Y, Y}}]*
          (56 + 36*M - 6*M^2 - 64*\[Omega][{{Y}, {X, X, Y}}]^2 - 
           8*\[Omega][{{Y}, {X, X, Y}}]*(-14 + 5*M - 4*\[Omega][{{X, Y}, 
                {X, Y}}]) + 8*(-2 + M)*\[Omega][{{X, Y}, {X, Y}}]) + 
         \[Omega][{{Y}, {X, X, Y}}]*(88 + 36*M - 10*M^2 + 
           16*(-2 + M)*\[Omega][{{X, Y}, {X, Y}}])))/
      (E^(T*(1 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          2*\[Omega][{{Y}, {X, X, Y}}]))*(-4 + M + 2*\[Lambda][{x}])*
       (1 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}])*(2 + M + 2*\[Lambda][{x}] + 
        4*\[Omega][{{Y}, {X, X, Y}}])*(M + 2*\[Lambda][{x}] + 
        2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
        2*\[Omega][{{X, Y}, {X, Y}}]))))/((1 + 2*\[Omega][{{X}, {X, Y, Y}}])*
    (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
    (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
     \[Omega][{{X, Y}, {X, Y}}])) + 
  (2*(-288*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M + 
     192*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M - 
     264*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^2 + 
     152*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2 - 
     24*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
      M^2 + 36*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 
          8*\[Omega][{{X}, {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*
      M^3 + 32*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3 - 
     64*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
         5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3 - 
     36*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
      M^3 + 12*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 
          8*\[Omega][{{X}, {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*
      M^4 + 2*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^4 + 
     8*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
         5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^4 - 
     12*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
      M^4 + 2*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
         5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^5 + 
     32*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*(-4 + M)*
      \[Omega][{{X}, {X, Y, Y}}]^5 - 
     192*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] - 
     1152*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*\[Omega][{{Y}, {X, X, Y}}] + 
     528*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M*
      \[Omega][{{Y}, {X, X, Y}}] - 
     48*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M*
      \[Omega][{{Y}, {X, X, Y}}] - 
     448*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^2*\[Omega][{{Y}, {X, X, Y}}] + 
     356*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
      \[Omega][{{Y}, {X, X, Y}}] - 
     128*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
         5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
      \[Omega][{{Y}, {X, X, Y}}] - 
     160*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
      M^2*\[Omega][{{Y}, {X, X, Y}}] + 
     120*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^3*\[Omega][{{Y}, {X, X, Y}}] + 
     48*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3*
      \[Omega][{{Y}, {X, X, Y}}] - 
     48*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
         5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3*
      \[Omega][{{Y}, {X, X, Y}}] - 
     96*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
      M^3*\[Omega][{{Y}, {X, X, Y}}] + 
     16*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^4*\[Omega][{{Y}, {X, X, Y}}] + 
     E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^4*
      \[Omega][{{Y}, {X, X, Y}}] + 
     16*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
         5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^4*
      \[Omega][{{Y}, {X, X, Y}}] - 
     8*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
      M^4*\[Omega][{{Y}, {X, X, Y}}] + 
     E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
         5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^5*
      \[Omega][{{Y}, {X, X, Y}}] - 
     800*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 
     1504*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     488*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 
     176*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 
     120*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^2*\[Omega][{{Y}, {X, X, Y}}]^
       2 + 308*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 
     128*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
         5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 
     240*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
      M^2*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     108*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^3*\[Omega][{{Y}, {X, X, Y}}]^
       2 + 24*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3*
      \[Omega][{{Y}, {X, X, Y}}]^2 + 
     8*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
         5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 
     48*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
      M^3*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     4*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^4*\[Omega][{{Y}, {X, X, Y}}]^
       2 + 6*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
         5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^4*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 
     1120*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3 - 
     768*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     264*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M*
      \[Omega][{{Y}, {X, X, Y}}]^3 - 
     192*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M*
      \[Omega][{{Y}, {X, X, Y}}]^3 + 
     96*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^2*\[Omega][{{Y}, {X, X, Y}}]^
       3 + 132*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
      \[Omega][{{Y}, {X, X, Y}}]^3 - 
     32*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
         5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
      \[Omega][{{Y}, {X, X, Y}}]^3 - 
     96*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
      M^2*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     24*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^3*\[Omega][{{Y}, {X, X, Y}}]^
       3 + 4*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3*
      \[Omega][{{Y}, {X, X, Y}}]^3 + 
     8*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
         5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3*
      \[Omega][{{Y}, {X, X, Y}}]^3 - 
     640*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4 - 
     128*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*\[Omega][{{Y}, {X, X, Y}}]^4 + 
     128*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M*
      \[Omega][{{Y}, {X, X, Y}}]^4 - 
     64*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M*
      \[Omega][{{Y}, {X, X, Y}}]^4 + 
     32*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^2*\[Omega][{{Y}, {X, X, Y}}]^
       4 + 24*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
      \[Omega][{{Y}, {X, X, Y}}]^4 - 
     128*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^5 + 
     32*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M*
      \[Omega][{{Y}, {X, X, Y}}]^5 + 
     192*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{X, Y}, {X, Y}}] - 
     288*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*\[Omega][{{X, Y}, {X, Y}}] + 
     464*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M*
      \[Omega][{{X, Y}, {X, Y}}] + 
     48*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M*
      \[Omega][{{X, Y}, {X, Y}}] - 
     264*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^2*\[Omega][{{X, Y}, {X, Y}}] + 
     156*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
      \[Omega][{{X, Y}, {X, Y}}] + 
     128*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
         5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
      \[Omega][{{X, Y}, {X, Y}}] + 
     24*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
      M^2*\[Omega][{{X, Y}, {X, Y}}] + 
     36*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^3*\[Omega][{{X, Y}, {X, Y}}] + 
     8*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3*
      \[Omega][{{X, Y}, {X, Y}}] - 
     36*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
      M^3*\[Omega][{{X, Y}, {X, Y}}] + 
     12*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^4*\[Omega][{{X, Y}, {X, Y}}] - 
     E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^4*
      \[Omega][{{X, Y}, {X, Y}}] - 
     4*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
         5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^4*
      \[Omega][{{X, Y}, {X, Y}}] - 
     12*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
      M^4*\[Omega][{{X, Y}, {X, Y}}] - 
     E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
         5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^5*
      \[Omega][{{X, Y}, {X, Y}}] + 
     704*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}] - 
     864*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, Y}, {X, Y}}] + 
     1248*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}] + 
     80*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}] - 
     184*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, Y}, {X, Y}}] + 
     248*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}] + 
     160*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
         5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}] - 
     128*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
      M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}] + 
     84*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, Y}, {X, Y}}] + 
     4*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}] - 
     16*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
         5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}] - 
     96*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
      M^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}] + 
     4*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, Y}, {X, Y}}] - 
     6*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
         5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^4*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}] - 
     8*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
      M^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}] + 
     864*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, Y}, {X, Y}}] - 
     640*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, Y}, {X, Y}}] + 
     1160*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, Y}, {X, Y}}] - 
     112*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, Y}, {X, Y}}] + 
     64*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, Y}, {X, Y}}] + 
     116*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, Y}, {X, Y}}] + 
     48*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
         5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, Y}, {X, Y}}] - 
     240*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
      M^2*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, Y}, {X, Y}}] + 
     24*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, Y}, {X, Y}}] - 
     12*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
         5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, Y}, {X, Y}}] - 
     48*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
      M^3*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, Y}, {X, Y}}] + 
     512*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, Y}, {X, Y}}] - 
     128*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, Y}, {X, Y}}] + 
     416*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, Y}, {X, Y}}] - 
     192*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, Y}, {X, Y}}] + 
     32*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
          8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, Y}, {X, Y}}] + 
     8*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, Y}, {X, Y}}] - 
     96*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
      M^2*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, Y}, {X, Y}}] + 
     128*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, Y}, {X, Y}}] + 
     32*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, Y}, {X, Y}}] - 
     64*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, Y}, {X, Y}}] + 
     96*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{X, Y}, {X, Y}}]^2 - 
     8*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M*
      \[Omega][{{X, Y}, {X, Y}}]^2 + 
     96*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M*
      \[Omega][{{X, Y}, {X, Y}}]^2 - 
     76*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
      \[Omega][{{X, Y}, {X, Y}}]^2 - 
     32*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
         5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
      \[Omega][{{X, Y}, {X, Y}}]^2 + 
     96*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
      M^2*\[Omega][{{X, Y}, {X, Y}}]^2 - 
     12*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3*
      \[Omega][{{X, Y}, {X, Y}}]^2 + 
     8*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
         5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3*
      \[Omega][{{X, Y}, {X, Y}}]^2 + 
     352*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}]^2 - 
     136*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}]^2 + 
     256*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}]^2 - 
     100*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}]^2 + 
     64*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
      M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}]^2 - 
     4*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}]^2 + 
     384*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, Y}, {X, Y}}]^2 - 
     128*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, Y}, {X, Y}}]^2 + 
     128*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, Y}, {X, Y}}]^2 - 
     24*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, Y}, {X, Y}}]^2 + 
     128*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, Y}, {X, Y}}]^2 - 
     32*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, Y}, {X, Y}}]^2 - 
     96*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{X, Y}, {X, Y}}]^3 - 
     136*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M*
      \[Omega][{{X, Y}, {X, Y}}]^3 + 
     48*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M*
      \[Omega][{{X, Y}, {X, Y}}]^3 - 
     20*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
      \[Omega][{{X, Y}, {X, Y}}]^3 - 
     16*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
         5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
      \[Omega][{{X, Y}, {X, Y}}]^3 + 
     48*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
      M^2*\[Omega][{{X, Y}, {X, Y}}]^3 + 
     4*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
         5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3*
      \[Omega][{{X, Y}, {X, Y}}]^3 - 
     256*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}]^3 - 
     160*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}]^3 + 
     128*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}]^3 - 
     8*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}]^3 + 
     32*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
      M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}]^3 - 
     128*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, Y}, {X, Y}}]^3 - 
     32*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, Y}, {X, Y}}]^3 + 
     64*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, Y}, {X, Y}}]^3 + 
     4*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Lambda][{x}]^3*(6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
       4*\[Omega][{{Y}, {X, X, Y}}])*(2 + \[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}])*
      (2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
       2*\[Omega][{{X, Y}, {X, Y}}]) + 8*\[Omega][{{X}, {X, Y, Y}}]^4*
      (20*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
        (-4 + M)*\[Omega][{{Y}, {X, X, Y}}] + 
       E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}]))*
        (-8*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X, Y}, {X, Y}}])*M + 
         4*E^((T*(4 + M + 2*\[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
              2*\[Omega][{{Y}, {X, X, Y}}]))/2)*(-4 + M)*M + 
         E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*(-80 + 16*M + 3*M^2) + 
         4*E^(T*\[Omega][{{X, Y}, {X, Y}}])*
          (-2*E^((M*T)/2 + T*\[Lambda][{x}])*M + E^(2*T)*(4 + M))*
          \[Omega][{{X, Y}, {X, Y}}])) + 4*\[Omega][{{X}, {X, Y, Y}}]^3*
      (-280*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])) - 
       192*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
            8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M + 
       66*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M - 
       48*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
        M + 24*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 
            8*\[Omega][{{X}, {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*
        M^2 + 33*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2 - 
       8*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2 - 
       24*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
        M^2 + 6*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 
            8*\[Omega][{{X}, {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*
        M^3 + E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3 + 
       2*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3 + 
       80*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
        (-4 + M)*\[Omega][{{Y}, {X, X, Y}}]^2 + 
       2*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}]))*
        (4*E^((T*(4 + M + 2*\[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
              2*\[Omega][{{Y}, {X, X, Y}}]))/2)*(-4 + M)*M - 
         12*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X, Y}, {X, Y}}])*M*
          (2 + M) + E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*(64 + 52*M + M^2))*
        \[Omega][{{X, Y}, {X, Y}}] - 
       8*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
        (-4 + M)*\[Omega][{{X, Y}, {X, Y}}]^2 + 
       8*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]*
        (-8*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X, Y}, {X, Y}}])*M + 
         4*E^((T*(4 + M + 2*\[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
              2*\[Omega][{{Y}, {X, X, Y}}]))/2)*(-4 + M)*M + 
         E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*(-80 + 16*M + 3*M^2) + 
         4*E^(T*\[Omega][{{X, Y}, {X, Y}}])*
          (-2*E^((M*T)/2 + T*\[Lambda][{x}])*M + E^(2*T)*(4 + M))*
          \[Omega][{{X, Y}, {X, Y}}])) + 2*\[Omega][{{X}, {X, Y, Y}}]^2*
      (-400*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])) - 
       752*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
            8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M + 
       244*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M - 
       88*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
        M - 60*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 
            8*\[Omega][{{X}, {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*
        M^2 + 154*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2 - 
       64*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2 - 
       120*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
        M^2 + 54*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 
            8*\[Omega][{{X}, {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*
        M^3 + 12*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3 + 
       4*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3 - 
       24*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
        M^3 + 2*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 
            8*\[Omega][{{X}, {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*
        M^4 + 3*E^(T*(5 + 2*M + \[Lambda][{x}] + 
           5*\[Omega][{{X}, {X, Y, Y}}] + 5*\[Omega][{{Y}, {X, X, Y}}] + 
           \[Omega][{{X, Y}, {X, Y}}]))*M^4 + 
       160*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
        (-4 + M)*\[Omega][{{Y}, {X, X, Y}}]^3 + 
       2*(-3*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
             5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (-4 + M)*M^2 + 2*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 
              8*\[Omega][{{X}, {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/
            2)*M*(-80 + 8*M + 3*M^2) - 
         4*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
              6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/
            2)*M*(7 + 15*M + 3*M^2) + 
         E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (216 + 290*M + 29*M^2))*\[Omega][{{X, Y}, {X, Y}}] - 
       4*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
        (-16*E^((M*T)/2 + T*\[Lambda][{x}])*M + E^(2*T)*(-48 + 16*M + 3*M^2))*
        \[Omega][{{X, Y}, {X, Y}}]^2 - 
       16*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
        (-2*E^((M*T)/2 + T*\[Lambda][{x}])*M + E^(2*T)*(4 + M))*
        \[Omega][{{X, Y}, {X, Y}}]^3 + 
       24*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^2*
        (-8*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X, Y}, {X, Y}}])*M + 
         4*E^((T*(4 + M + 2*\[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
              2*\[Omega][{{Y}, {X, X, Y}}]))/2)*(-4 + M)*M + 
         E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*(-80 + 16*M + 3*M^2) + 
         4*E^(T*\[Omega][{{X, Y}, {X, Y}}])*
          (-2*E^((M*T)/2 + T*\[Lambda][{x}])*M + E^(2*T)*(4 + M))*
          \[Omega][{{X, Y}, {X, Y}}]) + 6*\[Omega][{{Y}, {X, X, Y}}]*
        (2*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
             5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (-4 + M)*M^2 - 24*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 
              6*\[Omega][{{X}, {X, Y, Y}}] + 6*\[Omega][{{Y}, {X, X, Y}}] + 
              2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M*(2 + M) + 
         6*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
              8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*(-32 + 4*M + M^2) + 
         E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (-280 + 66*M + 33*M^2 + M^3) + 
         2*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}]))*
          (4*E^((T*(4 + M + 2*\[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
                2*\[Omega][{{Y}, {X, X, Y}}]))/2)*(-4 + M)*M - 
           12*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X, Y}, {X, Y}}])*M*
            (2 + M) + E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*
            (64 + 52*M + M^2))*\[Omega][{{X, Y}, {X, Y}}] - 
         8*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (-4 + M)*\[Omega][{{X, Y}, {X, Y}}]^2)) + 
     \[Omega][{{X}, {X, Y, Y}}]*
      (-192*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])) - 
       1152*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 
            8*\[Omega][{{X}, {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*
        M + 528*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M - 
       48*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
        M - 448*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 
            8*\[Omega][{{X}, {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*
        M^2 + 356*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2 - 
       128*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2 - 
       160*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
        M^2 + 120*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 
            8*\[Omega][{{X}, {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*
        M^3 + 48*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3 - 
       48*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3 - 
       96*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
        M^3 + 16*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 
            8*\[Omega][{{X}, {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*
        M^4 + E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^4 + 
       16*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^4 - 
       8*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
        M^4 + E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^5 + 
       160*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
        (-4 + M)*\[Omega][{{Y}, {X, X, Y}}]^4 + 
       2*(-(E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
              5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
           (-80 + 8*M + 3*M^2)) - 
         4*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
              6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/
            2)*M*(-10 + 16*M + 12*M^2 + M^3) + 
         2*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
              8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*(-216 - 46*M + 21*M^2 + 
           M^3) + 2*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (176 + 312*M + 62*M^2 + M^3))*\[Omega][{{X, Y}, {X, Y}}] - 
       4*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
        (-16*E^((M*T)/2 + T*\[Lambda][{x}])*M*(4 + M) + 
         E^(2*T)*(-88 + 34*M + 25*M^2 + M^3))*\[Omega][{{X, Y}, {X, Y}}]^2 - 
       8*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
        (-4*E^((M*T)/2 + T*\[Lambda][{x}])*M*(4 + M) + 
         E^(2*T)*(32 + 20*M + M^2))*\[Omega][{{X, Y}, {X, Y}}]^3 + 
       32*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^3*
        (-8*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X, Y}, {X, Y}}])*M + 
         4*E^((T*(4 + M + 2*\[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
              2*\[Omega][{{Y}, {X, X, Y}}]))/2)*(-4 + M)*M + 
         E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*(-80 + 16*M + 3*M^2) + 
         4*E^(T*\[Omega][{{X, Y}, {X, Y}}])*
          (-2*E^((M*T)/2 + T*\[Lambda][{x}])*M + E^(2*T)*(4 + M))*
          \[Omega][{{X, Y}, {X, Y}}]) + 12*\[Omega][{{Y}, {X, X, Y}}]^2*
        (2*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
             5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (-4 + M)*M^2 - 24*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 
              6*\[Omega][{{X}, {X, Y, Y}}] + 6*\[Omega][{{Y}, {X, X, Y}}] + 
              2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M*(2 + M) + 
         6*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
              8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*(-32 + 4*M + M^2) + 
         E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (-280 + 66*M + 33*M^2 + M^3) + 
         2*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}]))*
          (4*E^((T*(4 + M + 2*\[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
                2*\[Omega][{{Y}, {X, X, Y}}]))/2)*(-4 + M)*M - 
           12*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X, Y}, {X, Y}}])*M*
            (2 + M) + E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*
            (64 + 52*M + M^2))*\[Omega][{{X, Y}, {X, Y}}] - 
         8*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (-4 + M)*\[Omega][{{X, Y}, {X, Y}}]^2) + 
       4*\[Omega][{{Y}, {X, X, Y}}]*
        (E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
             5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
          (-64 + 4*M + 3*M^2) - 8*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 
              6*\[Omega][{{X}, {X, Y, Y}}] + 6*\[Omega][{{Y}, {X, X, Y}}] + 
              2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M*(11 + 15*M + 3*M^2) + 
         2*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
              8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*(-376 - 30*M + 27*M^2 + 
           M^3) + 2*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (-200 + 122*M + 77*M^2 + 6*M^3) + 
         2*(-3*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, 
                  {X, Y, Y}}] + 5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][
                {{X, Y}, {X, Y}}]))*(-4 + M)*M^2 + 
           2*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, 
                   {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*
            (-80 + 8*M + 3*M^2) - 4*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 
                6*\[Omega][{{X}, {X, Y, Y}}] + 6*\[Omega][{{Y}, {X, X, Y}}] + 
                2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M*(7 + 15*M + 3*M^2) + 
           E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 3*\[Omega][
                 {{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
            (216 + 290*M + 29*M^2))*\[Omega][{{X, Y}, {X, Y}}] - 
         4*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (-16*E^((M*T)/2 + T*\[Lambda][{x}])*M + E^(2*T)*(-48 + 16*M + 
             3*M^2))*\[Omega][{{X, Y}, {X, Y}}]^2 - 
         16*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (-2*E^((M*T)/2 + T*\[Lambda][{x}])*M + E^(2*T)*(4 + M))*
          \[Omega][{{X, Y}, {X, Y}}]^3)) + 4*\[Lambda][{x}]^2*
      (-24*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])) + 
       36*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
            8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M + 
       8*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M - 
       12*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
        M + 6*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 
            8*\[Omega][{{X}, {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*
        M^2 + 14*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2 + 
       16*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2 - 
       6*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
        M^2 + 2*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3 + 
       2*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3 + 
       24*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
        \[Omega][{{X}, {X, Y, Y}}]^4 + 
       24*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
        \[Omega][{{Y}, {X, X, Y}}]^4 - 
       36*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
        \[Omega][{{X, Y}, {X, Y}}] + 
       36*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
            8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*\[Omega][{{X, Y}, {X, Y}}] - 
       24*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
        M*\[Omega][{{X, Y}, {X, Y}}] + 
       6*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
            8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^2*
        \[Omega][{{X, Y}, {X, Y}}] - 
       5*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
        \[Omega][{{X, Y}, {X, Y}}] - 
       4*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
        \[Omega][{{X, Y}, {X, Y}}] - 
       6*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
        M^2*\[Omega][{{X, Y}, {X, Y}}] - 
       E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3*
        \[Omega][{{X, Y}, {X, Y}}] - 
       E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3*
        \[Omega][{{X, Y}, {X, Y}}] - 
       6*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M*
        \[Omega][{{X, Y}, {X, Y}}]^2 - 
       12*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
        M*\[Omega][{{X, Y}, {X, Y}}]^2 - 
       E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
        \[Omega][{{X, Y}, {X, Y}}]^2 - 
       2*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
        \[Omega][{{X, Y}, {X, Y}}]^2 + 
       12*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
        \[Omega][{{X, Y}, {X, Y}}]^3 + 
       2*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M*
        \[Omega][{{X, Y}, {X, Y}}]^3 + 
       2*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^3*
        (4*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X}, {X, Y, Y}}] + 
            T*\[Omega][{{Y}, {X, X, Y}}])*M + 
         E^(T*\[Omega][{{X, Y}, {X, Y}}])*(50 + 13*M) - 
         4*E^(T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}]) + 
       2*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^3*
        (50*E^(T*\[Omega][{{X, Y}, {X, Y}}]) + 
         4*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X}, {X, Y, Y}}] + 
            T*\[Omega][{{Y}, {X, X, Y}}])*M + 
         13*E^(T*\[Omega][{{X, Y}, {X, Y}}])*M + 
         48*E^(T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{Y}, {X, X, Y}}] - 
         4*E^(T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}]) + 
       \[Omega][{{Y}, {X, X, Y}}]^2*
        (E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}]))*
          (-8*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X, Y}, {X, Y}}])*
            M + 4*E^(T*(3 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, 
                  {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][
                {{X, Y}, {X, Y}}]))*M^2 + 
           2*E^((T*(4 + M + 2*\[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
                2*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*(22 + M) + 
           3*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*(40 + 30*M + 3*M^2)) - 
         2*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}]))*
          (-4*E^((T*(4 + M + 2*\[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, 
                    Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}]))/2)*M + 
           4*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X, Y}, {X, Y}}])*
            M + E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*(6 + 9*M))*
          \[Omega][{{X, Y}, {X, Y}}] - 
         24*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          \[Omega][{{X, Y}, {X, Y}}]^2) + \[Omega][{{Y}, {X, X, Y}}]*
        (-4*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, 
                 {X, Y, Y}}] + 6*\[Omega][{{Y}, {X, X, Y}}] + 
              2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M*(5 + M) + 
         8*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
              8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*(9 + M) + 
         E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
             5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
          (16 + M) + E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (20 + 80*M + 25*M^2 + M^3) - 
         2*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}]))*
          (E^(T*(3 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*
                \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
            M^2 + 2*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X, Y}, 
                 {X, Y}}])*M*(7 + M) - E^((T*(4 + M + 2*\[Lambda][{x}] + 
                2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}]))/
              2)*M*(18 + M) + 2*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*
            (6 + 5*M + 2*M^2))*\[Omega][{{X, Y}, {X, Y}}] - 
         2*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (4*E^((M*T)/2 + T*\[Lambda][{x}])*M + E^(2*T)*(18 + 5*M))*
          \[Omega][{{X, Y}, {X, Y}}]^2 + 
         8*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          \[Omega][{{X, Y}, {X, Y}}]^3) + \[Omega][{{X}, {X, Y, Y}}]^2*
        (E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}]))*
          (-8*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X, Y}, {X, Y}}])*
            M + 4*E^(T*(3 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, 
                  {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][
                {{X, Y}, {X, Y}}]))*M^2 + 
           2*E^((T*(4 + M + 2*\[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
                2*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*(22 + M) + 
           3*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*(40 + 30*M + 3*M^2)) + 
         144*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          \[Omega][{{Y}, {X, X, Y}}]^2 - 
         2*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}]))*
          (-4*E^((T*(4 + M + 2*\[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, 
                    Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}]))/2)*M + 
           4*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X, Y}, {X, Y}}])*
            M + E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*(6 + 9*M))*
          \[Omega][{{X, Y}, {X, Y}}] - 
         24*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          \[Omega][{{X, Y}, {X, Y}}]^2 + 
         6*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]*
          (4*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X}, {X, Y, Y}}] + 
              T*\[Omega][{{Y}, {X, X, Y}}])*M + 
           E^(T*\[Omega][{{X, Y}, {X, Y}}])*(50 + 13*M) - 
           4*E^(T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}])) + 
       \[Omega][{{X}, {X, Y, Y}}]*
        (20*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])) + 
         72*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, 
                 {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M + 
         80*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M - 
         20*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, 
                 {X, Y, Y}}] + 6*\[Omega][{{Y}, {X, X, Y}}] + 
              2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M + 
         8*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
              8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^2 + 
         25*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          M^2 + 16*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, 
                {X, Y, Y}}] + 5*\[Omega][{{Y}, {X, X, Y}}] + 
             \[Omega][{{X, Y}, {X, Y}}]))*M^2 - 
         4*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
              6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/
            2)*M^2 + E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          M^3 + E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, 
                {X, Y, Y}}] + 5*\[Omega][{{Y}, {X, X, Y}}] + 
             \[Omega][{{X, Y}, {X, Y}}]))*M^3 + 
         96*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          \[Omega][{{Y}, {X, X, Y}}]^3 - 
         2*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}]))*
          (E^(T*(3 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*
                \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
            M^2 + 2*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X, Y}, 
                 {X, Y}}])*M*(7 + M) - E^((T*(4 + M + 2*\[Lambda][{x}] + 
                2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}]))/
              2)*M*(18 + M) + 2*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*
            (6 + 5*M + 2*M^2))*\[Omega][{{X, Y}, {X, Y}}] - 
         2*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (4*E^((M*T)/2 + T*\[Lambda][{x}])*M + E^(2*T)*(18 + 5*M))*
          \[Omega][{{X, Y}, {X, Y}}]^2 + 
         8*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          \[Omega][{{X, Y}, {X, Y}}]^3 + 
         6*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^2*
          (4*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X}, {X, Y, Y}}] + 
              T*\[Omega][{{Y}, {X, X, Y}}])*M + 
           E^(T*\[Omega][{{X, Y}, {X, Y}}])*(50 + 13*M) - 
           4*E^(T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}]) + 
         2*\[Omega][{{Y}, {X, X, Y}}]*
          (E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 3*\[Omega][
                 {{Y}, {X, X, Y}}]))*(-8*E^((M*T)/2 + T*\[Lambda][{x}] + 
                T*\[Omega][{{X, Y}, {X, Y}}])*M + 
             4*E^(T*(3 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
                 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
              M^2 + 2*E^((T*(4 + M + 2*\[Lambda][{x}] + 2*\[Omega][
                    {{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*
              (22 + M) + 3*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*
              (40 + 30*M + 3*M^2)) - 
           2*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 3*\[Omega][
                 {{Y}, {X, X, Y}}]))*(-4*E^((T*(4 + M + 2*\[Lambda][{x}] + 
                  2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, 
                      Y}}]))/2)*M + 4*E^((M*T)/2 + T*\[Lambda][{x}] + 
                T*\[Omega][{{X, Y}, {X, Y}}])*M + 
             E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*(6 + 9*M))*
            \[Omega][{{X, Y}, {X, Y}}] - 
           24*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 3*\[Omega][
                 {{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
            \[Omega][{{X, Y}, {X, Y}}]^2))) + \[Lambda][{x}]*
      (-192*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])) - 
       144*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
            8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M - 
       128*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M - 
       48*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
        M + 192*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 
            8*\[Omega][{{X}, {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*
        M^2 + 8*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2 - 
       128*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2 - 
       96*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
        M^2 + 36*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 
            8*\[Omega][{{X}, {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*
        M^3 + 16*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3 + 
       48*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3 - 
       36*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
        M^3 + 2*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^4 + 
       8*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^4 + 
       64*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
        \[Omega][{{X}, {X, Y, Y}}]^5 + 
       64*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
        \[Omega][{{Y}, {X, X, Y}}]^5 - 
       144*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
            8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*\[Omega][{{X, Y}, {X, Y}}] + 
       48*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M*
        \[Omega][{{X, Y}, {X, Y}}] - 
       48*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
        M*\[Omega][{{X, Y}, {X, Y}}] + 
       192*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
            8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^2*
        \[Omega][{{X, Y}, {X, Y}}] - 
       4*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
        \[Omega][{{X, Y}, {X, Y}}] - 
       32*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
        \[Omega][{{X, Y}, {X, Y}}] - 
       144*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
        M^2*\[Omega][{{X, Y}, {X, Y}}] + 
       36*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
            8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^3*
        \[Omega][{{X, Y}, {X, Y}}] - 
       8*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3*
        \[Omega][{{X, Y}, {X, Y}}] - 
       16*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3*
        \[Omega][{{X, Y}, {X, Y}}] - 
       36*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
        M^3*\[Omega][{{X, Y}, {X, Y}}] - 
       E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^4*
        \[Omega][{{X, Y}, {X, Y}}] - 
       4*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^4*
        \[Omega][{{X, Y}, {X, Y}}] + 
       144*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
        \[Omega][{{X, Y}, {X, Y}}]^2 - 
       24*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M*
        \[Omega][{{X, Y}, {X, Y}}]^2 + 
       48*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
        M*\[Omega][{{X, Y}, {X, Y}}]^2 - 
       8*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
        \[Omega][{{X, Y}, {X, Y}}]^2 + 
       32*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
        \[Omega][{{X, Y}, {X, Y}}]^2 - 
       48*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
        M^2*\[Omega][{{X, Y}, {X, Y}}]^2 - 
       4*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^3*
        \[Omega][{{X, Y}, {X, Y}}]^2 - 
       48*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
        \[Omega][{{X, Y}, {X, Y}}]^3 + 
       16*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M*
        \[Omega][{{X, Y}, {X, Y}}]^3 + 
       48*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
            6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
        M*\[Omega][{{X, Y}, {X, Y}}]^3 + 
       4*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
        \[Omega][{{X, Y}, {X, Y}}]^3 + 
       8*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
        \[Omega][{{X, Y}, {X, Y}}]^3 + 
       32*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^4*
        (2*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X}, {X, Y, Y}}] + 
            T*\[Omega][{{Y}, {X, X, Y}}])*M + 
         E^(T*\[Omega][{{X, Y}, {X, Y}}])*(4 + 3*M) - 
         2*E^(T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}]) + 
       32*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^4*
        (4*E^(T*\[Omega][{{X, Y}, {X, Y}}]) + 
         2*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X}, {X, Y, Y}}] + 
            T*\[Omega][{{Y}, {X, X, Y}}])*M + 
         3*E^(T*\[Omega][{{X, Y}, {X, Y}}])*M + 
         10*E^(T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{Y}, {X, X, Y}}] - 
         2*E^(T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}]) + 
       4*\[Omega][{{Y}, {X, X, Y}}]^3*
        (E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}]))*
          (-24*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X, Y}, {X, Y}}])*
            M + 4*E^(T*(3 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, 
                  {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][
                {{X, Y}, {X, Y}}]))*M^2 + 
           16*E^((T*(4 + M + 2*\[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, 
                    Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*(5 + M) + 
           E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*(-92 + 64*M + 13*M^2)) - 
         8*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}]))*
          (-2*E^((T*(4 + M + 2*\[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, 
                    Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}]))/2)*M + 
           3*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X, Y}, {X, Y}}])*
            M + 3*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*(2 + M))*
          \[Omega][{{X, Y}, {X, Y}}] - 
         16*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          \[Omega][{{X, Y}, {X, Y}}]^2) + 4*\[Omega][{{Y}, {X, X, Y}}]^2*
        (E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
             5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
          (8 + 5*M) - 4*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][
                {{X}, {X, Y, Y}}] + 6*\[Omega][{{Y}, {X, X, Y}}] + 
              2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M*(17 + 6*M) + 
         E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
              8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*(100 + 80*M + 3*M^2) + 
         E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (-284 - 2*M + 38*M^2 + 3*M^3) - 
         E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}]))*
          (6*E^(T*(3 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*
                \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
            M^2 - 16*E^((T*(4 + M + 2*\[Lambda][{x}] + 2*\[Omega][
                  {{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*
            (4 + M) + 12*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][
                {{X, Y}, {X, Y}}])*M*(7 + 2*M) + 
           E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*(84 + 28*M + 13*M^2))*
          \[Omega][{{X, Y}, {X, Y}}] - 
         8*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (E^(2*T) + 2*E^((M*T)/2 + T*\[Lambda][{x}]))*M*
          \[Omega][{{X, Y}, {X, Y}}]^2 + 
         16*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          \[Omega][{{X, Y}, {X, Y}}]^3) + \[Omega][{{Y}, {X, X, Y}}]*
        (16*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, 
                 {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^2*
          (28 + 3*M) + 4*E^(T*(5 + 2*M + \[Lambda][{x}] + 
             5*\[Omega][{{X}, {X, Y, Y}}] + 5*\[Omega][{{Y}, {X, X, Y}}] + 
             \[Omega][{{X, Y}, {X, Y}}]))*M^2*(-16 + 16*M + M^2) - 
         8*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, {X, Y, Y}}] + 
              6*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/
            2)*M*(28 + 26*M + 3*M^2) + 
         E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (-896 - 336*M + 100*M^2 + 32*M^3 + M^4) - 
         4*(4*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
               5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
            M^2*(4 + M) + 2*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 
                6*\[Omega][{{X}, {X, Y, Y}}] + 6*\[Omega][{{Y}, {X, X, Y}}] + 
                2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M*(40 + 30*M + 3*M^2) - 
           E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
                8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*(36 + 64*M + 3*M^2) + 
           E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 3*\[Omega][
                 {{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
            (72 - 8*M + 12*M^2 + 3*M^3))*\[Omega][{{X, Y}, {X, Y}}] - 
         4*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (8*E^((M*T)/2 + T*\[Lambda][{x}])*M*(2 + M) + 
           E^(2*T)*(-60 + 8*M + M^2))*\[Omega][{{X, Y}, {X, Y}}]^2 + 
         32*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (E^((M*T)/2 + T*\[Lambda][{x}])*M + E^(2*T)*(2 + M))*
          \[Omega][{{X, Y}, {X, Y}}]^3) + 4*\[Omega][{{X}, {X, Y, Y}}]^3*
        (E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}]))*
          (-24*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X, Y}, {X, Y}}])*
            M + 4*E^(T*(3 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, 
                  {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][
                {{X, Y}, {X, Y}}]))*M^2 + 
           16*E^((T*(4 + M + 2*\[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, 
                    Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*(5 + M) + 
           E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*(-92 + 64*M + 13*M^2)) + 
         160*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          \[Omega][{{Y}, {X, X, Y}}]^2 - 
         8*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}]))*
          (-2*E^((T*(4 + M + 2*\[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, 
                    Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}]))/2)*M + 
           3*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X, Y}, {X, Y}}])*
            M + 3*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*(2 + M))*
          \[Omega][{{X, Y}, {X, Y}}] - 
         16*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          \[Omega][{{X, Y}, {X, Y}}]^2 + 
         32*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]*
          (2*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X}, {X, Y, Y}}] + 
              T*\[Omega][{{Y}, {X, X, Y}}])*M + 
           E^(T*\[Omega][{{X, Y}, {X, Y}}])*(4 + 3*M) - 
           2*E^(T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}])) + 
       4*\[Omega][{{X}, {X, Y, Y}}]^2*
        (-284*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])) + 
         100*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, 
                 {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M - 
         2*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M - 
         68*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, 
                 {X, Y, Y}}] + 6*\[Omega][{{Y}, {X, X, Y}}] + 
              2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M + 
         80*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, 
                 {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^2 + 
         38*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          M^2 + 8*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, 
                {X, Y, Y}}] + 5*\[Omega][{{Y}, {X, X, Y}}] + 
             \[Omega][{{X, Y}, {X, Y}}]))*M^2 - 
         24*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, 
                 {X, Y, Y}}] + 6*\[Omega][{{Y}, {X, X, Y}}] + 
              2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M^2 + 
         3*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
              8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^3 + 
         3*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          M^3 + 5*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, 
                {X, Y, Y}}] + 5*\[Omega][{{Y}, {X, X, Y}}] + 
             \[Omega][{{X, Y}, {X, Y}}]))*M^3 + 
         160*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          \[Omega][{{Y}, {X, X, Y}}]^3 - 
         E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}]))*
          (6*E^(T*(3 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*
                \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
            M^2 - 16*E^((T*(4 + M + 2*\[Lambda][{x}] + 2*\[Omega][
                  {{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*
            (4 + M) + 12*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][
                {{X, Y}, {X, Y}}])*M*(7 + 2*M) + 
           E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*(84 + 28*M + 13*M^2))*
          \[Omega][{{X, Y}, {X, Y}}] - 
         8*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (E^(2*T) + 2*E^((M*T)/2 + T*\[Lambda][{x}]))*M*
          \[Omega][{{X, Y}, {X, Y}}]^2 + 
         16*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          \[Omega][{{X, Y}, {X, Y}}]^3 + 
         48*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^2*
          (2*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X}, {X, Y, Y}}] + 
              T*\[Omega][{{Y}, {X, X, Y}}])*M + 
           E^(T*\[Omega][{{X, Y}, {X, Y}}])*(4 + 3*M) - 
           2*E^(T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}]) + 
         3*\[Omega][{{Y}, {X, X, Y}}]*
          (E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 3*\[Omega][
                 {{Y}, {X, X, Y}}]))*(-24*E^((M*T)/2 + T*\[Lambda][{x}] + 
                T*\[Omega][{{X, Y}, {X, Y}}])*M + 
             4*E^(T*(3 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
                 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
              M^2 + 16*E^((T*(4 + M + 2*\[Lambda][{x}] + 2*\[Omega][
                    {{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*
              (5 + M) + E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*
              (-92 + 64*M + 13*M^2)) - 
           8*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 3*\[Omega][
                 {{Y}, {X, X, Y}}]))*(-2*E^((T*(4 + M + 2*\[Lambda][{x}] + 
                  2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, 
                      Y}}]))/2)*M + 3*E^((M*T)/2 + T*\[Lambda][{x}] + 
                T*\[Omega][{{X, Y}, {X, Y}}])*M + 
             3*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*(2 + M))*
            \[Omega][{{X, Y}, {X, Y}}] - 
           16*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 3*\[Omega][
                 {{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
            \[Omega][{{X, Y}, {X, Y}}]^2)) + \[Omega][{{X}, {X, Y, Y}}]*
        (-896*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])) - 
         336*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M - 
         224*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, 
                 {X, Y, Y}}] + 6*\[Omega][{{Y}, {X, X, Y}}] + 
              2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M + 
         448*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, 
                 {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^2 + 
         100*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          M^2 - 64*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, 
                {X, Y, Y}}] + 5*\[Omega][{{Y}, {X, X, Y}}] + 
             \[Omega][{{X, Y}, {X, Y}}]))*M^2 - 
         208*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, 
                 {X, Y, Y}}] + 6*\[Omega][{{Y}, {X, X, Y}}] + 
              2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M^2 + 
         48*E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, 
                 {X, Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M^3 + 
         32*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          M^3 + 64*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, 
                {X, Y, Y}}] + 5*\[Omega][{{Y}, {X, X, Y}}] + 
             \[Omega][{{X, Y}, {X, Y}}]))*M^3 - 
         24*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 6*\[Omega][{{X}, 
                 {X, Y, Y}}] + 6*\[Omega][{{Y}, {X, X, Y}}] + 
              2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M^3 + 
         E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          M^4 + 4*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, 
                {X, Y, Y}}] + 5*\[Omega][{{Y}, {X, X, Y}}] + 
             \[Omega][{{X, Y}, {X, Y}}]))*M^4 + 
         320*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          \[Omega][{{Y}, {X, X, Y}}]^4 - 
         4*(4*E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
               5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
            M^2*(4 + M) + 2*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 
                6*\[Omega][{{X}, {X, Y, Y}}] + 6*\[Omega][{{Y}, {X, X, Y}}] + 
                2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M*(40 + 30*M + 3*M^2) - 
           E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
                8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*(36 + 64*M + 3*M^2) + 
           E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 3*\[Omega][
                 {{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
            (72 - 8*M + 12*M^2 + 3*M^3))*\[Omega][{{X, Y}, {X, Y}}] - 
         4*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (8*E^((M*T)/2 + T*\[Lambda][{x}])*M*(2 + M) + 
           E^(2*T)*(-60 + 8*M + M^2))*\[Omega][{{X, Y}, {X, Y}}]^2 + 
         32*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (E^((M*T)/2 + T*\[Lambda][{x}])*M + E^(2*T)*(2 + M))*
          \[Omega][{{X, Y}, {X, Y}}]^3 + 
         128*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^3*
          (2*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X}, {X, Y, Y}}] + 
              T*\[Omega][{{Y}, {X, X, Y}}])*M + 
           E^(T*\[Omega][{{X, Y}, {X, Y}}])*(4 + 3*M) - 
           2*E^(T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}]) + 
         12*\[Omega][{{Y}, {X, X, Y}}]^2*
          (E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 3*\[Omega][
                 {{Y}, {X, X, Y}}]))*(-24*E^((M*T)/2 + T*\[Lambda][{x}] + 
                T*\[Omega][{{X, Y}, {X, Y}}])*M + 
             4*E^(T*(3 + M + \[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
                 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
              M^2 + 16*E^((T*(4 + M + 2*\[Lambda][{x}] + 2*\[Omega][
                    {{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*
              (5 + M) + E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*
              (-92 + 64*M + 13*M^2)) - 
           8*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 3*\[Omega][
                 {{Y}, {X, X, Y}}]))*(-2*E^((T*(4 + M + 2*\[Lambda][{x}] + 
                  2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, 
                      Y}}]))/2)*M + 3*E^((M*T)/2 + T*\[Lambda][{x}] + 
                T*\[Omega][{{X, Y}, {X, Y}}])*M + 
             3*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*(2 + M))*
            \[Omega][{{X, Y}, {X, Y}}] - 
           16*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 3*\[Omega][
                 {{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
            \[Omega][{{X, Y}, {X, Y}}]^2) + 8*\[Omega][{{Y}, {X, X, Y}}]*
          (E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 5*
                \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2*
            (8 + 5*M) - 4*E^((T*(4 + 3*M + 2*\[Lambda][{x}] + 
                6*\[Omega][{{X}, {X, Y, Y}}] + 6*\[Omega][{{Y}, {X, X, Y}}] + 
                2*\[Omega][{{X, Y}, {X, Y}}]))/2)*M*(17 + 6*M) + 
           E^((T*(8 + 3*M + 2*\[Lambda][{x}] + 8*\[Omega][{{X}, {X, Y, Y}}] + 
                8*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*(100 + 80*M + 3*M^2) + 
           E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 3*\[Omega][
                 {{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
            (-284 - 2*M + 38*M^2 + 3*M^3) - 
           E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 3*\[Omega][
                 {{Y}, {X, X, Y}}]))*(6*E^(T*(3 + M + \[Lambda][{x}] + 
                 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, 
                     Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*M^2 - 
             16*E^((T*(4 + M + 2*\[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, 
                      Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}]))/2)*M*(4 + M) + 
             12*E^((M*T)/2 + T*\[Lambda][{x}] + T*\[Omega][{{X, Y}, {X, Y}}])*
              M*(7 + 2*M) + E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*
              (84 + 28*M + 13*M^2))*\[Omega][{{X, Y}, {X, Y}}] - 
           8*E^(T*(2 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 3*\[Omega][
                 {{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
            (E^(2*T) + 2*E^((M*T)/2 + T*\[Lambda][{x}]))*M*
            \[Omega][{{X, Y}, {X, Y}}]^2 + 
           16*E^(T*(4 + M + 3*\[Omega][{{X}, {X, Y, Y}}] + 3*\[Omega][
                 {{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
            \[Omega][{{X, Y}, {X, Y}}]^3)))))/
   (E^(T*(5 + 2*M + \[Lambda][{x}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
       5*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
    (-4 + M + 2*\[Lambda][{x}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{Y}, {X, X, Y}}])*(1 + M + \[Lambda][{x}] + 
     2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
    (6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
    (M + 2*\[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
     2*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, Y}, {X, Y}}])*
    (2 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] - 
     \[Omega][{{X, Y}, {X, Y}}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])*
    (1 + 2*\[Omega][{{X, Y}, {X, Y}}])*
    (2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
     2*\[Omega][{{X, Y}, {X, Y}}]))}
