{{-(((-8*(-2 + M)*M*\[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*
       (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (3 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
       (2 + M + 6*\[Lambda][{y}] + 4*\[Omega][{{Y}, {X, X, Y}}] + 
        2*\[Omega][{{X, X}, {Y, Y}}]))/
      (E^((T*(M + 4*\[Omega][{{X}, {X, Y, Y}}]))/2)*(M - 2*\[Lambda][{y}])*
       (M + 4*\[Omega][{{X}, {X, Y, Y}}])*(3*\[Lambda][{y}] + 
        2*\[Omega][{{Y}, {X, X, Y}}])*(2 + M + 2*\[Lambda][{y}] + 
        4*\[Omega][{{Y}, {X, X, Y}}])*(\[Lambda][{y}] + 
        \[Omega][{{X, X}, {Y, Y}}])*(2 + M + 2*\[Omega][{{X, X}, {Y, Y}}])) - 
     (2*M^2*(-1 + \[Lambda][{y}])*\[Lambda][{y}]*
       (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (M + M^2 + 2*(1 + M)*\[Lambda][{y}] - 6*\[Lambda][{y}]^2 + 
        8*\[Omega][{{Y}, {X, X, Y}}]^2 + \[Omega][{{Y}, {X, X, Y}}]*
         (4 + 6*M - 8*\[Omega][{{X, X}, {Y, Y}}]) - 
        2*\[Omega][{{X, X}, {Y, Y}}] - 3*M*\[Omega][{{X, X}, {Y, Y}}] + 
        2*\[Omega][{{X, X}, {Y, Y}}]^2)*
       (-(\[Omega][{{X, X}, {Y, Y}}]*(2*\[Omega][{{X}, {X, Y, Y}}] + 
           \[Omega][{{X, X}, {Y, Y}}])) + 2*\[Lambda][{y}]*
         (2*\[Omega][{{X}, {X, Y, Y}}]^2 - \[Omega][{{X, X}, {Y, Y}}] + 
          \[Omega][{{X}, {X, Y, Y}}]*(1 + \[Omega][{{X, X}, {Y, Y}}]))))/
      (E^(T*(3*\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}]))*(M - 4*\[Lambda][{y}])*
       (1 + M - 3*\[Lambda][{y}])*(M + 4*\[Omega][{{Y}, {X, X, Y}}] - 
        2*\[Omega][{{X, X}, {Y, Y}}])*(1 + M - 2*\[Lambda][{y}] + 
        2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
       (3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
        \[Omega][{{X, X}, {Y, Y}}])*(2*\[Lambda][{y}] + 
        \[Omega][{{X, X}, {Y, Y}}])*(3*\[Lambda][{y}] + 
        2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])) + 
     (4*M^2*(-1 + \[Lambda][{y}])*\[Lambda][{y}]^2*\[Omega][{{X}, {X, Y, Y}}]*
       (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (3 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
       (8*\[Omega][{{Y}, {X, X, Y}}]^2*(M + \[Omega][{{X, X}, {Y, Y}}]) + 
        2*\[Lambda][{y}]^2*(6*M + 7*\[Omega][{{X, X}, {Y, Y}}]) + 
        M*(M*(1 + M) + (2 + 3*M)*\[Omega][{{X, X}, {Y, Y}}] + 
          2*\[Omega][{{X, X}, {Y, Y}}]^2) + 2*\[Omega][{{Y}, {X, X, Y}}]*
         (M*(2 + 3*M) + (2 + 5*M)*\[Omega][{{X, X}, {Y, Y}}] + 
          2*\[Omega][{{X, X}, {Y, Y}}]^2) + \[Lambda][{y}]*
         (4*M*(2 + 3*M) + (6 + 17*M)*\[Omega][{{X, X}, {Y, Y}}] + 
          6*\[Omega][{{X, X}, {Y, Y}}]^2 + 24*\[Omega][{{Y}, {X, X, Y}}]*
           (M + \[Omega][{{X, X}, {Y, Y}}]))))/
      (E^(T*(\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}]))*
       (M - 2*\[Lambda][{y}])*(\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}])*
       (1 + M + 2*\[Omega][{{Y}, {X, X, Y}}])*(5*\[Lambda][{y}] + 
        2*\[Omega][{{Y}, {X, X, Y}}])*(M + 4*\[Lambda][{y}] + 
        4*\[Omega][{{Y}, {X, X, Y}}])*(1 + M - \[Lambda][{y}] + 
        \[Omega][{{X, X}, {Y, Y}}])*(2*\[Lambda][{y}] + 
        \[Omega][{{X, X}, {Y, Y}}])*(M + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
     (M^2*(-1 + \[Lambda][{y}])*(2*\[Omega][{{Y}, {X, X, Y}}]*
         (\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
         (2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}]) + 
        \[Lambda][{y}]^2*(4*\[Omega][{{X}, {X, Y, Y}}]^3 + 
          2*\[Omega][{{X}, {X, Y, Y}}]^2*(4 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
            \[Omega][{{X, X}, {Y, Y}}]) + \[Omega][{{X}, {X, Y, Y}}]*
           (3 + 2*\[Omega][{{Y}, {X, X, Y}}]*(-2 + \[Omega][{{X, X}, 
                {Y, Y}}]) + \[Omega][{{X, X}, {Y, Y}}]) - 
          5*(\[Omega][{{Y}, {X, X, Y}}]*(-3 + \[Omega][{{X, X}, {Y, Y}}]) + 
            3*\[Omega][{{X, X}, {Y, Y}}])) - \[Lambda][{y}]*
         (4*\[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] + 
          \[Omega][{{X}, {X, Y, Y}}]*(4*\[Omega][{{Y}, {X, X, Y}}]^2 + 
            2*\[Omega][{{Y}, {X, X, Y}}]*(-2 + \[Omega][{{X, X}, {Y, Y}}]) + 
            5*\[Omega][{{X, X}, {Y, Y}}]) + \[Omega][{{Y}, {X, X, Y}}]*
           (2*\[Omega][{{Y}, {X, X, Y}}]*(-8 + \[Omega][{{X, X}, {Y, Y}}]) + 
            11*\[Omega][{{X, X}, {Y, Y}}]))))/
      (E^(2*T*(3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}]))*(M - 6*\[Lambda][{y}])*
       (1 + M - 5*\[Lambda][{y}])*(3*\[Lambda][{y}] + 
        \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (5*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
        \[Omega][{{X, X}, {Y, Y}}])) - 
     (M^2*\[Lambda][{y}]^3*(1 + 2*\[Omega][{{X}, {X, Y, Y}}])*
       (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (3 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
       (M^2 + M^3 + 108*\[Lambda][{y}]^3 + 128*\[Omega][{{X}, {X, Y, Y}}]^3 + 
        4*M*\[Omega][{{Y}, {X, X, Y}}] + 6*M^2*\[Omega][{{Y}, {X, X, Y}}] + 
        8*M*\[Omega][{{Y}, {X, X, Y}}]^2 + 2*M*\[Omega][{{X, X}, {Y, Y}}] + 
        3*M^2*\[Omega][{{X, X}, {Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}]*
         \[Omega][{{X, X}, {Y, Y}}] + 10*M*\[Omega][{{Y}, {X, X, Y}}]*
         \[Omega][{{X, X}, {Y, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]^2*
         \[Omega][{{X, X}, {Y, Y}}] + 2*M*\[Omega][{{X, X}, {Y, Y}}]^2 + 
        4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
        18*\[Lambda][{y}]^2*(2 + 5*M + 22*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 4*\[Omega][{{X, X}, {Y, Y}}]) + 
        4*\[Omega][{{X}, {X, Y, Y}}]^2*(10 + 21*M + 
          36*\[Omega][{{Y}, {X, X, Y}}] + 18*\[Omega][{{X, X}, {Y, Y}}]) + 
        2*\[Omega][{{X}, {X, Y, Y}}]*(2*M*(3 + 4*M) + 
          24*\[Omega][{{Y}, {X, X, Y}}]^2 + 3*(2 + 5*M)*
           \[Omega][{{X, X}, {Y, Y}}] + 6*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          6*\[Omega][{{Y}, {X, X, Y}}]*(2 + 5*M + 
            4*\[Omega][{{X, X}, {Y, Y}}])) + 2*\[Lambda][{y}]*
         (7*M + 10*M^2 + 212*\[Omega][{{X}, {X, Y, Y}}]^2 + 
          12*\[Omega][{{Y}, {X, X, Y}}]^2 + 6*\[Omega][{{X, X}, {Y, Y}}] + 
          16*M*\[Omega][{{X, X}, {Y, Y}}] + 6*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          3*\[Omega][{{Y}, {X, X, Y}}]*(2 + 9*M + 
            8*\[Omega][{{X, X}, {Y, Y}}]) + \[Omega][{{X}, {X, Y, Y}}]*
           (46 + 99*M + 144*\[Omega][{{Y}, {X, X, Y}}] + 
            76*\[Omega][{{X, X}, {Y, Y}}]))))/
      ((\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}])*
       (M + 4*\[Omega][{{X}, {X, Y, Y}}])*(3*\[Lambda][{y}] + 
        \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (1 + M + \[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}])*(M + 6*\[Lambda][{y}] + 
        4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
       (1 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
       (3*\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}])*(M + 2*\[Lambda][{y}] + 
        4*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
     (4*M*\[Lambda][{y}]^2*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}])*(2 + M + 2*\[Lambda][{y}] + 
        4*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
       (-4*\[Lambda][{y}]^2*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}])*(M + 2*\[Omega][{{X, X}, {Y, Y}}]) + 
        \[Omega][{{X, X}, {Y, Y}}]*(M^2 - 8*M*\[Omega][{{X}, {X, Y, Y}}]^2 - 
          (-10 + M)*M*\[Omega][{{X, X}, {Y, Y}}] - 2*(-8 + M)*
           \[Omega][{{X, X}, {Y, Y}}]^2 - 2*\[Omega][{{X}, {X, Y, Y}}]*
           ((-2 + M)*M + 4*(-4 + M)*\[Omega][{{X, X}, {Y, Y}}])) + 
        \[Lambda][{y}]*(4*\[Omega][{{X}, {X, Y, Y}}]^2*((-4 + M)*M + 
            4*(-2 + M)*\[Omega][{{X, X}, {Y, Y}}]) - 
          (-1 + \[Omega][{{X, X}, {Y, Y}}])*(M^2 + 
            6*M*\[Omega][{{X, X}, {Y, Y}}] + 8*\[Omega][{{X, X}, {Y, Y}}]^
              2) + 2*\[Omega][{{X}, {X, Y, Y}}]*(2*(-2 + M)*M + 
            (-8 + 2*M + M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
            4*(-4 + M)*\[Omega][{{X, X}, {Y, Y}}]^2))))/
      (E^((T*(M + 2*\[Lambda][{y}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
           2*\[Omega][{{X, X}, {Y, Y}}]))/2)*(M - 4*\[Lambda][{y}])*
       (2 + M - 2*\[Lambda][{y}])*(2 + M + 4*\[Omega][{{Y}, {X, X, Y}}] - 
        2*\[Omega][{{X, X}, {Y, Y}}])*(2*\[Lambda][{y}] + 
        2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
       (\[Lambda][{y}] + \[Omega][{{X, X}, {Y, Y}}])*
       (M + 2*\[Omega][{{X, X}, {Y, Y}}])*(M + 2*\[Lambda][{y}] + 
        4*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
     (2*\[Lambda][{y}]*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}])*
       ((2 + M)*(1 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}])*(2 + 3*M + M^2 + 
          (4 + 3*M)*\[Omega][{{X, X}, {Y, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]^
            2) - 3*(-2 + M)*\[Lambda][{y}]^3*
         (4*M*\[Omega][{{X}, {X, Y, Y}}]^2 - (1 + \[Omega][{{X, X}, {Y, Y}}])*
           (2 + M + 2*\[Omega][{{X, X}, {Y, Y}}]) + 
          2*\[Omega][{{X}, {X, Y, Y}}]*(2*(-1 + M) + (-2 + M)*
             \[Omega][{{X, X}, {Y, Y}}])) + \[Lambda][{y}]*
         (2 + M + 2*\[Omega][{{X, X}, {Y, Y}}])*(-10 - 15*M - 4*M^2 + M^3 + 
          4*M*\[Omega][{{X}, {X, Y, Y}}]^2 + (-18 - 11*M + 3*M^2)*
           \[Omega][{{X, X}, {Y, Y}}] + 2*(-4 + M)*\[Omega][{{X, X}, {Y, Y}}]^
            2 + 2*\[Omega][{{X}, {X, Y, Y}}]*(2*(-5 - 2*M + M^2) + 
            (-8 + 3*M)*\[Omega][{{X, X}, {Y, Y}}])) + \[Lambda][{y}]^2*
         (28 + 28*M - M^2 - 4*M^3 + (68 + 30*M - 16*M^2 - 3*M^3)*
           \[Omega][{{X, X}, {Y, Y}}] + (52 - 4*M - 9*M^2)*
           \[Omega][{{X, X}, {Y, Y}}]^2 - 6*(-2 + M)*
           \[Omega][{{X, X}, {Y, Y}}]^3 + 8*M*\[Omega][{{X}, {X, Y, Y}}]^2*
           (-4 - M + M^2 + (-3 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
          2*\[Omega][{{X}, {X, Y, Y}}]*(28 - 8*M - 11*M^2 + 3*M^3 + 
            (40 - 24*M - 5*M^2 + 2*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
            2*(6 - 6*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]^2))))/
      (E^(T*(1 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}]))*(1 + M - 3*\[Lambda][{y}])*
       (2 + M - 2*\[Lambda][{y}])*(1 + M - \[Lambda][{y}] + 
        \[Omega][{{X, X}, {Y, Y}}])*(1 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}])*(-\[Lambda][{y}] - 
        2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
       (2 + M + 2*\[Omega][{{X, X}, {Y, Y}}])) - 
     (4*M*\[Lambda][{y}]*(-6*\[Omega][{{Y}, {X, X, Y}}]*
         (M + 4*\[Omega][{{Y}, {X, X, Y}}])*
         (M + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
         (M + 4*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
         (2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}]) - 
        24*\[Lambda][{y}]^4*(4*M*\[Omega][{{X}, {X, Y, Y}}]^3 + 
          12*\[Omega][{{Y}, {X, X, Y}}]^2*(-3 + \[Omega][{{X, X}, {Y, Y}}]) + 
          2*\[Omega][{{X}, {X, Y, Y}}]^2*(10*M + 2*(6 + M)*
             \[Omega][{{Y}, {X, X, Y}}] + (-6 + M)*\[Omega][{{X, X}, {Y, 
                Y}}]) + 3*(-3*M + (6 + M)*\[Omega][{{X, X}, {Y, Y}}] - 
            6*\[Omega][{{X, X}, {Y, Y}}]^2) + \[Omega][{{Y}, {X, X, Y}}]*
           (-9*(4 + M) + (54 + M)*\[Omega][{{X, X}, {Y, Y}}] - 
            6*\[Omega][{{X, X}, {Y, Y}}]^2) + \[Omega][{{X}, {X, Y, Y}}]*
           (15*M + 24*\[Omega][{{Y}, {X, X, Y}}]^2 + (-18 + 7*M)*
             \[Omega][{{X, X}, {Y, Y}}] - 6*\[Omega][{{X, X}, {Y, Y}}]^2 + 
            2*\[Omega][{{Y}, {X, X, Y}}]*(18 + 4*M + M*\[Omega][
                {{X, X}, {Y, Y}}]))) + 2*\[Lambda][{y}]^3*
         (-48*\[Omega][{{Y}, {X, X, Y}}]^3*(-27 + 
            5*\[Omega][{{X, X}, {Y, Y}}]) - 8*\[Omega][{{Y}, {X, X, Y}}]^2*
           (-3*(42 + 19*M) + (171 + 10*M)*\[Omega][{{X, X}, {Y, Y}}] - 
            15*\[Omega][{{X, X}, {Y, Y}}]^2) + \[Omega][{{Y}, {X, X, Y}}]*
           (3*M*(96 + 11*M) - 3*(24 + 110*M + 3*M^2)*\[Omega][
              {{X, X}, {Y, Y}}] + 30*(12 + M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
          9*(M^2 + (2 - 3*M)*M*\[Omega][{{X, X}, {Y, Y}}] + 
            2*(-12 + 5*M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
          4*\[Omega][{{X}, {X, Y, Y}}]^3*(4*(-12 + M)*\[Omega][
              {{Y}, {X, X, Y}}] + 3*(M^2 - 2*(-4 + M)*\[Omega][{{X, X}, 
                 {Y, Y}}])) + \[Omega][{{X}, {X, Y, Y}}]*
           (-480*\[Omega][{{Y}, {X, X, Y}}]^3 + 8*\[Omega][{{Y}, {X, X, Y}}]^
              2*(18 - 17*M + (-12 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
            3*(M*(48 + 11*M) + 3*(-16 - 2*M + M^2)*\[Omega][{{X, X}, 
                 {Y, Y}}] - 2*(-24 + M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
            2*\[Omega][{{Y}, {X, X, Y}}]*(72*(2 + M) + (-180 - 16*M + 3*M^2)*
               \[Omega][{{X, X}, {Y, Y}}] - 6*(-14 + M)*\[Omega][
                 {{X, X}, {Y, Y}}]^2)) + 2*\[Omega][{{X}, {X, Y, Y}}]^2*
           (8*(-42 + M)*\[Omega][{{Y}, {X, X, Y}}]^2 + 
            2*\[Omega][{{Y}, {X, X, Y}}]*(-72 - 22*M + 3*M^2 - 
              4*(-15 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
            3*(2*M*(8 + 3*M) + (24 - 8*M + M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
              2*(-4 + M)*\[Omega][{{X, X}, {Y, Y}}]^2))) + 
        \[Lambda][{y}]^2*(-192*\[Omega][{{Y}, {X, X, Y}}]^4*
           (-13 + \[Omega][{{X, X}, {Y, Y}}]) - 16*\[Omega][{{Y}, {X, X, Y}}]^
            3*(-24 - 71*M + 3*(38 + 3*M)*\[Omega][{{X, X}, {Y, Y}}] - 
            6*\[Omega][{{X, X}, {Y, Y}}]^2) - 
          9*M*(M^2 + (-8 + M)*M*\[Omega][{{X, X}, {Y, Y}}] - 
            2*(-10 + M)*\[Omega][{{X, X}, {Y, Y}}]^2) - 
          4*\[Omega][{{Y}, {X, X, Y}}]^2*((36 - 35*M)*M + 
            3*(-152 + 64*M + 3*M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
            8*(9 + 2*M)*\[Omega][{{X, X}, {Y, Y}}]^2) - 
          3*\[Omega][{{Y}, {X, X, Y}}]*(-((-32 + M)*M^2) + 
            M*(-248 + 42*M + M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
            2*(-168 + 32*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
          4*\[Omega][{{X}, {X, Y, Y}}]^3*(32*(-3 + M)*
             \[Omega][{{Y}, {X, X, Y}}]^2 + (-8 + M)*M*
             (M - 2*\[Omega][{{X, X}, {Y, Y}}]) + 
            4*\[Omega][{{Y}, {X, X, Y}}]*(M*(-10 + 3*M) - 4*(-3 + M)*\[Omega][
                {{X, X}, {Y, Y}}])) + 2*\[Omega][{{X}, {X, Y, Y}}]^2*
           (64*(-6 + M)*\[Omega][{{Y}, {X, X, Y}}]^3 + 
            8*\[Omega][{{Y}, {X, X, Y}}]^2*(12 + 8*M + 3*M^2 + 
              12*\[Omega][{{X, X}, {Y, Y}}]) + M*(6*(-8 + M)*M + 
              (72 - 20*M + M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
              2*(-8 + M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
            2*\[Omega][{{Y}, {X, X, Y}}]*(M*(-28 + 28*M + M^2) + 
              4*(-6 - 11*M + M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
              8*(-3 + M)*\[Omega][{{X, X}, {Y, Y}}]^2)) + 
          \[Omega][{{X}, {X, Y, Y}}]*(-384*\[Omega][{{Y}, {X, X, Y}}]^4 + 
            32*\[Omega][{{Y}, {X, X, Y}}]^3*(84 - 3*M + 2*(-3 + M)*\[Omega][
                {{X, X}, {Y, Y}}]) + 8*\[Omega][{{Y}, {X, X, Y}}]^2*
             (8*(15 + 19*M) + 3*(-80 + M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
              4*(-6 + M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
            2*\[Omega][{{Y}, {X, X, Y}}]*(2*M*(12 + 35*M) + (48 - 264*M + 
                10*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}] - 2*(-72 + M^2)*
               \[Omega][{{X, X}, {Y, Y}}]^2) + 3*((-24 + M)*M^2 + 
              M*(48 - 22*M + M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
              2*(48 - 16*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]^2))) - 
        \[Lambda][{y}]*(-768*\[Omega][{{Y}, {X, X, Y}}]^5 - 
          9*M*(M + 4*\[Omega][{{X}, {X, Y, Y}}])*
           (M - 2*\[Omega][{{X, X}, {Y, Y}}])*\[Omega][{{X, X}, {Y, Y}}] + 
          32*\[Omega][{{Y}, {X, X, Y}}]^4*(48 - 13*M + 2*(-24 + M)*
             \[Omega][{{X}, {X, Y, Y}}] + (12 + M)*\[Omega][{{X, X}, {Y, 
                Y}}]) + 16*\[Omega][{{Y}, {X, X, Y}}]^3*(-4*(-18 + M)*M + 
            8*(-6 + M)*\[Omega][{{X}, {X, Y, Y}}]^2 + (-144 + 15*M + M^2)*
             \[Omega][{{X, X}, {Y, Y}}] - M*\[Omega][{{X, X}, {Y, Y}}]^2 + 
            2*\[Omega][{{X}, {X, Y, Y}}]*(12 - 14*M + M^2 + (24 + M)*\[Omega][
                {{X, X}, {Y, Y}}])) + 2*\[Omega][{{Y}, {X, X, Y}}]^2*
           (-((-144 + M)*M^2) + 32*M*\[Omega][{{X}, {X, Y, Y}}]^3 + 
            M*(-600 + 30*M + M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
            2*(-192 + 12*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
            8*\[Omega][{{X}, {X, Y, Y}}]^2*(M*(-2 + 3*M) - 2*(-12 + M)*
               \[Omega][{{X, X}, {Y, Y}}]) + 2*\[Omega][{{X}, {X, Y, Y}}]*
             (M*(120 + 6*M + M^2) + 4*(-72 + 12*M + M^2)*\[Omega][
                {{X, X}, {Y, Y}}] - 8*M*\[Omega][{{X, X}, {Y, Y}}]^2)) + 
          2*\[Omega][{{Y}, {X, X, Y}}]*(8*M*\[Omega][{{X}, {X, Y, Y}}]^3*
             (M - 2*\[Omega][{{X, X}, {Y, Y}}]) + 
            2*M*\[Omega][{{X}, {X, Y, Y}}]^2*(M*(10 + M) - 12*\[Omega][
                {{X, X}, {Y, Y}}] - 4*\[Omega][{{X, X}, {Y, Y}}]^2) + 
            3*M*(4*M^2 + (-32 + M)*M*\[Omega][{{X, X}, {Y, Y}}] - 
              2*(-22 + M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
            \[Omega][{{X}, {X, Y, Y}}]*(M^2*(48 + 5*M) + M*(-240 + 6*M + M^2)*
               \[Omega][{{X, X}, {Y, Y}}] - 2*(-120 + 12*M + M^2)*
               \[Omega][{{X, X}, {Y, Y}}]^2)))))/
      (E^((T*(M + 6*\[Lambda][{y}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
           4*\[Omega][{{Y}, {X, X, Y}}]))/2)*(M - 6*\[Lambda][{y}])*
       (2 + M - 4*\[Lambda][{y}])*(3*\[Lambda][{y}] + 
        2*\[Omega][{{Y}, {X, X, Y}}])*(M + 4*\[Lambda][{y}] + 
        4*\[Omega][{{Y}, {X, X, Y}}])*(M + 6*\[Lambda][{y}] + 
        4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
       (M + 4*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
       (2*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
        \[Omega][{{X, X}, {Y, Y}}])) + 
     (-((2 + M)*(1 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          2*\[Omega][{{Y}, {X, X, Y}}])*(2 + 3*M + M^2 + 
          (8 + 6*M)*\[Omega][{{Y}, {X, X, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]^
            2)*(2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
         (2 + 3*M + M^2 + 8*\[Omega][{{Y}, {X, X, Y}}]^2 + 
          \[Omega][{{Y}, {X, X, Y}}]*(8 + 6*M - 
            8*\[Omega][{{X, X}, {Y, Y}}]) - (4 + 3*M)*
           \[Omega][{{X, X}, {Y, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]^2)) - 
       80*\[Lambda][{y}]^6*(1 + M + 2*\[Omega][{{Y}, {X, X, Y}}])*
        (10 + 8*M + 2*M*\[Omega][{{X}, {X, Y, Y}}]^2 + 
         8*\[Omega][{{Y}, {X, X, Y}}]^2 - 10*\[Omega][{{X, X}, {Y, Y}}] + 
         3*M*\[Omega][{{X, X}, {Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}]*
          (3*(8 + M) + (-4 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
         \[Omega][{{X}, {X, Y, Y}}]*(4 + 9*M + 2*(4 + M)*
            \[Omega][{{Y}, {X, X, Y}}] + (-4 + M)*\[Omega][
             {{X, X}, {Y, Y}}])) + \[Lambda][{y}]*
        (1 + M + 2*\[Omega][{{Y}, {X, X, Y}}])*
        (2 + M + 4*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
        (512*\[Omega][{{Y}, {X, X, Y}}]^4 + 4*(2 + M)*
          \[Omega][{{X}, {X, Y, Y}}]^2*(2 + M + 4*\[Omega][{{Y}, {X, X, Y}}])*
          (1 + M + 2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][
            {{X, X}, {Y, Y}}]) + 16*\[Omega][{{Y}, {X, X, Y}}]^3*
          (58 + 45*M + (-30 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
         4*\[Omega][{{Y}, {X, X, Y}}]^2*(156 + 238*M + 88*M^2 + 
           (-160 - 118*M + 5*M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
           2*(-14 + M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
         2*\[Omega][{{Y}, {X, X, Y}}]*(92 + 208*M + 151*M^2 + 35*M^3 + 
           (-148 - 220*M - 73*M^2 + 4*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
           (48 + 34*M - 3*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
         (2 + M)*(5*(1 + M)^2*(2 + M) + (-24 - 41*M - 16*M^2 + M^3)*
            \[Omega][{{X, X}, {Y, Y}}] + (10 + 10*M - M^2)*
            \[Omega][{{X, X}, {Y, Y}}]^2) + 2*\[Omega][{{X}, {X, Y, Y}}]*
          (16*(18 + M)*\[Omega][{{Y}, {X, X, Y}}]^3 + 
           4*\[Omega][{{Y}, {X, X, Y}}]^2*(100 + 76*M + 5*M^2 - 
             64*\[Omega][{{X, X}, {Y, Y}}]) + 2*\[Omega][{{Y}, {X, X, Y}}]*
            (88 + 126*M + 49*M^2 + 4*M^3 - 2*(58 + 37*M)*\[Omega][{{X, X}, 
                {Y, Y}}] - 2*(-14 + M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
           (2 + M)*(12 + 20*M + 9*M^2 + M^3 - 7*(4 + 3*M)*\[Omega][{{X, X}, 
                {Y, Y}}] - (-12 + M)*\[Omega][{{X, X}, {Y, Y}}]^2))) + 
       4*\[Lambda][{y}]^5*(4*M*\[Omega][{{X}, {X, Y, Y}}]^3*
          (-20 - 3*M + 11*M^2 + (-40 + 34*M)*\[Omega][{{Y}, {X, X, Y}}] - 
           7*M*\[Omega][{{X, X}, {Y, Y}}]) + 2*\[Omega][{{X}, {X, Y, Y}}]^2*
          (-80 - 246*M - 9*M^2 + 103*M^3 + 4*(-80 - 10*M + 17*M^2)*
            \[Omega][{{Y}, {X, X, Y}}]^2 + (80 + 30*M - 96*M^2 + 11*M^3)*
            \[Omega][{{X, X}, {Y, Y}}] - 7*M^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           2*\[Omega][{{Y}, {X, X, Y}}]*(-160 - 256*M + 164*M^2 + 11*M^3 + 
             10*(8 - 5*M + M^2)*\[Omega][{{X, X}, {Y, Y}}])) + 
         (1 + M + 2*\[Omega][{{Y}, {X, X, Y}}])*(-160 - 8*M + 57*M^2 - 
           480*\[Omega][{{Y}, {X, X, Y}}]^3 + 4*(5 - 7*M + 3*M^2)*
            \[Omega][{{X, X}, {Y, Y}}] + (140 - 90*M)*
            \[Omega][{{X, X}, {Y, Y}}]^2 + 4*\[Omega][{{Y}, {X, X, Y}}]^2*
            (-352 - 27*M + 5*(8 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
           2*\[Omega][{{Y}, {X, X, Y}}]*(-452 - 81*M + 6*M^2 + 
             2*(66 + 17*M + M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
             5*(-4 + 3*M)*\[Omega][{{X, X}, {Y, Y}}]^2)) + 
         2*\[Omega][{{X}, {X, Y, Y}}]*(-232 - 361*M + 42*M^2 + 117*M^3 + 
           40*(-20 + M)*\[Omega][{{Y}, {X, X, Y}}]^3 + 
           (212 + 66*M - 156*M^2 + 35*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
           (20 + 5*M - 36*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           2*\[Omega][{{Y}, {X, X, Y}}]^2*(-864 - 230*M + 65*M^2 + 
             (160 - 40*M + 17*M^2)*\[Omega][{{X, X}, {Y, Y}}]) + 
           \[Omega][{{Y}, {X, X, Y}}]*(-1128 - 962*M + 357*M^2 + 37*M^3 + 
             (584 - 172*M + 62*M^2 + 11*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
             (40 - 30*M - 7*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2))) + 
       \[Lambda][{y}]^2*(2 + M + 4*\[Omega][{{Y}, {X, X, Y}}] - 
         2*\[Omega][{{X, X}, {Y, Y}}])*(8*M*\[Omega][{{X}, {X, Y, Y}}]^3*
          (2 + M + 4*\[Omega][{{Y}, {X, X, Y}}])*(1 + M + 
           2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}]) + 
         4*\[Omega][{{X}, {X, Y, Y}}]^2*(16*(-14 + 3*M)*
            \[Omega][{{Y}, {X, X, Y}}]^3 + 4*\[Omega][{{Y}, {X, X, Y}}]^2*
            (-88 - 34*M + 13*M^2 - 4*(-7 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
           2*\[Omega][{{Y}, {X, X, Y}}]*(-92 - 90*M - 2*M^2 + 9*M^3 - 
             4*(-13 - 4*M + M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
             2*M*\[Omega][{{X, X}, {Y, Y}}]^2) + (2 + M)*(-16 - 17*M + M^2 + 
             2*M^3 + (12 + 2*M - M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
             M*\[Omega][{{X, X}, {Y, Y}}]^2)) + 2*\[Omega][{{X}, {X, Y, Y}}]*
          (-188 - 382*M - 234*M^2 - 33*M^3 + 8*M^4 + M^5 + 
           96*(-16 + M)*\[Omega][{{Y}, {X, X, Y}}]^4 + 
           (200 + 294*M + 135*M^2 + 23*M^3)*\[Omega][{{X, X}, {Y, Y}}] - 
           (12 + 48*M + 31*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           8*\[Omega][{{Y}, {X, X, Y}}]^3*(-488 - 206*M + 17*M^2 - 
             2*(-68 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
           4*\[Omega][{{Y}, {X, X, Y}}]^2*(-868 - 820*M - 125*M^2 + 17*M^3 - 
             3*(-156 - 80*M + M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
             4*(10 + M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
           2*\[Omega][{{Y}, {X, X, Y}}]*(-664 - 982*M - 372*M^2 - 7*M^3 + 
             7*M^4 + (532 + 532*M + 131*M^2 - M^3)*\[Omega][{{X, X}, 
                {Y, Y}}] - 4*(13 + 20*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]^
               2)) + (1 + M + 2*\[Omega][{{Y}, {X, X, Y}}])*
          (5*(-28 - 54*M - 30*M^2 - 3*M^3 + M^4) + 32*(-34 + M)*
            \[Omega][{{Y}, {X, X, Y}}]^4 + (128 + 198*M + 103*M^2 + 22*M^3)*
            \[Omega][{{X, X}, {Y, Y}}] + (12 - 36*M - 29*M^2)*
            \[Omega][{{X, X}, {Y, Y}}]^2 + 8*\[Omega][{{Y}, {X, X, Y}}]^3*
            (-344 - 144*M + 5*M^2 + 108*\[Omega][{{X, X}, {Y, Y}}]) + 
           8*\[Omega][{{Y}, {X, X, Y}}]^2*(-306 - 276*M - 43*M^2 + 2*M^3 + 
             (166 + 99*M)*\[Omega][{{X, X}, {Y, Y}}] - (20 + M)*
              \[Omega][{{X, X}, {Y, Y}}]^2) + 2*\[Omega][{{Y}, {X, X, Y}}]*
            (-476 - 668*M - 231*M^2 - 5*M^3 + M^4 + (356 + 388*M + 115*M^2)*
              \[Omega][{{X, X}, {Y, Y}}] - (24 + 78*M + M^2)*
              \[Omega][{{X, X}, {Y, Y}}]^2))) - 2*\[Lambda][{y}]^4*
        (4*M*\[Omega][{{X}, {X, Y, Y}}]^3*(-68 - 50*M + 15*M^2 + 13*M^3 + 
           8*(-30 + 11*M)*\[Omega][{{Y}, {X, X, Y}}]^2 + (60 + 12*M - 23*M^2)*
            \[Omega][{{X, X}, {Y, Y}}] + 6*M*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           2*\[Omega][{{Y}, {X, X, Y}}]*(-128 - 20*M + 35*M^2 + 
             (60 - 28*M)*\[Omega][{{X, X}, {Y, Y}}])) + 
         2*\[Omega][{{X}, {X, Y, Y}}]^2*(-32 - 712*M - 550*M^2 + 95*M^3 + 
           109*M^4 + 16*(40 - 50*M + 11*M^2)*\[Omega][{{Y}, {X, X, Y}}]^3 + 
           (112 + 584*M + 62*M^2 - 188*M^3 + 13*M^4)*\[Omega][
             {{X, X}, {Y, Y}}] + (-80 + 86*M^2 - 23*M^3)*
            \[Omega][{{X, X}, {Y, Y}}]^2 + 6*M^2*\[Omega][{{X, X}, {Y, Y}}]^
             3 + 4*\[Omega][{{Y}, {X, X, Y}}]^2*(128 - 696*M + 106*M^2 + 
             35*M^3 + 2*(10 - 3*M)*M*\[Omega][{{X, X}, {Y, Y}}]) + 
           2*\[Omega][{{Y}, {X, X, Y}}]*(16 - 1308*M - 362*M^2 + 290*M^3 + 
             13*M^4 + 4*(28 + 123*M - 59*M^2 + 3*M^3)*\[Omega][{{X, X}, 
                {Y, Y}}] + (-80 + 80*M - 22*M^2)*\[Omega][{{X, X}, {Y, Y}}]^
               2)) - (1 + M + 2*\[Omega][{{Y}, {X, X, Y}}])*
          (380 + 772*M + 437*M^2 + 70*M^3 + (-704 - 634*M - 37*M^2 + 24*M^3)*
            \[Omega][{{X, X}, {Y, Y}}] + (284 + 170*M - 12*M^2)*
            \[Omega][{{X, X}, {Y, Y}}]^2 + (40 - 60*M)*
            \[Omega][{{X, X}, {Y, Y}}]^3 + 16*\[Omega][{{Y}, {X, X, Y}}]^3*
            (102 + 37*M + 10*(-4 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
           4*\[Omega][{{Y}, {X, X, Y}}]^2*(764 + 798*M + 65*M^2 + 
             4*(-204 + 13*M + 4*M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
             10*(-8 + M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
           2*\[Omega][{{Y}, {X, X, Y}}]*(940 + 1484*M + 490*M^2 + 13*M^3 + 
             (-1416 - 612*M + 89*M^2 + 4*M^3)*\[Omega][{{X, X}, {Y, Y}}] - 
             2*(-246 + 16*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 - 
             10*M*\[Omega][{{X, X}, {Y, Y}}]^3)) + 
         2*\[Omega][{{X}, {X, Y, Y}}]*(-156 - 1114*M - 975*M^2 - 81*M^3 + 
           80*M^4 - 320*(-2 + M)*\[Omega][{{Y}, {X, X, Y}}]^4 + 
           (488 + 960*M + 113*M^2 - 147*M^3 + 35*M^4)*
            \[Omega][{{X, X}, {Y, Y}}] - (332 + 120*M - 124*M^2 + 67*M^3)*
            \[Omega][{{X, X}, {Y, Y}}]^2 + 2*M*(5 + 14*M)*
            \[Omega][{{X, X}, {Y, Y}}]^3 + 8*\[Omega][{{Y}, {X, X, Y}}]^3*
            (100 - 298*M - 3*M^2 + (80 - 40*M + 11*M^2)*\[Omega][{{X, X}, 
                {Y, Y}}]) + 2*\[Omega][{{Y}, {X, X, Y}}]^2*(-152 - 2852*M - 
             506*M^2 + 65*M^3 + (1024 + 88*M - 20*M^2 + 35*M^3)*
              \[Omega][{{X, X}, {Y, Y}}] - 4*(60 - 25*M + 7*M^2)*
              \[Omega][{{X, X}, {Y, Y}}]^2) + \[Omega][{{Y}, {X, X, Y}}]*
            (-584 - 4524*M - 2274*M^2 + 239*M^3 + 31*M^4 + 
             (1840 + 1816*M - 372*M^2 + 120*M^3 + 13*M^4)*\[Omega][{{X, X}, 
                {Y, Y}}] - (904 - 284*M + 94*M^2 + 23*M^3)*
              \[Omega][{{X, X}, {Y, Y}}]^2 + 2*M*(10 + 3*M)*
              \[Omega][{{X, X}, {Y, Y}}]^3))) + \[Lambda][{y}]^3*
        (16*M*\[Omega][{{X}, {X, Y, Y}}]^3*(-16 - 22*M - 7*M^2 + 2*M^3 + 
           M^4 + 16*(-5 + M)*\[Omega][{{Y}, {X, X, Y}}]^3 + 
           (26 + 24*M + M^2 - 3*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
           2*(-5 - 2*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           4*\[Omega][{{Y}, {X, X, Y}}]^2*(-36 - 13*M + 5*M^2 - 
             4*(-5 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
           2*\[Omega][{{Y}, {X, X, Y}}]*(-42 - 37*M - M^2 + 4*M^3 + 
             (46 + 17*M - 7*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
             2*(-5 + M)*\[Omega][{{X, X}, {Y, Y}}]^2)) + 
         4*\[Omega][{{X}, {X, Y, Y}}]^2*(120 - 76*M - 310*M^2 - 149*M^3 + 
           16*M^4 + 15*M^5 + 64*(20 - 10*M + M^2)*\[Omega][{{Y}, {X, X, Y}}]^
             4 + (-144 + 204*M + 304*M^2 + 21*M^3 - 37*M^4 + 2*M^5)*
            \[Omega][{{X, X}, {Y, Y}}] + (24 - 76*M - 6*M^2 + 28*M^3 - 6*M^4)*
            \[Omega][{{X, X}, {Y, Y}}]^2 + 4*M*(-5 - 2*M + M^2)*
            \[Omega][{{X, X}, {Y, Y}}]^3 + 16*\[Omega][{{Y}, {X, X, Y}}]^3*
            (188 - 72*M - 23*M^2 + 5*M^3 - 2*(40 - 15*M + M^2)*
              \[Omega][{{X, X}, {Y, Y}}]) + 4*\[Omega][{{Y}, {X, X, Y}}]^2*
            (632 - 220*M - 330*M^2 + 33*M^3 + 8*M^4 + 
             (-480 + 184*M + 22*M^2 - 4*M^3)*\[Omega][{{X, X}, {Y, Y}}] - 
             4*(-20 + M^2)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
           2*\[Omega][{{Y}, {X, X, Y}}]*(2*(228 - 96*M - 291*M^2 - 51*M^3 + 
               27*M^4 + M^5) + (-464 + 312*M + 254*M^2 - 67*M^3 + 2*M^4)*
              \[Omega][{{X, X}, {Y, Y}}] - 2*(-52 + 10*M - 16*M^2 + 5*M^3)*
              \[Omega][{{X, X}, {Y, Y}}]^2 + 4*(-5 + M)*M*\[Omega][
                {{X, X}, {Y, Y}}]^3)) - 
         (1 + M + 2*\[Omega][{{Y}, {X, X, Y}}])*(-280 - 116*M + 330*M^2 + 
           273*M^3 + 57*M^4 - 2560*\[Omega][{{Y}, {X, X, Y}}]^5 + 
           (8 - 76*M + 6*M^2 + 69*M^3 + 18*M^4)*\[Omega][{{X, X}, {Y, Y}}] + 
           (376 + 92*M - 232*M^2 - 78*M^3)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           4*(-26 - 7*M + 15*M^2)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
           320*\[Omega][{{Y}, {X, X, Y}}]^4*(-36 - 7*M + 
             (8 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
           16*\[Omega][{{Y}, {X, X, Y}}]^3*(-944 - 464*M - 29*M^2 + 
             18*(28 + 11*M + M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
             20*(2 + M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
           4*\[Omega][{{Y}, {X, X, Y}}]^2*(4*(-558 - 369*M - 21*M^2 + 2*
                M^3) + (1480 + 1348*M + 366*M^2 + 19*M^3)*\[Omega][{{X, X}, 
                {Y, Y}}] - 2*(64 + 196*M + 23*M^2)*\[Omega][{{X, X}, {Y, Y}}]^
               2 + 20*M*\[Omega][{{X, X}, {Y, Y}}]^3) + 
           2*\[Omega][{{Y}, {X, X, Y}}]*(-1264 - 812*M + 324*M^2 + 215*M^3 + 
             6*M^4 + (656 + 860*M + 538*M^2 + 139*M^3 + 3*M^4)*
              \[Omega][{{X, X}, {Y, Y}}] - (-320 + 472*M + 338*M^2 + 13*M^3)*
              \[Omega][{{X, X}, {Y, Y}}]^2 + 2*(-80 + 68*M + 5*M^2)*
              \[Omega][{{X, X}, {Y, Y}}]^3)) + 2*\[Omega][{{X}, {X, Y, Y}}]*
          (656 + 512*M - 512*M^2 - 554*M^3 - 102*M^4 + 12*M^5 - 
           640*(-8 + M)*\[Omega][{{Y}, {X, X, Y}}]^5 - 
           720*\[Omega][{{X, X}, {Y, Y}}] - 384*M*\[Omega][
             {{X, X}, {Y, Y}}] + 224*M^2*\[Omega][{{X, X}, {Y, Y}}] + 
           37*M^3*\[Omega][{{X, X}, {Y, Y}}] - 
           34*M^4*\[Omega][{{X, X}, {Y, Y}}] + 
           9*M^5*\[Omega][{{X, X}, {Y, Y}}] - 16*\[Omega][{{X, X}, {Y, Y}}]^
             2 + 72*M*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           282*M^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           99*M^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
           23*M^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           80*\[Omega][{{X, X}, {Y, Y}}]^3 - 56*M*\[Omega][{{X, X}, {Y, Y}}]^
             3 - 74*M^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
           14*M^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
           64*\[Omega][{{Y}, {X, X, Y}}]^4*(314 + 37*M - 11*M^2 + 
             (-80 + M^2)*\[Omega][{{X, X}, {Y, Y}}]) + 
           8*\[Omega][{{Y}, {X, X, Y}}]^3*(3416 + 1292*M - 166*M^2 - 25*M^3 + 
             2*(-944 - 242*M + 2*M^2 + 5*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
             (160 + 60*M - 8*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
           8*\[Omega][{{Y}, {X, X, Y}}]^2*(2180 + 1250*M - 219*M^2 - 88*M^3 + 
             M^4 + 2*(-872 - 470*M - 61*M^2 + 5*M^3 + 2*M^4)*
              \[Omega][{{X, X}, {Y, Y}}] + (276 + 256*M + 27*M^2 - 7*M^3)*
              \[Omega][{{X, X}, {Y, Y}}]^2 + 2*(-10 + M)*M*
              \[Omega][{{X, X}, {Y, Y}}]^3) + 2*\[Omega][{{Y}, {X, X, Y}}]*
            (2696 + 1888*M - 852*M^2 - 627*M^3 - 10*M^4 + 3*M^5 + 
             (-2640 - 1732*M - 250*M^2 - 95*M^3 + 25*M^4 + 2*M^5)*
              \[Omega][{{X, X}, {Y, Y}}] + (376 + 772*M + 398*M^2 - 21*M^3 - 
               6*M^4)*\[Omega][{{X, X}, {Y, Y}}]^2 + 2*(40 - 88*M - 7*M^2 + 2*
                M^3)*\[Omega][{{X, X}, {Y, Y}}]^3))))/
      (E^(T*(1 + M + \[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          2*\[Omega][{{Y}, {X, X, Y}}]))*(1 + M - 5*\[Lambda][{y}])*
       (2 + M - 4*\[Lambda][{y}])*(1 + M + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (1 + M + \[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}])*(2 + M + 2*\[Lambda][{y}] + 
        4*\[Omega][{{Y}, {X, X, Y}}])*(2 + M + 4*\[Omega][{{Y}, {X, X, Y}}] - 
        2*\[Omega][{{X, X}, {Y, Y}}])*(1 + M - 2*\[Lambda][{y}] + 
        2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
       (\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
        \[Omega][{{X, X}, {Y, Y}}])))/((1 + 2*\[Omega][{{X}, {X, Y, Y}}])*
     (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
     (3 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))), 
  ((2*M*(-1 + \[Lambda][{y}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{Y}, {X, X, Y}}])*(-4*M*\[Lambda][{y}]^2 + 
       \[Lambda][{y}]*(M*(3 + M) + 12*\[Omega][{{X}, {X, Y, Y}}] - 
         6*\[Omega][{{X, X}, {Y, Y}}]) + (M + 4*\[Omega][{{X}, {X, Y, Y}}] - 
         2*\[Omega][{{X, X}, {Y, Y}}])*(2*\[Omega][{{X}, {X, Y, Y}}] - 
         \[Omega][{{X, X}, {Y, Y}}]))*
      (-(\[Omega][{{X, X}, {Y, Y}}]*(2*\[Omega][{{Y}, {X, X, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}])) + 2*\[Lambda][{y}]*
        (2*\[Omega][{{Y}, {X, X, Y}}]^2 - \[Omega][{{X, X}, {Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}]*(1 + \[Omega][{{X, X}, {Y, Y}}]))))/
     (E^(T*(3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
         \[Omega][{{X, X}, {Y, Y}}]))*(M - 4*\[Lambda][{y}])*
      (M + 4*\[Omega][{{X}, {X, Y, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
      (1 + M - 2*\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] - 
       \[Omega][{{X, X}, {Y, Y}}])*(3*\[Lambda][{y}] + 
       2*\[Omega][{{X}, {X, Y, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
      (2*\[Lambda][{y}] + \[Omega][{{X, X}, {Y, Y}}])*
      (3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
       \[Omega][{{X, X}, {Y, Y}}])) + 
    (8*\[Lambda][{y}]*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{Y}, {X, X, Y}}])*(M^2 + 8*M*\[Omega][{{Y}, {X, X, Y}}]^2 - 
       2*M*\[Omega][{{X, X}, {Y, Y}}] + M^2*\[Omega][{{X, X}, {Y, Y}}] - 
       8*\[Omega][{{X, X}, {Y, Y}}]^2 + 2*M*\[Omega][{{X, X}, {Y, Y}}]^2 + 
       2*\[Omega][{{Y}, {X, X, Y}}]*(M*(2 + M) + 4*(-2 + M)*
          \[Omega][{{X, X}, {Y, Y}}]) - 4*\[Lambda][{y}]*
        (M + 2*M*\[Omega][{{Y}, {X, X, Y}}]^2 + 
         2*\[Omega][{{X, X}, {Y, Y}}] + M*\[Omega][{{Y}, {X, X, Y}}]*
          (3 + \[Omega][{{X, X}, {Y, Y}}]))))/
     (E^((T*(M + 2*\[Lambda][{y}] + 4*\[Omega][{{Y}, {X, X, Y}}] + 
          2*\[Omega][{{X, X}, {Y, Y}}]))/2)*(M - 4*\[Lambda][{y}])*
      (2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
      (M + 2*\[Omega][{{X, X}, {Y, Y}}])*(M + 2*\[Lambda][{y}] + 
       4*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}])) - 
    (4*M*(-1 + \[Lambda][{y}])*\[Lambda][{y}]*\[Omega][{{Y}, {X, X, Y}}]*
      (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
      (3 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
      (20*\[Lambda][{y}]^2 + 2*\[Omega][{{X}, {X, Y, Y}}]*
        (M + 4*\[Omega][{{X}, {X, Y, Y}}]) + \[Lambda][{y}]*
        (28*\[Omega][{{X}, {X, Y, Y}}] + 
         M*(5 + M + 2*\[Omega][{{X, X}, {Y, Y}}]))))/
     (E^(T*(\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}]))*
      (1 + M + 2*\[Omega][{{X}, {X, Y, Y}}])*(5*\[Lambda][{y}] + 
       2*\[Omega][{{X}, {X, Y, Y}}])*(M + 4*\[Lambda][{y}] + 
       4*\[Omega][{{X}, {X, Y, Y}}])*(\[Lambda][{y}] + 
       2*\[Omega][{{Y}, {X, X, Y}}])*(2*\[Lambda][{y}] + 
       \[Omega][{{X, X}, {Y, Y}}])*(M + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
    (M*\[Lambda][{y}]^2*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{Y}, {X, X, Y}}])*(1 + 2*\[Omega][{{Y}, {X, X, Y}}])*
      (3 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
      (2*(18 + M)*\[Lambda][{y}]^2 + 2*(\[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}])*(M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}]) + \[Lambda][{y}]*
        (36*\[Omega][{{X}, {X, Y, Y}}] + 4*(9 + M)*
          \[Omega][{{Y}, {X, X, Y}}] + 
         M*(6 + M + 2*\[Omega][{{X, X}, {Y, Y}}]))))/
     ((3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{Y}, {X, X, Y}}])*(\[Lambda][{y}] + 
       2*\[Omega][{{Y}, {X, X, Y}}])*(1 + M + \[Lambda][{y}] + 
       2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
      (M + 6*\[Lambda][{y}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
       4*\[Omega][{{Y}, {X, X, Y}}])*(3*\[Lambda][{y}] + 
       2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
      (M + 2*\[Lambda][{y}] + 4*\[Omega][{{Y}, {X, X, Y}}] + 
       2*\[Omega][{{X, X}, {Y, Y}}])) - 
    (M^2*(-1 + \[Lambda][{y}])*(2*\[Omega][{{X}, {X, Y, Y}}]*
        (\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
        (2*\[Omega][{{X}, {X, Y, Y}}] - \[Omega][{{X, X}, {Y, Y}}]) + 
       \[Lambda][{y}]^2*(4*\[Omega][{{Y}, {X, X, Y}}]^3 + 
         \[Omega][{{X}, {X, Y, Y}}]*(4*\[Omega][{{Y}, {X, X, Y}}]^2 - 
           5*(-3 + \[Omega][{{X, X}, {Y, Y}}]) + 2*\[Omega][{{Y}, {X, X, Y}}]*
            (-2 + \[Omega][{{X, X}, {Y, Y}}])) - 
         15*\[Omega][{{X, X}, {Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}]*
          (3 + \[Omega][{{X, X}, {Y, Y}}]) + 2*\[Omega][{{Y}, {X, X, Y}}]^2*
          (4 + \[Omega][{{X, X}, {Y, Y}}])) - \[Lambda][{y}]*
        (5*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
         2*\[Omega][{{X}, {X, Y, Y}}]^2*(-8 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
           \[Omega][{{X, X}, {Y, Y}}]) + \[Omega][{{X}, {X, Y, Y}}]*
          (4*\[Omega][{{Y}, {X, X, Y}}]^2 + 2*\[Omega][{{Y}, {X, X, Y}}]*
            (-2 + \[Omega][{{X, X}, {Y, Y}}]) + 
           11*\[Omega][{{X, X}, {Y, Y}}]))))/
     (E^(2*T*(3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}]))*(M - 6*\[Lambda][{y}])*
      (1 + M - 5*\[Lambda][{y}])*(5*\[Lambda][{y}] + 
       2*\[Omega][{{X}, {X, Y, Y}}])*(3*\[Lambda][{y}] + 
       \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
      (3*\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] - 
       \[Omega][{{X, X}, {Y, Y}}])) + 
    (4*M*\[Lambda][{y}]*((M + 4*\[Omega][{{X}, {X, Y, Y}}])*
        (M + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
        (3*M + 2*(-6 + M)*\[Omega][{{X}, {X, Y, Y}}] + 
         2*M*\[Omega][{{Y}, {X, X, Y}}] + 6*\[Omega][{{X, X}, {Y, Y}}]) + 
       8*\[Lambda][{y}]^2*(-9*M + 4*M*\[Omega][{{Y}, {X, X, Y}}]^3 + 
         18*\[Omega][{{X, X}, {Y, Y}}] - 6*M*\[Omega][{{X, X}, {Y, Y}}] + 
         2*M*\[Omega][{{Y}, {X, X, Y}}]^2*(7 + \[Omega][{{X, X}, {Y, Y}}]) + 
         2*M*\[Omega][{{Y}, {X, X, Y}}]*(3 + 2*\[Omega][{{X, X}, {Y, Y}}]) + 
         2*\[Omega][{{X}, {X, Y, Y}}]*(-18 + 2*M*\[Omega][{{Y}, {X, X, Y}}]^
             2 - M*\[Omega][{{X, X}, {Y, Y}}] + M*\[Omega][{{Y}, {X, X, Y}}]*
            (1 + \[Omega][{{X, X}, {Y, Y}}]))) - 2*\[Lambda][{y}]*
        (16*M*\[Omega][{{Y}, {X, X, Y}}]^3 + 4*M*\[Omega][{{Y}, {X, X, Y}}]^2*
          (14 + M + 2*\[Omega][{{X, X}, {Y, Y}}]) + 
         3*M*(M + 2*(-5 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
         8*\[Omega][{{X}, {X, Y, Y}}]^2*(30 - 2*M + 
           2*M*\[Omega][{{Y}, {X, X, Y}}] + M*\[Omega][{{X, X}, {Y, Y}}]) + 
         2*\[Omega][{{Y}, {X, X, Y}}]*(4*M*(6 + M) + (-24 + 12*M + M^2)*
            \[Omega][{{X, X}, {Y, Y}}]) + 2*\[Omega][{{X}, {X, Y, Y}}]*
          (-2*(-18 + M)*M + 16*M*\[Omega][{{Y}, {X, X, Y}}]^2 + 
           (-60 + 12*M + M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
           2*\[Omega][{{Y}, {X, X, Y}}]*(24 + 10*M + M^2 + 
             4*M*\[Omega][{{X, X}, {Y, Y}}])))))/
     (E^((T*(M + 6*\[Lambda][{y}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}]))/2)*(M - 6*\[Lambda][{y}])*
      (2 + M - 4*\[Lambda][{y}])*(M + 4*\[Lambda][{y}] + 
       4*\[Omega][{{X}, {X, Y, Y}}])*(M + 6*\[Lambda][{y}] + 
       4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
      (M + 4*\[Omega][{{X}, {X, Y, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])) - 
    ((2 + M)*(1 + M + 2*\[Omega][{{X}, {X, Y, Y}}])*
       (1 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
       (5 + 5*M + 2*M*\[Omega][{{X}, {X, Y, Y}}] + 
        2*(1 + M)*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]) + 
      40*\[Lambda][{y}]^4*(-4*M*\[Omega][{{Y}, {X, X, Y}}]^3 + 
        (1 + M)*(-10 + M - 2*\[Omega][{{X, X}, {Y, Y}}]) - 
        2*M*\[Omega][{{Y}, {X, X, Y}}]^2*(9 + \[Omega][{{X, X}, {Y, Y}}]) - 
        2*\[Omega][{{Y}, {X, X, Y}}]*(2 + 11*M + 
          3*M*\[Omega][{{X, X}, {Y, Y}}]) - 2*\[Omega][{{X}, {X, Y, Y}}]*
         (10 - M + 2*M*\[Omega][{{Y}, {X, X, Y}}]^2 + 
          2*\[Omega][{{X, X}, {Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}]*
           (4 + 3*M + M*\[Omega][{{X, X}, {Y, Y}}]))) + 
      \[Lambda][{y}]*(32*(-10 + M)*M*\[Omega][{{X}, {X, Y, Y}}]^4 + 
        8*\[Omega][{{X}, {X, Y, Y}}]^3*(-140 - 146*M - 36*M^2 + 5*M^3 + 
          (-56 - 76*M + 8*M^2)*\[Omega][{{Y}, {X, X, Y}}] - 
          2*(14 - 11*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]) + 
        4*\[Omega][{{X}, {X, Y, Y}}]^2*(-440 - 692*M - 286*M^2 - 7*M^3 + 
          4*M^4 + 8*(-14 - 9*M + M^2)*\[Omega][{{Y}, {X, X, Y}}]^2 + 
          (52 + 54*M + 32*M^2 - 3*M^3)*\[Omega][{{X, X}, {Y, Y}}] - 
          2*(-14 + M)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          2*\[Omega][{{Y}, {X, X, Y}}]*(-228 - 258*M - 58*M^2 + 9*M^3 - 
            4*(-10 + M)*M*\[Omega][{{X, X}, {Y, Y}}])) + 
        2*\[Omega][{{X}, {X, Y, Y}}]*(-460 - 1046*M - 758*M^2 - 159*M^3 + 
          14*M^4 + M^5 + (168 + 296*M + 138*M^2 + 8*M^3 - M^4)*
           \[Omega][{{X, X}, {Y, Y}}] + (52 + 50*M - 4*M^2)*
           \[Omega][{{X, X}, {Y, Y}}]^2 + 4*\[Omega][{{Y}, {X, X, Y}}]^2*
           (-64 - 86*M - 21*M^2 + 5*M^3 - 2*(-14 - 9*M + M^2)*
             \[Omega][{{X, X}, {Y, Y}}]) + 2*\[Omega][{{Y}, {X, X, Y}}]*
           (-412 - 656*M - 283*M^2 - 11*M^3 + 6*M^4 + 
            (128 + 150*M + 47*M^2 - 4*M^3)*\[Omega][{{X, X}, {Y, Y}}] - 
            2*(-14 + M)*\[Omega][{{X, X}, {Y, Y}}]^2)) + 
        (1 + M)*(5*(-32 - 64*M - 38*M^2 - 5*M^3 + M^4) + 
          8*M*(2 + M)*\[Omega][{{Y}, {X, X, Y}}]^3 + 
          (88 + 156*M + 66*M^2 - 3*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
          (24 + 24*M - 2*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          4*\[Omega][{{Y}, {X, X, Y}}]^2*(-36 - 24*M + M^2 + 2*M^3 + 
            (28 + 20*M - M^2)*\[Omega][{{X, X}, {Y, Y}}]) + 
          2*\[Omega][{{Y}, {X, X, Y}}]*(-212 - 234*M - 58*M^2 + 5*M^3 + M^4 + 
            (128 + 120*M + 14*M^2 - M^3)*\[Omega][{{X, X}, {Y, Y}}] - 
            2*(-14 + M)*\[Omega][{{X, X}, {Y, Y}}]^2))) + 
      2*\[Lambda][{y}]^3*(4*M*\[Omega][{{Y}, {X, X, Y}}]^3*
         (34 + 11*M + M^2 - 2*M*\[Omega][{{X, X}, {Y, Y}}]) + 
        (1 + M)*(40 + 126*M + 11*M^2 + (-92 + 48*M)*
           \[Omega][{{X, X}, {Y, Y}}] - 20*\[Omega][{{X, X}, {Y, Y}}]^2) + 
        2*\[Omega][{{Y}, {X, X, Y}}]*(-192 + 212*M + 197*M^2 + 9*M^3 + 
          (-60 + 52*M + 25*M^2 + 3*M^3)*\[Omega][{{X, X}, {Y, Y}}] - 
          6*M^2*\[Omega][{{X, X}, {Y, Y}}]^2) + 
        2*\[Omega][{{Y}, {X, X, Y}}]^2*(-80 + 246*M + 119*M^2 + 9*M^3 + 
          M*(34 - 7*M + M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
          2*M^2*\[Omega][{{X, X}, {Y, Y}}]^2) + 
        8*\[Omega][{{X}, {X, Y, Y}}]^2*(2*M^2*\[Omega][{{Y}, {X, X, Y}}]^2 + 
          5*(-2 + M)*(5 + \[Omega][{{X, X}, {Y, Y}}]) + 
          \[Omega][{{Y}, {X, X, Y}}]*(-20 + 10*M + 3*M^2 + 
            M^2*\[Omega][{{X, X}, {Y, Y}}])) + 2*\[Omega][{{X}, {X, Y, Y}}]*
         (-60 + 76*M + 61*M^2 + 2*(-80 + 54*M + 29*M^2 + M^3)*
           \[Omega][{{Y}, {X, X, Y}}]^2 + 8*M^2*\[Omega][{{Y}, {X, X, Y}}]^
            3 + 2*(-56 + 19*M + 5*M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
          20*\[Omega][{{X, X}, {Y, Y}}]^2 + \[Omega][{{Y}, {X, X, Y}}]*
           (-424 + 278*M + 89*M^2 + 3*M^3 + (-120 + 54*M + 17*M^2 + M^3)*
             \[Omega][{{X, X}, {Y, Y}}] - 2*M^2*\[Omega][{{X, X}, {Y, Y}}]^
              2))) - \[Lambda][{y}]^2*(16*M*\[Omega][{{Y}, {X, X, Y}}]^3*
         (8 + 7*M + M^2 - M*\[Omega][{{X, X}, {Y, Y}}]) + 
        16*\[Omega][{{X}, {X, Y, Y}}]^3*(2*(-50 + 5*M + M^2) + 
          2*(-20 + M^2)*\[Omega][{{Y}, {X, X, Y}}] + (-20 + M^2)*
           \[Omega][{{X, X}, {Y, Y}}]) + 4*\[Omega][{{Y}, {X, X, Y}}]^2*
         (-96 + 6*M + 95*M^2 + 30*M^3 + M^4 + (40 + 56*M - 6*M^2)*
           \[Omega][{{X, X}, {Y, Y}}] - 2*M^2*\[Omega][{{X, X}, {Y, Y}}]^2) + 
        (1 + M)*(-300 - 264*M + 17*M^2 + 31*M^3 + M*(66 - 17*M + 6*M^2)*
           \[Omega][{{X, X}, {Y, Y}}] - 6*(-2 - 7*M + 2*M^2)*
           \[Omega][{{X, X}, {Y, Y}}]^2) + 2*\[Omega][{{Y}, {X, X, Y}}]*
         (-540 - 486*M + 90*M^2 + 116*M^3 + 8*M^4 + 
          (116 + 192*M + 23*M^2 + 8*M^3 + M^4)*\[Omega][{{X, X}, {Y, Y}}] - 
          2*(-20 - 20*M + 7*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
        4*\[Omega][{{X}, {X, Y, Y}}]^2*(-740 - 480*M + 113*M^2 + 6*M^3 + 
          16*(-10 + M^2)*\[Omega][{{Y}, {X, X, Y}}]^2 + 
          (52 - 144*M + 21*M^2 + 3*M^3)*\[Omega][{{X, X}, {Y, Y}}] - 
          2*(-20 + M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          \[Omega][{{Y}, {X, X, Y}}]*(-696 - 208*M + 70*M^2 + 6*M^3 + 
            4*M^2*\[Omega][{{X, X}, {Y, Y}}])) + 2*\[Omega][{{X}, {X, Y, Y}}]*
         (2*(-420 - 552*M - 97*M^2 + 61*M^3 + M^4) + 
          16*M^2*\[Omega][{{Y}, {X, X, Y}}]^3 + (92 + 54*M - 102*M^2 + 
            26*M^3 + M^4)*\[Omega][{{X, X}, {Y, Y}}] - 
          2*(-26 - 41*M + 7*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          4*\[Omega][{{Y}, {X, X, Y}}]^2*(-136 - 66*M + 45*M^2 + 5*M^3 - 
            2*(-20 + M^2)*\[Omega][{{X, X}, {Y, Y}}]) + 
          2*\[Omega][{{Y}, {X, X, Y}}]*(-848 - 558*M + 113*M^2 + 42*M^3 + 
            M^4 + (116 - 4*M + 15*M^2 + 3*M^3)*\[Omega][{{X, X}, {Y, Y}}] - 
            4*(-10 + M^2)*\[Omega][{{X, X}, {Y, Y}}]^2))))/
     (E^(T*(1 + M + \[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}]))*(1 + M - 5*\[Lambda][{y}])*
      (2 + M - 4*\[Lambda][{y}])*(1 + M + 2*\[Omega][{{X}, {X, Y, Y}}])*
      (1 + M + \[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
       2*\[Omega][{{Y}, {X, X, Y}}])*(2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] - 
       2*\[Omega][{{X, X}, {Y, Y}}])*(1 + M - 2*\[Lambda][{y}] + 
       2*\[Omega][{{X}, {X, Y, Y}}] - \[Omega][{{X, X}, {Y, Y}}])))/
   ((3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
    (1 + 2*\[Omega][{{Y}, {X, X, Y}}])*(3 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
     \[Omega][{{X, X}, {Y, Y}}])), 
  (8*(-2 + M)*\[Lambda][{y}]^2*\[Omega][{{X, X}, {Y, Y}}])/
    (E^((T*(M + 4*\[Omega][{{X, X}, {Y, Y}}]))/2)*(M - 2*\[Lambda][{y}])*
     (2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
     (\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
      \[Omega][{{X, X}, {Y, Y}}])*(1 + 2*\[Omega][{{X, X}, {Y, Y}}])*
     (M + 4*\[Omega][{{X, X}, {Y, Y}}])) + 
   (M^2*(-1 + \[Lambda][{y}])*\[Lambda][{y}]*
     (M + M^2 + 2*(1 + M)*\[Lambda][{y}] - 6*\[Lambda][{y}]^2 + 
      8*\[Omega][{{Y}, {X, X, Y}}]^2 + \[Omega][{{Y}, {X, X, Y}}]*
       (4 + 6*M - 8*\[Omega][{{X, X}, {Y, Y}}]) - 
      2*\[Omega][{{X, X}, {Y, Y}}] - 3*M*\[Omega][{{X, X}, {Y, Y}}] + 
      2*\[Omega][{{X, X}, {Y, Y}}]^2)*(-4*\[Omega][{{X}, {X, Y, Y}}]^2 + 
      \[Omega][{{X, X}, {Y, Y}}]^2 + 2*\[Lambda][{y}]*
       (2*\[Omega][{{X}, {X, Y, Y}}]*(-1 + \[Omega][{{X, X}, {Y, Y}}]) + 
        \[Omega][{{X, X}, {Y, Y}}]*(2 + \[Omega][{{X, X}, {Y, Y}}]))))/
    (E^(T*(3*\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}]))*(M - 4*\[Lambda][{y}])*
     (1 + M - 3*\[Lambda][{y}])*(M + 4*\[Omega][{{Y}, {X, X, Y}}] - 
      2*\[Omega][{{X, X}, {Y, Y}}])*(2*\[Lambda][{y}] + 
      2*\[Omega][{{X}, {X, Y, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
     (1 + M - 2*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
      \[Omega][{{X, X}, {Y, Y}}])*(3*\[Lambda][{y}] + 
      2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
     (3 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
     (3*\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
      \[Omega][{{X, X}, {Y, Y}}])*(1 + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
   (M*(-1 + \[Lambda][{y}])*(-4*M*\[Lambda][{y}]^2 + 
      \[Lambda][{y}]*(M*(3 + M) + 12*\[Omega][{{X}, {X, Y, Y}}] - 
        6*\[Omega][{{X, X}, {Y, Y}}]) + (M + 4*\[Omega][{{X}, {X, Y, Y}}] - 
        2*\[Omega][{{X, X}, {Y, Y}}])*(2*\[Omega][{{X}, {X, Y, Y}}] - 
        \[Omega][{{X, X}, {Y, Y}}]))*(-4*\[Omega][{{Y}, {X, X, Y}}]^2 + 
      \[Omega][{{X, X}, {Y, Y}}]^2 + 2*\[Lambda][{y}]*
       (2*\[Omega][{{Y}, {X, X, Y}}]*(-1 + \[Omega][{{X, X}, {Y, Y}}]) + 
        \[Omega][{{X, X}, {Y, Y}}]*(2 + \[Omega][{{X, X}, {Y, Y}}]))))/
    (E^(T*(3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}]))*(M - 4*\[Lambda][{y}])*
     (M + 4*\[Omega][{{X}, {X, Y, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
     (1 + M - 2*\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] - 
      \[Omega][{{X, X}, {Y, Y}}])*(3*\[Lambda][{y}] + 
      2*\[Omega][{{X}, {X, Y, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
     (2*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
      \[Omega][{{X, X}, {Y, Y}}])*(3 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
      \[Omega][{{X, X}, {Y, Y}}])*(3*\[Lambda][{y}] + 
      2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
     (1 + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
   (4*M*\[Lambda][{y}]^2*(2 + M + 2*\[Lambda][{y}] + 
      4*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
     (M^2 + 8*(-4 + M)*\[Omega][{{X}, {X, Y, Y}}]^2 + 
      6*M*\[Omega][{{X, X}, {Y, Y}}] + M^2*\[Omega][{{X, X}, {Y, Y}}] + 
      8*\[Omega][{{X, X}, {Y, Y}}]^2 + 2*M*\[Omega][{{X, X}, {Y, Y}}]^2 + 
      2*M*\[Omega][{{X}, {X, Y, Y}}]*(-2 + M + 
        4*\[Omega][{{X, X}, {Y, Y}}]) - 4*\[Lambda][{y}]*
       (M + (-2 + 3*M)*\[Omega][{{X, X}, {Y, Y}}] + 
        M*\[Omega][{{X, X}, {Y, Y}}]^2 + 2*\[Omega][{{X}, {X, Y, Y}}]*
         (2 + M*\[Omega][{{X, X}, {Y, Y}}]))))/
    (E^((T*(M + 2*\[Lambda][{y}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))/2)*(M - 4*\[Lambda][{y}])*
     (2 + M - 2*\[Lambda][{y}])*(M + 4*\[Omega][{{X}, {X, Y, Y}}] - 
      2*\[Omega][{{X, X}, {Y, Y}}])*(2 + M + 4*\[Omega][{{Y}, {X, X, Y}}] - 
      2*\[Omega][{{X, X}, {Y, Y}}])*(2*\[Lambda][{y}] + 
      2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
     (3 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
     (1 + 2*\[Omega][{{X, X}, {Y, Y}}])*(M + 2*\[Lambda][{y}] + 
      4*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
   (8*\[Lambda][{y}]*(-((2*\[Omega][{{Y}, {X, X, Y}}] - 
         \[Omega][{{X, X}, {Y, Y}}])*(M^2 + 8*M*\[Omega][{{Y}, {X, X, Y}}] + 
         16*\[Omega][{{Y}, {X, X, Y}}]^2 - 4*\[Omega][{{X, X}, {Y, Y}}]^2)) - 
      \[Lambda][{y}]*(M^2 - 32*\[Omega][{{Y}, {X, X, Y}}]^3 + 
        M*(-2 + 3*M)*\[Omega][{{X, X}, {Y, Y}}] + (12 - 6*M + M^2)*
         \[Omega][{{X, X}, {Y, Y}}]^2 - 2*(-2 + M)*\[Omega][{{X, X}, {Y, Y}}]^
          3 + 2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]*
         (-8 + 4*M + M^2 + 4*\[Omega][{{X, X}, {Y, Y}}]) + 
        8*\[Omega][{{Y}, {X, X, Y}}]^2*(-2 - M + (-2 + M)*
           \[Omega][{{X, X}, {Y, Y}}])) + 2*\[Lambda][{y}]^2*
       (2*M + 8*\[Omega][{{Y}, {X, X, Y}}]^2 + 4*(-1 + M)*
         \[Omega][{{X, X}, {Y, Y}}] + (-2 + M)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
        2*\[Omega][{{Y}, {X, X, Y}}]*(4 + M + 
          M*\[Omega][{{X, X}, {Y, Y}}]))))/
    (E^((T*(M + 2*\[Lambda][{y}] + 4*\[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))/2)*(M - 4*\[Lambda][{y}])*
     (2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
     (M + 4*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
     (\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
      \[Omega][{{X, X}, {Y, Y}}])*(3 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
      \[Omega][{{X, X}, {Y, Y}}])*(1 + 2*\[Omega][{{X, X}, {Y, Y}}])*
     (M + 2*\[Lambda][{y}] + 4*\[Omega][{{Y}, {X, X, Y}}] + 
      2*\[Omega][{{X, X}, {Y, Y}}])) - 
   (\[Lambda][{y}]*(6*(-2 + M)*\[Lambda][{y}]^3*
       (2 + M + 4*\[Omega][{{X, X}, {Y, Y}}] + 
        2*\[Omega][{{X, X}, {Y, Y}}]^2 + 4*\[Omega][{{X}, {X, Y, Y}}]*
         (1 + \[Omega][{{X, X}, {Y, Y}}])) + \[Lambda][{y}]^2*
       (56 + 56*M - 2*M^2 - 8*M^3 + (88 + 32*M - 16*M^2 - 7*M^3)*
         \[Omega][{{X, X}, {Y, Y}}] - 2*(-4 + 4*M + M^3)*
         \[Omega][{{X, X}, {Y, Y}}]^2 + 4*(-6 + M^2)*
         \[Omega][{{X, X}, {Y, Y}}]^3 - 8*\[Omega][{{X}, {X, Y, Y}}]^2*
         (-12 + M^2 + 2*(-6 + M^2)*\[Omega][{{X, X}, {Y, Y}}]) - 
        2*\[Omega][{{X}, {X, Y, Y}}]*(-80 - 40*M + 18*M^2 + M^3 + 
          2*(-40 - 20*M + 13*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}])) + 
      (2 + M)*(2*(1 + M)^2*(2 + M) + 16*M*\[Omega][{{X}, {X, Y, Y}}]^3 + 
        (4 + 6*M + 3*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}] - 
        (4 + 6*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 - 
        2*(2 + M)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
        4*(2 + M)*\[Omega][{{X}, {X, Y, Y}}]^2*(2 + 3*M + 
          2*\[Omega][{{X, X}, {Y, Y}}]) + 2*\[Omega][{{X}, {X, Y, Y}}]*
         (8 + 16*M + 9*M^2 + M^3 + 2*(4 + 5*M + M^2)*
           \[Omega][{{X, X}, {Y, Y}}] - 2*M*\[Omega][{{X, X}, {Y, Y}}]^2)) + 
      \[Lambda][{y}]*(2*(-20 - 40*M - 23*M^2 - 2*M^3 + M^4) + 
        16*(-6 + M)*M*\[Omega][{{X}, {X, Y, Y}}]^3 + 
        (-48 - 56*M - 12*M^2 + 2*M^3 + M^4)*\[Omega][{{X, X}, {Y, Y}}] + 
        (24 + 28*M + 4*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
        (32 + 8*M - 2*M^2)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
        4*\[Omega][{{X}, {X, Y, Y}}]^2*(-32 - 44*M - 12*M^2 + 3*M^3 + 
          2*(-16 - 4*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]) + 
        2*\[Omega][{{X}, {X, Y, Y}}]*(-72 - 108*M - 40*M^2 + 2*M^3 + M^4 + 
          4*(-18 - 18*M + M^3)*\[Omega][{{X, X}, {Y, Y}}] - 
          2*(-6 + M)*M*\[Omega][{{X, X}, {Y, Y}}]^2))))/
    (E^(T*(1 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}]))*(1 + M - 3*\[Lambda][{y}])*
     (2 + M - 2*\[Lambda][{y}])*(2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] - 
      2*\[Omega][{{X, X}, {Y, Y}}])*(1 + M - \[Lambda][{y}] + 
      2*\[Omega][{{X}, {X, Y, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
     (\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
      \[Omega][{{X, X}, {Y, Y}}])*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
      \[Omega][{{X, X}, {Y, Y}}])*(1 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
      \[Omega][{{X, X}, {Y, Y}}])*(1 + 2*\[Omega][{{X, X}, {Y, Y}}])) - 
   (M^2*(-2*(\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (2*\[Omega][{{X}, {X, Y, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
       (2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
       (\[Omega][{{X}, {X, Y, Y}}]^2 + 3*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}]^2 + \[Omega][{{X}, {X, Y, Y}}]*
         (3 + 2*\[Omega][{{Y}, {X, X, Y}}]) - \[Omega][{{X, X}, {Y, Y}}]*
         (3 + \[Omega][{{X, X}, {Y, Y}}])) + 3*\[Lambda][{y}]^4*
       (2*\[Omega][{{X}, {X, Y, Y}}]^2*(2*\[Omega][{{Y}, {X, X, Y}}]*
           (-5 + 2*\[Omega][{{X, X}, {Y, Y}}]) + \[Omega][{{X, X}, {Y, Y}}]*
           (1 + 2*\[Omega][{{X, X}, {Y, Y}}])) + \[Omega][{{X, X}, {Y, Y}}]*
         (9 + 21*\[Omega][{{X, X}, {Y, Y}}] + 6*\[Omega][{{X, X}, {Y, Y}}]^
            2 + \[Omega][{{Y}, {X, X, Y}}]^2*
           (2 + 4*\[Omega][{{X, X}, {Y, Y}}]) + \[Omega][{{Y}, {X, X, Y}}]*
           (24 + 19*\[Omega][{{X, X}, {Y, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]^
              2)) + \[Omega][{{X}, {X, Y, Y}}]*
         (4*\[Omega][{{Y}, {X, X, Y}}]^2*(-5 + 
            2*\[Omega][{{X, X}, {Y, Y}}]) + 4*\[Omega][{{Y}, {X, X, Y}}]*
           (-15 + 7*\[Omega][{{X, X}, {Y, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]^
              2) + \[Omega][{{X, X}, {Y, Y}}]*
           (24 + 19*\[Omega][{{X, X}, {Y, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]^
              2))) + \[Lambda][{y}]*(16*\[Omega][{{X}, {X, Y, Y}}]^4*
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X}, {X, Y, Y}}]^3*
         (24*\[Omega][{{Y}, {X, X, Y}}]^2 + \[Omega][{{Y}, {X, X, Y}}]*
           (2 - 8*\[Omega][{{X, X}, {Y, Y}}]) + 
          (5 - 2*\[Omega][{{X, X}, {Y, Y}}])*\[Omega][{{X, X}, {Y, Y}}]) + 
        \[Omega][{{X, X}, {Y, Y}}]*(\[Omega][{{Y}, {X, X, Y}}]^3*
           (10 - 4*\[Omega][{{X, X}, {Y, Y}}]) + 6*\[Omega][{{X, X}, {Y, Y}}]^
            2*(3 + \[Omega][{{X, X}, {Y, Y}}]) - \[Omega][{{Y}, {X, X, Y}}]*
           \[Omega][{{X, X}, {Y, Y}}]*(69 + 17*\[Omega][{{X, X}, {Y, Y}}]) + 
          \[Omega][{{Y}, {X, X, Y}}]^2*(66 + 11*\[Omega][{{X, X}, {Y, Y}}] + 
            4*\[Omega][{{X, X}, {Y, Y}}]^2)) + \[Omega][{{X}, {X, Y, Y}}]^2*
         (48*\[Omega][{{Y}, {X, X, Y}}]^3 + \[Omega][{{Y}, {X, X, Y}}]^2*
           (8 - 32*\[Omega][{{X, X}, {Y, Y}}]) + \[Omega][{{X, X}, {Y, Y}}]*
           (66 + 11*\[Omega][{{X, X}, {Y, Y}}] + 4*\[Omega][{{X, X}, {Y, Y}}]^
              2) - 2*\[Omega][{{Y}, {X, X, Y}}]*
           (66 + 19*\[Omega][{{X, X}, {Y, Y}}] + 6*\[Omega][{{X, X}, {Y, Y}}]^
              2)) + \[Omega][{{X}, {X, Y, Y}}]*
         (16*\[Omega][{{Y}, {X, X, Y}}]^4 + \[Omega][{{Y}, {X, X, Y}}]^3*
           (4 - 16*\[Omega][{{X, X}, {Y, Y}}]) - \[Omega][{{X, X}, {Y, Y}}]^2*
           (69 + 17*\[Omega][{{X, X}, {Y, Y}}]) + 
          2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]*
           (102 + 23*\[Omega][{{X, X}, {Y, Y}}] + 
            4*\[Omega][{{X, X}, {Y, Y}}]^2) - 2*\[Omega][{{Y}, {X, X, Y}}]^2*
           (66 + 19*\[Omega][{{X, X}, {Y, Y}}] + 6*\[Omega][{{X, X}, {Y, Y}}]^
              2))) - \[Lambda][{y}]^2*(4*\[Omega][{{X}, {X, Y, Y}}]^4*
         (2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]) + 
        2*\[Omega][{{X}, {X, Y, Y}}]^3*(12*\[Omega][{{Y}, {X, X, Y}}]^2 + 
          4*\[Omega][{{Y}, {X, X, Y}}]*(-8 + \[Omega][{{X, X}, {Y, Y}}]) - 
          (-6 + \[Omega][{{X, X}, {Y, Y}}])*\[Omega][{{X, X}, {Y, Y}}]) + 
        2*\[Omega][{{X}, {X, Y, Y}}]^2*(12*\[Omega][{{Y}, {X, X, Y}}]^3 + 
          4*\[Omega][{{Y}, {X, X, Y}}]^2*(-16 + \[Omega][{{X, X}, {Y, Y}}]) + 
          \[Omega][{{X, X}, {Y, Y}}]*(27 + 8*\[Omega][{{X, X}, {Y, Y}}] - 
            \[Omega][{{X, X}, {Y, Y}}]^2) - 3*\[Omega][{{Y}, {X, X, Y}}]*
           (34 - 10*\[Omega][{{X, X}, {Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}]^
             2)) + \[Omega][{{X, X}, {Y, Y}}]*
         (4*\[Omega][{{Y}, {X, X, Y}}]^4 - 2*\[Omega][{{Y}, {X, X, Y}}]^3*
           (-6 + \[Omega][{{X, X}, {Y, Y}}]) + 15*\[Omega][{{X, X}, {Y, Y}}]*
           (3 + \[Omega][{{X, X}, {Y, Y}}]) - 2*\[Omega][{{Y}, {X, X, Y}}]^2*
           (-27 - 8*\[Omega][{{X, X}, {Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}]^
             2) - 3*\[Omega][{{Y}, {X, X, Y}}]*
           (30 + 23*\[Omega][{{X, X}, {Y, Y}}] + 6*\[Omega][{{X, X}, {Y, Y}}]^
              2)) + \[Omega][{{X}, {X, Y, Y}}]*
         (8*\[Omega][{{Y}, {X, X, Y}}]^4 + 8*\[Omega][{{Y}, {X, X, Y}}]^3*
           (-8 + \[Omega][{{X, X}, {Y, Y}}]) - 6*\[Omega][{{Y}, {X, X, Y}}]^2*
           (34 - 10*\[Omega][{{X, X}, {Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}]^
             2) - 3*\[Omega][{{X, X}, {Y, Y}}]*
           (30 + 23*\[Omega][{{X, X}, {Y, Y}}] + 6*\[Omega][{{X, X}, {Y, Y}}]^
              2) - 4*\[Omega][{{Y}, {X, X, Y}}]*
           (-45 - 78*\[Omega][{{X, X}, {Y, Y}}] - 
            8*\[Omega][{{X, X}, {Y, Y}}]^2 + \[Omega][{{X, X}, {Y, Y}}]^
             3))) + \[Lambda][{y}]^3*(2*\[Omega][{{X}, {X, Y, Y}}]^3*
         (\[Omega][{{X, X}, {Y, Y}}]*(-5 + 2*\[Omega][{{X, X}, {Y, Y}}]) + 
          \[Omega][{{Y}, {X, X, Y}}]*(-22 + 4*\[Omega][{{X, X}, {Y, Y}}])) + 
        \[Omega][{{X}, {X, Y, Y}}]^2*(8*\[Omega][{{Y}, {X, X, Y}}]^2*
           (-11 + 2*\[Omega][{{X, X}, {Y, Y}}]) + \[Omega][{{X, X}, {Y, Y}}]*
           (-18 + 11*\[Omega][{{X, X}, {Y, Y}}] - 
            2*\[Omega][{{X, X}, {Y, Y}}]^2) + 2*\[Omega][{{Y}, {X, X, Y}}]*
           (-6 + 7*\[Omega][{{X, X}, {Y, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]^
              2)) - \[Omega][{{X, X}, {Y, Y}}]*(\[Omega][{{Y}, {X, X, Y}}]^3*
           (10 - 4*\[Omega][{{X, X}, {Y, Y}}]) + \[Omega][{{Y}, {X, X, Y}}]^2*
           (18 - 11*\[Omega][{{X, X}, {Y, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]^
              2) + 3*(9 + 6*\[Omega][{{X, X}, {Y, Y}}] + 
            7*\[Omega][{{X, X}, {Y, Y}}]^2 + 2*\[Omega][{{X, X}, {Y, Y}}]^
              3) + \[Omega][{{Y}, {X, X, Y}}]*(162 + 
            57*\[Omega][{{X, X}, {Y, Y}}] + 13*\[Omega][{{X, X}, {Y, Y}}]^2 + 
            2*\[Omega][{{X, X}, {Y, Y}}]^3)) + \[Omega][{{X}, {X, Y, Y}}]*
         (\[Omega][{{Y}, {X, X, Y}}]^3*(-44 + 8*\[Omega][{{X, X}, {Y, Y}}]) + 
          2*\[Omega][{{Y}, {X, X, Y}}]^2*(-6 + 7*\[Omega][{{X, X}, {Y, Y}}] + 
            2*\[Omega][{{X, X}, {Y, Y}}]^2) - 2*\[Omega][{{Y}, {X, X, Y}}]*
           (-180 - 12*\[Omega][{{X, X}, {Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}]^
             2 + 2*\[Omega][{{X, X}, {Y, Y}}]^3) - \[Omega][{{X, X}, {Y, Y}}]*
           (162 + 57*\[Omega][{{X, X}, {Y, Y}}] + 
            13*\[Omega][{{X, X}, {Y, Y}}]^2 + 2*\[Omega][{{X, X}, {Y, Y}}]^
              3)))))/(E^(2*T*(3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}]))*(M - 6*\[Lambda][{y}])*
     (1 + M - 5*\[Lambda][{y}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
      \[Omega][{{Y}, {X, X, Y}}])*(3*\[Lambda][{y}] + 
      \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
     (5*\[Lambda][{y}] + 2*(\[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}]))*
     (3*\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] - 
      \[Omega][{{X, X}, {Y, Y}}])*(3*\[Lambda][{y}] + 
      2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
     (3 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
     (3 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
     (1 + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
   (M*\[Lambda][{y}]^2*(648*\[Lambda][{y}]^6 + 108*\[Lambda][{y}]^5*
       (6 + 13*M + M^2 + 34*\[Omega][{{X}, {X, Y, Y}}] + 
        22*\[Omega][{{Y}, {X, X, Y}}] + 22*\[Omega][{{X, X}, {Y, Y}}] + 
        4*M*\[Omega][{{X, X}, {Y, Y}}]) + 6*\[Lambda][{y}]^4*
       (90*M + 143*M^2 + 20*M^3 + 1248*\[Omega][{{X}, {X, Y, Y}}]^2 + 
        456*\[Omega][{{Y}, {X, X, Y}}]^2 + 252*\[Omega][{{X, X}, {Y, Y}}] + 
        602*M*\[Omega][{{X, X}, {Y, Y}}] + 
        112*M^2*\[Omega][{{X, X}, {Y, Y}}] + 516*\[Omega][{{X, X}, {Y, Y}}]^
          2 + 128*M*\[Omega][{{X, X}, {Y, Y}}]^2 + 
        6*\[Omega][{{Y}, {X, X, Y}}]*(30 + 87*M + 11*M^2 + 
          2*(83 + 22*M)*\[Omega][{{X, X}, {Y, Y}}]) + 
        2*\[Omega][{{X}, {X, Y, Y}}]*(198 + 459*M + 17*M^2 + 
          876*\[Omega][{{Y}, {X, X, Y}}] + (858 + 68*M)*
           \[Omega][{{X, X}, {Y, Y}}])) + \[Lambda][{y}]^3*
       (126*M^2 + 183*M^3 + 39*M^4 + 6720*\[Omega][{{X}, {X, Y, Y}}]^3 + 
        1248*\[Omega][{{Y}, {X, X, Y}}]^3 + 
        828*M*\[Omega][{{X, X}, {Y, Y}}] + 1438*M^2*
         \[Omega][{{X, X}, {Y, Y}}] + 304*M^3*\[Omega][{{X, X}, {Y, Y}}] + 
        1080*\[Omega][{{X, X}, {Y, Y}}]^2 + 2980*M*\[Omega][{{X, X}, {Y, Y}}]^
          2 + 704*M^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
        1656*\[Omega][{{X, X}, {Y, Y}}]^3 + 448*M*\[Omega][{{X, X}, {Y, Y}}]^
          3 + 8*\[Omega][{{X}, {X, Y, Y}}]^2*(342 + 894*M + 13*M^2 + 
          2016*\[Omega][{{Y}, {X, X, Y}}] + 4*(489 + 13*M)*
           \[Omega][{{X, X}, {Y, Y}}]) + 24*\[Omega][{{Y}, {X, X, Y}}]^2*
         (24 + 103*M + 19*M^2 + 4*(55 + 19*M)*\[Omega][{{X, X}, {Y, Y}}]) + 
        2*\[Omega][{{Y}, {X, X, Y}}]*(M*(378 + 689*M + 140*M^2) + 
          2*(522 + 1525*M + 386*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
          4*(675 + 212*M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
        2*\[Omega][{{X}, {X, Y, Y}}]*(M*(630 + 1019*M + 80*M^2) + 
          5040*\[Omega][{{Y}, {X, X, Y}}]^2 + (2052 + 4946*M + 488*M^2)*
           \[Omega][{{X, X}, {Y, Y}}] + 84*(57 + 8*M)*
           \[Omega][{{X, X}, {Y, Y}}]^2 + 4*\[Omega][{{Y}, {X, X, Y}}]*
           (450 + 1209*M + 76*M^2 + 4*(609 + 76*M)*\[Omega][{{X, X}, {Y, 
                Y}}]))) + (\[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}])*(2*\[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}])*(32*\[Omega][{{X}, {X, Y, Y}}]^3*
         (M + 4*\[Omega][{{Y}, {X, X, Y}}] + 6*\[Omega][{{X, X}, {Y, Y}}]) + 
        (M + 4*\[Omega][{{Y}, {X, X, Y}}])*(M + 2*\[Omega][{{X, X}, {Y, Y}}])*
         (M*(1 + M) + 8*\[Omega][{{Y}, {X, X, Y}}]^2 + 
          (6 + 8*M)*\[Omega][{{X, X}, {Y, Y}}] + 8*\[Omega][{{X, X}, {Y, Y}}]^
            2 + 2*\[Omega][{{Y}, {X, X, Y}}]*(2 + 3*M + 
            6*\[Omega][{{X, X}, {Y, Y}}])) + 16*\[Omega][{{X}, {X, Y, Y}}]^2*
         (M*(1 + 2*M) + 16*\[Omega][{{Y}, {X, X, Y}}]^2 + 
          3*(2 + 5*M)*\[Omega][{{X, X}, {Y, Y}}] + 
          14*\[Omega][{{X, X}, {Y, Y}}]^2 + 4*\[Omega][{{Y}, {X, X, Y}}]*
           (1 + 3*M + 7*\[Omega][{{X, X}, {Y, Y}}])) + 
        2*\[Omega][{{X}, {X, Y, Y}}]*(M^2*(4 + 5*M) + 
          64*\[Omega][{{Y}, {X, X, Y}}]^3 + 4*M*(7 + 11*M)*
           \[Omega][{{X, X}, {Y, Y}}] + 4*(6 + 19*M)*
           \[Omega][{{X, X}, {Y, Y}}]^2 + 32*\[Omega][{{X, X}, {Y, Y}}]^3 + 
          32*\[Omega][{{Y}, {X, X, Y}}]^2*(1 + 3*M + 
            5*\[Omega][{{X, X}, {Y, Y}}]) + 8*\[Omega][{{Y}, {X, X, Y}}]*
           (M*(3 + 5*M) + (8 + 22*M)*\[Omega][{{X, X}, {Y, Y}}] + 
            20*\[Omega][{{X, X}, {Y, Y}}]^2))) + \[Lambda][{y}]^2*
       (9*M^3 + 12*M^4 + 3*M^5 + 2688*\[Omega][{{X}, {X, Y, Y}}]^4 + 
        192*\[Omega][{{Y}, {X, X, Y}}]^4 + 
        114*M^2*\[Omega][{{X, X}, {Y, Y}}] + 
        175*M^3*\[Omega][{{X, X}, {Y, Y}}] + 
        40*M^4*\[Omega][{{X, X}, {Y, Y}}] + 324*M*\[Omega][{{X, X}, {Y, Y}}]^
          2 + 660*M^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
        172*M^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
        216*\[Omega][{{X, X}, {Y, Y}}]^3 + 836*M*\[Omega][{{X, X}, {Y, Y}}]^
          3 + 272*M^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
        288*\[Omega][{{X, X}, {Y, Y}}]^4 + 128*M*\[Omega][{{X, X}, {Y, Y}}]^
          4 + 16*\[Omega][{{X}, {X, Y, Y}}]^3*(78 + 246*M + M^2 + 
          664*\[Omega][{{Y}, {X, X, Y}}] + 4*(167 + M)*
           \[Omega][{{X, X}, {Y, Y}}]) + 16*\[Omega][{{Y}, {X, X, Y}}]^3*
         (6 + 51*M + 13*M^2 + 2*(61 + 26*M)*\[Omega][{{X, X}, {Y, Y}}]) + 
        4*\[Omega][{{Y}, {X, X, Y}}]^2*(M*(84 + 175*M + 44*M^2) + 
          (228 + 840*M + 242*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
          4*(205 + 66*M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
        \[Omega][{{Y}, {X, X, Y}}]*(M^2*(114 + 175*M + 43*M^2) + 
          6*M*(138 + 259*M + 60*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
          4*(306 + 891*M + 223*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          8*(261 + 70*M)*\[Omega][{{X, X}, {Y, Y}}]^3) + 
        8*\[Omega][{{X}, {X, Y, Y}}]^2*(M*(123 + 208*M + 8*M^2) + 
          1456*\[Omega][{{Y}, {X, X, Y}}]^2 + (474 + 1184*M + 51*M^2)*
           \[Omega][{{X, X}, {Y, Y}}] + 4*(314 + 19*M)*
           \[Omega][{{X, X}, {Y, Y}}]^2 + 8*\[Omega][{{Y}, {X, X, Y}}]*
           (54 + 148*M + 4*M^2 + (333 + 16*M)*\[Omega][{{X, X}, {Y, Y}}])) + 
        \[Omega][{{X}, {X, Y, Y}}]*(M^2*(198 + 265*M + 31*M^2) + 
          3904*\[Omega][{{Y}, {X, X, Y}}]^3 + 2*M*(726 + 1197*M + 136*M^2)*
           \[Omega][{{X, X}, {Y, Y}}] + 4*(486 + 1341*M + 187*M^2)*
           \[Omega][{{X, X}, {Y, Y}}]^2 + 24*(135 + 26*M)*
           \[Omega][{{X, X}, {Y, Y}}]^3 + 16*\[Omega][{{Y}, {X, X, Y}}]^2*
           (108 + 355*M + 28*M^2 + 2*(393 + 56*M)*\[Omega][{{X, X}, {Y, 
                Y}}]) + 4*\[Omega][{{Y}, {X, X, Y}}]*
           (3*M*(114 + 197*M + 20*M^2) + 4*(276 + 751*M + 92*M^2)*
             \[Omega][{{X, X}, {Y, Y}}] + 4*(761 + 128*M)*
             \[Omega][{{X, X}, {Y, Y}}]^2))) + \[Lambda][{y}]*
       (384*\[Omega][{{X}, {X, Y, Y}}]^5 + 64*\[Omega][{{X}, {X, Y, Y}}]^4*
         (3 + 14*M + 46*\[Omega][{{Y}, {X, X, Y}}] + 
          52*\[Omega][{{X, X}, {Y, Y}}]) + 32*\[Omega][{{Y}, {X, X, Y}}]^4*
         (M*(3 + M) + 4*(2 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
        16*\[Omega][{{Y}, {X, X, Y}}]^3*(M*(3 + 7*M + 2*M^2) + 
          (8 + 42*M + 11*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
          2*(25 + 6*M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
        2*\[Omega][{{Y}, {X, X, Y}}]^2*(M^2*(12 + 19*M + 5*M^2) + 
          2*M*(58 + 113*M + 21*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
          8*(24 + 74*M + 13*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          16*(25 + 4*M)*\[Omega][{{X, X}, {Y, Y}}]^3) + 
        M*\[Omega][{{X, X}, {Y, Y}}]*(M^2*(3 + 4*M + M^2) + 
          M*(24 + 38*M + 9*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
          4*(9 + 23*M + 7*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          4*(16 + 9*M)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
          16*\[Omega][{{X, X}, {Y, Y}}]^4) + \[Omega][{{Y}, {X, X, Y}}]*
         (M^3*(3 + 4*M + M^2) + M^2*(62 + 91*M + 16*M^2)*
           \[Omega][{{X, X}, {Y, Y}}] + 4*M*(57 + 107*M + 19*M^2)*
           \[Omega][{{X, X}, {Y, Y}}]^2 + 4*(54 + 165*M + 32*M^2)*
           \[Omega][{{X, X}, {Y, Y}}]^3 + 32*(9 + 2*M)*
           \[Omega][{{X, X}, {Y, Y}}]^4) + 8*\[Omega][{{X}, {X, Y, Y}}]^3*
         (M*(38 + 69*M + M^2) + 656*\[Omega][{{Y}, {X, X, Y}}]^2 + 
          2*(90 + 233*M + 3*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
          (540 + 8*M)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          4*\[Omega][{{Y}, {X, X, Y}}]*(38 + 113*M + M^2 + 
            4*(73 + M)*\[Omega][{{X, X}, {Y, Y}}])) + 
        2*\[Omega][{{X}, {X, Y, Y}}]^2*(M^2*(50 + 64*M + 3*M^2) + 
          1600*\[Omega][{{Y}, {X, X, Y}}]^3 + 2*M*(194 + 315*M + 15*M^2)*
           \[Omega][{{X, X}, {Y, Y}}] + 4*(138 + 385*M + 24*M^2)*
           \[Omega][{{X, X}, {Y, Y}}]^2 + 16*(61 + 6*M)*
           \[Omega][{{X, X}, {Y, Y}}]^3 + 16*\[Omega][{{Y}, {X, X, Y}}]^2*
           (40 + 120*M + 3*M^2 + 4*(70 + 3*M)*\[Omega][{{X, X}, {Y, Y}}]) + 
          8*\[Omega][{{Y}, {X, X, Y}}]*(M*(48 + 82*M + 3*M^2) + 
            (170 + 461*M + 21*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
            (518 + 36*M)*\[Omega][{{X, X}, {Y, Y}}]^2)) + 
        \[Omega][{{X}, {X, Y, Y}}]*(M^3*(9 + 10*M + M^2) + 
          512*\[Omega][{{Y}, {X, X, Y}}]^4 + M^2*(110 + 151*M + 18*M^2)*
           \[Omega][{{X, X}, {Y, Y}}] + 4*M*(87 + 161*M + 23*M^2)*
           \[Omega][{{X, X}, {Y, Y}}]^2 + 12*(18 + 71*M + 14*M^2)*
           \[Omega][{{X, X}, {Y, Y}}]^3 + 96*(3 + M)*
           \[Omega][{{X, X}, {Y, Y}}]^4 + 32*\[Omega][{{Y}, {X, X, Y}}]^3*
           (8 + 38*M + 3*M^2 + 4*(25 + 3*M)*\[Omega][{{X, X}, {Y, Y}}]) + 
          8*\[Omega][{{Y}, {X, X, Y}}]^2*(M*(52 + 97*M + 9*M^2) + 
            2*(88 + 270*M + 27*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
            (596 + 72*M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
          2*\[Omega][{{Y}, {X, X, Y}}]*(M^2*(62 + 83*M + 8*M^2) + 
            16*M*(30 + 51*M + 5*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
            4*(186 + 517*M + 60*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
            32*(43 + 6*M)*\[Omega][{{X, X}, {Y, Y}}]^3)))))/
    ((3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
      \[Omega][{{Y}, {X, X, Y}}])*(1 + M + \[Lambda][{y}] + 
      2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
     (M + 6*\[Lambda][{y}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
      4*\[Omega][{{Y}, {X, X, Y}}])*(1 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
      \[Omega][{{X, X}, {Y, Y}}])*(3*\[Lambda][{y}] + 
      2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
     (3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
      \[Omega][{{X, X}, {Y, Y}}])*(\[Lambda][{y}] + 
      2*\[Omega][{{X, X}, {Y, Y}}])*(M + 2*\[Lambda][{y}] + 
      4*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}])*
     (M + 2*\[Lambda][{y}] + 4*\[Omega][{{Y}, {X, X, Y}}] + 
      2*\[Omega][{{X, X}, {Y, Y}}])*(M + 4*\[Omega][{{X, X}, {Y, Y}}])) + 
   (4*M*\[Lambda][{y}]*((2*\[Omega][{{Y}, {X, X, Y}}] - 
        \[Omega][{{X, X}, {Y, Y}}])*(32*(-6 + M)*\[Omega][{{X}, {X, Y, Y}}]^4*
         (M + 4*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}]) + 
        16*(-6 + M)*\[Omega][{{X}, {X, Y, Y}}]^3*(M*(6 + M) + 
          24*\[Omega][{{Y}, {X, X, Y}}]^2 + 2*\[Omega][{{Y}, {X, X, Y}}]*
           (12 + 5*M - 8*\[Omega][{{X, X}, {Y, Y}}]) - 
          3*(4 + M)*\[Omega][{{X, X}, {Y, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]^
            2) + (M + 4*\[Omega][{{Y}, {X, X, Y}}])*
         \[Omega][{{X, X}, {Y, Y}}]*(16*(6 + M)*\[Omega][{{Y}, {X, X, Y}}]^
            3 + 8*\[Omega][{{Y}, {X, X, Y}}]^2*(36 + 6*M + M^2 - 
            3*(2 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
          6*(3 + \[Omega][{{X, X}, {Y, Y}}])*(M^2 - 
            6*M*\[Omega][{{X, X}, {Y, Y}}] + 8*\[Omega][{{X, X}, {Y, Y}}]^
              2) + \[Omega][{{Y}, {X, X, Y}}]*(M*(144 + 6*M + M^2) - 
            6*(72 - 2*M + M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
            8*(-12 + M)*\[Omega][{{X, X}, {Y, Y}}]^2)) + 
        2*\[Omega][{{X}, {X, Y, Y}}]^2*(M^2*(-144 + 18*M + M^2) + 
          192*(-6 + M)*\[Omega][{{Y}, {X, X, Y}}]^3 - 2*M*(-360 + 42*M + M^2)*
           \[Omega][{{X, X}, {Y, Y}}] - 8*(108 - 24*M + M^2)*
           \[Omega][{{X, X}, {Y, Y}}]^2 + 16*(-12 + M)*
           \[Omega][{{X, X}, {Y, Y}}]^3 + 16*\[Omega][{{Y}, {X, X, Y}}]^2*
           (-144 - 18*M + 7*M^2 - 8*(-9 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
          4*\[Omega][{{Y}, {X, X, Y}}]*(M*(-288 + 18*M + 5*M^2) - 
            2*(-360 + 6*M + 5*M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
            4*(-6 + M)*\[Omega][{{X, X}, {Y, Y}}]^2)) + 
        \[Omega][{{X}, {X, Y, Y}}]*(6*(-6 + M)*M^3 + 128*(-6 + M)*
           \[Omega][{{Y}, {X, X, Y}}]^4 + M^2*(360 - 42*M + M^2)*
           \[Omega][{{X, X}, {Y, Y}}] - 6*M*(144 - 22*M + M^2)*
           \[Omega][{{X, X}, {Y, Y}}]^2 + 8*(72 - 30*M + M^2)*
           \[Omega][{{X, X}, {Y, Y}}]^3 + 192*\[Omega][{{X, X}, {Y, Y}}]^4 + 
          96*\[Omega][{{Y}, {X, X, Y}}]^3*(-24 - 2*M + M^2 + 
            16*\[Omega][{{X, X}, {Y, Y}}]) + 8*\[Omega][{{Y}, {X, X, Y}}]^2*
           (3*M*(-72 + 6*M + M^2) + 2*(360 + 6*M + M^2)*
             \[Omega][{{X, X}, {Y, Y}}] - 4*(-6 + 5*M)*
             \[Omega][{{X, X}, {Y, Y}}]^2) + 2*\[Omega][{{Y}, {X, X, Y}}]*
           (M^2*(-216 + 30*M + M^2) + 4*M*(360 - 30*M + M^2)*
             \[Omega][{{X, X}, {Y, Y}}] - 32*(54 - 9*M + M^2)*
             \[Omega][{{X, X}, {Y, Y}}]^2 + 32*(-12 + M)*
             \[Omega][{{X, X}, {Y, Y}}]^3))) + 8*\[Lambda][{y}]^3*
       (-8*M*\[Omega][{{X}, {X, Y, Y}}]^3*(-1 + \[Omega][{{X, X}, {Y, Y}}])*
         (3 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]) + 
        8*\[Omega][{{Y}, {X, X, Y}}]^3*(3*M + 2*(-9 + 2*M)*
           \[Omega][{{X, X}, {Y, Y}}] + 2*M*\[Omega][{{X, X}, {Y, Y}}]^2) + 
        3*(3 + \[Omega][{{X, X}, {Y, Y}}])*(3*M^2 + 2*(-6 + M)*M*
           \[Omega][{{X, X}, {Y, Y}}] + (12 - 4*M + M^2)*
           \[Omega][{{X, X}, {Y, Y}}]^2 - 2*M*\[Omega][{{X, X}, {Y, Y}}]^3) + 
        2*\[Omega][{{Y}, {X, X, Y}}]^2*(3*M*(18 + M) + 2*(-126 + 33*M + M^2)*
           \[Omega][{{X, X}, {Y, Y}}] + M*(40 + M)*\[Omega][{{X, X}, {Y, Y}}]^
            2 + 2*M*\[Omega][{{X, X}, {Y, Y}}]^3) + 
        \[Omega][{{Y}, {X, X, Y}}]*(27*M*(4 + M) + 27*(-8 + M^2)*
           \[Omega][{{X, X}, {Y, Y}}] + (180 + 48*M + 11*M^2)*
           \[Omega][{{X, X}, {Y, Y}}]^2 + (36 + 2*M + M^2)*
           \[Omega][{{X, X}, {Y, Y}}]^3 - 2*M*\[Omega][{{X, X}, {Y, Y}}]^4) + 
        2*\[Omega][{{X}, {X, Y, Y}}]^2*(3*M*(18 + M) + 2*(-18 - 12*M + M^2)*
           \[Omega][{{X, X}, {Y, Y}}] + (-36 - 26*M + M^2)*
           \[Omega][{{X, X}, {Y, Y}}]^2 - 4*M*\[Omega][{{X, X}, {Y, Y}}]^3 + 
          8*\[Omega][{{Y}, {X, X, Y}}]^2*(18 - M + 
            M*\[Omega][{{X, X}, {Y, Y}}]) + 2*\[Omega][{{Y}, {X, X, Y}}]*
           (36 + 36*M - M^2 + (-10 + M)*M*\[Omega][{{X, X}, {Y, Y}}] - 
            2*M*\[Omega][{{X, X}, {Y, Y}}]^2)) + \[Omega][{{X}, {X, Y, Y}}]*
         (27*M*(4 + M) + 27*(-8 - 4*M + M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
          (-36 - 132*M + 11*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          (36 - 34*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]^3 - 
          2*M*\[Omega][{{X, X}, {Y, Y}}]^4 + 32*\[Omega][{{Y}, {X, X, Y}}]^3*
           (9 - M + M*\[Omega][{{X, X}, {Y, Y}}]) + 
          4*\[Omega][{{Y}, {X, X, Y}}]^2*(252 - M^2 + (-36 + 38*M + M^2)*
             \[Omega][{{X, X}, {Y, Y}}] + 4*M*\[Omega][{{X, X}, {Y, Y}}]^2) + 
          4*\[Omega][{{Y}, {X, X, Y}}]*(36*(3 + 2*M) + (-108 + 27*M + 5*M^2)*
             \[Omega][{{X, X}, {Y, Y}}] + (-18 + M + M^2)*
             \[Omega][{{X, X}, {Y, Y}}]^2 - M*\[Omega][{{X, X}, {Y, Y}}]^
              3))) + 2*\[Lambda][{y}]^2*(32*M*\[Omega][{{X}, {X, Y, Y}}]^4*
         (3 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]) + 
        32*\[Omega][{{Y}, {X, X, Y}}]^4*(3*M + 2*(-15 + M)*
           \[Omega][{{X, X}, {Y, Y}}] + 2*M*\[Omega][{{X, X}, {Y, Y}}]^2) + 
        8*\[Omega][{{Y}, {X, X, Y}}]^3*(2*M*(27 + 2*M) + 
          (-348 + 5*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 2*(36 + 9*M + 2*M^2)*
           \[Omega][{{X, X}, {Y, Y}}]^2 - 8*M*\[Omega][{{X, X}, {Y, Y}}]^3) + 
        3*M*(3 + \[Omega][{{X, X}, {Y, Y}}])*(M^2 + 3*(-4 + M)*M*
           \[Omega][{{X, X}, {Y, Y}}] + 2*(10 - 11*M + M^2)*
           \[Omega][{{X, X}, {Y, Y}}]^2 - 4*(-8 + 3*M)*
           \[Omega][{{X, X}, {Y, Y}}]^3 + 16*\[Omega][{{X, X}, {Y, Y}}]^4) + 
        2*\[Omega][{{Y}, {X, X, Y}}]^2*(M*(216 + 72*M + M^2) + 
          (144 - 804*M + 118*M^2 + 3*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
          2*(804 - 178*M + 30*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}]^2 - 
          4*(-30 + 45*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]^3 - 
          8*M*\[Omega][{{X, X}, {Y, Y}}]^4) + \[Omega][{{Y}, {X, X, Y}}]*
         (9*M^2*(16 + M) + (-792*M + 38*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
          (-144 - 204*M - 82*M^2 + 21*M^3)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          2*(-456 - 102*M - 41*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}]^3 - 
          4*(36 - 8*M + 3*M^2)*\[Omega][{{X, X}, {Y, Y}}]^4 + 
          16*M*\[Omega][{{X, X}, {Y, Y}}]^5) + 
        16*\[Omega][{{X}, {X, Y, Y}}]^3*(M*(27 + 2*M) + (-30 + 21*M + 2*M^2)*
           \[Omega][{{X, X}, {Y, Y}}] + (-30 - 2*M + M^2)*
           \[Omega][{{X, X}, {Y, Y}}]^2 - 2*M*\[Omega][{{X, X}, {Y, Y}}]^3 + 
          8*\[Omega][{{Y}, {X, X, Y}}]^2*(15 - M + 
            M*\[Omega][{{X, X}, {Y, Y}}]) + 2*\[Omega][{{Y}, {X, X, Y}}]*
           (30 + 23*M - M^2 + M*(8 + M)*\[Omega][{{X, X}, {Y, Y}}])) + 
        2*\[Omega][{{X}, {X, Y, Y}}]^2*(M*(216 + 72*M + M^2) + 
          (-720 + 60*M + 106*M^2 + 3*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
          2*(-60 - 136*M + 11*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}]^2 - 
          4*(-66 + 13*M + 2*M^2)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
          8*M*\[Omega][{{X, X}, {Y, Y}}]^4 + 32*\[Omega][{{Y}, {X, X, Y}}]^3*
           (60 - 7*M + 4*M*\[Omega][{{X, X}, {Y, Y}}]) + 
          8*\[Omega][{{Y}, {X, X, Y}}]^2*(408 + 62*M - 9*M^2 + 
            2*(-66 + 31*M + 3*M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
            4*M*\[Omega][{{X, X}, {Y, Y}}]^2) + 2*\[Omega][{{Y}, {X, X, Y}}]*
           (720 + 432*M + 22*M^2 - 3*M^3 + 2*(-348 + 102*M + 32*M^2 + M^3)*
             \[Omega][{{X, X}, {Y, Y}}] + 4*(-60 - 11*M + M^2)*
             \[Omega][{{X, X}, {Y, Y}}]^2 - 16*M*\[Omega][{{X, X}, {Y, Y}}]^
              3)) + \[Omega][{{X}, {X, Y, Y}}]*(9*M^2*(16 + M) + 
          2*M*(-180 + 36*M + 19*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
          (720 - 132*M - 166*M^2 + 21*M^3)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          2*(120 + 138*M - 53*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}]^3 - 
          4*(36 - 32*M + 3*M^2)*\[Omega][{{X, X}, {Y, Y}}]^4 + 
          16*M*\[Omega][{{X, X}, {Y, Y}}]^5 + 128*\[Omega][{{Y}, {X, X, Y}}]^
            4*(15 - 2*M + M*\[Omega][{{X, X}, {Y, Y}}]) + 
          16*\[Omega][{{Y}, {X, X, Y}}]^3*(348 + 28*M - 7*M^2 + 
            4*(-48 + 13*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]) + 
          4*\[Omega][{{Y}, {X, X, Y}}]^2*(-144 + 936*M - 38*M^2 - 3*M^3 + 
            2*(-1212 + 108*M + 45*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
            8*(3 - 12*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 - 
            24*M*\[Omega][{{X, X}, {Y, Y}}]^3) + 4*\[Omega][{{Y}, {X, X, Y}}]*
           (2*M*(90 + 69*M - 4*M^2) + (-288 - 600*M + 154*M^2 + 9*M^3)*
             \[Omega][{{X, X}, {Y, Y}}] + (744 - 362*M + 5*M^2 + 2*M^3)*
             \[Omega][{{X, X}, {Y, Y}}]^2 - 2*(-96 + 34*M + 3*M^2)*
             \[Omega][{{X, X}, {Y, Y}}]^3))) - \[Lambda][{y}]*
       (9*M^4 + 72*M^3*\[Omega][{{Y}, {X, X, Y}}] + 
        9*M^4*\[Omega][{{Y}, {X, X, Y}}] + 144*M^2*\[Omega][{{Y}, {X, X, Y}}]^
          2 + 72*M^3*\[Omega][{{Y}, {X, X, Y}}]^2 + 
        2*M^4*\[Omega][{{Y}, {X, X, Y}}]^2 + 
        144*M^2*\[Omega][{{Y}, {X, X, Y}}]^3 + 
        16*M^3*\[Omega][{{Y}, {X, X, Y}}]^3 + 
        32*M^2*\[Omega][{{Y}, {X, X, Y}}]^4 - 
        36*M^3*\[Omega][{{X, X}, {Y, Y}}] + 
        12*M^4*\[Omega][{{X, X}, {Y, Y}}] - 
        432*M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
        120*M^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
        8*M^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
        2304*M*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
        576*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
        72*M^3*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
        2*M^4*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
        4608*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
        1344*M*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
        272*M^2*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
        24*M^3*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
        768*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
        640*M*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
        96*M^2*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
        768*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] + 
        128*M*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] + 
        144*M^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
        66*M^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
        3*M^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
        2448*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
        696*M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
        4*M^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
        M^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
        7488*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
        2352*M*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
        40*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
        2496*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
        224*M*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
        48*M^2*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
        768*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
        128*M*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
        648*M*\[Omega][{{X, X}, {Y, Y}}]^3 + 
        120*M^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
        18*M^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 4320*\[Omega][{{Y}, {X, X, Y}}]*
         \[Omega][{{X, X}, {Y, Y}}]^3 + 1176*M*\[Omega][{{Y}, {X, X, Y}}]*
         \[Omega][{{X, X}, {Y, Y}}]^3 - 160*M^2*\[Omega][{{Y}, {X, X, Y}}]*
         \[Omega][{{X, X}, {Y, Y}}]^3 - 6*M^3*\[Omega][{{Y}, {X, X, Y}}]*
         \[Omega][{{X, X}, {Y, Y}}]^3 + 2208*\[Omega][{{Y}, {X, X, Y}}]^2*
         \[Omega][{{X, X}, {Y, Y}}]^3 - 496*M*\[Omega][{{Y}, {X, X, Y}}]^2*
         \[Omega][{{X, X}, {Y, Y}}]^3 - 32*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*
         \[Omega][{{X, X}, {Y, Y}}]^3 - 192*\[Omega][{{Y}, {X, X, Y}}]^3*
         \[Omega][{{X, X}, {Y, Y}}]^3 - 32*M*\[Omega][{{Y}, {X, X, Y}}]^3*
         \[Omega][{{X, X}, {Y, Y}}]^3 + 864*\[Omega][{{X, X}, {Y, Y}}]^4 - 
        216*M*\[Omega][{{X, X}, {Y, Y}}]^4 + 
        24*M^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 1152*\[Omega][{{Y}, {X, X, Y}}]*
         \[Omega][{{X, X}, {Y, Y}}]^4 + 192*M*\[Omega][{{Y}, {X, X, Y}}]*
         \[Omega][{{X, X}, {Y, Y}}]^4 + 8*M^2*\[Omega][{{Y}, {X, X, Y}}]*
         \[Omega][{{X, X}, {Y, Y}}]^4 + 192*\[Omega][{{Y}, {X, X, Y}}]^2*
         \[Omega][{{X, X}, {Y, Y}}]^4 + 32*M*\[Omega][{{Y}, {X, X, Y}}]^2*
         \[Omega][{{X, X}, {Y, Y}}]^4 + 288*\[Omega][{{X, X}, {Y, Y}}]^5 + 
        32*\[Omega][{{X}, {X, Y, Y}}]^4*(M^2 + 2*(-12 - 2*M + M^2)*
           \[Omega][{{Y}, {X, X, Y}}] + 8*(-6 + M)*\[Omega][{{Y}, {X, X, Y}}]^
            2 + (12 - 6*M + M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
          2*(-6 + M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
        16*\[Omega][{{X}, {X, Y, Y}}]^3*(M^2*(9 + M) + 
          48*(-6 + M)*\[Omega][{{Y}, {X, X, Y}}]^3 + 
          (72 - 30*M + 5*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
          (48 + 4*M - 3*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          2*(-18 + M)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
          4*\[Omega][{{Y}, {X, X, Y}}]^2*(-48 - 16*M + 5*M^2 - 
            2*(-18 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
          2*\[Omega][{{Y}, {X, X, Y}}]*(-72 + 6*M + 3*M^2 + M^3 + 
            2*M*(6 + M)*\[Omega][{{X, X}, {Y, Y}}] - 6*(-6 + M)*
             \[Omega][{{X, X}, {Y, Y}}]^2)) + 2*\[Omega][{{X}, {X, Y, Y}}]^2*
         (M^2*(72 + 36*M + M^2) + 384*(-6 + M)*\[Omega][{{Y}, {X, X, Y}}]^4 + 
          M*(-432 + 48*M + 32*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}] - 
          2*(-288 + 72*M + 34*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}]^2 - 
          8*(30 + 7*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
          16*(6 + M)*\[Omega][{{X, X}, {Y, Y}}]^4 + 
          32*\[Omega][{{Y}, {X, X, Y}}]^3*(-60 - 26*M + 7*M^2 - 
            2*(-42 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
          8*\[Omega][{{Y}, {X, X, Y}}]^2*(432 - 180*M + 16*M^2 + 5*M^3 + 
            4*(90 + 31*M + M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
            4*(-6 + 5*M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
          2*\[Omega][{{Y}, {X, X, Y}}]*(M*(432 - 12*M + 24*M^2 + M^3) + 
            4*(-360 + 150*M + 23*M^2 + 2*M^3)*\[Omega][{{X, X}, {Y, Y}}] - 
            4*(60 + 6*M + 7*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
            8*(-42 + M)*\[Omega][{{X, X}, {Y, Y}}]^3)) + 
        \[Omega][{{X}, {X, Y, Y}}]*(9*M^3*(8 + M) + 256*(-6 + M)*
           \[Omega][{{Y}, {X, X, Y}}]^5 + 4*M^2*(-108 + 27*M + 2*M^2)*
           \[Omega][{{X, X}, {Y, Y}}] + M*(1728 - 600*M - 8*M^2 + M^3)*
           \[Omega][{{X, X}, {Y, Y}}]^2 - 2*(1296 - 468*M + 56*M^2 + 3*M^3)*
           \[Omega][{{X, X}, {Y, Y}}]^3 + 8*(-72 + 24*M + M^2)*
           \[Omega][{{X, X}, {Y, Y}}]^4 + 64*\[Omega][{{Y}, {X, X, Y}}]^4*
           (3*(-8 - 4*M + M^2) + 2*(30 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
          16*\[Omega][{{Y}, {X, X, Y}}]^3*(576 - 192*M + 14*M^2 + 3*M^3 + 
            8*(54 + 16*M + M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
            4*(18 + 5*M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
          4*\[Omega][{{Y}, {X, X, Y}}]^2*(M*(1152 - 204*M + 36*M^2 + M^3) + 
            2*(-2304 + 936*M + 46*M^2 + 5*M^3)*\[Omega][{{X, X}, {Y, Y}}] - 
            4*(408 + 24*M + 7*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 - 
            8*(30 + M)*\[Omega][{{X, X}, {Y, Y}}]^3) + 
          4*\[Omega][{{Y}, {X, X, Y}}]*(4*M^2*(54 + M^2) + 
            M*(-1440 + 432*M + 16*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}] - 
            (-3024 + 1164*M + 20*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}]^2 - 
            4*(-180 + 38*M + 3*M^2)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
            16*(6 + M)*\[Omega][{{X, X}, {Y, Y}}]^4)))))/
    (E^((T*(M + 6*\[Lambda][{y}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}]))/2)*(M - 6*\[Lambda][{y}])*
     (2 + M - 4*\[Lambda][{y}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
      \[Omega][{{Y}, {X, X, Y}}])*(M + 6*\[Lambda][{y}] + 
      4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
     (M + 4*\[Lambda][{y}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
      4*\[Omega][{{Y}, {X, X, Y}}] - 4*\[Omega][{{X, X}, {Y, Y}}])*
     (M + 4*\[Omega][{{X}, {X, Y, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
     (M + 4*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
     (2*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
      \[Omega][{{X, X}, {Y, Y}}])*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
      \[Omega][{{X, X}, {Y, Y}}])*(3 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
      \[Omega][{{X, X}, {Y, Y}}])*(1 + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
   (2*M*\[Lambda][{y}]*(-((\[Lambda][{y}]*(648*\[Lambda][{y}]^6 + 
          108*\[Lambda][{y}]^5*(6 + 13*M + M^2 + 
            34*\[Omega][{{X}, {X, Y, Y}}] + 22*\[Omega][{{Y}, {X, X, Y}}] + 
            22*\[Omega][{{X, X}, {Y, Y}}] + 4*M*\[Omega][{{X, X}, {Y, Y}}]) + 
          6*\[Lambda][{y}]^4*(90*M + 143*M^2 + 20*M^3 + 
            1248*\[Omega][{{X}, {X, Y, Y}}]^2 + 
            456*\[Omega][{{Y}, {X, X, Y}}]^2 + 
            252*\[Omega][{{X, X}, {Y, Y}}] + 602*M*\[Omega][{{X, X}, {Y, 
                Y}}] + 112*M^2*\[Omega][{{X, X}, {Y, Y}}] + 
            516*\[Omega][{{X, X}, {Y, Y}}]^2 + 
            128*M*\[Omega][{{X, X}, {Y, Y}}]^2 + 6*\[Omega][{{Y}, {X, X, Y}}]*
             (30 + 87*M + 11*M^2 + 2*(83 + 22*M)*\[Omega][{{X, X}, 
                 {Y, Y}}]) + 2*\[Omega][{{X}, {X, Y, Y}}]*(198 + 459*M + 
              17*M^2 + 876*\[Omega][{{Y}, {X, X, Y}}] + (858 + 68*M)*\[Omega][
                {{X, X}, {Y, Y}}])) + \[Lambda][{y}]^3*(126*M^2 + 183*M^3 + 
            39*M^4 + 6720*\[Omega][{{X}, {X, Y, Y}}]^3 + 
            1248*\[Omega][{{Y}, {X, X, Y}}]^3 + 828*M*\[Omega][
              {{X, X}, {Y, Y}}] + 1438*M^2*\[Omega][{{X, X}, {Y, Y}}] + 
            304*M^3*\[Omega][{{X, X}, {Y, Y}}] + 
            1080*\[Omega][{{X, X}, {Y, Y}}]^2 + 2980*M*
             \[Omega][{{X, X}, {Y, Y}}]^2 + 704*M^2*\[Omega][{{X, X}, 
                {Y, Y}}]^2 + 1656*\[Omega][{{X, X}, {Y, Y}}]^3 + 
            448*M*\[Omega][{{X, X}, {Y, Y}}]^3 + 8*\[Omega][{{X}, {X, Y, Y}}]^
              2*(342 + 894*M + 13*M^2 + 2016*\[Omega][{{Y}, {X, X, Y}}] + 
              4*(489 + 13*M)*\[Omega][{{X, X}, {Y, Y}}]) + 
            24*\[Omega][{{Y}, {X, X, Y}}]^2*(24 + 103*M + 19*M^2 + 
              4*(55 + 19*M)*\[Omega][{{X, X}, {Y, Y}}]) + 
            2*\[Omega][{{Y}, {X, X, Y}}]*(M*(378 + 689*M + 140*M^2) + 
              2*(522 + 1525*M + 386*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
              4*(675 + 212*M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
            2*\[Omega][{{X}, {X, Y, Y}}]*(M*(630 + 1019*M + 80*M^2) + 
              5040*\[Omega][{{Y}, {X, X, Y}}]^2 + (2052 + 4946*M + 488*M^2)*
               \[Omega][{{X, X}, {Y, Y}}] + 84*(57 + 8*M)*\[Omega][
                 {{X, X}, {Y, Y}}]^2 + 4*\[Omega][{{Y}, {X, X, Y}}]*(450 + 
                1209*M + 76*M^2 + 4*(609 + 76*M)*\[Omega][{{X, X}, 
                   {Y, Y}}]))) + (\[Omega][{{X}, {X, Y, Y}}] + 
            \[Omega][{{Y}, {X, X, Y}}])*(2*\[Omega][{{X}, {X, Y, Y}}] + 
            \[Omega][{{X, X}, {Y, Y}}])*(32*\[Omega][{{X}, {X, Y, Y}}]^3*
             (M + 4*\[Omega][{{Y}, {X, X, Y}}] + 6*\[Omega][{{X, X}, 
                 {Y, Y}}]) + (M + 4*\[Omega][{{Y}, {X, X, Y}}])*
             (M + 2*\[Omega][{{X, X}, {Y, Y}}])*(M*(1 + M) + 
              8*\[Omega][{{Y}, {X, X, Y}}]^2 + (6 + 8*M)*\[Omega][
                {{X, X}, {Y, Y}}] + 8*\[Omega][{{X, X}, {Y, Y}}]^2 + 
              2*\[Omega][{{Y}, {X, X, Y}}]*(2 + 3*M + 6*\[Omega][{{X, X}, 
                   {Y, Y}}])) + 16*\[Omega][{{X}, {X, Y, Y}}]^2*
             (M*(1 + 2*M) + 16*\[Omega][{{Y}, {X, X, Y}}]^2 + 
              3*(2 + 5*M)*\[Omega][{{X, X}, {Y, Y}}] + 
              14*\[Omega][{{X, X}, {Y, Y}}]^2 + 4*\[Omega][{{Y}, {X, X, Y}}]*(
                1 + 3*M + 7*\[Omega][{{X, X}, {Y, Y}}])) + 
            2*\[Omega][{{X}, {X, Y, Y}}]*(M^2*(4 + 5*M) + 
              64*\[Omega][{{Y}, {X, X, Y}}]^3 + 4*M*(7 + 11*M)*\[Omega][
                {{X, X}, {Y, Y}}] + 4*(6 + 19*M)*\[Omega][{{X, X}, {Y, Y}}]^
                2 + 32*\[Omega][{{X, X}, {Y, Y}}]^3 + 
              32*\[Omega][{{Y}, {X, X, Y}}]^2*(1 + 3*M + 5*\[Omega][
                  {{X, X}, {Y, Y}}]) + 8*\[Omega][{{Y}, {X, X, Y}}]*(
                M*(3 + 5*M) + (8 + 22*M)*\[Omega][{{X, X}, {Y, Y}}] + 
                20*\[Omega][{{X, X}, {Y, Y}}]^2))) + \[Lambda][{y}]^2*
           (9*M^3 + 12*M^4 + 3*M^5 + 2688*\[Omega][{{X}, {X, Y, Y}}]^4 + 
            192*\[Omega][{{Y}, {X, X, Y}}]^4 + 114*M^2*\[Omega][
              {{X, X}, {Y, Y}}] + 175*M^3*\[Omega][{{X, X}, {Y, Y}}] + 
            40*M^4*\[Omega][{{X, X}, {Y, Y}}] + 
            324*M*\[Omega][{{X, X}, {Y, Y}}]^2 + 660*M^2*
             \[Omega][{{X, X}, {Y, Y}}]^2 + 172*M^3*\[Omega][{{X, X}, 
                {Y, Y}}]^2 + 216*\[Omega][{{X, X}, {Y, Y}}]^3 + 
            836*M*\[Omega][{{X, X}, {Y, Y}}]^3 + 272*M^2*
             \[Omega][{{X, X}, {Y, Y}}]^3 + 288*\[Omega][{{X, X}, {Y, Y}}]^
              4 + 128*M*\[Omega][{{X, X}, {Y, Y}}]^4 + 
            16*\[Omega][{{X}, {X, Y, Y}}]^3*(78 + 246*M + M^2 + 
              664*\[Omega][{{Y}, {X, X, Y}}] + 4*(167 + M)*\[Omega][
                {{X, X}, {Y, Y}}]) + 16*\[Omega][{{Y}, {X, X, Y}}]^3*
             (6 + 51*M + 13*M^2 + 2*(61 + 26*M)*\[Omega][{{X, X}, {Y, Y}}]) + 
            4*\[Omega][{{Y}, {X, X, Y}}]^2*(M*(84 + 175*M + 44*M^2) + 
              (228 + 840*M + 242*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
              4*(205 + 66*M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
            \[Omega][{{Y}, {X, X, Y}}]*(M^2*(114 + 175*M + 43*M^2) + 
              6*M*(138 + 259*M + 60*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
              4*(306 + 891*M + 223*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
              8*(261 + 70*M)*\[Omega][{{X, X}, {Y, Y}}]^3) + 
            8*\[Omega][{{X}, {X, Y, Y}}]^2*(M*(123 + 208*M + 8*M^2) + 
              1456*\[Omega][{{Y}, {X, X, Y}}]^2 + (474 + 1184*M + 51*M^2)*
               \[Omega][{{X, X}, {Y, Y}}] + 4*(314 + 19*M)*\[Omega][
                 {{X, X}, {Y, Y}}]^2 + 8*\[Omega][{{Y}, {X, X, Y}}]*(54 + 
                148*M + 4*M^2 + (333 + 16*M)*\[Omega][{{X, X}, {Y, Y}}])) + 
            \[Omega][{{X}, {X, Y, Y}}]*(M^2*(198 + 265*M + 31*M^2) + 
              3904*\[Omega][{{Y}, {X, X, Y}}]^3 + 2*M*(726 + 1197*M + 
                136*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 4*(486 + 1341*M + 
                187*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 24*(135 + 26*M)*
               \[Omega][{{X, X}, {Y, Y}}]^3 + 16*\[Omega][{{Y}, {X, X, Y}}]^
                2*(108 + 355*M + 28*M^2 + 2*(393 + 56*M)*\[Omega][
                  {{X, X}, {Y, Y}}]) + 4*\[Omega][{{Y}, {X, X, Y}}]*(
                3*M*(114 + 197*M + 20*M^2) + 4*(276 + 751*M + 92*M^2)*
                 \[Omega][{{X, X}, {Y, Y}}] + 4*(761 + 128*M)*
                 \[Omega][{{X, X}, {Y, Y}}]^2))) + \[Lambda][{y}]*
           (384*\[Omega][{{X}, {X, Y, Y}}]^5 + 64*\[Omega][{{X}, {X, Y, Y}}]^
              4*(3 + 14*M + 46*\[Omega][{{Y}, {X, X, Y}}] + 52*\[Omega][
                {{X, X}, {Y, Y}}]) + 32*\[Omega][{{Y}, {X, X, Y}}]^4*
             (M*(3 + M) + 4*(2 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
            16*\[Omega][{{Y}, {X, X, Y}}]^3*(M*(3 + 7*M + 2*M^2) + 
              (8 + 42*M + 11*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
              2*(25 + 6*M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
            2*\[Omega][{{Y}, {X, X, Y}}]^2*(M^2*(12 + 19*M + 5*M^2) + 
              2*M*(58 + 113*M + 21*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
              8*(24 + 74*M + 13*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
              16*(25 + 4*M)*\[Omega][{{X, X}, {Y, Y}}]^3) + 
            M*\[Omega][{{X, X}, {Y, Y}}]*(M^2*(3 + 4*M + M^2) + 
              M*(24 + 38*M + 9*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
              4*(9 + 23*M + 7*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
              4*(16 + 9*M)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
              16*\[Omega][{{X, X}, {Y, Y}}]^4) + \[Omega][{{Y}, {X, X, Y}}]*
             (M^3*(3 + 4*M + M^2) + M^2*(62 + 91*M + 16*M^2)*\[Omega][
                {{X, X}, {Y, Y}}] + 4*M*(57 + 107*M + 19*M^2)*\[Omega][
                 {{X, X}, {Y, Y}}]^2 + 4*(54 + 165*M + 32*M^2)*\[Omega][
                 {{X, X}, {Y, Y}}]^3 + 32*(9 + 2*M)*\[Omega][{{X, X}, 
                  {Y, Y}}]^4) + 8*\[Omega][{{X}, {X, Y, Y}}]^3*
             (M*(38 + 69*M + M^2) + 656*\[Omega][{{Y}, {X, X, Y}}]^2 + 
              2*(90 + 233*M + 3*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
              (540 + 8*M)*\[Omega][{{X, X}, {Y, Y}}]^2 + 4*\[Omega][
                {{Y}, {X, X, Y}}]*(38 + 113*M + M^2 + 4*(73 + M)*
                 \[Omega][{{X, X}, {Y, Y}}])) + 2*\[Omega][{{X}, {X, Y, Y}}]^
              2*(M^2*(50 + 64*M + 3*M^2) + 1600*\[Omega][{{Y}, {X, X, Y}}]^
                3 + 2*M*(194 + 315*M + 15*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
              4*(138 + 385*M + 24*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
              16*(61 + 6*M)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
              16*\[Omega][{{Y}, {X, X, Y}}]^2*(40 + 120*M + 3*M^2 + 
                4*(70 + 3*M)*\[Omega][{{X, X}, {Y, Y}}]) + 8*\[Omega][
                {{Y}, {X, X, Y}}]*(M*(48 + 82*M + 3*M^2) + (170 + 461*M + 
                  21*M^2)*\[Omega][{{X, X}, {Y, Y}}] + (518 + 36*M)*
                 \[Omega][{{X, X}, {Y, Y}}]^2)) + \[Omega][{{X}, {X, Y, Y}}]*
             (M^3*(9 + 10*M + M^2) + 512*\[Omega][{{Y}, {X, X, Y}}]^4 + 
              M^2*(110 + 151*M + 18*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
              4*M*(87 + 161*M + 23*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
              12*(18 + 71*M + 14*M^2)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
              96*(3 + M)*\[Omega][{{X, X}, {Y, Y}}]^4 + 
              32*\[Omega][{{Y}, {X, X, Y}}]^3*(8 + 38*M + 3*M^2 + 
                4*(25 + 3*M)*\[Omega][{{X, X}, {Y, Y}}]) + 
              8*\[Omega][{{Y}, {X, X, Y}}]^2*(M*(52 + 97*M + 9*M^2) + 
                2*(88 + 270*M + 27*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
                (596 + 72*M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
              2*\[Omega][{{Y}, {X, X, Y}}]*(M^2*(62 + 83*M + 8*M^2) + 
                16*M*(30 + 51*M + 5*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
                4*(186 + 517*M + 60*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
                32*(43 + 6*M)*\[Omega][{{X, X}, {Y, Y}}]^3)))))/
        (\[Lambda][{y}] + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
      (648*\[Lambda][{y}]^6*(1 + 2*\[Omega][{{X, X}, {Y, Y}}]) + 
        (M + 4*\[Omega][{{X}, {X, Y, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
         (M + 4*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
         (2*\[Omega][{{X}, {X, Y, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
         (\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] - 
          \[Omega][{{X, X}, {Y, Y}}])*(M + M^2 + 8*\[Omega][{{X}, {X, Y, Y}}]^
            2 + 8*\[Omega][{{Y}, {X, X, Y}}]^2 + 2*\[Omega][{{Y}, {X, X, Y}}]*
           (2 + 3*M - 8*\[Omega][{{X, X}, {Y, Y}}]) + 
          2*\[Omega][{{X}, {X, Y, Y}}]*(2 + 3*M + 
            8*\[Omega][{{Y}, {X, X, Y}}] - 8*\[Omega][{{X, X}, {Y, Y}}]) - 
          4*\[Omega][{{X, X}, {Y, Y}}] - 6*M*\[Omega][{{X, X}, {Y, Y}}] + 
          8*\[Omega][{{X, X}, {Y, Y}}]^2) + 4*\[Lambda][{y}]^5*
         (162 + 351*M + 27*M^2 + 594*\[Omega][{{X, X}, {Y, Y}}] + 
          830*M*\[Omega][{{X, X}, {Y, Y}}] + 
          64*M^2*\[Omega][{{X, X}, {Y, Y}}] + 500*\[Omega][{{X, X}, {Y, Y}}]^
            2 + 196*M*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          2*\[Omega][{{X}, {X, Y, Y}}]*(459 + 958*\[Omega][{{X, X}, {Y, 
                Y}}]) + 2*\[Omega][{{Y}, {X, X, Y}}]*
           (297 + (594 + 20*M)*\[Omega][{{X, X}, {Y, Y}}])) + 
        2*\[Lambda][{y}]^4*(270*M + 429*M^2 + 60*M^3 + 
          108*\[Omega][{{X, X}, {Y, Y}}] + 862*M*\[Omega][{{X, X}, {Y, Y}}] + 
          1036*M^2*\[Omega][{{X, X}, {Y, Y}}] + 
          136*M^3*\[Omega][{{X, X}, {Y, Y}}] + 844*\[Omega][{{X, X}, {Y, Y}}]^
            2 + 780*M*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          314*M^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          1088*\[Omega][{{X, X}, {Y, Y}}]^3 + 
          124*M*\[Omega][{{X, X}, {Y, Y}}]^3 + 32*\[Omega][{{X}, {X, Y, Y}}]^
            2*(117 + 236*\[Omega][{{X, X}, {Y, Y}}]) + 
          8*\[Omega][{{Y}, {X, X, Y}}]^2*(171 + (342 + 34*M)*
             \[Omega][{{X, X}, {Y, Y}}]) + 6*\[Omega][{{Y}, {X, X, Y}}]*
           (90 + 261*M + 33*M^2 + (282 + 650*M + 88*M^2)*
             \[Omega][{{X, X}, {Y, Y}}] + 4*(53 + 42*M)*
             \[Omega][{{X, X}, {Y, Y}}]^2) + 2*\[Omega][{{X}, {X, Y, Y}}]*
           (594 + 1377*M + 51*M^2 + 2*(883 + 1433*M + 69*M^2)*
             \[Omega][{{X, X}, {Y, Y}}] + 12*(109 + 28*M)*
             \[Omega][{{X, X}, {Y, Y}}]^2 + 12*\[Omega][{{Y}, {X, X, Y}}]*
             (219 + 2*(217 + 6*M)*\[Omega][{{X, X}, {Y, Y}}]))) + 
        \[Lambda][{y}]^3*(126*M^2 + 183*M^3 + 39*M^4 - 
          172*M*\[Omega][{{X, X}, {Y, Y}}] - 
          26*M^2*\[Omega][{{X, X}, {Y, Y}}] + 316*M^3*
           \[Omega][{{X, X}, {Y, Y}}] + 60*M^4*\[Omega][{{X, X}, {Y, Y}}] + 
          488*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          1412*M*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          162*M^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          96*M^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 8*\[Omega][{{X, X}, {Y, Y}}]^
            3 + 1916*M*\[Omega][{{X, X}, {Y, Y}}]^3 + 
          284*M^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
          288*\[Omega][{{X, X}, {Y, Y}}]^4 + 152*M*\[Omega][{{X, X}, {Y, Y}}]^
            4 + 960*\[Omega][{{X}, {X, Y, Y}}]^3*
           (7 + 13*\[Omega][{{X, X}, {Y, Y}}]) + 
          32*\[Omega][{{Y}, {X, X, Y}}]^3*(39 + 2*(39 + 8*M)*
             \[Omega][{{X, X}, {Y, Y}}]) + 8*\[Omega][{{Y}, {X, X, Y}}]^2*
           (72 + 309*M + 57*M^2 + 2*(60 + 371*M + 68*M^2)*
             \[Omega][{{X, X}, {Y, Y}}] + 80*(1 + 2*M)*
             \[Omega][{{X, X}, {Y, Y}}]^2) + 2*\[Omega][{{Y}, {X, X, Y}}]*
           (M*(378 + 689*M + 140*M^2) + 2*(-18 + 241*M + 703*M^2 + 125*M^3)*
             \[Omega][{{X, X}, {Y, Y}}] + 4*(407 + 72*M + 85*M^2)*
             \[Omega][{{X, X}, {Y, Y}}]^2 + 8*(246 + 23*M)*
             \[Omega][{{X, X}, {Y, Y}}]^3) + 8*\[Omega][{{X}, {X, Y, Y}}]^2*
           (342 + 894*M + 13*M^2 + 4*(164 + 405*M + 10*M^2)*
             \[Omega][{{X, X}, {Y, Y}}] + (504 + 76*M)*
             \[Omega][{{X, X}, {Y, Y}}]^2 + 8*\[Omega][{{Y}, {X, X, Y}}]*
             (252 + (457 + 7*M)*\[Omega][{{X, X}, {Y, Y}}])) + 
          2*\[Omega][{{X}, {X, Y, Y}}]*(M*(630 + 1019*M + 80*M^2) + 
            2*(-82 + 333*M + 972*M^2 + 79*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
            4*(625 + 176*M + 77*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
            48*(62 + 5*M)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
            16*\[Omega][{{Y}, {X, X, Y}}]^2*(315 + (566 + 32*M)*\[Omega][
                {{X, X}, {Y, Y}}]) + 4*\[Omega][{{Y}, {X, X, Y}}]*
             (450 + 1209*M + 76*M^2 + 2*(298 + 1180*M + 91*M^2)*\[Omega][
                {{X, X}, {Y, Y}}] + 8*(37 + 35*M)*\[Omega][{{X, X}, {Y, Y}}]^
                2))) + \[Lambda][{y}]^2*(9*M^3 + 12*M^4 + 3*M^5 - 
          78*M^2*\[Omega][{{X, X}, {Y, Y}}] - 101*M^3*
           \[Omega][{{X, X}, {Y, Y}}] - 10*M^4*\[Omega][{{X, X}, {Y, Y}}] + 
          2*M^5*\[Omega][{{X, X}, {Y, Y}}] + 316*M*\[Omega][{{X, X}, {Y, Y}}]^
            2 + 552*M^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          51*M^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          M^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 296*\[Omega][{{X, X}, {Y, Y}}]^
            3 - 716*M*\[Omega][{{X, X}, {Y, Y}}]^3 + 
          314*M^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
          48*M^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
          464*\[Omega][{{X, X}, {Y, Y}}]^4 - 336*M*\[Omega][{{X, X}, {Y, Y}}]^
            4 + 20*M^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
          224*\[Omega][{{X, X}, {Y, Y}}]^5 + 16*M*\[Omega][{{X, X}, {Y, Y}}]^
            5 + 128*\[Omega][{{X}, {X, Y, Y}}]^4*
           (21 + 34*\[Omega][{{X, X}, {Y, Y}}]) + 
          64*\[Omega][{{Y}, {X, X, Y}}]^4*(3 + 2*(3 + M)*
             \[Omega][{{X, X}, {Y, Y}}]) + 16*\[Omega][{{Y}, {X, X, Y}}]^3*
           (6 + 51*M + 13*M^2 + 2*(-11 + 47*M + 9*M^2)*\[Omega][
              {{X, X}, {Y, Y}}] + 4*(-1 + 3*M)*\[Omega][{{X, X}, {Y, Y}}]^
              2) + 4*\[Omega][{{Y}, {X, X, Y}}]^2*(M*(84 + 175*M + 44*M^2) + 
            2*(-30 - 110*M + 90*M^2 + 21*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
            4*(167 - 28*M + 4*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
            8*(67 + 4*M)*\[Omega][{{X, X}, {Y, Y}}]^3) + 
          \[Omega][{{Y}, {X, X, Y}}]*(M^2*(114 + 175*M + 43*M^2) + 
            2*M*(-206 - 323*M + 21*M^2 + 17*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
            4*(206 + 647*M + 15*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
            8*(-159 + 260*M + 26*M^2)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
            16*(-41 + 4*M)*\[Omega][{{X, X}, {Y, Y}}]^4) + 
          16*\[Omega][{{X}, {X, Y, Y}}]^3*(78 + 246*M + M^2 + 
            (8 + 366*M + 4*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
            4*(18 + M)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
            8*\[Omega][{{Y}, {X, X, Y}}]*(83 + (124 + M)*\[Omega][
                {{X, X}, {Y, Y}}])) + 8*\[Omega][{{X}, {X, Y, Y}}]^2*
           (M*(123 + 208*M + 8*M^2) + (-114 - 218*M + 293*M^2 + 12*M^3)*
             \[Omega][{{X, X}, {Y, Y}}] + 2*(386 - 12*M + 9*M^2)*
             \[Omega][{{X, X}, {Y, Y}}]^2 + 8*(76 + 3*M)*
             \[Omega][{{X, X}, {Y, Y}}]^3 + 16*\[Omega][{{Y}, {X, X, Y}}]^2*
             (91 + 3*(44 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
            4*\[Omega][{{Y}, {X, X, Y}}]*(4*(27 + 74*M + 2*M^2) + 
              (-102 + 433*M + 15*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
              2*(-22 + 9*M)*\[Omega][{{X, X}, {Y, Y}}]^2)) + 
          \[Omega][{{X}, {X, Y, Y}}]*(M^2*(198 + 265*M + 31*M^2) + 
            2*M*(-318 - 419*M + 134*M^2 + 16*M^3)*\[Omega][{{X, X}, {Y, 
                Y}}] + 4*(322 + 953*M - 27*M^2 + 11*M^3)*
             \[Omega][{{X, X}, {Y, Y}}]^2 + 8*(-243 + 352*M + 27*M^2)*
             \[Omega][{{X, X}, {Y, Y}}]^3 + 16*(-65 + 6*M)*
             \[Omega][{{X, X}, {Y, Y}}]^4 + 64*\[Omega][{{Y}, {X, X, Y}}]^3*
             (61 + 6*(15 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
            16*\[Omega][{{Y}, {X, X, Y}}]^2*(108 + 355*M + 28*M^2 + 
              2*(-97 + 273*M + 21*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
              4*(-25 + 9*M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
            4*\[Omega][{{Y}, {X, X, Y}}]*(3*M*(114 + 197*M + 20*M^2) + 
              (-424 - 812*M + 772*M^2 + 70*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
              4*(605 - 72*M + 18*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
              8*(239 + 12*M)*\[Omega][{{X, X}, {Y, Y}}]^3))) + 
        \[Lambda][{y}]*(128*\[Omega][{{X}, {X, Y, Y}}]^5*
           (3 + 4*\[Omega][{{X, X}, {Y, Y}}]) + 32*\[Omega][{{Y}, {X, X, Y}}]^
            4*(M*(3 + M) + 2*(-2 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
          16*\[Omega][{{Y}, {X, X, Y}}]^3*(M*(3 + 7*M + 2*M^2) - 
            (4 + 30*M + 3*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
            (54 - 4*M)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
            20*\[Omega][{{X, X}, {Y, Y}}]^3) + 2*\[Omega][{{Y}, {X, X, Y}}]^2*
           (M^2*(12 + 19*M + 5*M^2) - 2*M*(46 + 93*M + 16*M^2)*
             \[Omega][{{X, X}, {Y, Y}}] + 4*(44 + 174*M + 13*M^2)*
             \[Omega][{{X, X}, {Y, Y}}]^2 + 8*(-76 + 27*M)*
             \[Omega][{{X, X}, {Y, Y}}]^3 - 112*\[Omega][{{X, X}, {Y, Y}}]^
              4) - \[Omega][{{X, X}, {Y, Y}}]*(M^3*(5 + 6*M + M^2) - 
            M^2*(46 + 62*M + 7*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
            2*M*(64 + 113*M + 5*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
            4*(-28 - 89*M + 5*M^2)*\[Omega][{{X, X}, {Y, Y}}]^3 - 
            8*(-26 + 7*M)*\[Omega][{{X, X}, {Y, Y}}]^4 + 
            32*\[Omega][{{X, X}, {Y, Y}}]^5) + \[Omega][{{Y}, {X, X, Y}}]*
           (M^3*(3 + 4*M + M^2) - M^2*(62 + 89*M + 16*M^2)*
             \[Omega][{{X, X}, {Y, Y}}] + 2*M*(154 + 286*M + 31*M^2)*
             \[Omega][{{X, X}, {Y, Y}}]^2 + 4*(-90 - 289*M + 13*M^2)*
             \[Omega][{{X, X}, {Y, Y}}]^3 + (880 - 208*M)*
             \[Omega][{{X, X}, {Y, Y}}]^4 + 160*\[Omega][{{X, X}, {Y, Y}}]^
              5) + 64*\[Omega][{{X}, {X, Y, Y}}]^4*(3 + 14*M + 
            2*(-6 + 7*M)*\[Omega][{{X, X}, {Y, Y}}] + 
            2*\[Omega][{{X, X}, {Y, Y}}]^2 + \[Omega][{{Y}, {X, X, Y}}]*
             (46 + 44*\[Omega][{{X, X}, {Y, Y}}])) + 
          8*\[Omega][{{X}, {X, Y, Y}}]^3*(M*(38 + 69*M + M^2) + 
            (-60 - 226*M + 54*M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
            4*(-99 + 8*M)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
            144*\[Omega][{{X, X}, {Y, Y}}]^3 + 16*\[Omega][{{Y}, {X, X, Y}}]^
              2*(41 + 36*\[Omega][{{X, X}, {Y, Y}}]) + 
            4*\[Omega][{{Y}, {X, X, Y}}]*(38 + 113*M + M^2 + 
              4*(-43 + 24*M)*\[Omega][{{X, X}, {Y, Y}}] - 
              32*\[Omega][{{X, X}, {Y, Y}}]^2)) + 
          2*\[Omega][{{X}, {X, Y, Y}}]^2*(M^2*(50 + 64*M + 3*M^2) + 
            2*M*(-138 - 233*M + 11*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
            (472 + 1588*M - 68*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
            8*(-166 + 59*M)*\[Omega][{{X, X}, {Y, Y}}]^3 - 
            272*\[Omega][{{X, X}, {Y, Y}}]^4 + 64*\[Omega][{{Y}, {X, X, Y}}]^
              3*(25 + 22*\[Omega][{{X, X}, {Y, Y}}]) + 
            16*\[Omega][{{Y}, {X, X, Y}}]^2*(40 + 120*M + 3*M^2 + 
              (-216 + 98*M)*\[Omega][{{X, X}, {Y, Y}}] - 
              52*\[Omega][{{X, X}, {Y, Y}}]^2) + 8*\[Omega][{{Y}, {X, X, Y}}]*
             (M*(48 + 82*M + 3*M^2) + (-122 - 333*M + 52*M^2)*\[Omega][
                {{X, X}, {Y, Y}}] + (590 - 64*M)*\[Omega][{{X, X}, {Y, Y}}]^
                2 + 204*\[Omega][{{X, X}, {Y, Y}}]^3)) + 
          \[Omega][{{X}, {X, Y, Y}}]*(M^3*(9 + 10*M + M^2) - 
            M^2*(122 + 157*M + 10*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
            2*M*(246 + 430*M + 9*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
            4*(-138 - 427*M + 39*M^2)*\[Omega][{{X, X}, {Y, Y}}]^3 - 
            16*(-77 + 20*M)*\[Omega][{{X, X}, {Y, Y}}]^4 + 
            224*\[Omega][{{X, X}, {Y, Y}}]^5 + 512*\[Omega][{{Y}, {X, X, Y}}]^
              4*(1 + \[Omega][{{X, X}, {Y, Y}}]) + 
            32*\[Omega][{{Y}, {X, X, Y}}]^3*(8 + 38*M + 3*M^2 + 
              8*(-9 + 4*M)*\[Omega][{{X, X}, {Y, Y}}] - 
              16*\[Omega][{{X, X}, {Y, Y}}]^2) + 8*\[Omega][{{Y}, {X, X, Y}}]^
              2*(M*(52 + 97*M + 9*M^2) + 8*(-16 - 52*M + 5*M^2)*\[Omega][
                {{X, X}, {Y, Y}}] + (780 - 80*M)*\[Omega][{{X, X}, {Y, Y}}]^
                2 + 272*\[Omega][{{X, X}, {Y, Y}}]^3) + 
            2*\[Omega][{{Y}, {X, X, Y}}]*(M^2*(62 + 83*M + 8*M^2) - 
              10*M*(40 + 68*M + M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
              (808 + 2484*M - 24*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
              16*(-139 + 46*M)*\[Omega][{{X, X}, {Y, Y}}]^3 - 
              448*\[Omega][{{X, X}, {Y, Y}}]^4))))/
       (1 + 2*\[Omega][{{X, X}, {Y, Y}}])))/
    (E^(T*(\[Lambda][{y}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
     (M - 2*\[Lambda][{y}])*(5*\[Lambda][{y}] + 
      2*(\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] - 
        \[Omega][{{X, X}, {Y, Y}}]))*(M + 4*\[Lambda][{y}] + 
      4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}] - 
      4*\[Omega][{{X, X}, {Y, Y}}])*(M + 4*\[Omega][{{X}, {X, Y, Y}}] - 
      2*\[Omega][{{X, X}, {Y, Y}}])*(1 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
      2*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
     (M + 4*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
     (1 + M - \[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] - 
      \[Omega][{{X, X}, {Y, Y}}])*(2*\[Lambda][{y}] + 
      2*\[Omega][{{X}, {X, Y, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
     (2*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
      \[Omega][{{X, X}, {Y, Y}}])) + 
   ((2*M*\[Lambda][{y}]^2*(648*\[Lambda][{y}]^6 + 108*\[Lambda][{y}]^5*
         (6 + 13*M + M^2 + 34*\[Omega][{{X}, {X, Y, Y}}] + 
          22*\[Omega][{{Y}, {X, X, Y}}] + 22*\[Omega][{{X, X}, {Y, Y}}] + 
          4*M*\[Omega][{{X, X}, {Y, Y}}]) + 6*\[Lambda][{y}]^4*
         (90*M + 143*M^2 + 20*M^3 + 1248*\[Omega][{{X}, {X, Y, Y}}]^2 + 
          456*\[Omega][{{Y}, {X, X, Y}}]^2 + 252*\[Omega][{{X, X}, {Y, Y}}] + 
          602*M*\[Omega][{{X, X}, {Y, Y}}] + 112*M^2*
           \[Omega][{{X, X}, {Y, Y}}] + 516*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          128*M*\[Omega][{{X, X}, {Y, Y}}]^2 + 6*\[Omega][{{Y}, {X, X, Y}}]*
           (30 + 87*M + 11*M^2 + 2*(83 + 22*M)*\[Omega][{{X, X}, {Y, Y}}]) + 
          2*\[Omega][{{X}, {X, Y, Y}}]*(198 + 459*M + 17*M^2 + 
            876*\[Omega][{{Y}, {X, X, Y}}] + (858 + 68*M)*
             \[Omega][{{X, X}, {Y, Y}}])) + \[Lambda][{y}]^3*
         (126*M^2 + 183*M^3 + 39*M^4 + 6720*\[Omega][{{X}, {X, Y, Y}}]^3 + 
          1248*\[Omega][{{Y}, {X, X, Y}}]^3 + 
          828*M*\[Omega][{{X, X}, {Y, Y}}] + 1438*M^2*
           \[Omega][{{X, X}, {Y, Y}}] + 304*M^3*\[Omega][{{X, X}, {Y, Y}}] + 
          1080*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          2980*M*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          704*M^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          1656*\[Omega][{{X, X}, {Y, Y}}]^3 + 
          448*M*\[Omega][{{X, X}, {Y, Y}}]^3 + 8*\[Omega][{{X}, {X, Y, Y}}]^2*
           (342 + 894*M + 13*M^2 + 2016*\[Omega][{{Y}, {X, X, Y}}] + 
            4*(489 + 13*M)*\[Omega][{{X, X}, {Y, Y}}]) + 
          24*\[Omega][{{Y}, {X, X, Y}}]^2*(24 + 103*M + 19*M^2 + 
            4*(55 + 19*M)*\[Omega][{{X, X}, {Y, Y}}]) + 
          2*\[Omega][{{Y}, {X, X, Y}}]*(M*(378 + 689*M + 140*M^2) + 
            2*(522 + 1525*M + 386*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
            4*(675 + 212*M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
          2*\[Omega][{{X}, {X, Y, Y}}]*(M*(630 + 1019*M + 80*M^2) + 
            5040*\[Omega][{{Y}, {X, X, Y}}]^2 + (2052 + 4946*M + 488*M^2)*
             \[Omega][{{X, X}, {Y, Y}}] + 84*(57 + 8*M)*
             \[Omega][{{X, X}, {Y, Y}}]^2 + 4*\[Omega][{{Y}, {X, X, Y}}]*
             (450 + 1209*M + 76*M^2 + 4*(609 + 76*M)*\[Omega][{{X, X}, 
                 {Y, Y}}]))) + (\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}])*(2*\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}])*(32*\[Omega][{{X}, {X, Y, Y}}]^3*
           (M + 4*\[Omega][{{Y}, {X, X, Y}}] + 
            6*\[Omega][{{X, X}, {Y, Y}}]) + 
          (M + 4*\[Omega][{{Y}, {X, X, Y}}])*
           (M + 2*\[Omega][{{X, X}, {Y, Y}}])*(M*(1 + M) + 
            8*\[Omega][{{Y}, {X, X, Y}}]^2 + (6 + 8*M)*\[Omega][
              {{X, X}, {Y, Y}}] + 8*\[Omega][{{X, X}, {Y, Y}}]^2 + 
            2*\[Omega][{{Y}, {X, X, Y}}]*(2 + 3*M + 6*\[Omega][{{X, X}, 
                 {Y, Y}}])) + 16*\[Omega][{{X}, {X, Y, Y}}]^2*
           (M*(1 + 2*M) + 16*\[Omega][{{Y}, {X, X, Y}}]^2 + 
            3*(2 + 5*M)*\[Omega][{{X, X}, {Y, Y}}] + 
            14*\[Omega][{{X, X}, {Y, Y}}]^2 + 4*\[Omega][{{Y}, {X, X, Y}}]*
             (1 + 3*M + 7*\[Omega][{{X, X}, {Y, Y}}])) + 
          2*\[Omega][{{X}, {X, Y, Y}}]*(M^2*(4 + 5*M) + 
            64*\[Omega][{{Y}, {X, X, Y}}]^3 + 4*M*(7 + 11*M)*
             \[Omega][{{X, X}, {Y, Y}}] + 4*(6 + 19*M)*
             \[Omega][{{X, X}, {Y, Y}}]^2 + 32*\[Omega][{{X, X}, {Y, Y}}]^3 + 
            32*\[Omega][{{Y}, {X, X, Y}}]^2*(1 + 3*M + 5*\[Omega][
                {{X, X}, {Y, Y}}]) + 8*\[Omega][{{Y}, {X, X, Y}}]*
             (M*(3 + 5*M) + (8 + 22*M)*\[Omega][{{X, X}, {Y, Y}}] + 
              20*\[Omega][{{X, X}, {Y, Y}}]^2))) + \[Lambda][{y}]^2*
         (9*M^3 + 12*M^4 + 3*M^5 + 2688*\[Omega][{{X}, {X, Y, Y}}]^4 + 
          192*\[Omega][{{Y}, {X, X, Y}}]^4 + 114*M^2*
           \[Omega][{{X, X}, {Y, Y}}] + 175*M^3*\[Omega][{{X, X}, {Y, Y}}] + 
          40*M^4*\[Omega][{{X, X}, {Y, Y}}] + 
          324*M*\[Omega][{{X, X}, {Y, Y}}]^2 + 660*M^2*
           \[Omega][{{X, X}, {Y, Y}}]^2 + 172*M^3*\[Omega][{{X, X}, {Y, Y}}]^
            2 + 216*\[Omega][{{X, X}, {Y, Y}}]^3 + 
          836*M*\[Omega][{{X, X}, {Y, Y}}]^3 + 272*M^2*
           \[Omega][{{X, X}, {Y, Y}}]^3 + 288*\[Omega][{{X, X}, {Y, Y}}]^4 + 
          128*M*\[Omega][{{X, X}, {Y, Y}}]^4 + 16*\[Omega][{{X}, {X, Y, Y}}]^
            3*(78 + 246*M + M^2 + 664*\[Omega][{{Y}, {X, X, Y}}] + 
            4*(167 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
          16*\[Omega][{{Y}, {X, X, Y}}]^3*(6 + 51*M + 13*M^2 + 
            2*(61 + 26*M)*\[Omega][{{X, X}, {Y, Y}}]) + 
          4*\[Omega][{{Y}, {X, X, Y}}]^2*(M*(84 + 175*M + 44*M^2) + 
            (228 + 840*M + 242*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
            4*(205 + 66*M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
          \[Omega][{{Y}, {X, X, Y}}]*(M^2*(114 + 175*M + 43*M^2) + 
            6*M*(138 + 259*M + 60*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
            4*(306 + 891*M + 223*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
            8*(261 + 70*M)*\[Omega][{{X, X}, {Y, Y}}]^3) + 
          8*\[Omega][{{X}, {X, Y, Y}}]^2*(M*(123 + 208*M + 8*M^2) + 
            1456*\[Omega][{{Y}, {X, X, Y}}]^2 + (474 + 1184*M + 51*M^2)*
             \[Omega][{{X, X}, {Y, Y}}] + 4*(314 + 19*M)*
             \[Omega][{{X, X}, {Y, Y}}]^2 + 8*\[Omega][{{Y}, {X, X, Y}}]*
             (54 + 148*M + 4*M^2 + (333 + 16*M)*\[Omega][{{X, X}, 
                 {Y, Y}}])) + \[Omega][{{X}, {X, Y, Y}}]*
           (M^2*(198 + 265*M + 31*M^2) + 3904*\[Omega][{{Y}, {X, X, Y}}]^3 + 
            2*M*(726 + 1197*M + 136*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
            4*(486 + 1341*M + 187*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
            24*(135 + 26*M)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
            16*\[Omega][{{Y}, {X, X, Y}}]^2*(108 + 355*M + 28*M^2 + 
              2*(393 + 56*M)*\[Omega][{{X, X}, {Y, Y}}]) + 
            4*\[Omega][{{Y}, {X, X, Y}}]*(3*M*(114 + 197*M + 20*M^2) + 
              4*(276 + 751*M + 92*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
              4*(761 + 128*M)*\[Omega][{{X, X}, {Y, Y}}]^2))) + 
        \[Lambda][{y}]*(384*\[Omega][{{X}, {X, Y, Y}}]^5 + 
          64*\[Omega][{{X}, {X, Y, Y}}]^4*(3 + 14*M + 
            46*\[Omega][{{Y}, {X, X, Y}}] + 52*\[Omega][{{X, X}, {Y, Y}}]) + 
          32*\[Omega][{{Y}, {X, X, Y}}]^4*(M*(3 + M) + 4*(2 + M)*
             \[Omega][{{X, X}, {Y, Y}}]) + 16*\[Omega][{{Y}, {X, X, Y}}]^3*
           (M*(3 + 7*M + 2*M^2) + (8 + 42*M + 11*M^2)*\[Omega][
              {{X, X}, {Y, Y}}] + 2*(25 + 6*M)*\[Omega][{{X, X}, {Y, Y}}]^
              2) + 2*\[Omega][{{Y}, {X, X, Y}}]^2*(M^2*(12 + 19*M + 5*M^2) + 
            2*M*(58 + 113*M + 21*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
            8*(24 + 74*M + 13*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
            16*(25 + 4*M)*\[Omega][{{X, X}, {Y, Y}}]^3) + 
          M*\[Omega][{{X, X}, {Y, Y}}]*(M^2*(3 + 4*M + M^2) + 
            M*(24 + 38*M + 9*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
            4*(9 + 23*M + 7*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
            4*(16 + 9*M)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
            16*\[Omega][{{X, X}, {Y, Y}}]^4) + \[Omega][{{Y}, {X, X, Y}}]*
           (M^3*(3 + 4*M + M^2) + M^2*(62 + 91*M + 16*M^2)*
             \[Omega][{{X, X}, {Y, Y}}] + 4*M*(57 + 107*M + 19*M^2)*
             \[Omega][{{X, X}, {Y, Y}}]^2 + 4*(54 + 165*M + 32*M^2)*
             \[Omega][{{X, X}, {Y, Y}}]^3 + 32*(9 + 2*M)*
             \[Omega][{{X, X}, {Y, Y}}]^4) + 8*\[Omega][{{X}, {X, Y, Y}}]^3*
           (M*(38 + 69*M + M^2) + 656*\[Omega][{{Y}, {X, X, Y}}]^2 + 
            2*(90 + 233*M + 3*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
            (540 + 8*M)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
            4*\[Omega][{{Y}, {X, X, Y}}]*(38 + 113*M + M^2 + 
              4*(73 + M)*\[Omega][{{X, X}, {Y, Y}}])) + 
          2*\[Omega][{{X}, {X, Y, Y}}]^2*(M^2*(50 + 64*M + 3*M^2) + 
            1600*\[Omega][{{Y}, {X, X, Y}}]^3 + 2*M*(194 + 315*M + 15*M^2)*
             \[Omega][{{X, X}, {Y, Y}}] + 4*(138 + 385*M + 24*M^2)*
             \[Omega][{{X, X}, {Y, Y}}]^2 + 16*(61 + 6*M)*
             \[Omega][{{X, X}, {Y, Y}}]^3 + 16*\[Omega][{{Y}, {X, X, Y}}]^2*
             (40 + 120*M + 3*M^2 + 4*(70 + 3*M)*\[Omega][{{X, X}, {Y, Y}}]) + 
            8*\[Omega][{{Y}, {X, X, Y}}]*(M*(48 + 82*M + 3*M^2) + 
              (170 + 461*M + 21*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
              (518 + 36*M)*\[Omega][{{X, X}, {Y, Y}}]^2)) + 
          \[Omega][{{X}, {X, Y, Y}}]*(M^3*(9 + 10*M + M^2) + 
            512*\[Omega][{{Y}, {X, X, Y}}]^4 + M^2*(110 + 151*M + 18*M^2)*
             \[Omega][{{X, X}, {Y, Y}}] + 4*M*(87 + 161*M + 23*M^2)*
             \[Omega][{{X, X}, {Y, Y}}]^2 + 12*(18 + 71*M + 14*M^2)*
             \[Omega][{{X, X}, {Y, Y}}]^3 + 96*(3 + M)*
             \[Omega][{{X, X}, {Y, Y}}]^4 + 32*\[Omega][{{Y}, {X, X, Y}}]^3*
             (8 + 38*M + 3*M^2 + 4*(25 + 3*M)*\[Omega][{{X, X}, {Y, Y}}]) + 
            8*\[Omega][{{Y}, {X, X, Y}}]^2*(M*(52 + 97*M + 9*M^2) + 
              2*(88 + 270*M + 27*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
              (596 + 72*M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
            2*\[Omega][{{Y}, {X, X, Y}}]*(M^2*(62 + 83*M + 8*M^2) + 
              16*M*(30 + 51*M + 5*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
              4*(186 + 517*M + 60*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
              32*(43 + 6*M)*\[Omega][{{X, X}, {Y, Y}}]^3)))))/
      (1 + M + \[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
       2*\[Omega][{{Y}, {X, X, Y}}]) - 
     (1296*M*\[Lambda][{y}]^7*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}])*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{X, X}, {Y, Y}}])*(3 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
         \[Omega][{{X, X}, {Y, Y}}])*(1 + 2*\[Omega][{{X, X}, {Y, Y}}]) + 
       (2 + M)*(2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] - 
         2*\[Omega][{{X, X}, {Y, Y}}])*(2*\[Omega][{{Y}, {X, X, Y}}] - 
         \[Omega][{{X, X}, {Y, Y}}])*(2 + 3*M + M^2 + 
         8*\[Omega][{{Y}, {X, X, Y}}]^2 + \[Omega][{{Y}, {X, X, Y}}]*
          (8 + 6*M - 8*\[Omega][{{X, X}, {Y, Y}}]) - 
         (4 + 3*M)*\[Omega][{{X, X}, {Y, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]^
           2)*(16*M*\[Omega][{{X}, {X, Y, Y}}]^4 + 
         4*\[Omega][{{X}, {X, Y, Y}}]^3*(4 + 20*M + 3*M^2 + 
           12*M*\[Omega][{{Y}, {X, X, Y}}] - 
           6*M*\[Omega][{{X, X}, {Y, Y}}]) + 8*\[Omega][{{Y}, {X, X, Y}}]^3*
          (2*(1 + M) + (2 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
         2*\[Omega][{{Y}, {X, X, Y}}]^2*(32 + 38*M + 6*M^2 + 
           (12 + 2*M + 3*M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
           4*(3 + 2*M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
         (3 + \[Omega][{{X, X}, {Y, Y}}])*(2*(1 + M)^2*(2 + M) - 
           (14 + 25*M + 11*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
           2*(4 + 5*M)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           8*\[Omega][{{X, X}, {Y, Y}}]^3) + \[Omega][{{Y}, {X, X, Y}}]*
          (2*(26 + 47*M + 22*M^2 + M^3) + (-68 - 70*M + 5*M^2 + M^3)*
            \[Omega][{{X, X}, {Y, Y}}] - 6*(12 + 5*M + M^2)*
            \[Omega][{{X, X}, {Y, Y}}]^2 + 8*M*\[Omega][{{X, X}, {Y, Y}}]^
             3) + 2*\[Omega][{{X}, {X, Y, Y}}]^2*(32 + 64*M + 27*M^2 + M^3 + 
           24*M*\[Omega][{{Y}, {X, X, Y}}]^2 + (4 - 60*M - 3*M^2)*
            \[Omega][{{X, X}, {Y, Y}}] + 4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           4*\[Omega][{{Y}, {X, X, Y}}]*(6 + 22*M + 3*M^2 + 
             (2 - 5*M)*\[Omega][{{X, X}, {Y, Y}}])) + 
         \[Omega][{{X}, {X, Y, Y}}]*(52 + 106*M + 62*M^2 + 8*M^3 + 
           16*M*\[Omega][{{Y}, {X, X, Y}}]^3 + (-72 - 124*M - 33*M^2 + M^3)*
            \[Omega][{{X, X}, {Y, Y}}] + (-56 + 30*M - 6*M^2)*
            \[Omega][{{X, X}, {Y, Y}}]^2 + 8*(-2 + M)*
            \[Omega][{{X, X}, {Y, Y}}]^3 + 4*\[Omega][{{Y}, {X, X, Y}}]^2*
            (12 + 28*M + 3*M^2 - 2*(-4 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 
           2*\[Omega][{{Y}, {X, X, Y}}]*(64 + 102*M + 33*M^2 + M^3 + 
             (16 - 58*M)*\[Omega][{{X, X}, {Y, Y}}] - 8*(1 + M)*
              \[Omega][{{X, X}, {Y, Y}}]^2))) + 8*\[Lambda][{y}]^6*
        (320*\[Omega][{{Y}, {X, X, Y}}]^4*(-4 + M - 
           4*\[Omega][{{X, X}, {Y, Y}}]) - 4*M*\[Omega][{{X}, {X, Y, Y}}]^3*
          (3 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
          (-257 + (-594 + 20*M)*\[Omega][{{X, X}, {Y, Y}}]) + 
         20*\[Omega][{{Y}, {X, X, Y}}]^3*(-320 + 105*M + 8*M^2 + 
           (-256 + 109*M)*\[Omega][{{X, X}, {Y, Y}}] + (64 + 46*M)*
            \[Omega][{{X, X}, {Y, Y}}]^2) + 2*\[Omega][{{Y}, {X, X, Y}}]^2*
          (-4640 + 2245*M + 787*M^2 + 81*M^3 + (-1760 + 7012*M + 1487*M^2 + 
             159*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
           (3360 + 7133*M + 1154*M^2 + 44*M^3)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           2*(80 + 729*M + 118*M^2)*\[Omega][{{X, X}, {Y, Y}}]^3) + 
         (3 + \[Omega][{{X, X}, {Y, Y}}])*(-320 - 520*M + 1481*M^2 + 
           223*M^3 + (560 + 5146*M + 4461*M^2 + 477*M^3)*
            \[Omega][{{X, X}, {Y, Y}}] + 2*(280 + 6717*M + 1731*M^2 + 66*M^3)*
            \[Omega][{{X, X}, {Y, Y}}]^2 + 4*(-200 + 921*M + 177*M^2)*
            \[Omega][{{X, X}, {Y, Y}}]^3) + \[Omega][{{Y}, {X, X, Y}}]*
          (-5120 + 2690*M + 4763*M^2 + 709*M^3 + 2*(1040 + 13779*M + 
             6895*M^2 + 756*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
           (9120 + 50014*M + 11833*M^2 + 555*M^3)*\[Omega][{{X, X}, {Y, Y}}]^
             2 + 2*M*(8495 + 1639*M + 22*M^2)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
           4*(-80 + 307*M + 59*M^2)*\[Omega][{{X, X}, {Y, Y}}]^4) + 
         2*\[Omega][{{X}, {X, Y, Y}}]^2*(-320 + 6979*M + 427*M^2 + 81*M^3 + 
           (-320 + 20944*M + 947*M^2 + 159*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
           (480 + 11663*M + 914*M^2 + 44*M^3)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           2*(80 + 911*M + 108*M^2)*\[Omega][{{X, X}, {Y, Y}}]^3 - 
           16*\[Omega][{{Y}, {X, X, Y}}]^2*(40 - 98*M + (40 - 206*M + 5*M^2)*
              \[Omega][{{X, X}, {Y, Y}}]) + 2*\[Omega][{{Y}, {X, X, Y}}]*
            (-480 + 3449*M + 149*M^2 + 27*M^3 + (-640 + 9061*M + 206*M^2 + 44*
                M^3)*\[Omega][{{X, X}, {Y, Y}}] + 98*M*(27 + 2*M)*
              \[Omega][{{X, X}, {Y, Y}}]^2)) + \[Omega][{{X}, {X, Y, Y}}]*
          (-2240 + 13598*M + 4043*M^2 + 709*M^3 + 
           2*(-400 + 28683*M + 6355*M^2 + 756*M^3)*\[Omega][
             {{X, X}, {Y, Y}}] + (3360 + 64090*M + 11113*M^2 + 555*M^3)*
            \[Omega][{{X, X}, {Y, Y}}]^2 + 2*M*(9587 + 1579*M + 22*M^2)*
            \[Omega][{{X, X}, {Y, Y}}]^3 + 4*(-80 + 307*M + 59*M^2)*
            \[Omega][{{X, X}, {Y, Y}}]^4 + 40*\[Omega][{{Y}, {X, X, Y}}]^3*
            (-64 + 35*M + (-64 + 46*M)*\[Omega][{{X, X}, {Y, Y}}]) + 
           4*\[Omega][{{Y}, {X, X, Y}}]^2*(-2080 + 2231*M + 189*M^2 + 
             27*M^3 + (-1920 + 5383*M + 386*M^2 + 44*M^3)*\[Omega][{{X, X}, 
                {Y, Y}}] + (320 + 2282*M + 216*M^2)*\[Omega][{{X, X}, 
                 {Y, Y}}]^2) + 4*\[Omega][{{Y}, {X, X, Y}}]*
            (-2000 + 4852*M + 934*M^2 + 162*M^3 + (-1600 + 16520*M + 2555*
                M^2 + 291*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
             2*(560 + 6541*M + 871*M^2 + 22*M^3)*\[Omega][{{X, X}, {Y, Y}}]^
               2 + 2*(80 + 820*M + 113*M^2)*\[Omega][{{X, X}, {Y, Y}}]^3))) + 
       4*\[Lambda][{y}]^5*(640*\[Omega][{{Y}, {X, X, Y}}]^5*
          (-4 + M - 4*\[Omega][{{X, X}, {Y, Y}}]) + 
         16*M*\[Omega][{{X}, {X, Y, Y}}]^4*
          (3 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
          (151 + 10*M + 2*(171 + 5*M)*\[Omega][{{X, X}, {Y, Y}}]) + 
         32*\[Omega][{{Y}, {X, X, Y}}]^4*(-264 + 176*M + M^2 + 
           8*(-13 + 24*M)*\[Omega][{{X, X}, {Y, Y}}] + 2*(80 + 33*M)*
            \[Omega][{{X, X}, {Y, Y}}]^2) + 4*\[Omega][{{Y}, {X, X, Y}}]^3*
          (160 + 5456*M + 921*M^2 + 129*M^3 + (4192 + 10102*M + 2597*M^2 + 
             361*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
           2*(2256 + 2655*M + 859*M^2 + 51*M^3)*\[Omega][{{X, X}, {Y, Y}}]^
             2 + 4*(-120 + 179*M + 84*M^2)*\[Omega][{{X, X}, {Y, Y}}]^3) + 
         2*\[Omega][{{Y}, {X, X, Y}}]^2*(5536 + 19704*M + 8833*M^2 + 
           1468*M^3 + 48*M^4 + (8064 + 38670*M + 25716*M^2 + 4371*M^3 + 
             142*M^4)*\[Omega][{{X, X}, {Y, Y}}] + 
           (2176 + 31048*M + 21097*M^2 + 2151*M^3 + 42*M^4)*
            \[Omega][{{X, X}, {Y, Y}}]^2 + 2*(-3312 + 8771*M + 3947*M^2 + 
             150*M^3)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
           4*(-160 + 897*M + 267*M^2)*\[Omega][{{X, X}, {Y, Y}}]^4) + 
         (3 + \[Omega][{{X, X}, {Y, Y}}])*(448 + 1856*M + 1244*M^2 + 
           889*M^3 + 162*M^4 + (-464 - 3832*M + 3898*M^2 + 2951*M^3 + 
             426*M^4)*\[Omega][{{X, X}, {Y, Y}}] + 
           2*(-552 + 946*M + 4431*M^2 + 1308*M^3 + 63*M^4)*
            \[Omega][{{X, X}, {Y, Y}}]^2 + 2*(-80 + 14934*M + 4728*M^2 + 
             297*M^3)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
           4*(320 + 2352*M + 549*M^2)*\[Omega][{{X, X}, {Y, Y}}]^4) + 
         \[Omega][{{Y}, {X, X, Y}}]*(7168 + 23888*M + 21950*M^2 + 5053*M^3 + 
           450*M^4 + 2*(864 + 13918*M + 33339*M^2 + 8213*M^3 + 663*M^4)*
            \[Omega][{{X, X}, {Y, Y}}] + (-9888 + 45748*M + 75706*M^2 + 
             12843*M^3 + 520*M^4)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           2*(-7600 + 53674*M + 23883*M^2 + 1633*M^3 + 21*M^4)*
            \[Omega][{{X, X}, {Y, Y}}]^3 + 2*(-96 + 20310*M + 5878*M^2 + 
             99*M^3)*\[Omega][{{X, X}, {Y, Y}}]^4 + 4*(160 + 784*M + 183*M^2)*
            \[Omega][{{X, X}, {Y, Y}}]^5) + 4*\[Omega][{{X}, {X, Y, Y}}]^3*
          (-320 + 8562*M + 2003*M^2 + 81*M^3 + (-320 + 26706*M + 3951*M^2 + 
             219*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
           2*(240 + 9026*M + 563*M^2 + 32*M^3)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           4*(40 + 844*M + 3*M^2)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
           16*\[Omega][{{Y}, {X, X, Y}}]^2*(-40 + 208*M + 10*M^2 + 
             (-40 + 416*M + 11*M^2)*\[Omega][{{X, X}, {Y, Y}}]) + 
           2*\[Omega][{{Y}, {X, X, Y}}]*(-480 + 5270*M + 801*M^2 + 27*M^3 + 
             2*(-320 + 6874*M + 631*M^2 + 32*M^3)*\[Omega][{{X, X}, 
                {Y, Y}}] + 56*M*(90 + M)*\[Omega][{{X, X}, {Y, Y}}]^2)) + 
         2*\[Omega][{{X}, {X, Y, Y}}]^2*(-1792 + 20012*M + 14643*M^2 + 
           1076*M^3 + 48*M^4 + (-32 + 88398*M + 33032*M^2 + 3009*M^3 + 
             142*M^4)*\[Omega][{{X, X}, {Y, Y}}] + 
           (3648 + 118308*M + 14979*M^2 + 1667*M^3 + 42*M^4)*
            \[Omega][{{X, X}, {Y, Y}}]^2 + 2*(-592 + 23983*M + 2033*M^2 + 
             131*M^3)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
           (-640 + 5828*M + 724*M^2)*\[Omega][{{X, X}, {Y, Y}}]^4 + 
           16*\[Omega][{{Y}, {X, X, Y}}]^3*(-240 + 381*M + 10*M^2 + 
             2*(-120 + 311*M + 6*M^2)*\[Omega][{{X, X}, {Y, Y}}]) + 
           8*\[Omega][{{Y}, {X, X, Y}}]^2*(-1008 + 3981*M + 568*M^2 + 
             41*M^3 + (-1008 + 8925*M + 1004*M^2 + 83*M^3)*\[Omega][{{X, X}, 
                {Y, Y}}] + 2*(160 + 1513*M + 99*M^2)*\[Omega][{{X, X}, 
                 {Y, Y}}]^2) + 2*\[Omega][{{Y}, {X, X, Y}}]*
            (-3328 + 24104*M + 8041*M^2 + 612*M^3 + 16*M^4 + 
             (-2304 + 73566*M + 16121*M^2 + 1461*M^3 + 42*M^4)*
              \[Omega][{{X, X}, {Y, Y}}] + (4000 + 58262*M + 4926*M^2 + 428*
                M^3)*\[Omega][{{X, X}, {Y, Y}}]^2 + 4*(120 + 2659*M + 274*
                M^2)*\[Omega][{{X, X}, {Y, Y}}]^3)) + 
         \[Omega][{{X}, {X, Y, Y}}]*(1216 + 9392*M + 29954*M^2 + 4429*M^3 + 
           450*M^4 + 2*(3040 + 36318*M + 39035*M^2 + 6723*M^3 + 663*M^4)*
            \[Omega][{{X, X}, {Y, Y}}] + (2656 + 205476*M + 64150*M^2 + 
             11307*M^3 + 520*M^4)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           2*(-4880 + 97546*M + 18231*M^2 + 1519*M^3 + 21*M^4)*
            \[Omega][{{X, X}, {Y, Y}}]^3 + 2*(-416 + 27030*M + 4846*M^2 + 
             99*M^3)*\[Omega][{{X, X}, {Y, Y}}]^4 + 4*(160 + 784*M + 183*M^2)*
            \[Omega][{{X, X}, {Y, Y}}]^5 + 128*\[Omega][{{Y}, {X, X, Y}}]^4*
            (-60 + 34*M + (-60 + 33*M)*\[Omega][{{X, X}, {Y, Y}}]) + 
           8*\[Omega][{{Y}, {X, X, Y}}]^3*(-2592 + 4086*M + 399*M^2 + 
             55*M^3 + 2*(-896 + 3516*M + 401*M^2 + 51*M^3)*\[Omega][{{X, X}, 
                {Y, Y}}] + 40*(32 + 49*M + 9*M^2)*\[Omega][{{X, X}, {Y, Y}}]^
               2) + 4*\[Omega][{{Y}, {X, X, Y}}]^2*(-2848 + 21718*M + 
             5939*M^2 + 828*M^3 + 16*M^4 + (1888 + 50498*M + 13687*M^2 + 1831*
                M^3 + 42*M^4)*\[Omega][{{X, X}, {Y, Y}}] + 
             (8672 + 32080*M + 7582*M^2 + 466*M^3)*\[Omega][{{X, X}, {Y, Y}}]^
               2 + 4*(-40 + 1994*M + 355*M^2)*\[Omega][{{X, X}, {Y, Y}}]^3) + 
           4*\[Omega][{{Y}, {X, X, Y}}]*(240 + 19462*M + 12134*M^2 + 
             1749*M^3 + 96*M^4 + (4800 + 61270*M + 30076*M^2 + 4524*M^3 + 268*
                M^4)*\[Omega][{{X, X}, {Y, Y}}] + (7232 + 76366*M + 20978*
                M^2 + 2503*M^3 + 42*M^4)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
             (-2624 + 42258*M + 8188*M^2 + 281*M^3)*\[Omega][{{X, X}, 
                 {Y, Y}}]^3 + (-640 + 4708*M + 896*M^2)*\[Omega][
                {{X, X}, {Y, Y}}]^4))) - \[Lambda][{y}]*
        (2 + M + 4*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
        (96 + 432*M + 792*M^2 + 756*M^3 + 396*M^4 + 108*M^5 + 12*M^6 + 
         1664*\[Omega][{{Y}, {X, X, Y}}] + 
         6144*M*\[Omega][{{Y}, {X, X, Y}}] + 8880*M^2*
          \[Omega][{{Y}, {X, X, Y}}] + 6288*M^3*\[Omega][{{Y}, {X, X, Y}}] + 
         2196*M^4*\[Omega][{{Y}, {X, X, Y}}] + 
         312*M^5*\[Omega][{{Y}, {X, X, Y}}] + 
         4*M^6*\[Omega][{{Y}, {X, X, Y}}] + 7840*\[Omega][{{Y}, {X, X, Y}}]^
           2 + 23024*M*\[Omega][{{Y}, {X, X, Y}}]^2 + 
         25096*M^2*\[Omega][{{Y}, {X, X, Y}}]^2 + 
         12236*M^3*\[Omega][{{Y}, {X, X, Y}}]^2 + 
         2416*M^4*\[Omega][{{Y}, {X, X, Y}}]^2 + 
         92*M^5*\[Omega][{{Y}, {X, X, Y}}]^2 + 
         15104*\[Omega][{{Y}, {X, X, Y}}]^3 + 
         33312*M*\[Omega][{{Y}, {X, X, Y}}]^3 + 25168*M^2*
          \[Omega][{{Y}, {X, X, Y}}]^3 + 7296*M^3*\[Omega][{{Y}, {X, X, Y}}]^
           3 + 576*M^4*\[Omega][{{Y}, {X, X, Y}}]^3 + 
         11904*\[Omega][{{Y}, {X, X, Y}}]^4 + 
         18368*M*\[Omega][{{Y}, {X, X, Y}}]^4 + 
         8800*M^2*\[Omega][{{Y}, {X, X, Y}}]^4 + 
         1296*M^3*\[Omega][{{Y}, {X, X, Y}}]^4 + 
         2560*\[Omega][{{Y}, {X, X, Y}}]^5 + 
         3200*M*\[Omega][{{Y}, {X, X, Y}}]^5 + 
         960*M^2*\[Omega][{{Y}, {X, X, Y}}]^5 - 
         904*\[Omega][{{X, X}, {Y, Y}}] - 3252*M*\[Omega][{{X, X}, {Y, Y}}] - 
         4482*M^2*\[Omega][{{X, X}, {Y, Y}}] - 2883*M^3*
          \[Omega][{{X, X}, {Y, Y}}] - 789*M^4*\[Omega][{{X, X}, {Y, Y}}] - 
         21*M^5*\[Omega][{{X, X}, {Y, Y}}] + 
         19*M^6*\[Omega][{{X, X}, {Y, Y}}] - 9920*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, X}, {Y, Y}}] - 28304*M*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, X}, {Y, Y}}] - 29040*M^2*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, X}, {Y, Y}}] - 12236*M^3*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, X}, {Y, Y}}] - 1368*M^4*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, X}, {Y, Y}}] + 220*M^5*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, X}, {Y, Y}}] + 8*M^6*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, X}, {Y, Y}}] - 29728*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}] - 62384*M*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}] - 41736*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}] - 8068*M^3*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}] + 676*M^4*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}] + 96*M^5*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}] - 30272*\[Omega][{{Y}, {X, X, Y}}]^3*
          \[Omega][{{X, X}, {Y, Y}}] - 41472*M*\[Omega][{{Y}, {X, X, Y}}]^3*
          \[Omega][{{X, X}, {Y, Y}}] - 13824*M^2*\[Omega][{{Y}, {X, X, Y}}]^3*
          \[Omega][{{X, X}, {Y, Y}}] + 696*M^3*\[Omega][{{Y}, {X, X, Y}}]^3*
          \[Omega][{{X, X}, {Y, Y}}] + 424*M^4*\[Omega][{{Y}, {X, X, Y}}]^3*
          \[Omega][{{X, X}, {Y, Y}}] - 6272*\[Omega][{{Y}, {X, X, Y}}]^4*
          \[Omega][{{X, X}, {Y, Y}}] - 5376*M*\[Omega][{{Y}, {X, X, Y}}]^4*
          \[Omega][{{X, X}, {Y, Y}}] + 704*M^2*\[Omega][{{Y}, {X, X, Y}}]^4*
          \[Omega][{{X, X}, {Y, Y}}] + 784*M^3*\[Omega][{{Y}, {X, X, Y}}]^4*
          \[Omega][{{X, X}, {Y, Y}}] + 512*\[Omega][{{Y}, {X, X, Y}}]^5*
          \[Omega][{{X, X}, {Y, Y}}] + 1024*M*\[Omega][{{Y}, {X, X, Y}}]^5*
          \[Omega][{{X, X}, {Y, Y}}] + 512*M^2*\[Omega][{{Y}, {X, X, Y}}]^5*
          \[Omega][{{X, X}, {Y, Y}}] + 2760*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         7460*M*\[Omega][{{X, X}, {Y, Y}}]^2 + 6806*M^2*
          \[Omega][{{X, X}, {Y, Y}}]^2 + 1997*M^3*\[Omega][{{X, X}, {Y, Y}}]^
           2 - 265*M^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         151*M^5*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         5*M^6*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         17184*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         32960*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         17004*M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         554*M^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         1586*M^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         30*M^5*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         2*M^6*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         22144*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         21536*M*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         2840*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         5620*M^3*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         596*M^4*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         16*M^5*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         4480*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         11968*M*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         9616*M^2*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         2168*M^3*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         40*M^4*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         9088*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         8128*M*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         2624*M^2*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         32*M^3*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         2048*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         768*M*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         2840*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         4552*M*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         900*M^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         1299*M^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         407*M^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         44*M^5*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         3856*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         2672*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         9124*M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         3652*M^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         8*M^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         18*M^5*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         16320*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         24016*M*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         10752*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^
           3 + 1420*M^3*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}]^3 - 100*M^4*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}]^3 + 19136*\[Omega][{{Y}, {X, X, Y}}]^3*
          \[Omega][{{X, X}, {Y, Y}}]^3 + 12864*M*\[Omega][{{Y}, {X, X, Y}}]^3*
          \[Omega][{{X, X}, {Y, Y}}]^3 + 3632*M^2*\[Omega][{{Y}, {X, X, Y}}]^
           3*\[Omega][{{X, X}, {Y, Y}}]^3 - 160*M^3*
          \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         4992*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         2112*M*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         64*M^2*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         472*\[Omega][{{X, X}, {Y, Y}}]^4 - 2340*M*\[Omega][{{X, X}, {Y, Y}}]^
           4 - 2234*M^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         361*M^3*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         131*M^4*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         10080*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         11664*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         2804*M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         70*M^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         56*M^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         14048*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         5840*M*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         1648*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         200*M^3*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         3328*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         2048*M*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         160*M^2*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         1760*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         1436*M*\[Omega][{{X, X}, {Y, Y}}]^5 - 
         202*M^2*\[Omega][{{X, X}, {Y, Y}}]^5 - 
         152*M^3*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         3664*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
         432*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         84*M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
         72*M^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
         352*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         848*M*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 - 
         128*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 - 
         176*\[Omega][{{X, X}, {Y, Y}}]^6 + 512*M*\[Omega][{{X, X}, {Y, Y}}]^
           6 + 44*M^2*\[Omega][{{X, X}, {Y, Y}}]^6 + 
         960*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^6 - 
         160*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^6 + 
         32*M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^6 - 
         224*\[Omega][{{X, X}, {Y, Y}}]^7 + 16*M*\[Omega][{{X, X}, {Y, Y}}]^
           7 + 128*M*\[Omega][{{X}, {X, Y, Y}}]^5*(2 + 3*M + M^2 + 
           24*\[Omega][{{Y}, {X, X, Y}}]^2 + (-8 - 5*M + M^2)*
            \[Omega][{{X, X}, {Y, Y}}] - (-4 + M)*\[Omega][{{X, X}, {Y, Y}}]^
             2 + 2*\[Omega][{{Y}, {X, X, Y}}]*(10 + 8*M + 
             (-10 + M)*\[Omega][{{X, X}, {Y, Y}}])) + 
         32*\[Omega][{{X}, {X, Y, Y}}]^4*(8 + 56*M + 78*M^2 + 34*M^3 + 
           4*M^4 + 288*M*\[Omega][{{Y}, {X, X, Y}}]^3 + 
           (-28 - 200*M - 155*M^2 - 7*M^3 + 4*M^4)*\[Omega][
             {{X, X}, {Y, Y}}] + (16 + 146*M + 37*M^2 - 12*M^3)*
            \[Omega][{{X, X}, {Y, Y}}]^2 + (-4 - 34*M + 8*M^2)*
            \[Omega][{{X, X}, {Y, Y}}]^3 + 4*\[Omega][{{Y}, {X, X, Y}}]^2*
            (32 + 190*M + 73*M^2 + 2*(2 - 53*M + 3*M^2)*\[Omega][{{X, X}, 
                {Y, Y}}]) + \[Omega][{{Y}, {X, X, Y}}]*(96 + 540*M + 
             466*M^2 + 78*M^3 + 4*(-20 - 164*M - 56*M^2 + 5*M^3)*
              \[Omega][{{X, X}, {Y, Y}}] - 4*M*(-52 + 7*M)*
              \[Omega][{{X, X}, {Y, Y}}]^2)) + 8*\[Omega][{{X}, {X, Y, Y}}]^3*
          (144 + 560*M + 748*M^2 + 422*M^3 + 95*M^4 + 5*M^5 + 
           1152*M*\[Omega][{{Y}, {X, X, Y}}]^4 + (-632 - 2024*M - 1844*M^2 - 
             473*M^3 + 34*M^4 + 5*M^5)*\[Omega][{{X, X}, {Y, Y}}] + 
           (248 + 1776*M + 1152*M^2 - 107*M^3 - 23*M^4)*
            \[Omega][{{X, X}, {Y, Y}}]^2 + 2*(-8 - 436*M + 2*M^2 + 15*M^3)*
            \[Omega][{{X, X}, {Y, Y}}]^3 + (96 + 64*M - 12*M^2)*
            \[Omega][{{X, X}, {Y, Y}}]^4 + 32*\[Omega][{{Y}, {X, X, Y}}]^3*
            (48 + 178*M + 53*M^2 + (20 - 76*M + 3*M^2)*\[Omega][{{X, X}, 
                {Y, Y}}]) + 4*\[Omega][{{Y}, {X, X, Y}}]^2*
            (872 + 2296*M + 1508*M^2 + 197*M^3 + 2*(-72 - 1116*M - 254*M^2 + 
               15*M^3)*\[Omega][{{X, X}, {Y, Y}}] + (-96 + 424*M - 44*M^2)*
              \[Omega][{{X, X}, {Y, Y}}]^2) + 2*\[Omega][{{Y}, {X, X, Y}}]*
            (920 + 2728*M + 2556*M^2 + 859*M^3 + 61*M^4 + 
             (-1072 - 4112*M - 2804*M^2 - 146*M^3 + 23*M^4)*\[Omega][{{X, X}, 
                {Y, Y}}] - 4*(16 - 582*M - 63*M^2 + 17*M^3)*
              \[Omega][{{X, X}, {Y, Y}}]^2 + 4*(-20 - 64*M + 11*M^2)*
              \[Omega][{{X, X}, {Y, Y}}]^3)) + 4*\[Omega][{{X}, {X, Y, Y}}]^2*
          (336 + 1248*M + 1768*M^2 + 1188*M^3 + 379*M^4 + 48*M^5 + M^6 + 
           768*M*\[Omega][{{Y}, {X, X, Y}}]^5 + (-2000 - 5648*M - 5514*M^2 - 
             2083*M^3 - 170*M^4 + 36*M^5 + M^6)*\[Omega][{{X, X}, {Y, Y}}] + 
           (2320 + 5688*M + 3982*M^2 + 539*M^3 - 199*M^4 - 4*M^5)*
            \[Omega][{{X, X}, {Y, Y}}]^2 + (384 - 1856*M - 1336*M^2 + 
             330*M^3 - 3*M^4)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
           2*(-164 + 618*M - 110*M^2 + 11*M^3)*\[Omega][{{X, X}, {Y, Y}}]^4 - 
           4*(86 - 13*M + 4*M^2)*\[Omega][{{X, X}, {Y, Y}}]^5 + 
           32*\[Omega][{{Y}, {X, X, Y}}]^4*(96 + 222*M + 57*M^2 + 
             2*(34 - 33*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]) + 
           16*\[Omega][{{Y}, {X, X, Y}}]^3*(768 + 1410*M + 731*M^2 + 83*M^3 + 
             (36 - 928*M - 173*M^2 + 8*M^3)*\[Omega][{{X, X}, {Y, Y}}] - 
             2*(120 - 46*M + 5*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
           4*\[Omega][{{Y}, {X, X, Y}}]^2*(3312 + 7384*M + 5508*M^2 + 
             1508*M^3 + 91*M^4 + 4*(-884 - 2016*M - 1095*M^2 - 44*M^3 + 5*
                M^4)*\[Omega][{{X, X}, {Y, Y}}] + (-1848 + 2944*M + 34*M^2 - 
               50*M^3)*\[Omega][{{X, X}, {Y, Y}}]^2 + 4*(52 + 5*M^2)*
              \[Omega][{{X, X}, {Y, Y}}]^3) + 2*\[Omega][{{Y}, {X, X, Y}}]*
            (2384 + 6808*M + 7016*M^2 + 3126*M^3 + 539*M^4 + 17*M^5 + 
             (-6064 - 14104*M - 10390*M^2 - 2391*M^3 + 69*M^4 + 9*M^5)*
              \[Omega][{{X, X}, {Y, Y}}] + (1328 + 6604*M + 3982*M^2 - 400*
                M^3 - 33*M^4)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
             2*(956 - 1376*M + 221*M^2 + 10*M^3)*\[Omega][{{X, X}, {Y, Y}}]^
               3 + 4*(120 - 32*M + 3*M^2)*\[Omega][{{X, X}, {Y, Y}}]^4)) + 
         2*\[Omega][{{X}, {X, Y, Y}}]*(2*(2 + 3*M + M^2)^2*
            (38 + 39*M + 4*M^2) + (-2328 - 7244*M - 8262*M^2 - 4061*M^3 - 
             687*M^4 + 37*M^5 + 9*M^6)*\[Omega][{{X, X}, {Y, Y}}] + 
           (4864 + 11088*M + 7894*M^2 + 1417*M^3 - 315*M^4 - 55*M^5 + M^6)*
            \[Omega][{{X, X}, {Y, Y}}]^2 - (1888 + 2804*M + 688*M^2 - 
             367*M^3 - 107*M^4 + 9*M^5)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
           (-2176 - 984*M + 494*M^2 - 111*M^3 + 28*M^4)*
            \[Omega][{{X, X}, {Y, Y}}]^4 - 2*(-276 + 532*M - 65*M^2 + 18*M^3)*
            \[Omega][{{X, X}, {Y, Y}}]^5 + 16*(30 - 5*M + M^2)*
            \[Omega][{{X, X}, {Y, Y}}]^6 + 256*\[Omega][{{Y}, {X, X, Y}}]^5*
            (8 + 9*M + 2*M^2 + 8*\[Omega][{{X, X}, {Y, Y}}]) + 
           16*\[Omega][{{Y}, {X, X, Y}}]^4*(840 + 1156*M + 438*M^2 + 43*M^3 + 
             2*(76 - 108*M - 13*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}] - 
             8*(56 + 9*M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
           8*\[Omega][{{Y}, {X, X, Y}}]^3*(3152 + 5796*M + 3478*M^2 + 
             738*M^3 + 38*M^4 + (-2864 - 4092*M - 1416*M^2 + 3*M^3 + 5*M^4)*
              \[Omega][{{X, X}, {Y, Y}}] - 4*(632 + 80*M + 74*M^2 + M^3)*
              \[Omega][{{X, X}, {Y, Y}}]^2 + (816 + 304*M - 12*M^2)*
              \[Omega][{{X, X}, {Y, Y}}]^3) + 4*\[Omega][{{Y}, {X, X, Y}}]^2*
            (2*(2232 + 5530*M + 4857*M^2 + 1781*M^3 + 237*M^4 + 6*M^5) + 
             (-10376 - 18452*M - 10072*M^2 - 1504*M^3 + 77*M^4 + 4*M^5)*
              \[Omega][{{X, X}, {Y, Y}}] + (856 + 2420*M + 740*M^2 - 498*
                M^3 - 5*M^4)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
             (5600 + 576*M + 804*M^2 - 30*M^3)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
             4*(-8 - 112*M + 11*M^2)*\[Omega][{{X, X}, {Y, Y}}]^4) + 
           2*\[Omega][{{Y}, {X, X, Y}}]*(2392 + 7652*M + 9254*M^2 + 
             5195*M^3 + 1313*M^4 + 113*M^5 + M^6 + (-10032 - 24220*M - 19908*
                M^2 - 6109*M^3 - 346*M^4 + 56*M^5 + M^6)*\[Omega][{{X, X}, 
                {Y, Y}}] + (9144 + 15332*M + 6874*M^2 + 61*M^3 - 317*M^4)*
              \[Omega][{{X, X}, {Y, Y}}]^2 + (3616 + 2012*M + 36*M^2 + 615*
                M^3 - 28*M^4)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
             8*(-476 + 126*M - 75*M^2 + 9*M^3)*\[Omega][{{X, X}, {Y, Y}}]^4 - 
             8*(110 - 37*M + 6*M^2)*\[Omega][{{X, X}, {Y, Y}}]^5))) + 
       2*\[Lambda][{y}]^4*(2496 + 1032*M - 3996*M^2 - 4032*M^3 - 1065*M^4 - 
         51*M^5 + 22912*\[Omega][{{Y}, {X, X, Y}}] + 
         14552*M*\[Omega][{{Y}, {X, X, Y}}] - 14532*M^2*
          \[Omega][{{Y}, {X, X, Y}}] - 9138*M^3*\[Omega][{{Y}, {X, X, Y}}] - 
         1345*M^4*\[Omega][{{Y}, {X, X, Y}}] - 
         83*M^5*\[Omega][{{Y}, {X, X, Y}}] + 89536*\[Omega][{{Y}, {X, X, Y}}]^
           2 + 64256*M*\[Omega][{{Y}, {X, X, Y}}]^2 - 
         5648*M^2*\[Omega][{{Y}, {X, X, Y}}]^2 - 
         7722*M^3*\[Omega][{{Y}, {X, X, Y}}]^2 - 
         1050*M^4*\[Omega][{{Y}, {X, X, Y}}]^2 - 
         22*M^5*\[Omega][{{Y}, {X, X, Y}}]^2 + 
         178688*\[Omega][{{Y}, {X, X, Y}}]^3 + 104608*M*
          \[Omega][{{Y}, {X, X, Y}}]^3 - 6560*M^2*\[Omega][{{Y}, {X, X, Y}}]^
           3 - 5692*M^3*\[Omega][{{Y}, {X, X, Y}}]^3 - 
         240*M^4*\[Omega][{{Y}, {X, X, Y}}]^3 + 
         176384*\[Omega][{{Y}, {X, X, Y}}]^4 + 
         49568*M*\[Omega][{{Y}, {X, X, Y}}]^4 - 13376*M^2*
          \[Omega][{{Y}, {X, X, Y}}]^4 - 1328*M^3*\[Omega][{{Y}, {X, X, Y}}]^
           4 + 72704*\[Omega][{{Y}, {X, X, Y}}]^5 - 
         576*M*\[Omega][{{Y}, {X, X, Y}}]^5 - 3776*M^2*
          \[Omega][{{Y}, {X, X, Y}}]^5 + 10240*\[Omega][{{Y}, {X, X, Y}}]^6 - 
         2560*M*\[Omega][{{Y}, {X, X, Y}}]^6 - 
         14480*\[Omega][{{X, X}, {Y, Y}}] - 10480*M*
          \[Omega][{{X, X}, {Y, Y}}] - 732*M^2*\[Omega][{{X, X}, {Y, Y}}] + 
         2772*M^3*\[Omega][{{X, X}, {Y, Y}}] + 
         302*M^4*\[Omega][{{X, X}, {Y, Y}}] - 
         44*M^5*\[Omega][{{X, X}, {Y, Y}}] - 95904*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, X}, {Y, Y}}] - 72048*M*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, X}, {Y, Y}}] + 12540*M^2*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, X}, {Y, Y}}] + 12244*M^3*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, X}, {Y, Y}}] + 1926*M^4*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, X}, {Y, Y}}] - 40*M^5*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, X}, {Y, Y}}] - 235968*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}] - 87504*M*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}] + 63340*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}] + 19228*M^3*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}] + 1106*M^4*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}] - 6*M^5*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}] - 219008*\[Omega][{{Y}, {X, X, Y}}]^3*
          \[Omega][{{X, X}, {Y, Y}}] + 55984*M*\[Omega][{{Y}, {X, X, Y}}]^3*
          \[Omega][{{X, X}, {Y, Y}}] + 61816*M^2*\[Omega][{{Y}, {X, X, Y}}]^3*
          \[Omega][{{X, X}, {Y, Y}}] + 7748*M^3*\[Omega][{{Y}, {X, X, Y}}]^3*
          \[Omega][{{X, X}, {Y, Y}}] + 200*M^4*\[Omega][{{Y}, {X, X, Y}}]^3*
          \[Omega][{{X, X}, {Y, Y}}] - 9984*\[Omega][{{Y}, {X, X, Y}}]^4*
          \[Omega][{{X, X}, {Y, Y}}] + 85664*M*\[Omega][{{Y}, {X, X, Y}}]^4*
          \[Omega][{{X, X}, {Y, Y}}] + 14080*M^2*\[Omega][{{Y}, {X, X, Y}}]^4*
          \[Omega][{{X, X}, {Y, Y}}] + 960*M^3*\[Omega][{{Y}, {X, X, Y}}]^4*
          \[Omega][{{X, X}, {Y, Y}}] + 44544*\[Omega][{{Y}, {X, X, Y}}]^5*
          \[Omega][{{X, X}, {Y, Y}}] + 20288*M*\[Omega][{{Y}, {X, X, Y}}]^5*
          \[Omega][{{X, X}, {Y, Y}}] + 10240*\[Omega][{{Y}, {X, X, Y}}]^6*
          \[Omega][{{X, X}, {Y, Y}}] + 17312*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         14320*M*\[Omega][{{X, X}, {Y, Y}}]^2 + 29924*M^2*
          \[Omega][{{X, X}, {Y, Y}}]^2 + 24484*M^3*\[Omega][{{X, X}, {Y, Y}}]^
           2 + 4359*M^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         9*M^5*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         46336*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         51704*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         114316*M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         46426*M^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         4945*M^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         3*M^5*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         58816*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         45704*M*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         114048*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^
           2 + 30818*M^3*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}]^2 + 1344*M^4*\[Omega][{{Y}, {X, X, Y}}]^
           2*\[Omega][{{X, X}, {Y, Y}}]^2 - 257664*\[Omega][{{Y}, {X, X, Y}}]^
           3*\[Omega][{{X, X}, {Y, Y}}]^2 - 72080*M*
          \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         42952*M^2*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^
           2 + 6616*M^3*\[Omega][{{Y}, {X, X, Y}}]^3*
          \[Omega][{{X, X}, {Y, Y}}]^2 + 72*M^4*\[Omega][{{Y}, {X, X, Y}}]^3*
          \[Omega][{{X, X}, {Y, Y}}]^2 - 162048*\[Omega][{{Y}, {X, X, Y}}]^4*
          \[Omega][{{X, X}, {Y, Y}}]^2 - 20544*M*\[Omega][{{Y}, {X, X, Y}}]^4*
          \[Omega][{{X, X}, {Y, Y}}]^2 + 5152*M^2*\[Omega][{{Y}, {X, X, Y}}]^
           4*\[Omega][{{X, X}, {Y, Y}}]^2 + 288*M^3*
          \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         28160*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         896*M*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         19328*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         6912*M*\[Omega][{{X, X}, {Y, Y}}]^3 + 32852*M^2*
          \[Omega][{{X, X}, {Y, Y}}]^3 + 19062*M^3*\[Omega][{{X, X}, {Y, Y}}]^
           3 + 2604*M^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         143200*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         221312*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         117076*M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         27390*M^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         1792*M^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         287808*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         220976*M*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         89924*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^
           3 + 12072*M^3*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}]^3 + 308*M^4*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}]^3 + 151168*\[Omega][{{Y}, {X, X, Y}}]^3*
          \[Omega][{{X, X}, {Y, Y}}]^3 + 69728*M*\[Omega][{{Y}, {X, X, Y}}]^3*
          \[Omega][{{X, X}, {Y, Y}}]^3 + 24048*M^2*\[Omega][{{Y}, {X, X, Y}}]^
           3*\[Omega][{{X, X}, {Y, Y}}]^3 + 1632*M^3*
          \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         24320*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         7040*M*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         1728*M^2*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         36224*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         22632*M*\[Omega][{{X, X}, {Y, Y}}]^4 + 25972*M^2*
          \[Omega][{{X, X}, {Y, Y}}]^4 + 4974*M^3*\[Omega][{{X, X}, {Y, Y}}]^
           4 + 408*M^4*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         95552*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         96496*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         48412*M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         4682*M^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         136*M^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         31808*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         47544*M*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         19560*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^
           4 + 1008*M^3*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}]^4 - 1920*\[Omega][{{Y}, {X, X, Y}}]^3*
          \[Omega][{{X, X}, {Y, Y}}]^4 + 5376*M*\[Omega][{{Y}, {X, X, Y}}]^3*
          \[Omega][{{X, X}, {Y, Y}}]^4 + 2208*M^2*\[Omega][{{Y}, {X, X, Y}}]^
           3*\[Omega][{{X, X}, {Y, Y}}]^4 + 5328*\[Omega][{{X, X}, {Y, Y}}]^
           5 + 31488*M*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         13092*M^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         396*M^3*\[Omega][{{X, X}, {Y, Y}}]^5 - 
         17856*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         28224*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         11036*M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         132*M^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
         6720*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         6160*M*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         2224*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         6240*\[Omega][{{X, X}, {Y, Y}}]^6 + 
         8016*M*\[Omega][{{X, X}, {Y, Y}}]^6 + 2328*M^2*
          \[Omega][{{X, X}, {Y, Y}}]^6 + 2240*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, X}, {Y, Y}}]^6 + 2672*M*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, X}, {Y, Y}}]^6 + 776*M^2*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, X}, {Y, Y}}]^6 - 64*M*\[Omega][{{X}, {X, Y, Y}}]^5*
          (-59 + 5*M - 78*\[Omega][{{X, X}, {Y, Y}}])*
          (3 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]) - 
         16*\[Omega][{{X}, {X, Y, Y}}]^4*(-320 - 3470*M - 403*M^2 + 89*M^3 + 
           (-320 - 6866*M - 1093*M^2 + 59*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
           2*(240 - 2900*M - 196*M^2 + 5*M^3)*\[Omega][{{X, X}, {Y, Y}}]^2 - 
           8*(-20 + 162*M + 3*M^2)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
           8*\[Omega][{{Y}, {X, X, Y}}]^2*(-80 - 197*M + 16*M^2 + 
             (-80 - 294*M + 8*M^2)*\[Omega][{{X, X}, {Y, Y}}]) + 
           2*\[Omega][{{Y}, {X, X, Y}}]*(-480 - 2392*M - 29*M^2 + 29*M^3 + 
             10*(-64 - 407*M - 24*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}] - 
             4*M*(471 + 2*M)*\[Omega][{{X, X}, {Y, Y}}]^2)) - 
         4*\[Omega][{{X}, {X, Y, Y}}]^3*(-6528 - 20000*M - 10238*M^2 + 
           1345*M^3 + 84*M^4 + (-1088 - 48300*M - 23278*M^2 + 237*M^3 + 
             32*M^4)*\[Omega][{{X, X}, {Y, Y}}] + 
           2*(5856 - 43470*M - 8480*M^2 - 493*M^3 + M^4)*
            \[Omega][{{X, X}, {Y, Y}}]^2 - 4*(584 + 10778*M + 993*M^2 + 
             76*M^3)*\[Omega][{{X, X}, {Y, Y}}]^3 - 8*(220 + 778*M + 23*M^2)*
            \[Omega][{{X, X}, {Y, Y}}]^4 + 32*\[Omega][{{Y}, {X, X, Y}}]^3*
            (-280 - 182*M + 17*M^2 + 2*(-140 - 152*M + 7*M^2)*
              \[Omega][{{X, X}, {Y, Y}}]) + 16*\[Omega][{{Y}, {X, X, Y}}]^2*
            (-1736 - 2277*M + 10*M^2 + 24*M^3 + (-1416 - 4124*M - 195*M^2 + 4*
                M^3)*\[Omega][{{X, X}, {Y, Y}}] - 2*(-220 + 896*M + 45*M^2)*
              \[Omega][{{X, X}, {Y, Y}}]^2) + 2*\[Omega][{{Y}, {X, X, Y}}]*
            (-12352 - 27348*M - 4134*M^2 + 787*M^3 + 26*M^4 + 
             2*(-3728 - 28292*M - 4988*M^2 + 35*M^3 + M^4)*\[Omega][{{X, X}, 
                {Y, Y}}] - 4*(-2600 + 13604*M + 1421*M^2 + 72*M^3)*
              \[Omega][{{X, X}, {Y, Y}}]^2 - 8*(-140 + 1522*M + 75*M^2)*
              \[Omega][{{X, X}, {Y, Y}}]^3)) + 2*\[Omega][{{X}, {X, Y, Y}}]^2*
          (19008 + 26808*M + 19132*M^2 - 2099*M^3 - 777*M^4 - 11*M^5 + 
           (-21056 + 36616*M + 61834*M^2 + 6160*M^3 - 251*M^4 - 3*M^5)*
            \[Omega][{{X, X}, {Y, Y}}] + (-37696 + 179788*M + 97296*M^2 + 
             14381*M^3 + 410*M^4)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           2*(19424 + 110748*M + 18379*M^2 + 2671*M^3 + 67*M^4)*
            \[Omega][{{X, X}, {Y, Y}}]^3 + (4256 + 74100*M + 6284*M^2 + 
             456*M^3)*\[Omega][{{X, X}, {Y, Y}}]^4 + 
           8*(-420 + 827*M + 114*M^2)*\[Omega][{{X, X}, {Y, Y}}]^5 - 
           64*\[Omega][{{Y}, {X, X, Y}}]^4*(-400 - 7*M + 6*M^2 + 
             (-400 - 74*M + 6*M^2)*\[Omega][{{X, X}, {Y, Y}}]) + 
           16*\[Omega][{{Y}, {X, X, Y}}]^3*(7216 + 2850*M - 301*M^2 - 
             13*M^3 + 2*(2408 + 2948*M + 57*M^2 + 10*M^3)*\[Omega][{{X, X}, 
                {Y, Y}}] + 4*(-620 + 618*M + 67*M^2)*\[Omega][{{X, X}, 
                 {Y, Y}}]^2) + 8*\[Omega][{{Y}, {X, X, Y}}]^2*
            (20736 + 17836*M + 677*M^2 - 413*M^3 - 15*M^4 + 
             (4656 + 35324*M + 5691*M^2 + 190*M^3 + 8*M^4)*\[Omega][{{X, X}, 
                {Y, Y}}] + 2*(-8904 + 14362*M + 2917*M^2 + 180*M^3)*
              \[Omega][{{X, X}, {Y, Y}}]^2 + (880 + 6888*M + 740*M^2)*
              \[Omega][{{X, X}, {Y, Y}}]^3) + 2*\[Omega][{{Y}, {X, X, Y}}]*
            (47648 + 64768*M + 14980*M^2 - 2777*M^3 - 383*M^4 - 3*M^5 + 
             (-18176 + 125492*M + 53294*M^2 + 2617*M^3 + 26*M^4)*
              \[Omega][{{X, X}, {Y, Y}}] + 2*(-37440 + 94194*M + 33007*M^2 + 
               3150*M^3 + 75*M^4)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
             4*(5032 + 28566*M + 3993*M^2 + 284*M^3)*\[Omega][{{X, X}, 
                 {Y, Y}}]^3 + 8*(620 + 1968*M + 229*M^2)*\[Omega][
                {{X, X}, {Y, Y}}]^4)) + \[Omega][{{X}, {X, Y, Y}}]*
          (18112 + 22664*M - 3252*M^2 - 3966*M^3 - 1993*M^4 - 83*M^5 - 
           2*(28112 + 33112*M - 18026*M^2 - 9360*M^3 + 9*M^4 + 20*M^5)*
            \[Omega][{{X, X}, {Y, Y}}] + (-6080 + 41240*M + 164084*M^2 + 
             52482*M^3 + 4093*M^4 - 3*M^5)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           2*(50352 + 160496*M + 52550*M^2 + 13485*M^3 + 836*M^4)*
            \[Omega][{{X, X}, {Y, Y}}]^3 + 2*(-21088 + 110720*M + 20342*M^2 + 
             2197*M^3 + 68*M^4)*\[Omega][{{X, X}, {Y, Y}}]^4 + 
           4*(-4144 + 12360*M + 2459*M^2 + 33*M^3)*\[Omega][{{X, X}, {Y, Y}}]^
             5 + 8*(280 + 334*M + 97*M^2)*\[Omega][{{X, X}, {Y, Y}}]^6 - 
           128*\[Omega][{{Y}, {X, X, Y}}]^5*(-280 + 57*M + 14*(-20 + M)*
              \[Omega][{{X, X}, {Y, Y}}]) + 64*\[Omega][{{Y}, {X, X, Y}}]^4*
            (3248 + 266*M - 170*M^2 + 3*M^3 + (2008 + 1099*M - 25*M^2 + 9*
                M^3)*\[Omega][{{X, X}, {Y, Y}}] + 2*(-620 + 147*M + 24*M^2)*
              \[Omega][{{X, X}, {Y, Y}}]^2) + 8*\[Omega][{{Y}, {X, X, Y}}]^3*
            (50848 + 20740*M - 3022*M^2 - 431*M^3 - 4*M^4 + 
             2*(1184 + 19044*M + 2520*M^2 + 262*M^3 + 9*M^4)*
              \[Omega][{{X, X}, {Y, Y}}] + 4*(-11208 + 4096*M + 1649*M^2 + 
               112*M^3)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
             8*(700 + 634*M + 143*M^2)*\[Omega][{{X, X}, {Y, Y}}]^3) + 
           4*\[Omega][{{Y}, {X, X, Y}}]^2*(82912 + 62664*M + 1782*M^2 - 
             3383*M^3 - 227*M^4 - 3*M^5 + (-70688 + 83092*M + 44294*M^2 + 
               3483*M^3 + 228*M^4)*\[Omega][{{X, X}, {Y, Y}}] + 
             2*(-61040 + 39442*M + 30304*M^2 + 3628*M^3 + 85*M^4)*
              \[Omega][{{X, X}, {Y, Y}}]^2 + 4*(13576 + 16626*M + 4815*M^2 + 
               310*M^3)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
             8*(340 + 1358*M + 275*M^2)*\[Omega][{{X, X}, {Y, Y}}]^4) - 
           4*\[Omega][{{Y}, {X, X, Y}}]*(-31024 - 29276*M - 6234*M^2 + 
             3670*M^3 + 648*M^4 + 20*M^5 + (59984 - 1472*M - 49380*M^2 - 6305*
                M^3 - 307*M^4 + 3*M^5)*\[Omega][{{X, X}, {Y, Y}}] - 
             (-44592 + 62260*M + 93748*M^2 + 17301*M^3 + 949*M^4)*
              \[Omega][{{X, X}, {Y, Y}}]^2 - (79504 + 135992*M + 42588*M^2 + 
               6049*M^3 + 144*M^4)*\[Omega][{{X, X}, {Y, Y}}]^3 - 
             8*(-48 + 6849*M + 1346*M^2 + 60*M^3)*\[Omega][{{X, X}, {Y, Y}}]^
               4 - 4*(-840 + 1212*M + 253*M^2)*\[Omega][{{X, X}, {Y, Y}}]^
               5))) + 2*\[Lambda][{y}]^3*(-2976 - 8184*M - 8040*M^2 - 
         3030*M^3 + 54*M^4 + 306*M^5 + 54*M^6 - 
         29312*\[Omega][{{Y}, {X, X, Y}}] - 67672*M*
          \[Omega][{{Y}, {X, X, Y}}] - 52768*M^2*\[Omega][{{Y}, {X, X, Y}}] - 
         13442*M^3*\[Omega][{{Y}, {X, X, Y}}] + 
         1713*M^4*\[Omega][{{Y}, {X, X, Y}}] + 1065*M^5*
          \[Omega][{{Y}, {X, X, Y}}] + 72*M^6*\[Omega][{{Y}, {X, X, Y}}] - 
         119456*\[Omega][{{Y}, {X, X, Y}}]^2 - 222000*M*
          \[Omega][{{Y}, {X, X, Y}}]^2 - 126568*M^2*
          \[Omega][{{Y}, {X, X, Y}}]^2 - 14368*M^3*\[Omega][{{Y}, {X, X, Y}}]^
           2 + 5803*M^4*\[Omega][{{Y}, {X, X, Y}}]^2 + 
         963*M^5*\[Omega][{{Y}, {X, X, Y}}]^2 + 
         18*M^6*\[Omega][{{Y}, {X, X, Y}}]^2 - 
         249024*\[Omega][{{Y}, {X, X, Y}}]^3 - 343648*M*
          \[Omega][{{Y}, {X, X, Y}}]^3 - 116544*M^2*
          \[Omega][{{Y}, {X, X, Y}}]^3 + 6576*M^3*\[Omega][{{Y}, {X, X, Y}}]^
           3 + 4554*M^4*\[Omega][{{Y}, {X, X, Y}}]^3 + 
         214*M^5*\[Omega][{{Y}, {X, X, Y}}]^3 - 
         268160*\[Omega][{{Y}, {X, X, Y}}]^4 - 237440*M*
          \[Omega][{{Y}, {X, X, Y}}]^4 - 25200*M^2*\[Omega][{{Y}, {X, X, Y}}]^
           4 + 8608*M^3*\[Omega][{{Y}, {X, X, Y}}]^4 + 
         936*M^4*\[Omega][{{Y}, {X, X, Y}}]^4 - 
         133376*\[Omega][{{Y}, {X, X, Y}}]^5 - 
         56832*M*\[Omega][{{Y}, {X, X, Y}}]^5 + 
         3936*M^2*\[Omega][{{Y}, {X, X, Y}}]^5 + 
         1760*M^3*\[Omega][{{Y}, {X, X, Y}}]^5 - 
         22528*\[Omega][{{Y}, {X, X, Y}}]^6 - 
         2816*M*\[Omega][{{Y}, {X, X, Y}}]^6 + 1152*M^2*
          \[Omega][{{Y}, {X, X, Y}}]^6 + 16408*\[Omega][{{X, X}, {Y, Y}}] + 
         35564*M*\[Omega][{{X, X}, {Y, Y}}] + 25826*M^2*
          \[Omega][{{X, X}, {Y, Y}}] + 6097*M^3*\[Omega][{{X, X}, {Y, Y}}] - 
         171*M^4*\[Omega][{{X, X}, {Y, Y}}] + 
         39*M^5*\[Omega][{{X, X}, {Y, Y}}] + 
         72*M^6*\[Omega][{{X, X}, {Y, Y}}] + 
         128576*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
         218264*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
         104508*M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
         6376*M^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
         3128*M^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
         594*M^5*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
         63*M^6*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
         375616*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
         436048*M*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
         84632*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
         26806*M^3*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
         652*M^4*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
         570*M^5*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
         12*M^6*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
         466944*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
         253280*M*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
         71272*M^2*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
         18276*M^3*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
         1298*M^4*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
         122*M^5*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
         188544*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
         72512*M*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
         57040*M^2*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
         1904*M^3*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
         424*M^4*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
         26624*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] - 
         60800*M*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] - 
         9632*M^2*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] + 
         512*M^3*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] - 
         17408*\[Omega][{{Y}, {X, X, Y}}]^6*\[Omega][{{X, X}, {Y, Y}}] - 
         7168*M*\[Omega][{{Y}, {X, X, Y}}]^6*\[Omega][{{X, X}, {Y, Y}}] - 
         26912*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         36856*M*\[Omega][{{X, X}, {Y, Y}}]^2 - 10160*M^2*
          \[Omega][{{X, X}, {Y, Y}}]^2 + 3998*M^3*\[Omega][{{X, X}, {Y, Y}}]^
           2 + 597*M^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         354*M^5*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         27*M^6*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         136608*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         87672*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         60872*M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         35920*M^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         1470*M^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         77*M^5*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         15*M^6*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         153600*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         176864*M*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         235028*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^
           2 + 23080*M^3*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}]^2 - 261*M^4*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}]^2 + 95*M^5*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}]^2 + 2*M^6*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}]^2 + 145408*\[Omega][{{Y}, {X, X, Y}}]^3*
          \[Omega][{{X, X}, {Y, Y}}]^2 + 460352*M*\[Omega][{{Y}, {X, X, Y}}]^
           3*\[Omega][{{X, X}, {Y, Y}}]^2 + 138648*M^2*
          \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         7324*M^3*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         372*M^4*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         24*M^5*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         263040*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         211008*M*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         29184*M^2*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^
           2 + 1120*M^3*\[Omega][{{Y}, {X, X, Y}}]^4*
          \[Omega][{{X, X}, {Y, Y}}]^2 + 96*M^4*\[Omega][{{Y}, {X, X, Y}}]^4*
          \[Omega][{{X, X}, {Y, Y}}]^2 + 83712*\[Omega][{{Y}, {X, X, Y}}]^5*
          \[Omega][{{X, X}, {Y, Y}}]^2 + 28160*M*\[Omega][{{Y}, {X, X, Y}}]^5*
          \[Omega][{{X, X}, {Y, Y}}]^2 + 1216*M^2*\[Omega][{{Y}, {X, X, Y}}]^
           5*\[Omega][{{X, X}, {Y, Y}}]^2 + 128*M^3*
          \[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         5120*\[Omega][{{Y}, {X, X, Y}}]^6*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         400*\[Omega][{{X, X}, {Y, Y}}]^3 - 25744*M*
          \[Omega][{{X, X}, {Y, Y}}]^3 - 28742*M^2*\[Omega][{{X, X}, {Y, Y}}]^
           3 - 4059*M^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         1507*M^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         192*M^5*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         3*M^6*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         81024*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         226208*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         91084*M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         13510*M^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         556*M^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         82*M^5*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         M^6*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         347232*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         403368*M*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         6184*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         8006*M^3*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         174*M^4*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         6*M^5*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         389120*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         155664*M*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         9584*M^2*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         1184*M^3*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         56*M^4*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         128384*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         19648*M*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         2880*M^2*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         32*M^3*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         12800*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         512*M*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         128*M^2*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         34176*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         50152*M*\[Omega][{{X, X}, {Y, Y}}]^4 + 22232*M^2*
          \[Omega][{{X, X}, {Y, Y}}]^4 + 1328*M^3*\[Omega][{{X, X}, {Y, Y}}]^
           4 + 1005*M^4*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         27*M^5*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         168096*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         169472*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         9272*M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         7330*M^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         563*M^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         9*M^5*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         210464*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         73296*M*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         18612*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^
           4 + 4156*M^3*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}]^4 + 76*M^4*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}]^4 + 67904*\[Omega][{{Y}, {X, X, Y}}]^3*
          \[Omega][{{X, X}, {Y, Y}}]^4 + 20304*M*\[Omega][{{Y}, {X, X, Y}}]^3*
          \[Omega][{{X, X}, {Y, Y}}]^4 + 7584*M^2*\[Omega][{{Y}, {X, X, Y}}]^
           3*\[Omega][{{X, X}, {Y, Y}}]^4 + 624*M^3*
          \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         8960*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         1792*M*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         832*M^2*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         21448*\[Omega][{{X, X}, {Y, Y}}]^5 - 
         8348*M*\[Omega][{{X, X}, {Y, Y}}]^5 + 11316*M^2*
          \[Omega][{{X, X}, {Y, Y}}]^5 + 630*M^3*\[Omega][{{X, X}, {Y, Y}}]^
           5 + 192*M^4*\[Omega][{{X, X}, {Y, Y}}]^5 - 
         32960*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         48456*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         8232*M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         1002*M^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         64*M^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         5024*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         20232*M*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         2296*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         264*M^3*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         640*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         2272*M*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         224*M^2*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^5 - 
         2368*\[Omega][{{X, X}, {Y, Y}}]^6 - 
         6392*M*\[Omega][{{X, X}, {Y, Y}}]^6 + 1920*M^2*
          \[Omega][{{X, X}, {Y, Y}}]^6 - 108*M^3*\[Omega][{{X, X}, {Y, Y}}]^
           6 - 13568*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
           6 + 2256*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
           6 + 1360*M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
           6 - 36*M^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
           6 - 2560*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^
           6 + 1264*M*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^
           6 + 240*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}]^6 + 2720*\[Omega][{{X, X}, {Y, Y}}]^7 + 
         768*M*\[Omega][{{X, X}, {Y, Y}}]^7 + 
         480*M^2*\[Omega][{{X, X}, {Y, Y}}]^7 + 
         640*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^7 + 
         256*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^7 + 
         160*M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^7 + 
         384*M*\[Omega][{{X}, {X, Y, Y}}]^6*
          (3 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
          (1 + 2*\[Omega][{{X, X}, {Y, Y}}]) + 
         32*M*\[Omega][{{X}, {X, Y, Y}}]^5*(-2 - 5*M + 16*M^2 + 
           (762 + 99*M + 6*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
           (896 + 34*M)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           208*\[Omega][{{X, X}, {Y, Y}}]^3 + 32*\[Omega][{{Y}, {X, X, Y}}]^2*
            (-2 + M + 11*\[Omega][{{X, X}, {Y, Y}}]) + 
           2*\[Omega][{{Y}, {X, X, Y}}]*(-22 + 13*M + 6*M^2 + 
             (520 + 42*M)*\[Omega][{{X, X}, {Y, Y}}] + 
             296*\[Omega][{{X, X}, {Y, Y}}]^2)) + 
         8*\[Omega][{{X}, {X, Y, Y}}]^4*(-768 - 3376*M - 1074*M^2 + 127*M^3 + 
           75*M^4 + (-128 + 9204*M + 3238*M^2 + 39*M^3 + 45*M^4)*
            \[Omega][{{X, X}, {Y, Y}}] + 2*(576 + 8234*M + 1596*M^2 - 5*M^3 + 
             3*M^4)*\[Omega][{{X, X}, {Y, Y}}]^2 + (-96 + 8216*M + 708*M^2)*
            \[Omega][{{X, X}, {Y, Y}}]^3 + 8*(-20 + 174*M + 3*M^2)*
            \[Omega][{{X, X}, {Y, Y}}]^4 + 32*\[Omega][{{Y}, {X, X, Y}}]^3*
            (-40 - 41*M + 11*M^2 + 2*(-20 + 39*M + M^2)*\[Omega][{{X, X}, 
                {Y, Y}}]) + 8*\[Omega][{{Y}, {X, X, Y}}]^2*
            (-592 - 658*M + 15*M^2 + 34*M^3 + 2*(-216 + 867*M + 68*M^2 + 4*
                M^3)*\[Omega][{{X, X}, {Y, Y}}] + 4*(20 + 199*M + M^2)*
              \[Omega][{{X, X}, {Y, Y}}]^2) + 2*\[Omega][{{Y}, {X, X, Y}}]*
            (-1792 - 3656*M - 600*M^2 + 217*M^3 + 27*M^4 + 
             2*(-368 + 5166*M + 961*M^2 + 53*M^3 + 3*M^4)*\[Omega][{{X, X}, 
                {Y, Y}}] + 8*(140 + 1322*M + 105*M^2 + 2*M^3)*
              \[Omega][{{X, X}, {Y, Y}}]^2 + 8*(20 + 334*M + 3*M^2)*
              \[Omega][{{X, X}, {Y, Y}}]^3)) + 2*\[Omega][{{X}, {X, Y, Y}}]^3*
          (-14400 - 36864*M - 21416*M^2 - 2334*M^3 + 1227*M^4 + 95*M^5 + 
           (9856 + 52800*M + 34052*M^2 + 954*M^3 + 729*M^4 + 63*M^5)*
            \[Omega][{{X, X}, {Y, Y}}] + 2*(10384 + 48252*M + 25410*M^2 + 
             756*M^3 + 16*M^4 + 5*M^5)*\[Omega][{{X, X}, {Y, Y}}]^2 - 
           4*(3840 - 21314*M - 4774*M^2 - 303*M^3 + 4*M^4)*
            \[Omega][{{X, X}, {Y, Y}}]^3 + 8*(-308 + 4164*M + 387*M^2 + 
             37*M^3)*\[Omega][{{X, X}, {Y, Y}}]^4 + 32*(50 + 118*M + 5*M^2)*
            \[Omega][{{X, X}, {Y, Y}}]^5 + 256*\[Omega][{{Y}, {X, X, Y}}]^4*
            (-60 - 23*M + 5*M^2 + 2*(-30 + 12*M + M^2)*\[Omega][{{X, X}, 
                {Y, Y}}]) + 64*\[Omega][{{Y}, {X, X, Y}}]^3*
            (-1316 - 741*M + 4*M^2 + 26*M^3 + (-876 + 802*M + 103*M^2 + 11*
                M^3)*\[Omega][{{X, X}, {Y, Y}}] + 8*(40 + 49*M)*
              \[Omega][{{X, X}, {Y, Y}}]^2) + 8*\[Omega][{{Y}, {X, X, Y}}]^2*
            (-17056 - 15756*M - 2602*M^2 + 626*M^3 + 77*M^4 + 
             2*(-2320 + 8962*M + 2000*M^2 + 213*M^3 + 14*M^4)*
              \[Omega][{{X, X}, {Y, Y}}] + 4*(2348 + 4458*M + 413*M^2 + 7*
                M^3)*\[Omega][{{X, X}, {Y, Y}}]^2 + 32*(-10 + 138*M + 7*M^2)*
              \[Omega][{{X, X}, {Y, Y}}]^3) + 2*\[Omega][{{Y}, {X, X, Y}}]*
            (-38944 - 60816*M - 21252*M^2 + 1042*M^3 + 867*M^4 + 33*M^5 + 
             2*(4080 + 39464*M + 13708*M^2 + 1362*M^3 + 201*M^4 + 5*M^5)*
              \[Omega][{{X, X}, {Y, Y}}] + 8*(4780 + 14431*M + 3149*M^2 + 76*
                M^3 + 5*M^4)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
             8*(-1044 + 7750*M + 851*M^2 + 33*M^3)*\[Omega][{{X, X}, {Y, Y}}]^
               3 + 128*(-20 + 77*M + 5*M^2)*\[Omega][{{X, X}, {Y, Y}}]^4)) + 
         \[Omega][{{X}, {X, Y, Y}}]^2*(-36736 - 87472*M - 67936*M^2 - 
           13132*M^3 + 2229*M^4 + 823*M^5 + 18*M^6 + 
           2*(38224 + 65472*M + 40028*M^2 + 5531*M^3 + 451*M^4 + 310*M^5 + 
             6*M^6)*\[Omega][{{X, X}, {Y, Y}}] + (23584 + 94432*M + 
             123164*M^2 + 17372*M^3 - 1179*M^4 + 79*M^5 + 2*M^6)*
            \[Omega][{{X, X}, {Y, Y}}]^2 - 2*(51520 - 24180*M - 32416*M^2 - 
             7627*M^3 + 30*M^4 + 4*M^5)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
           4*(7000 + 29636*M + 4869*M^2 + 1139*M^3 + 25*M^4)*
            \[Omega][{{X, X}, {Y, Y}}]^4 + 8*(1788 + 5009*M + 375*M^2 + 
             28*M^3)*\[Omega][{{X, X}, {Y, Y}}]^5 + 
           16*(-160 + 143*M + 27*M^2)*\[Omega][{{X, X}, {Y, Y}}]^6 + 
           256*\[Omega][{{Y}, {X, X, Y}}]^5*(-120 - 16*M + 3*M^2 + 
             2*(-60 + 4*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]) + 
           64*\[Omega][{{Y}, {X, X, Y}}]^4*(-3840 - 1244*M + 41*M^2 + 
             28*M^3 + 2*(-1200 + 10*M + 78*M^2 + 9*M^3)*\[Omega][{{X, X}, 
                {Y, Y}}] + 4*(280 + 76*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]^
               2) + 16*\[Omega][{{Y}, {X, X, Y}}]^3*(-37248 - 24392*M - 
             2242*M^2 + 693*M^3 + 67*M^4 + (-4928 + 3524*M + 2448*M^2 + 640*
                M^3 + 34*M^4)*\[Omega][{{X, X}, {Y, Y}}] + 
             4*(5868 + 3564*M + 215*M^2 + 9*M^3)*\[Omega][{{X, X}, {Y, Y}}]^
               2 + 16*(-160 + 173*M + 18*M^2)*\[Omega][{{X, X}, {Y, Y}}]^3) + 
           8*\[Omega][{{Y}, {X, X, Y}}]^2*(-72928 - 82576*M - 20152*M^2 + 
             1435*M^3 + 726*M^4 + 29*M^5 + (40624 + 38156*M + 13376*M^2 + 
               2845*M^3 + 431*M^4 + 11*M^5)*\[Omega][{{X, X}, {Y, Y}}] + 
             2*(36376 + 39930*M + 7208*M^2 + 131*M^3 + 11*M^4)*
              \[Omega][{{X, X}, {Y, Y}}]^2 + 4*(-5868 + 7268*M + 1517*M^2 + 
               71*M^3)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
             16*(-60 + 337*M + 34*M^2)*\[Omega][{{X, X}, {Y, Y}}]^4) + 
           2*\[Omega][{{Y}, {X, X, Y}}]*(-122496 - 213840*M - 97144*M^2 - 
             5692*M^3 + 3797*M^4 + 469*M^5 + 6*M^6 + 
             (161280 + 200464*M + 79212*M^2 + 10630*M^3 + 2655*M^4 + 227*
                M^5 + 2*M^6)*\[Omega][{{X, X}, {Y, Y}}] + 
             2*(62256 + 145312*M + 59078*M^2 + 1899*M^3 + 46*M^4 + 7*M^5)*
              \[Omega][{{X, X}, {Y, Y}}]^2 + 4*(-37072 + 36026*M + 16098*
                M^2 + 1290*M^3 + 19*M^4)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
             24*(-156 + 3015*M + 520*M^2 + 33*M^3)*\[Omega][{{X, X}, {Y, Y}}]^
               4 + 16*(380 + 543*M + 59*M^2)*\[Omega][{{X, X}, {Y, Y}}]^5)) + 
         \[Omega][{{X}, {X, Y, Y}}]*(-17888 - 45688*M - 40024*M^2 - 
           12434*M^3 + 819*M^4 + 861*M^5 + 72*M^6 + 
           66624*\[Omega][{{X, X}, {Y, Y}}] + 128104*M*
            \[Omega][{{X, X}, {Y, Y}}] + 70308*M^2*\[Omega][
             {{X, X}, {Y, Y}}] + 10828*M^3*\[Omega][{{X, X}, {Y, Y}}] - 
           222*M^4*\[Omega][{{X, X}, {Y, Y}}] + 664*M^5*
            \[Omega][{{X, X}, {Y, Y}}] + 63*M^6*\[Omega][{{X, X}, {Y, Y}}] - 
           45824*\[Omega][{{X, X}, {Y, Y}}]^2 - 24904*M*
            \[Omega][{{X, X}, {Y, Y}}]^2 + 45776*M^2*
            \[Omega][{{X, X}, {Y, Y}}]^2 + 22912*M^3*
            \[Omega][{{X, X}, {Y, Y}}]^2 - 2172*M^4*
            \[Omega][{{X, X}, {Y, Y}}]^2 - 89*M^5*\[Omega][{{X, X}, {Y, Y}}]^
             2 + 15*M^6*\[Omega][{{X, X}, {Y, Y}}]^2 - 
           71200*\[Omega][{{X, X}, {Y, Y}}]^3 - 138992*M*
            \[Omega][{{X, X}, {Y, Y}}]^3 - 10732*M^2*
            \[Omega][{{X, X}, {Y, Y}}]^3 + 22278*M^3*
            \[Omega][{{X, X}, {Y, Y}}]^3 + 682*M^4*\[Omega][{{X, X}, {Y, Y}}]^
             3 - 88*M^5*\[Omega][{{X, X}, {Y, Y}}]^3 + 
           M^6*\[Omega][{{X, X}, {Y, Y}}]^3 + 
           83232*\[Omega][{{X, X}, {Y, Y}}]^4 + 89104*M*
            \[Omega][{{X, X}, {Y, Y}}]^4 + 12888*M^2*
            \[Omega][{{X, X}, {Y, Y}}]^4 + 8818*M^3*
            \[Omega][{{X, X}, {Y, Y}}]^4 + 635*M^4*\[Omega][{{X, X}, {Y, Y}}]^
             4 - 9*M^5*\[Omega][{{X, X}, {Y, Y}}]^4 - 
           4256*\[Omega][{{X, X}, {Y, Y}}]^5 + 54600*M*
            \[Omega][{{X, X}, {Y, Y}}]^5 + 9480*M^2*
            \[Omega][{{X, X}, {Y, Y}}]^5 + 882*M^3*\[Omega][{{X, X}, {Y, Y}}]^
             5 + 64*M^4*\[Omega][{{X, X}, {Y, Y}}]^5 - 
           11328*\[Omega][{{X, X}, {Y, Y}}]^6 + 
           5328*M*\[Omega][{{X, X}, {Y, Y}}]^6 + 1936*M^2*
            \[Omega][{{X, X}, {Y, Y}}]^6 - 36*M^3*\[Omega][{{X, X}, {Y, Y}}]^
             6 + 640*\[Omega][{{X, X}, {Y, Y}}]^7 + 
           256*M*\[Omega][{{X, X}, {Y, Y}}]^7 + 160*M^2*
            \[Omega][{{X, X}, {Y, Y}}]^7 - 10240*\[Omega][{{Y}, {X, X, Y}}]^6*
            (1 + \[Omega][{{X, X}, {Y, Y}}]) + 64*\[Omega][{{Y}, {X, X, Y}}]^
             5*(-2152 - 424*M + 53*M^2 + 4*M^3 + 4*(-338 - 110*M + 11*M^2 + M^
                3)*\[Omega][{{X, X}, {Y, Y}}] + 8*(80 + 4*M + M^2)*
              \[Omega][{{X, X}, {Y, Y}}]^2) + 16*\[Omega][{{Y}, {X, X, Y}}]^4*
            (-30320 - 16080*M - 128*M^2 + 386*M^3 + 17*M^4 + 
             2*(-1264 - 4376*M - 167*M^2 + 158*M^3 + 6*M^4)*\[Omega][{{X, X}, 
                {Y, Y}}] + 8*(2514 + 871*M + 31*M^2 + 4*M^3)*
              \[Omega][{{X, X}, {Y, Y}}]^2 + 32*(-90 + 20*M + 3*M^2)*
              \[Omega][{{X, X}, {Y, Y}}]^3) + 4*\[Omega][{{Y}, {X, X, Y}}]^3*
            (-174144 - 169776*M - 27980*M^2 + 4498*M^3 + 897*M^4 + 25*M^5 + 
             4*(30640 + 228*M - 2932*M^2 + 792*M^3 + 152*M^4 + 3*M^5)*
              \[Omega][{{X, X}, {Y, Y}}] + 8*(20956 + 17233*M + 1973*M^2 + 52*
                M^3 + 5*M^4)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
             8*(-9580 + 1342*M + 789*M^2 + 37*M^3)*\[Omega][{{X, X}, {Y, Y}}]^
               3 + 64*(40 + 65*M + 11*M^2)*\[Omega][{{X, X}, {Y, Y}}]^4) + 
           2*\[Omega][{{Y}, {X, X, Y}}]^2*(-232416 - 345056*M - 130880*M^2 + 
             842*M^3 + 4739*M^4 + 433*M^5 + 6*M^6 + (392896 + 277280*M + 7412*
                M^2 + 2778*M^3 + 3187*M^4 + 237*M^5 + 2*M^6)*
              \[Omega][{{X, X}, {Y, Y}}] + 2*(81952 + 209172*M + 58906*M^2 + 
               886*M^3 + 51*M^4 + 8*M^5)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
             4*(-83504 - 202*M + 13002*M^2 + 1207*M^3 + 16*M^4)*
              \[Omega][{{X, X}, {Y, Y}}]^3 + 8*(3604 + 5382*M + 1455*M^2 + 
               101*M^3)*\[Omega][{{X, X}, {Y, Y}}]^4 + 
             32*(150 + 189*M + 28*M^2)*\[Omega][{{X, X}, {Y, Y}}]^5) + 
           2*\[Omega][{{Y}, {X, X, Y}}]*(-73200 - 147440*M - 93364*M^2 - 
             13078*M^3 + 3752*M^4 + 815*M^5 + 36*M^6 + 
             (200528 + 257640*M + 79564*M^2 - 326*M^3 + 2503*M^4 + 535*M^5 + 
               18*M^6)*\[Omega][{{X, X}, {Y, Y}}] + (-36112 + 133088*M + 
               143896*M^2 + 6942*M^3 - 834*M^4 + 33*M^5 + 2*M^6)*
              \[Omega][{{X, X}, {Y, Y}}]^2 + (-208912 - 128368*M + 63004*
                M^2 + 13166*M^3 + 291*M^4 - 7*M^5)*\[Omega][{{X, X}, {Y, Y}}]^
               3 + 4*(23184 + 15828*M + 5337*M^2 + 1005*M^3 + 22*M^4)*
              \[Omega][{{X, X}, {Y, Y}}]^4 + 4*(4496 + 7538*M + 1046*M^2 + 61*
                M^3)*\[Omega][{{X, X}, {Y, Y}}]^5 + 16*(-160 + 111*M + 21*
                M^2)*\[Omega][{{X, X}, {Y, Y}}]^6))) + 
       \[Lambda][{y}]^2*(1920 + 7584*M + 12048*M^2 + 9768*M^3 + 4164*M^4 + 
         804*M^5 + 12*M^6 - 12*M^7 + 23488*\[Omega][{{Y}, {X, X, Y}}] + 
         78272*M*\[Omega][{{Y}, {X, X, Y}}] + 101168*M^2*
          \[Omega][{{Y}, {X, X, Y}}] + 62848*M^3*\[Omega][{{Y}, {X, X, Y}}] + 
         18128*M^4*\[Omega][{{Y}, {X, X, Y}}] + 
         1456*M^5*\[Omega][{{Y}, {X, X, Y}}] - 
         224*M^6*\[Omega][{{Y}, {X, X, Y}}] - 
         16*M^7*\[Omega][{{Y}, {X, X, Y}}] + 
         112064*\[Omega][{{Y}, {X, X, Y}}]^2 + 304992*M*
          \[Omega][{{Y}, {X, X, Y}}]^2 + 305120*M^2*
          \[Omega][{{Y}, {X, X, Y}}]^2 + 132472*M^3*
          \[Omega][{{Y}, {X, X, Y}}]^2 + 20028*M^4*\[Omega][{{Y}, {X, X, Y}}]^
           2 - 948*M^5*\[Omega][{{Y}, {X, X, Y}}]^2 - 
         220*M^6*\[Omega][{{Y}, {X, X, Y}}]^2 - 
         4*M^7*\[Omega][{{Y}, {X, X, Y}}]^2 + 
         265216*\[Omega][{{Y}, {X, X, Y}}]^3 + 559808*M*
          \[Omega][{{Y}, {X, X, Y}}]^3 + 397920*M^2*
          \[Omega][{{Y}, {X, X, Y}}]^3 + 100224*M^3*
          \[Omega][{{Y}, {X, X, Y}}]^3 + 1792*M^4*\[Omega][{{Y}, {X, X, Y}}]^
           3 - 1072*M^5*\[Omega][{{Y}, {X, X, Y}}]^3 - 
         48*M^6*\[Omega][{{Y}, {X, X, Y}}]^3 + 
         322560*\[Omega][{{Y}, {X, X, Y}}]^4 + 486528*M*
          \[Omega][{{Y}, {X, X, Y}}]^4 + 211776*M^2*
          \[Omega][{{Y}, {X, X, Y}}]^4 + 18976*M^3*\[Omega][{{Y}, {X, X, Y}}]^
           4 - 2160*M^4*\[Omega][{{Y}, {X, X, Y}}]^4 - 
         208*M^5*\[Omega][{{Y}, {X, X, Y}}]^4 + 
         183296*\[Omega][{{Y}, {X, X, Y}}]^5 + 174848*M*
          \[Omega][{{Y}, {X, X, Y}}]^5 + 37248*M^2*\[Omega][{{Y}, {X, X, Y}}]^
           5 - 1408*M^3*\[Omega][{{Y}, {X, X, Y}}]^5 - 
         384*M^4*\[Omega][{{Y}, {X, X, Y}}]^5 + 
         33792*\[Omega][{{Y}, {X, X, Y}}]^6 + 
         21504*M*\[Omega][{{Y}, {X, X, Y}}]^6 + 
         256*M^2*\[Omega][{{Y}, {X, X, Y}}]^6 - 
         256*M^3*\[Omega][{{Y}, {X, X, Y}}]^6 - 
         12992*\[Omega][{{X, X}, {Y, Y}}] - 41512*M*
          \[Omega][{{X, X}, {Y, Y}}] - 50212*M^2*\[Omega][{{X, X}, {Y, Y}}] - 
         27602*M^3*\[Omega][{{X, X}, {Y, Y}}] - 
         5803*M^4*\[Omega][{{X, X}, {Y, Y}}] + 
         277*M^5*\[Omega][{{X, X}, {Y, Y}}] + 
         157*M^6*\[Omega][{{X, X}, {Y, Y}}] - 
         13*M^7*\[Omega][{{X, X}, {Y, Y}}] - 
         128160*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
         330208*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
         300200*M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
         105136*M^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
         4458*M^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
         3108*M^5*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
         34*M^6*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
         10*M^7*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
         453120*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
         878816*M*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
         522256*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
         66272*M^3*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
         19066*M^4*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
         1158*M^5*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
         84*M^6*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
         2*M^7*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
         699648*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
         893184*M*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
         225344*M^2*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
         49824*M^3*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
         9172*M^4*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
         136*M^5*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
         24*M^6*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
         426496*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
         235264*M*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
         63392*M^2*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
         27584*M^3*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
         560*M^4*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
         104*M^5*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
         36352*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] + 
         46080*M*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] + 
         37056*M^2*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] + 
         2048*M^3*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] - 
         192*M^4*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] + 
         16384*\[Omega][{{Y}, {X, X, Y}}]^6*\[Omega][{{X, X}, {Y, Y}}] + 
         18432*M*\[Omega][{{Y}, {X, X, Y}}]^6*\[Omega][{{X, X}, {Y, Y}}] + 
         1792*M^2*\[Omega][{{Y}, {X, X, Y}}]^6*\[Omega][{{X, X}, {Y, Y}}] - 
         128*M^3*\[Omega][{{Y}, {X, X, Y}}]^6*\[Omega][{{X, X}, {Y, Y}}] + 
         31312*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         72872*M*\[Omega][{{X, X}, {Y, Y}}]^2 + 53300*M^2*
          \[Omega][{{X, X}, {Y, Y}}]^2 + 7162*M^3*\[Omega][{{X, X}, {Y, Y}}]^
           2 - 5814*M^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         1335*M^5*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         132*M^6*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         3*M^7*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         218976*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         358800*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         122600*M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         47988*M^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         22934*M^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         753*M^5*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         52*M^6*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         M^7*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         445120*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         346240*M*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         156288*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^
           2 - 131704*M^3*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}]^2 - 5044*M^4*\[Omega][{{Y}, {X, X, Y}}]^
           2*\[Omega][{{X, X}, {Y, Y}}]^2 + 494*M^5*
          \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         6*M^6*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         176256*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         292736*M*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         331312*M^2*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^
           2 - 45616*M^3*\[Omega][{{Y}, {X, X, Y}}]^3*
          \[Omega][{{X, X}, {Y, Y}}]^2 + 660*M^4*\[Omega][{{Y}, {X, X, Y}}]^3*
          \[Omega][{{X, X}, {Y, Y}}]^2 + 112*M^5*\[Omega][{{Y}, {X, X, Y}}]^3*
          \[Omega][{{X, X}, {Y, Y}}]^2 - 235776*\[Omega][{{Y}, {X, X, Y}}]^4*
          \[Omega][{{X, X}, {Y, Y}}]^2 - 369088*M*\[Omega][{{Y}, {X, X, Y}}]^
           4*\[Omega][{{X, X}, {Y, Y}}]^2 - 110368*M^2*
          \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         2560*M^3*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         416*M^4*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         136192*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         92800*M*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         5952*M^2*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         576*M^3*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         17408*\[Omega][{{Y}, {X, X, Y}}]^6*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         2048*M*\[Omega][{{Y}, {X, X, Y}}]^6*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         256*M^2*\[Omega][{{Y}, {X, X, Y}}]^6*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         25632*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         22800*M*\[Omega][{{X, X}, {Y, Y}}]^3 + 22088*M^2*
          \[Omega][{{X, X}, {Y, Y}}]^3 + 26894*M^3*\[Omega][{{X, X}, {Y, Y}}]^
           3 + 5317*M^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         698*M^5*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         27*M^6*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         60160*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         103680*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         217040*M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         74580*M^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         3964*M^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         65*M^5*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         9*M^6*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         188928*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         587104*M*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         328984*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^
           3 + 13448*M^3*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}]^3 - 942*M^4*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}]^3 + 26*M^5*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}]^3 + 507776*\[Omega][{{Y}, {X, X, Y}}]^3*
          \[Omega][{{X, X}, {Y, Y}}]^3 + 541088*M*\[Omega][{{Y}, {X, X, Y}}]^
           3*\[Omega][{{X, X}, {Y, Y}}]^3 + 101680*M^2*
          \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         304*M^3*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         120*M^4*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         278528*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         142016*M*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         7680*M^2*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         512*M^3*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         48640*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         6016*M*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         512*M^2*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         12000*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         51024*M*\[Omega][{{X, X}, {Y, Y}}]^4 - 47428*M^2*
          \[Omega][{{X, X}, {Y, Y}}]^4 - 8514*M^3*\[Omega][{{X, X}, {Y, Y}}]^
           4 + 2476*M^4*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         84*M^5*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         152064*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         269632*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         88984*M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         14420*M^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         96*M^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         28*M^5*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         359680*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         261248*M*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         5088*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         2320*M^3*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         116*M^4*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         219008*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         52288*M*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         4032*M^2*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         80*M^3*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         43264*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         640*M*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         832*M^2*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         256*M*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         26432*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         32904*M*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         2716*M^2*\[Omega][{{X, X}, {Y, Y}}]^5 - 
         5324*M^3*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         108*M^4*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         96032*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         34752*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
         21896*M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
         244*M^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         36*M^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         58368*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 - 
         7584*M*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 - 
         744*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         216*M^3*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         6784*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         3424*M*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         480*M^2*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         512*M*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^5 - 
         6512*\[Omega][{{X, X}, {Y, Y}}]^6 + 
         4872*M*\[Omega][{{X, X}, {Y, Y}}]^6 + 6112*M^2*
          \[Omega][{{X, X}, {Y, Y}}]^6 - 48*M^3*\[Omega][{{X, X}, {Y, Y}}]^
           6 + 5088*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^6 + 
         19024*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^6 + 
         752*M^2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^6 - 
         16*M^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^6 + 
         9344*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^6 + 
         3072*M*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^6 - 
         48*M^2*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^6 + 
         448*M*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^6 - 
         3168*\[Omega][{{X, X}, {Y, Y}}]^7 - 
         3280*M*\[Omega][{{X, X}, {Y, Y}}]^7 - 
         4736*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^7 + 
         64*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^7 + 
         128*M*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^7 + 
         640*\[Omega][{{X, X}, {Y, Y}}]^8 + 128*M*\[Omega][{{X}, {X, Y, Y}}]^
           6*(3 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
          (1 + 2*\[Omega][{{X, X}, {Y, Y}}])*
          (M + 4*\[Omega][{{Y}, {X, X, Y}}] + 6*\[Omega][{{X, X}, {Y, Y}}]) - 
         64*M*\[Omega][{{X}, {X, Y, Y}}]^5*(-68 - 105*M - 21*M^2 + 2*M^3 + 
           16*\[Omega][{{Y}, {X, X, Y}}]^3*(-13 + M - 
             6*\[Omega][{{X, X}, {Y, Y}}]) + (-46 - 58*M - 24*M^2 + M^3)*
            \[Omega][{{X, X}, {Y, Y}}] - (484 + 57*M + 5*M^2)*
            \[Omega][{{X, X}, {Y, Y}}]^2 - 10*(31 + 2*M)*
            \[Omega][{{X, X}, {Y, Y}}]^3 - 52*\[Omega][{{X, X}, {Y, Y}}]^4 + 
           4*\[Omega][{{Y}, {X, X, Y}}]^2*(3*(-56 - 9*M + M^2) - 
             2*(41 + 6*M)*\[Omega][{{X, X}, {Y, Y}}] - 
             60*\[Omega][{{X, X}, {Y, Y}}]^2) + 2*\[Omega][{{Y}, {X, X, Y}}]*
            (-234 - 138*M - 2*M^2 + M^3 - 2*(46 + 40*M + M^2)*
              \[Omega][{{X, X}, {Y, Y}}] - 2*(159 + 17*M)*\[Omega][
                {{X, X}, {Y, Y}}]^2 - 100*\[Omega][{{X, X}, {Y, Y}}]^3)) - 
         8*\[Omega][{{X}, {X, Y, Y}}]^4*(-576 - 3680*M - 4392*M^2 - 
           1540*M^3 - 67*M^4 + 16*M^5 + 384*M*\[Omega][{{Y}, {X, X, Y}}]^4*
            (-11 + M - 2*\[Omega][{{X, X}, {Y, Y}}]) + 
           (640 + 4608*M + 1872*M^2 - 432*M^3 - 131*M^4 + 8*M^5)*
            \[Omega][{{X, X}, {Y, Y}}] + (224 - 7952*M - 2328*M^2 + 268*M^3 - 
             42*M^4)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           8*(-8 - 915*M - 266*M^2 + 4*M^3)*\[Omega][{{X, X}, {Y, Y}}]^3 - 
           8*(28 + 313*M + 28*M^2)*\[Omega][{{X, X}, {Y, Y}}]^4 - 
           336*M*\[Omega][{{X, X}, {Y, Y}}]^5 + 32*\[Omega][{{Y}, {X, X, Y}}]^
             3*(-136 - 648*M - 96*M^2 + 13*M^3 - 2*(28 + 9*M + 15*M^2)*
              \[Omega][{{X, X}, {Y, Y}}] - 84*M*\[Omega][{{X, X}, {Y, Y}}]^
               2) + 16*\[Omega][{{Y}, {X, X, Y}}]^2*(-512 - 2274*M - 
             889*M^2 - 23*M^3 + 9*M^4 + M*(448 - 95*M - 22*M^2)*
              \[Omega][{{X, X}, {Y, Y}}] - 8*(-7 + 98*M + 10*M^2)*
              \[Omega][{{X, X}, {Y, Y}}]^2 - 224*M*\[Omega][{{X, X}, {Y, Y}}]^
               3) + 2*\[Omega][{{Y}, {X, X, Y}}]*(-2080 - 10640*M - 
             8044*M^2 - 1252*M^3 + 51*M^4 + 8*M^5 + (1248 + 6904*M + 688*
                M^2 - 456*M^3 - 6*M^4)*\[Omega][{{X, X}, {Y, Y}}] - 
             4*(-152 + 2418*M + 329*M^2 + 27*M^3)*\[Omega][{{X, X}, {Y, Y}}]^
               2 - 8*(-28 + 741*M + 50*M^2)*\[Omega][{{X, X}, {Y, Y}}]^3 - 
             944*M*\[Omega][{{X, X}, {Y, Y}}]^4)) - 
         4*\[Omega][{{X}, {X, Y, Y}}]^3*(-5248 - 18096*M - 20832*M^2 - 
           10056*M^3 - 1573*M^4 + 70*M^5 + 10*M^6 + 
           256*M*\[Omega][{{Y}, {X, X, Y}}]^5*(-31 + 3*M - 
             2*\[Omega][{{X, X}, {Y, Y}}]) + (10368 + 33520*M + 24896*M^2 + 
             3820*M^3 - 985*M^4 - 25*M^5 + 5*M^6)*\[Omega][
             {{X, X}, {Y, Y}}] + (32 - 18496*M - 9656*M^2 + 2728*M^3 + 
             70*M^4 - 29*M^5)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           4*(-1400 - 2702*M - 2983*M^2 - 229*M^3 + 14*M^4)*
            \[Omega][{{X, X}, {Y, Y}}]^3 - 4*(248 + 2640*M + 311*M^2 + 
             28*M^3)*\[Omega][{{X, X}, {Y, Y}}]^4 - 8*(-180 + 439*M + 12*M^2)*
            \[Omega][{{X, X}, {Y, Y}}]^5 - 240*M*\[Omega][{{X, X}, {Y, Y}}]^
             6 + 64*\[Omega][{{Y}, {X, X, Y}}]^4*(-408 - 872*M - 123*M^2 + 
             18*M^3 - 2*(124 - 67*M + 16*M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
             52*M*\[Omega][{{X, X}, {Y, Y}}]^2) + 
           32*\[Omega][{{Y}, {X, X, Y}}]^3*(-2824 - 5262*M - 1815*M^2 - 
             37*M^3 + 19*M^4 + (-448 + 1624*M + 74*M^2 - 46*M^3)*
              \[Omega][{{X, X}, {Y, Y}}] - 32*(-19 + 28*M + 2*M^2)*
              \[Omega][{{X, X}, {Y, Y}}]^2 - 208*M*\[Omega][{{X, X}, {Y, Y}}]^
               3) + 4*\[Omega][{{Y}, {X, X, Y}}]^2*(-25312 - 54112*M - 
             31256*M^2 - 4428*M^3 + 186*M^4 + 33*M^5 + 
             (9952 + 36104*M + 8300*M^2 - 604*M^3 - 76*M^4)*\[Omega][{{X, X}, 
                {Y, Y}}] - 8*(-1192 + 2088*M + 347*M^2 + 15*M^3)*
              \[Omega][{{X, X}, {Y, Y}}]^2 - 32*(14 + 265*M + 23*M^2)*
              \[Omega][{{X, X}, {Y, Y}}]^3 - 1472*M*\[Omega][{{X, X}, 
                 {Y, Y}}]^4) + 2*\[Omega][{{Y}, {X, X, Y}}]*
            (-20896 - 55232*M - 47000*M^2 - 13912*M^3 - 649*M^4 + 103*M^5 + 
             5*M^6 + 4*(5872 + 16504*M + 7866*M^2 + 107*M^3 - 101*M^4 + M^5)*
              \[Omega][{{X, X}, {Y, Y}}] - 4*(-2024 + 8758*M + 1941*M^2 - 61*
                M^3 + 24*M^4)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
             8*(-640 - 2544*M - 853*M^2 + 3*M^3)*\[Omega][{{X, X}, {Y, Y}}]^
               3 - 8*(304 + 1001*M + 91*M^2)*\[Omega][{{X, X}, {Y, Y}}]^4 - 
             1072*M*\[Omega][{{X, X}, {Y, Y}}]^5)) + 
         2*\[Omega][{{X}, {X, Y, Y}}]^2*(12608 + 41408*M + 51096*M^2 + 
           29112*M^3 + 7260*M^4 + 378*M^5 - 74*M^6 - 2*M^7 - 
           512*(-10 + M)*M*\[Omega][{{Y}, {X, X, Y}}]^6 - 
           (43520 + 110704*M + 94592*M^2 + 27888*M^3 + 281*M^4 - 599*M^5 + 
             30*M^6 + M^7)*\[Omega][{{X, X}, {Y, Y}}] + 
           (30976 + 56576*M + 21320*M^2 - 6560*M^3 - 2842*M^4 + 212*M^5 + 
             4*M^6)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           (24416 + 23232*M + 27972*M^2 + 8216*M^3 - 491*M^4 + 9*M^5)*
            \[Omega][{{X, X}, {Y, Y}}]^3 + (-21760 + 3888*M + 2440*M^2 + 
             1284*M^3 - 66*M^4)*\[Omega][{{X, X}, {Y, Y}}]^4 + 
           4*(-1528 + 648*M + 59*M^2 + 37*M^3)*\[Omega][{{X, X}, {Y, Y}}]^5 - 
           8*(-424 - 232*M + 7*M^2)*\[Omega][{{X, X}, {Y, Y}}]^6 + 
           64*M*\[Omega][{{X, X}, {Y, Y}}]^7 - 128*\[Omega][{{Y}, {X, X, Y}}]^
             5*(-408 - 442*M - 61*M^2 + 9*M^3 - 2*(164 - 47*M + 6*M^2)*
              \[Omega][{{X, X}, {Y, Y}}] - 12*M*\[Omega][{{X, X}, {Y, Y}}]^
               2) - 64*\[Omega][{{Y}, {X, X, Y}}]^4*
            (2*(-2188 - 2357*M - 674*M^2 - 11*M^3 + 7*M^4) + 
             (-1128 + 846*M + 118*M^2 - 27*M^3)*\[Omega][{{X, X}, {Y, Y}}] - 
             2*(-752 + 203*M + 12*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 - 
             84*M*\[Omega][{{X, X}, {Y, Y}}]^3) - 
           8*\[Omega][{{Y}, {X, X, Y}}]^3*(-61120 - 89392*M - 39412*M^2 - 
             4924*M^3 + 247*M^4 + 38*M^5 + (28384 + 37832*M + 9780*M^2 - 356*
                M^3 - 74*M^4)*\[Omega][{{X, X}, {Y, Y}}] - 
             16*(-2312 + 299*M + 121*M^2 + 7*M^3)*\[Omega][{{X, X}, {Y, Y}}]^
               2 - 16*(424 + 318*M + 31*M^2)*\[Omega][{{X, X}, {Y, Y}}]^3 - 
             960*M*\[Omega][{{X, X}, {Y, Y}}]^4) - 
           4*\[Omega][{{Y}, {X, X, Y}}]^2*(-91040 - 179440*M - 118656*M^2 - 
             28416*M^3 - 1263*M^4 + 229*M^5 + 11*M^6 + 
             (123168 + 179232*M + 65804*M^2 + 4028*M^3 - 601*M^4 - 7*M^5)*
              \[Omega][{{X, X}, {Y, Y}}] - 2*(-21776 + 6060*M + 4102*M^2 + 26*
                M^3 + 61*M^4)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
             16*(-2988 - 1553*M - 448*M^2 + 2*M^3)*\[Omega][{{X, X}, {Y, Y}}]^
               3 - 8*(328 + 1053*M + 111*M^2)*\[Omega][{{X, X}, {Y, Y}}]^4 - 
             1200*M*\[Omega][{{X, X}, {Y, Y}}]^5) - 
           2*\[Omega][{{Y}, {X, X, Y}}]*(-58400 - 149952*M - 138344*M^2 - 
             54296*M^3 - 7252*M^4 + 191*M^5 + 58*M^6 + M^7 + 
             (137280 + 266816*M + 154672*M^2 + 24228*M^3 - 1359*M^4 - 119*
                M^5 + 7*M^6)*\[Omega][{{X, X}, {Y, Y}}] - 
             (27200 + 62512*M + 19980*M^2 - 4768*M^3 + 117*M^4 + 54*M^5)*
              \[Omega][{{X, X}, {Y, Y}}]^2 + 2*(-38096 - 25388*M - 13386*
                M^2 - 606*M^3 + 37*M^4)*\[Omega][{{X, X}, {Y, Y}}]^3 - 
             4*(-2712 + 6196*M + 1210*M^2 + 37*M^3)*\[Omega][{{X, X}, 
                 {Y, Y}}]^4 - 8*(-1012 + 921*M + 47*M^2)*\[Omega][
                {{X, X}, {Y, Y}}]^5 - 592*M*\[Omega][{{X, X}, {Y, Y}}]^6)) + 
         \[Omega][{{X}, {X, Y, Y}}]*(-8*(2 + M)^2*(-368 - 941*M - 789*M^2 - 
             216*M^3 + 2*M^4 + 2*M^5) - 2*(29888 + 83064*M + 82808*M^2 + 
             33838*M^3 + 3537*M^4 - 642*M^5 - 19*M^6 + 5*M^7)*
            \[Omega][{{X, X}, {Y, Y}}] + (92128 + 168848*M + 74632*M^2 - 
             13028*M^3 - 10378*M^4 + 83*M^5 + 58*M^6 - M^7)*
            \[Omega][{{X, X}, {Y, Y}}]^2 + (-11904 + 43712*M + 83560*M^2 + 
             31320*M^3 - 1136*M^4 - 89*M^5 + 9*M^6)*
            \[Omega][{{X, X}, {Y, Y}}]^3 - 4*(17312 + 23840*M + 5646*M^2 - 
             1729*M^3 - 12*M^4 + 7*M^5)*\[Omega][{{X, X}, {Y, Y}}]^4 + 
           4*(7392 - 1308*M - 2694*M^2 - M^3 + 9*M^4)*
            \[Omega][{{X, X}, {Y, Y}}]^5 - 16*(-682 - 735*M - 35*M^2 + M^3)*
            \[Omega][{{X, X}, {Y, Y}}]^6 + 64*(-54 + M)*
            \[Omega][{{X, X}, {Y, Y}}]^7 - 256*\[Omega][{{Y}, {X, X, Y}}]^6*
            (-136 - 56*M - 8*M^2 + M^3 + 4*(-34 + M)*\[Omega][{{X, X}, 
                {Y, Y}}]) - 128*\[Omega][{{Y}, {X, X, Y}}]^5*
            (-2328 - 1660*M - 289*M^2 - 4*M^3 + 3*M^4 + 8*(-101 - 50*M + M^2)*
              \[Omega][{{X, X}, {Y, Y}}] - 2*(-544 + M + 5*M^2)*
              \[Omega][{{X, X}, {Y, Y}}]^2 - 12*M*\[Omega][{{X, X}, {Y, Y}}]^
               3) - 16*\[Omega][{{Y}, {X, X, Y}}]^4*(-49344 - 56672*M - 
             18012*M^2 - 1408*M^3 + 104*M^4 + 13*M^5 + 
             4*(5464 + 1224*M - 335*M^2 - 24*M^3 + M^4)*\[Omega][{{X, X}, 
                {Y, Y}}] - 4*(-9352 - 3224*M + 17*M^2 + 29*M^3)*
              \[Omega][{{X, X}, {Y, Y}}]^2 - 8*(1304 + 209*M + 6*M^2)*
              \[Omega][{{X, X}, {Y, Y}}]^3 - 240*M*\[Omega][{{X, X}, {Y, Y}}]^
               4) - 8*\[Omega][{{Y}, {X, X, Y}}]^3*(-112768 - 189952*M - 
             101368*M^2 - 17600*M^3 - 291*M^4 + 144*M^5 + 6*M^6 + 
             2*(81040 + 73608*M + 10988*M^2 - 660*M^3 - 103*M^4 + 5*M^5)*
              \[Omega][{{X, X}, {Y, Y}}] - 4*(-15336 - 19038*M - 3341*M^2 + 
               47*M^3 + 30*M^4)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
             8*(-11320 - 3972*M - 381*M^2 + 13*M^3)*\[Omega][{{X, X}, 
                 {Y, Y}}]^3 - 8*(-784 + 355*M + 61*M^2)*\[Omega][
                {{X, X}, {Y, Y}}]^4 - 528*M*\[Omega][{{X, X}, {Y, Y}}]^5) - 
           4*\[Omega][{{Y}, {X, X, Y}}]^2*(-123488 - 280448*M - 223088*M^2 - 
             71072*M^3 - 6475*M^4 + 335*M^5 + 66*M^6 + M^7 + 
             (320000 + 484496*M + 200120*M^2 + 12036*M^3 - 1519*M^4 - 84*
                M^5 + 8*M^6)*\[Omega][{{X, X}, {Y, Y}}] + 
             (-90624 + 14608*M + 65144*M^2 + 7244*M^3 - 376*M^4 - 53*M^5)*
              \[Omega][{{X, X}, {Y, Y}}]^2 + 4*(-50616 - 44330*M - 6929*M^2 - 
               69*M^3 + 12*M^4)*\[Omega][{{X, X}, {Y, Y}}]^3 - 
             4*(-17896 + 2832*M + 1151*M^2 + 14*M^3)*\[Omega][{{X, X}, 
                 {Y, Y}}]^4 - 8*(-940 + 589*M + 50*M^2)*\[Omega][
                {{X, X}, {Y, Y}}]^5 - 464*M*\[Omega][{{X, X}, {Y, Y}}]^6) - 
           2*\[Omega][{{Y}, {X, X, Y}}]*(2*(-31536 - 91240*M - 98900*M^2 - 
               48562*M^3 - 9819*M^4 - 261*M^5 + 80*M^6 + 5*M^7) + 
             2*(120960 + 257600*M + 180148*M^2 + 40490*M^3 - 823*M^4 - 299*
                M^5 + 9*M^6 + M^7)*\[Omega][{{X, X}, {Y, Y}}] - 
             (219488 + 243968*M + 9056*M^2 - 39168*M^3 - 1588*M^4 + 267*M^5 + 
               7*M^6)*\[Omega][{{X, X}, {Y, Y}}]^2 - 2*(50544 + 110648*M + 
               62352*M^2 + 2042*M^3 - 277*M^4 + 11*M^5)*\[Omega][
                {{X, X}, {Y, Y}}]^3 + 4*(39248 + 16268*M - 4886*M^2 - 467*
                M^3 + 31*M^4)*\[Omega][{{X, X}, {Y, Y}}]^4 - 
             8*(468 - 1246*M + 31*M^2 + 32*M^3)*\[Omega][{{X, X}, {Y, Y}}]^
               5 + 16*(-664 - 212*M + 5*M^2)*\[Omega][{{X, X}, {Y, Y}}]^6 - 
             128*M*\[Omega][{{X, X}, {Y, Y}}]^7))))/
      ((3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (3 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
       (3 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
       (1 + 2*\[Omega][{{X, X}, {Y, Y}}])))/
    (E^(T*(1 + M + \[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}]))*(1 + M - 5*\[Lambda][{y}])*
     (2 + M - 4*\[Lambda][{y}])*(2 + M + 2*\[Lambda][{y}] + 
      4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}] - 
      4*\[Omega][{{X, X}, {Y, Y}}])*(2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] - 
      2*\[Omega][{{X, X}, {Y, Y}}])*(1 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
      2*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
     (2 + M + 4*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
     (1 + M - 2*\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] - 
      \[Omega][{{X, X}, {Y, Y}}])*(1 + M - 2*\[Lambda][{y}] + 
      2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
     (\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
      \[Omega][{{X, X}, {Y, Y}}]))}, 
 {(-2*((-8*(-2 + M)*M*\[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*
       (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, Y}, {X, Y}}]))/
      (E^((T*(M + 4*\[Omega][{{X}, {X, Y, Y}}]))/2)*(M - 2*\[Lambda][{y}])*
       (M + 4*\[Omega][{{X}, {X, Y, Y}}])*(3*\[Lambda][{y}] + 
        2*\[Omega][{{Y}, {X, X, Y}}])*(2 + M + 2*\[Lambda][{y}] + 
        4*\[Omega][{{Y}, {X, X, Y}}])*(\[Lambda][{y}] - 
        \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, Y}, {X, Y}}])) - 
     (M^2*\[Lambda][{y}]^3*(1 + 2*\[Omega][{{X}, {X, Y, Y}}])*
       (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, Y}, {X, Y}}])*(36*\[Lambda][{y}]^2 + 
        20*\[Omega][{{X}, {X, Y, Y}}]^2 + (M + 2*\[Omega][{{Y}, {X, X, Y}}])*
         (M + 2*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]) + 
        4*\[Omega][{{X}, {X, Y, Y}}]*(2*M + 6*\[Omega][{{Y}, {X, X, Y}}] + 
          3*\[Omega][{{X, Y}, {X, Y}}]) + 4*\[Lambda][{y}]*
         (2*M + 14*\[Omega][{{X}, {X, Y, Y}}] + 
          6*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, Y}, {X, Y}}])))/
      ((\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}])*
       (M + 4*\[Omega][{{X}, {X, Y, Y}}])*(3*\[Lambda][{y}] + 
        \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (1 + M + \[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}])*(M + 6*\[Lambda][{y}] + 
        4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
       (3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])*
       (M + 2*\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}])) + 
     (4*M^2*(-1 + \[Lambda][{y}])*\[Lambda][{y}]^2*\[Omega][{{X}, {X, Y, Y}}]*
       (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, Y}, {X, Y}}])*(10*\[Lambda][{y}]^2 + 
        (M + 2*\[Omega][{{Y}, {X, X, Y}}])*
         (M - 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
          2*\[Omega][{{X, Y}, {X, Y}}]) + \[Lambda][{y}]*
         (3*M - 6*\[Omega][{{X}, {X, Y, Y}}] + 
          10*\[Omega][{{Y}, {X, X, Y}}] + 6*\[Omega][{{X, Y}, {X, Y}}])))/
      (E^(T*(\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}]))*
       (M - 2*\[Lambda][{y}])*(\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}])*
       (1 + M + 2*\[Omega][{{Y}, {X, X, Y}}])*(5*\[Lambda][{y}] + 
        2*\[Omega][{{Y}, {X, X, Y}}])*(M + 4*\[Lambda][{y}] + 
        4*\[Omega][{{Y}, {X, X, Y}}])*(2*\[Lambda][{y}] - 
        \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, Y}, {X, Y}}])*(M - 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}])) - 
     (2*M^2*(-1 + \[Lambda][{y}])*\[Lambda][{y}]*
       (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (M - \[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}])*
       (\[Omega][{{X}, {X, Y, Y}}]^2 - (\[Omega][{{Y}, {X, X, Y}}] + 
          \[Omega][{{X, Y}, {X, Y}}])^2 + 2*\[Lambda][{y}]*
         (\[Omega][{{X}, {X, Y, Y}}]^2 - \[Omega][{{Y}, {X, X, Y}}] - 
          \[Omega][{{X, Y}, {X, Y}}] + \[Omega][{{X}, {X, Y, Y}}]*
           (2 + \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))))/
      (E^(T*(3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
       (M - 4*\[Lambda][{y}])*(M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, Y}, {X, Y}}])*
       (1 + M - 2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}])*
       (3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}])*
       (2*\[Lambda][{y}] - \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])*
       (3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])) + 
     (M^2*(-1 + \[Lambda][{y}])*(2*\[Omega][{{Y}, {X, X, Y}}]*
         (\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
         (\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] - 
          \[Omega][{{X, Y}, {X, Y}}]) + \[Lambda][{y}]^2*
         (2*\[Omega][{{X}, {X, Y, Y}}]^3 + \[Omega][{{X}, {X, Y, Y}}]^2*
           (7 + 4*\[Omega][{{Y}, {X, X, Y}}] + 
            2*\[Omega][{{X, Y}, {X, Y}}]) - 5*(\[Omega][{{Y}, {X, X, Y}}]^2 + 
            3*\[Omega][{{X, Y}, {X, Y}}] + \[Omega][{{Y}, {X, X, Y}}]*
             \[Omega][{{X, Y}, {X, Y}}]) + \[Omega][{{X}, {X, Y, Y}}]*
           (18 + 2*\[Omega][{{Y}, {X, X, Y}}]^2 + \[Omega][
             {{X, Y}, {X, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}]*
             (1 + \[Omega][{{X, Y}, {X, Y}}]))) - \[Lambda][{y}]*
         (\[Omega][{{X}, {X, Y, Y}}]^2*(-5 + 2*\[Omega][{{Y}, {X, X, Y}}]) + 
          \[Omega][{{X}, {X, Y, Y}}]*(4*\[Omega][{{Y}, {X, X, Y}}]^2 + 
            2*\[Omega][{{Y}, {X, X, Y}}]*(-5 + \[Omega][{{X, Y}, {X, Y}}]) + 
            5*\[Omega][{{X, Y}, {X, Y}}]) + \[Omega][{{Y}, {X, X, Y}}]*
           (2*\[Omega][{{Y}, {X, X, Y}}]^2 + 11*\[Omega][{{X, Y}, {X, Y}}] + 
            \[Omega][{{Y}, {X, X, Y}}]*(-5 + 2*\[Omega][{{X, Y}, 
                 {X, Y}}])))))/
      (E^(2*T*(3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}]))*(M - 6*\[Lambda][{y}])*
       (1 + M - 5*\[Lambda][{y}])*(3*\[Lambda][{y}] + 
        \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (5*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}])) - 
     (8*M*\[Lambda][{y}]^2*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}])*((\[Omega][{{X}, {X, Y, Y}}] - 
          \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}])*
         (-4*\[Omega][{{X}, {X, Y, Y}}]^2 + 
          (M + 2*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}])^
           2) + 2*\[Lambda][{y}]^2*((-2 + M)*\[Omega][{{X}, {X, Y, Y}}]^2 + 
          (2 + \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])*
           (M + 2*\[Omega][{{Y}, {X, X, Y}}] + 
            2*\[Omega][{{X, Y}, {X, Y}}]) + \[Omega][{{X}, {X, Y, Y}}]*
           (-4 + 4*M + M*\[Omega][{{Y}, {X, X, Y}}] + 
            M*\[Omega][{{X, Y}, {X, Y}}])) + \[Lambda][{y}]*
         (-M^2 + 2*(-2 + M)*\[Omega][{{X}, {X, Y, Y}}]^3 + 
          4*\[Omega][{{Y}, {X, X, Y}}]^3 + 4*\[Omega][{{X, Y}, {X, Y}}]^2 + 
          2*M*\[Omega][{{X, Y}, {X, Y}}]^2 + 4*\[Omega][{{X, Y}, {X, Y}}]^3 + 
          4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}]*
           (2 + M + 3*\[Omega][{{X, Y}, {X, Y}}]) - 
          \[Omega][{{X}, {X, Y, Y}}]^2*(12 - 6*M + M^2 + 
            4*\[Omega][{{Y}, {X, X, Y}}] + 4*\[Omega][{{X, Y}, {X, Y}}]) + 
          2*\[Omega][{{Y}, {X, X, Y}}]^2*(2 + M + 
            6*\[Omega][{{X, Y}, {X, Y}}]) - \[Omega][{{X}, {X, Y, Y}}]*
           (M*(-2 + 3*M) + 2*(-2 + M)*\[Omega][{{Y}, {X, X, Y}}]^2 + 
            (-8 + 4*M + M^2)*\[Omega][{{X, Y}, {X, Y}}] + 
            2*(-2 + M)*\[Omega][{{X, Y}, {X, Y}}]^2 + 
            \[Omega][{{Y}, {X, X, Y}}]*(-8 + 4*M + M^2 + 4*(-2 + M)*\[Omega][
                {{X, Y}, {X, Y}}])))))/
      (E^((T*(M + 2*\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
           2*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
       (M - 4*\[Lambda][{y}])*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, Y}, {X, Y}}])*
       (2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}])*
       (\[Lambda][{y}] - \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])*
       (M - 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        2*\[Omega][{{X, Y}, {X, Y}}])*(M + 2*\[Lambda][{y}] + 
        2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        2*\[Omega][{{X, Y}, {X, Y}}])) - 
     (4*M*\[Lambda][{y}]*(2*\[Omega][{{Y}, {X, X, Y}}]*
         (M + 4*\[Omega][{{Y}, {X, X, Y}}])*
         (M + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
         (\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] - 
          \[Omega][{{X, Y}, {X, Y}}])*((-6 + M)*\[Omega][{{X}, {X, Y, Y}}] + 
          (-6 + M)*\[Omega][{{Y}, {X, X, Y}}] + 
          6*\[Omega][{{X, Y}, {X, Y}}]) + 24*\[Lambda][{y}]^4*
         ((-6 + M)*\[Omega][{{X}, {X, Y, Y}}]^3 + 
          \[Omega][{{X}, {X, Y, Y}}]^2*(2*(-9 + M) + 2*(-9 + M)*
             \[Omega][{{Y}, {X, X, Y}}] + M*\[Omega][{{X, Y}, {X, Y}}]) + 
          \[Omega][{{X}, {X, Y, Y}}]*(-3*(12 + M) + (-18 + M)*
             \[Omega][{{Y}, {X, X, Y}}]^2 - M*\[Omega][{{X, Y}, {X, Y}}] + 
            6*\[Omega][{{X, Y}, {X, Y}}]^2 + \[Omega][{{Y}, {X, X, Y}}]*
             (-2*(18 + M) + M*\[Omega][{{X, Y}, {X, Y}}])) - 
          2*(9*M + (9 + 2*M)*\[Omega][{{Y}, {X, X, Y}}]^2 + 
            3*\[Omega][{{Y}, {X, X, Y}}]^3 + 6*(-3 + M)*
             \[Omega][{{X, Y}, {X, Y}}] - 9*\[Omega][{{X, Y}, {X, Y}}]^2 + 
            \[Omega][{{Y}, {X, X, Y}}]*(6*(3 + M) + 2*M*\[Omega][
                {{X, Y}, {X, Y}}] - 3*\[Omega][{{X, Y}, {X, Y}}]^2))) - 
        4*\[Lambda][{y}]^3*(-6*(-4 + M)*\[Omega][{{X}, {X, Y, Y}}]^4 + 
          (108 + 55*M)*\[Omega][{{Y}, {X, X, Y}}]^3 + 
          60*\[Omega][{{Y}, {X, X, Y}}]^4 + \[Omega][{{X}, {X, Y, Y}}]^3*
           (72 - 3*M - 22*(-6 + M)*\[Omega][{{Y}, {X, X, Y}}]) + 
          \[Omega][{{Y}, {X, X, Y}}]^2*(396 + 99*M + 6*M^2 + 
            8*(9 + 5*M)*\[Omega][{{X, Y}, {X, Y}}] - 
            60*\[Omega][{{X, Y}, {X, Y}}]^2) + 
          9*(M^2 + 2*(-5 + M)*M*\[Omega][{{X, Y}, {X, Y}}] + 
            (12 - 5*M)*\[Omega][{{X, Y}, {X, Y}}]^2) + 
          3*\[Omega][{{Y}, {X, X, Y}}]*(2*M*(33 + M) + 2*(-84 + 23*M + M^2)*
             \[Omega][{{X, Y}, {X, Y}}] - 5*(12 + M)*
             \[Omega][{{X, Y}, {X, Y}}]^2) + \[Omega][{{X}, {X, Y, Y}}]*
           ((204 - 10*M)*\[Omega][{{Y}, {X, X, Y}}]^3 + 
            \[Omega][{{Y}, {X, X, Y}}]^2*(288 + 83*M - 4*M*\[Omega][
                {{X, Y}, {X, Y}}]) + 3*(2*M*(30 + M) + 2*(-60 + 21*M + M^2)*
               \[Omega][{{X, Y}, {X, Y}}] + (-24 + M)*\[Omega][{{X, Y}, 
                  {X, Y}}]^2) + 2*\[Omega][{{Y}, {X, X, Y}}]*
             (6*(54 + 8*M + M^2) + 4*(9 + 8*M)*\[Omega][{{X, Y}, {X, Y}}] + 
              3*(-14 + M)*\[Omega][{{X, Y}, {X, Y}}]^2)) + 
          \[Omega][{{X}, {X, Y, Y}}]^2*((252 - 26*M)*
             \[Omega][{{Y}, {X, X, Y}}]^2 + \[Omega][{{Y}, {X, X, Y}}]*
             (252 + 25*M - 4*M*\[Omega][{{X, Y}, {X, Y}}]) + 
            3*(84 + 11*M + 2*M^2 + 12*M*\[Omega][{{X, Y}, {X, Y}}] + 
              2*(-4 + M)*\[Omega][{{X, Y}, {X, Y}}]^2))) + 
        2*\[Lambda][{y}]^2*((96 - 64*M)*\[Omega][{{Y}, {X, X, Y}}]^4 - 
          48*\[Omega][{{Y}, {X, X, Y}}]^5 + 2*\[Omega][{{X}, {X, Y, Y}}]^4*
           ((-8 + M)*M + 8*(-3 + M)*\[Omega][{{Y}, {X, X, Y}}]) + 
          \[Omega][{{X}, {X, Y, Y}}]^3*(-144 - 36*M + 7*M^2 + M^3 + 
            2*(-24 - 8*M + 5*M^2)*\[Omega][{{Y}, {X, X, Y}}] + 
            48*(-4 + M)*\[Omega][{{Y}, {X, X, Y}}]^2) - 
          \[Omega][{{Y}, {X, X, Y}}]^3*(552 - 88*M + 11*M^2 + 
            16*(15 + 2*M)*\[Omega][{{X, Y}, {X, Y}}] - 
            48*\[Omega][{{X, Y}, {X, Y}}]^2) + 
          9*M*(M^2 + 2*M*\[Omega][{{X, Y}, {X, Y}}] + (-10 + M)*
             \[Omega][{{X, Y}, {X, Y}}]^2) - 2*\[Omega][{{Y}, {X, X, Y}}]^2*
           ((75 - 23*M)*M + 4*(-132 + 27*M + M^2)*\[Omega][{{X, Y}, {X, 
                Y}}] - 8*(9 + 2*M)*\[Omega][{{X, Y}, {X, Y}}]^2) + 
          3*\[Omega][{{Y}, {X, X, Y}}]*(2*M^2*(7 + M) + (112 - 13*M)*M*
             \[Omega][{{X, Y}, {X, Y}}] + (-168 + 32*M + M^2)*
             \[Omega][{{X, Y}, {X, Y}}]^2) + \[Omega][{{X}, {X, Y, Y}}]^2*
           (6*M*(-33 + 6*M + M^2) + 2*M*(-24 + 7*M)*
             \[Omega][{{Y}, {X, X, Y}}]^2 + 48*(-6 + M)*
             \[Omega][{{Y}, {X, X, Y}}]^3 + (288 - 12*M - 8*M^2 + M^3)*
             \[Omega][{{X, Y}, {X, Y}}] - 2*(-8 + M)*M*
             \[Omega][{{X, Y}, {X, Y}}]^2 + \[Omega][{{Y}, {X, X, Y}}]*
             (-984 + 64*M + 27*M^2 + 2*M^3 + 4*(-24 - 20*M + M^2)*\[Omega][
                {{X, Y}, {X, Y}}] - 16*(-3 + M)*\[Omega][{{X, Y}, {X, Y}}]^
                2)) + \[Omega][{{X}, {X, Y, Y}}]*(2*(72 - 56*M + 3*M^2)*
             \[Omega][{{Y}, {X, X, Y}}]^3 + 16*(-12 + M)*
             \[Omega][{{Y}, {X, X, Y}}]^4 + \[Omega][{{Y}, {X, X, Y}}]^2*
             (-1392 + 188*M + 9*M^2 + M^3 + 4*(-84 - 28*M + M^2)*\[Omega][
                {{X, Y}, {X, Y}}] - 16*(-6 + M)*\[Omega][{{X, Y}, {X, Y}}]^
                2) + \[Omega][{{Y}, {X, X, Y}}]*(2*M*(-246 + 59*M + 3*M^2) + 
              (1488 - 372*M - 4*M^2 + M^3)*\[Omega][{{X, Y}, {X, Y}}] - 
              2*(-72 + M^2)*\[Omega][{{X, Y}, {X, Y}}]^2) + 
            3*(5*M^3 + M*(96 - 13*M + M^2)*\[Omega][{{X, Y}, {X, Y}}] - 
              (48 - 16*M + M^2)*\[Omega][{{X, Y}, {X, Y}}]^2))) + 
        \[Lambda][{y}]*(-16*M*\[Omega][{{X}, {X, Y, Y}}]^4*
           \[Omega][{{Y}, {X, X, Y}}] - 4*\[Omega][{{X}, {X, Y, Y}}]^3*
           (-3*(-6 + M)*M + (120 - 8*M + M^2)*\[Omega][{{Y}, {X, X, Y}}] + 
            16*(-3 + M)*\[Omega][{{Y}, {X, X, Y}}]^2) + 
          \[Omega][{{X}, {X, Y, Y}}]^2*(-96*(-6 + M)*
             \[Omega][{{Y}, {X, X, Y}}]^3 - 4*\[Omega][{{Y}, {X, X, Y}}]^2*
             (384 - 80*M + 3*M^2 + 48*\[Omega][{{X, Y}, {X, Y}}]) + 
            3*M*((-6 + M)*M - 4*(-12 + M)*\[Omega][{{X, Y}, {X, Y}}]) + 
            8*\[Omega][{{Y}, {X, X, Y}}]*(M*(-63 + 11*M) - 10*(-12 + M)*
               \[Omega][{{X, Y}, {X, Y}}] + 2*M*\[Omega][{{X, Y}, {X, Y}}]^
                2)) + (M + 4*\[Omega][{{Y}, {X, X, Y}}])*
           (-4*(-12 + M)*\[Omega][{{Y}, {X, X, Y}}]^4 + 
            4*\[Omega][{{Y}, {X, X, Y}}]^3*(-36 + 13*M - 12*\[Omega][
                {{X, Y}, {X, Y}}]) - 18*M*\[Omega][{{X, Y}, {X, Y}}]^2 - 
            3*\[Omega][{{Y}, {X, X, Y}}]*(-4*M^2 + (-20 + M)*M*\[Omega][
                {{X, Y}, {X, Y}}] - 4*(-16 + M)*\[Omega][{{X, Y}, {X, Y}}]^
                2) + \[Omega][{{Y}, {X, X, Y}}]^2*(M*(6 + 11*M) + 
              (336 - 32*M)*\[Omega][{{X, Y}, {X, Y}}] + 
              4*M*\[Omega][{{X, Y}, {X, Y}}]^2)) + \[Omega][{{X}, {X, Y, Y}}]*
           (-64*(-9 + M)*\[Omega][{{Y}, {X, X, Y}}]^4 - 
            3*M*\[Omega][{{X, Y}, {X, Y}}]*((-12 + M)*M + 24*\[Omega][
                {{X, Y}, {X, Y}}]) - 4*\[Omega][{{Y}, {X, X, Y}}]^3*
             (408 - 136*M + 3*M^2 + 96*\[Omega][{{X, Y}, {X, Y}}]) + 
            4*\[Omega][{{Y}, {X, X, Y}}]^2*(M*(-138 + 43*M) - 
              64*(-9 + M)*\[Omega][{{X, Y}, {X, Y}}] + 
              8*M*\[Omega][{{X, Y}, {X, Y}}]^2) + 
            2*\[Omega][{{Y}, {X, X, Y}}]*(M^2*(-6 + 7*M) - 4*M*(-96 + 7*M)*
               \[Omega][{{X, Y}, {X, Y}}] + 2*(-120 + 12*M + M^2)*
               \[Omega][{{X, Y}, {X, Y}}]^2)))))/
      (E^((T*(M + 6*\[Lambda][{y}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
           4*\[Omega][{{Y}, {X, X, Y}}]))/2)*(M - 6*\[Lambda][{y}])*
       (2 + M - 4*\[Lambda][{y}])*(3*\[Lambda][{y}] + 
        2*\[Omega][{{Y}, {X, X, Y}}])*(M + 4*\[Lambda][{y}] + 
        4*\[Omega][{{Y}, {X, X, Y}}])*(M + 6*\[Lambda][{y}] + 
        4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
       (M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
        2*\[Omega][{{X, Y}, {X, Y}}])*(2*\[Lambda][{y}] + 
        \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] - 
        \[Omega][{{X, Y}, {X, Y}}])) + 
     (-((2 + M)*(1 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          2*\[Omega][{{Y}, {X, X, Y}}])*(2 + 3*M + M^2 + 
          (8 + 6*M)*\[Omega][{{Y}, {X, X, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]^
            2)*(2 + 3*M + M^2 + 2*\[Omega][{{X}, {X, Y, Y}}]^2 + 
          2*\[Omega][{{Y}, {X, X, Y}}]^2 + \[Omega][{{Y}, {X, X, Y}}]*
           (4 + 3*M - 4*\[Omega][{{X, Y}, {X, Y}}]) + 
          \[Omega][{{X}, {X, Y, Y}}]*(4 + 3*M + 
            4*\[Omega][{{Y}, {X, X, Y}}] - 4*\[Omega][{{X, Y}, {X, Y}}]) - 
          4*\[Omega][{{X, Y}, {X, Y}}] - 3*M*\[Omega][{{X, Y}, {X, Y}}] + 
          2*\[Omega][{{X, Y}, {X, Y}}]^2)) + 40*\[Lambda][{y}]^5*
        (M^2*\[Omega][{{X}, {X, Y, Y}}]^3 + M*\[Omega][{{X}, {X, Y, Y}}]^2*
          (2 + 8*M + 2*(2 + M)*\[Omega][{{Y}, {X, X, Y}}] + 
           M*\[Omega][{{X, Y}, {X, Y}}]) + 
         2*(1 + M + 2*\[Omega][{{Y}, {X, X, Y}}])*(2 + 7*M + 
           M*\[Omega][{{Y}, {X, X, Y}}]^2 + (-2 + 3*M)*
            \[Omega][{{X, Y}, {X, Y}}] + \[Omega][{{Y}, {X, X, Y}}]*
            (2 + 5*M + M*\[Omega][{{X, Y}, {X, Y}}])) + 
         \[Omega][{{X}, {X, Y, Y}}]*(4 + 14*M + 19*M^2 + 
           M*(8 + M)*\[Omega][{{Y}, {X, X, Y}}]^2 + M*(2 + 5*M)*
            \[Omega][{{X, Y}, {X, Y}}] + \[Omega][{{Y}, {X, X, Y}}]*
            (8 + 24*M + 10*M^2 + M*(4 + M)*\[Omega][{{X, Y}, {X, Y}}]))) - 
       \[Lambda][{y}]*(1 + M + 2*\[Omega][{{Y}, {X, X, Y}}])*
        (16*(-14 + 3*M)*\[Omega][{{Y}, {X, X, Y}}]^4 + 
         4*\[Omega][{{X}, {X, Y, Y}}]^3*(3*(-8 - 2*M + M^2) + 
           4*(-14 + 3*M)*\[Omega][{{Y}, {X, X, Y}}]) + 
         4*\[Omega][{{Y}, {X, X, Y}}]^3*(-176 - 78*M + 19*M^2 - 
           16*(-7 + M)*\[Omega][{{X, Y}, {X, Y}}]) + 
         4*\[Omega][{{Y}, {X, X, Y}}]^2*(-194 - 213*M - 38*M^2 + 11*M^3 + 
           (224 + 120*M - 18*M^2)*\[Omega][{{X, Y}, {X, Y}}] + 
           4*(-14 + M)*\[Omega][{{X, Y}, {X, Y}}]^2) + 
         (2 + M)*(-28 - 54*M - 30*M^2 - 3*M^3 + M^4 + 
           (48 + 70*M + 18*M^2 - 3*M^3)*\[Omega][{{X, Y}, {X, Y}}] + 
           2*(-10 - 10*M + M^2)*\[Omega][{{X, Y}, {X, Y}}]^2) + 
         \[Omega][{{Y}, {X, X, Y}}]*(-352 - 620*M - 322*M^2 - 28*M^3 + 
           11*M^4 + (528 + 656*M + 152*M^2 - 26*M^3)*\[Omega][
             {{X, Y}, {X, Y}}] + 4*(-48 - 34*M + 3*M^2)*
            \[Omega][{{X, Y}, {X, Y}}]^2) + 4*\[Omega][{{X}, {X, Y, Y}}]^2*
          (12*(-14 + 3*M)*\[Omega][{{Y}, {X, X, Y}}]^2 + 
           \[Omega][{{Y}, {X, X, Y}}]*(-224 - 90*M + 25*M^2 - 
             16*(-7 + M)*\[Omega][{{X, Y}, {X, Y}}]) + 
           (2 + M)*(-33 - 14*M + 4*M^2 - 4*(-6 + M)*\[Omega][{{X, Y}, 
                {X, Y}}])) + \[Omega][{{X}, {X, Y, Y}}]*
          (48*(-14 + 3*M)*\[Omega][{{Y}, {X, X, Y}}]^3 + 
           4*\[Omega][{{Y}, {X, X, Y}}]^2*(-376 - 162*M + 41*M^2 - 
             32*(-7 + M)*\[Omega][{{X, Y}, {X, Y}}]) + 
           4*\[Omega][{{Y}, {X, X, Y}}]*(-260 - 274*M - 44*M^2 + 15*M^3 + 
             (272 + 136*M - 22*M^2)*\[Omega][{{X, Y}, {X, Y}}] + 
             4*(-14 + M)*\[Omega][{{X, Y}, {X, Y}}]^2) + 
           (2 + M)*(-112 - 126*M - 22*M^2 + 7*M^3 + (152 + 84*M - 14*M^2)*
              \[Omega][{{X, Y}, {X, Y}}] + 4*(-12 + M)*\[Omega][
                {{X, Y}, {X, Y}}]^2))) - 4*\[Lambda][{y}]^4*
        (6*M^2*\[Omega][{{X}, {X, Y, Y}}]^4 + M*\[Omega][{{X}, {X, Y, Y}}]^3*
          (-30 + 34*M + 15*M^2 + (-60 + 38*M)*\[Omega][{{Y}, {X, X, Y}}]) + 
         \[Omega][{{X}, {X, Y, Y}}]^2*(-60 - 216*M + 65*M^2 + 89*M^3 + 
           2*M*(-90 + 29*M)*\[Omega][{{Y}, {X, X, Y}}]^2 + 
           5*M*(-8 - 6*M + 3*M^2)*\[Omega][{{X, Y}, {X, Y}}] - 
           6*M^2*\[Omega][{{X, Y}, {X, Y}}]^2 + 2*\[Omega][{{Y}, {X, X, Y}}]*
            (3*(-20 - 67*M + 26*M^2 + 5*M^3) + 10*(-4 + M)*M*
              \[Omega][{{X, Y}, {X, Y}}])) - 
         (1 + M + 2*\[Omega][{{Y}, {X, X, Y}}])*(32 + 34*M - 33*M^2 + 
           30*M*\[Omega][{{Y}, {X, X, Y}}]^3 + (-12 + 46*M + 3*M^2)*
            \[Omega][{{X, Y}, {X, Y}}] + 10*(-2 + 3*M)*
            \[Omega][{{X, Y}, {X, Y}}]^2 + \[Omega][{{Y}, {X, X, Y}}]^2*
            (140 + 116*M + M^2 + 40*M*\[Omega][{{X, Y}, {X, Y}}]) + 
           \[Omega][{{Y}, {X, X, Y}}]*(172 + 182*M - 15*M^2 + 
             (-120 + 146*M + M^2)*\[Omega][{{X, Y}, {X, Y}}] + 
             10*M*\[Omega][{{X, Y}, {X, Y}}]^2)) + \[Omega][{{X}, {X, Y, Y}}]*
          (-92 - 354*M + 5*M^2 + 150*M^3 + 2*M*(-90 + 13*M)*
            \[Omega][{{Y}, {X, X, Y}}]^3 + (40 - 106*M - 117*M^2 + 44*M^3)*
            \[Omega][{{X, Y}, {X, Y}}] - 2*M*(5 + 14*M)*
            \[Omega][{{X, Y}, {X, Y}}]^2 + \[Omega][{{Y}, {X, X, Y}}]^2*
            (-400 - 634*M + 90*M^2 + 15*M^3 + 20*(-8 + M)*M*\[Omega][{{X, Y}, 
                {X, Y}}]) + \[Omega][{{Y}, {X, X, Y}}]*(-384 - 996*M + 
             158*M^2 + 88*M^3 + (80 - 372*M - 12*M^2 + 15*M^3)*
              \[Omega][{{X, Y}, {X, Y}}] - 2*M*(10 + 3*M)*\[Omega][
                {{X, Y}, {X, Y}}]^2))) + \[Lambda][{y}]^2*
        (8*M*\[Omega][{{X}, {X, Y, Y}}]^4*(2 + M + 
           4*\[Omega][{{Y}, {X, X, Y}}]) + 4*\[Omega][{{X}, {X, Y, Y}}]^3*
          (-12 + 12*M + 23*M^2 + 12*M^3 + 4*(-26 + 6*M + 19*M^2)*
            \[Omega][{{Y}, {X, X, Y}}] + 16*(-10 + 7*M)*
            \[Omega][{{Y}, {X, X, Y}}]^2) + 2*\[Omega][{{X}, {X, Y, Y}}]^2*
          (-132 - 16*M + 155*M^2 + 116*M^3 + 29*M^4 + 192*(-5 + 3*M)*
            \[Omega][{{Y}, {X, X, Y}}]^3 + (48 + 88*M + 32*M^2 - 18*M^3)*
            \[Omega][{{X, Y}, {X, Y}}] - 4*M*(2 + M)*
            \[Omega][{{X, Y}, {X, Y}}]^2 + 8*\[Omega][{{Y}, {X, X, Y}}]^2*
            (-220 + 31*M + 82*M^2 - 20*(-4 + M)*\[Omega][{{X, Y}, {X, Y}}]) + 
           4*\[Omega][{{Y}, {X, X, Y}}]*(-226 - 47*M + 138*M^2 + 61*M^3 + 
             (104 + 80*M - 28*M^2)*\[Omega][{{X, Y}, {X, Y}}] - 
             4*M*\[Omega][{{X, Y}, {X, Y}}]^2)) + \[Omega][{{X}, {X, Y, Y}}]*
          (-272 - 124*M + 452*M^2 + 491*M^3 + 169*M^4 + 18*M^5 + 
           64*(-30 + 17*M)*\[Omega][{{Y}, {X, X, Y}}]^4 + 
           (240 + 336*M + 212*M^2 + 38*M^3 - 18*M^4)*\[Omega][
             {{X, Y}, {X, Y}}] - 4*(12 + 48*M + 31*M^2 + M^3)*
            \[Omega][{{X, Y}, {X, Y}}]^2 + 16*\[Omega][{{Y}, {X, X, Y}}]^3*
            (-362 + 48*M + 109*M^2 - 40*(-4 + M)*\[Omega][{{X, Y}, 
                {X, Y}}]) + 4*\[Omega][{{Y}, {X, X, Y}}]^2*(-1396 - 380*M + 
             717*M^2 + 244*M^3 - 8*(-126 - 55*M + 19*M^2)*\[Omega][{{X, Y}, 
                {X, Y}}] - 16*(10 + M)*\[Omega][{{X, Y}, {X, Y}}]^2) + 
           2*\[Omega][{{Y}, {X, X, Y}}]*(-1064 - 568*M + 824*M^2 + 665*M^3 + 
             112*M^4 + (928 + 984*M + 204*M^2 - 92*M^3)*\[Omega][{{X, Y}, 
                {X, Y}}] - 16*(13 + 20*M + M^2)*\[Omega][{{X, Y}, {X, Y}}]^
               2)) + (1 + M + 2*\[Omega][{{Y}, {X, X, Y}}])*
          (-56 + 20*M + 174*M^2 + 141*M^3 + 33*M^4 + 16*(-20 + 11*M)*
            \[Omega][{{Y}, {X, X, Y}}]^4 + (32 + 60*M + 40*M^2 + 11*M^3)*
            \[Omega][{{X, Y}, {X, Y}}] + (24 - 72*M - 58*M^2)*
            \[Omega][{{X, Y}, {X, Y}}]^2 + 4*\[Omega][{{Y}, {X, X, Y}}]^3*
            (-296 + 62*M + 69*M^2 - 40*(-4 + M)*\[Omega][{{X, Y}, {X, Y}}]) + 
           2*\[Omega][{{Y}, {X, X, Y}}]^2*(-660 - 40*M + 333*M^2 + 65*M^3 - 
             8*(-80 - 20*M + 7*M^2)*\[Omega][{{X, Y}, {X, Y}}] - 
             8*(20 + M)*\[Omega][{{X, Y}, {X, Y}}]^2) + 
           \[Omega][{{Y}, {X, X, Y}}]*(-512 - 92*M + 548*M^2 + 291*M^3 + 
             18*M^4 + (528 + 504*M + 64*M^2 - 18*M^3)*\[Omega][{{X, Y}, 
                {X, Y}}] - 4*(24 + 78*M + M^2)*\[Omega][{{X, Y}, {X, Y}}]^
               2))) + 2*\[Lambda][{y}]^3*(8*M*\[Omega][{{X}, {X, Y, Y}}]^4*
          (-5 - 2*M + M^2 + 2*(-5 + M)*\[Omega][{{Y}, {X, X, Y}}]) + 
         2*\[Omega][{{X}, {X, Y, Y}}]^3*(-40 - 176*M - 71*M^2 + 17*M^3 + 
           4*M^4 + (-80 - 352*M - 30*M^2 + 20*M^3)*\[Omega][
             {{Y}, {X, X, Y}}] + 8*M*(-20 + 3*M)*\[Omega][{{Y}, {X, X, Y}}]^
             2) + \[Omega][{{X}, {X, Y, Y}}]^2*(-164 - 942*M - 650*M^2 - 
           25*M^3 + 39*M^4 + 4*M*(-528 - 51*M + 14*M^2)*
            \[Omega][{{Y}, {X, X, Y}}]^2 + 48*(-10 + M)*M*
            \[Omega][{{Y}, {X, X, Y}}]^3 + 4*(40 + 46*M - 13*M^2 - 7*M^3 + 
             2*M^4)*\[Omega][{{X, Y}, {X, Y}}] - 8*M*(-5 - 2*M + M^2)*
            \[Omega][{{X, Y}, {X, Y}}]^2 + 2*\[Omega][{{Y}, {X, X, Y}}]*
            (-164 - 1246*M - 497*M^2 + 50*M^3 + 8*M^4 + 
             8*(20 + 3*M - 6*M^2 + M^3)*\[Omega][{{X, Y}, {X, Y}}] - 
             8*(-5 + M)*M*\[Omega][{{X, Y}, {X, Y}}]^2)) - 
         (1 + M + 2*\[Omega][{{Y}, {X, X, Y}}])*(76 + 284*M + 271*M^2 + 
           68*M^3 + 2*(-80 + 196*M + 23*M^2)*\[Omega][{{Y}, {X, X, Y}}]^3 + 
           40*M*\[Omega][{{Y}, {X, X, Y}}]^4 + (-128 - 118*M + 43*M^2 + 
             27*M^3)*\[Omega][{{X, Y}, {X, Y}}] + (52 + 14*M - 30*M^2)*
            \[Omega][{{X, Y}, {X, Y}}]^2 + \[Omega][{{Y}, {X, X, Y}}]^2*
            (-44 + 954*M + 324*M^2 + 9*M^3 + 4*M*(4 + 9*M)*\[Omega][{{X, Y}, 
                {X, Y}}] - 40*M*\[Omega][{{X, Y}, {X, Y}}]^2) + 
           \[Omega][{{Y}, {X, X, Y}}]*(192 + 906*M + 631*M^2 + 47*M^3 + 
             (-328 - 48*M + 138*M^2 + 9*M^3)*\[Omega][{{X, Y}, {X, Y}}] - 
             2*(-80 + 68*M + 5*M^2)*\[Omega][{{X, Y}, {X, Y}}]^2)) + 
         \[Omega][{{X}, {X, Y, Y}}]*(-160 - 1066*M - 1161*M^2 - 302*M^3 + 
           25*M^4 + 4*(120 - 568*M - 73*M^2 + 6*M^3)*
            \[Omega][{{Y}, {X, X, Y}}]^3 + 16*(-20 + M)*M*
            \[Omega][{{Y}, {X, X, Y}}]^4 + 3*(72 + 144*M - 14*M^2 - 41*M^3 + 
             5*M^4)*\[Omega][{{X, Y}, {X, Y}}] + (-80 + 56*M + 74*M^2 - 
             14*M^3)*\[Omega][{{X, Y}, {X, Y}}]^2 + 
           2*\[Omega][{{Y}, {X, X, Y}}]^2*(M*(-2140 - 969*M + M^2 + 4*M^3) + 
             4*(40 + 2*M - 21*M^2 + 2*M^3)*\[Omega][{{X, Y}, {X, Y}}] - 
             8*(-10 + M)*M*\[Omega][{{X, Y}, {X, Y}}]^2) + 
           2*\[Omega][{{Y}, {X, X, Y}}]*(-220 - 1832*M - 1399*M^2 - 154*M^3 + 
             15*M^4 + (296 + 300*M - 226*M^2 - 17*M^3 + 4*M^4)*
              \[Omega][{{X, Y}, {X, Y}}] - 2*(40 - 88*M - 7*M^2 + 2*M^3)*
              \[Omega][{{X, Y}, {X, Y}}]^2))))/
      (E^(T*(1 + M + \[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          2*\[Omega][{{Y}, {X, X, Y}}]))*(1 + M - 5*\[Lambda][{y}])*
       (2 + M - 4*\[Lambda][{y}])*(1 + M + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (1 + M + \[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}])*(2 + M + 2*\[Lambda][{y}] + 
        4*\[Omega][{{Y}, {X, X, Y}}])*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, Y}, {X, Y}}])*
       (1 + M - 2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}]))))/
   ((1 + 2*\[Omega][{{X}, {X, Y, Y}}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{Y}, {X, X, Y}}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])), 
  (-2*((4*M^2*(-1 + \[Lambda][{y}])*\[Lambda][{y}]^2*
       \[Omega][{{Y}, {X, X, Y}}]*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])*
       (M + 5*\[Lambda][{y}] + 4*\[Omega][{{X}, {X, Y, Y}}] - 
        2*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/
      (E^(T*(\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}]))*
       (1 + M + 2*\[Omega][{{X}, {X, Y, Y}}])*(5*\[Lambda][{y}] + 
        2*\[Omega][{{X}, {X, Y, Y}}])*(M + 4*\[Lambda][{y}] + 
        4*\[Omega][{{X}, {X, Y, Y}}])*(\[Lambda][{y}] + 
        2*\[Omega][{{Y}, {X, X, Y}}])*(2*\[Lambda][{y}] + 
        \[Omega][{{X}, {X, Y, Y}}] - \[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, Y}, {X, Y}}])*(M + 2*\[Omega][{{X}, {X, Y, Y}}] - 
        2*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}])) - 
     (M^2*\[Lambda][{y}]^3*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}])*(1 + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, Y}, {X, Y}}])*(M + 8*\[Lambda][{y}] + 
        4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}] + 
        2*\[Omega][{{X, Y}, {X, Y}}]))/((3*\[Lambda][{y}] + 
        \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (1 + M + \[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}])*(M + 6*\[Lambda][{y}] + 
        4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
       (3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])*
       (M + 2*\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}])) + 
     (2*M^2*(-1 + \[Lambda][{y}])*\[Lambda][{y}]*
       (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (M - \[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}])*
       (\[Omega][{{X}, {X, Y, Y}}]^2 - \[Omega][{{Y}, {X, X, Y}}]^2 + 
        2*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, Y}, {X, Y}}] + 
        \[Omega][{{X, Y}, {X, Y}}]^2 - 2*\[Lambda][{y}]*
         (\[Omega][{{X}, {X, Y, Y}}]*(-1 + \[Omega][{{Y}, {X, X, Y}}]) + 
          \[Omega][{{Y}, {X, X, Y}}]^2 - \[Omega][{{X, Y}, {X, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}]*(2 + \[Omega][{{X, Y}, {X, Y}}]))))/
      (E^(T*(3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
       (M - 4*\[Lambda][{y}])*(M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, Y}, {X, Y}}])*
       (1 + M - 2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}])*
       (3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}])*
       (2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] - 
        \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])*
       (3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])) - 
     (4*M*\[Lambda][{y}]^2*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}])*(M^2 + 2*(-4 + M)*
         \[Omega][{{X}, {X, Y, Y}}]^2 + 6*M*\[Omega][{{Y}, {X, X, Y}}] + 
        M^2*\[Omega][{{Y}, {X, X, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]^2 + 
        2*M*\[Omega][{{Y}, {X, X, Y}}]^2 - 2*M*\[Omega][{{X, Y}, {X, Y}}] + 
        M^2*\[Omega][{{X, Y}, {X, Y}}] + 4*M*\[Omega][{{Y}, {X, X, Y}}]*
         \[Omega][{{X, Y}, {X, Y}}] - 8*\[Omega][{{X, Y}, {X, Y}}]^2 + 
        2*M*\[Omega][{{X, Y}, {X, Y}}]^2 + \[Omega][{{X}, {X, Y, Y}}]*
         ((-2 + M)*M + 4*M*\[Omega][{{Y}, {X, X, Y}}] + 
          4*(-4 + M)*\[Omega][{{X, Y}, {X, Y}}]) - 4*\[Lambda][{y}]*
         (M + M*\[Omega][{{Y}, {X, X, Y}}]^2 + \[Omega][{{X}, {X, Y, Y}}]*
           (2 + M*\[Omega][{{Y}, {X, X, Y}}]) + 
          2*\[Omega][{{X, Y}, {X, Y}}] + \[Omega][{{Y}, {X, X, Y}}]*
           (-2 + 3*M + M*\[Omega][{{X, Y}, {X, Y}}]))))/
      (E^((T*(M + 2*\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
           2*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
       (M - 4*\[Lambda][{y}])*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, Y}, {X, Y}}])*
       (2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}])*
       (M + 2*\[Omega][{{X}, {X, Y, Y}}] - 2*\[Omega][{{Y}, {X, X, Y}}] + 
        2*\[Omega][{{X, Y}, {X, Y}}])*(M + 2*\[Lambda][{y}] + 
        2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        2*\[Omega][{{X, Y}, {X, Y}}])) + 
     (M^2*(-1 + \[Lambda][{y}])*(2*\[Omega][{{X}, {X, Y, Y}}]*
         (\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
         (\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] - 
          \[Omega][{{X, Y}, {X, Y}}]) - \[Lambda][{y}]*
         (2*\[Omega][{{X}, {X, Y, Y}}]^3 + 5*\[Omega][{{Y}, {X, X, Y}}]*
           (-\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]) + 
          \[Omega][{{X}, {X, Y, Y}}]^2*(-5 + 4*\[Omega][{{Y}, {X, X, Y}}] + 
            2*\[Omega][{{X, Y}, {X, Y}}]) + \[Omega][{{X}, {X, Y, Y}}]*
           (2*\[Omega][{{Y}, {X, X, Y}}]^2 + 2*\[Omega][{{Y}, {X, X, Y}}]*
             (-5 + \[Omega][{{X, Y}, {X, Y}}]) + 
            11*\[Omega][{{X, Y}, {X, Y}}])) + \[Lambda][{y}]^2*
         (2*\[Omega][{{Y}, {X, X, Y}}]^3 + \[Omega][{{X}, {X, Y, Y}}]^2*
           (-5 + 2*\[Omega][{{Y}, {X, X, Y}}]) - 
          15*\[Omega][{{X, Y}, {X, Y}}] + \[Omega][{{Y}, {X, X, Y}}]*
           (18 + \[Omega][{{X, Y}, {X, Y}}]) + \[Omega][{{Y}, {X, X, Y}}]^2*
           (7 + 2*\[Omega][{{X, Y}, {X, Y}}]) + \[Omega][{{X}, {X, Y, Y}}]*
           (4*\[Omega][{{Y}, {X, X, Y}}]^2 - 5*\[Omega][{{X, Y}, {X, Y}}] + 
            2*\[Omega][{{Y}, {X, X, Y}}]*(1 + \[Omega][{{X, Y}, {X, Y}}])))))/
      (E^(2*T*(3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}]))*(M - 6*\[Lambda][{y}])*
       (1 + M - 5*\[Lambda][{y}])*(5*\[Lambda][{y}] + 
        2*\[Omega][{{X}, {X, Y, Y}}])*(3*\[Lambda][{y}] + 
        \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}])) + 
     (4*M*\[Lambda][{y}]*(-((M + 4*\[Omega][{{X}, {X, Y, Y}}])*
          (M + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
          (\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] - 
           \[Omega][{{X, Y}, {X, Y}}])*((-6 + M)*\[Omega][{{X}, {X, Y, Y}}] + 
           (-6 + M)*\[Omega][{{Y}, {X, X, Y}}] + 
           6*\[Omega][{{X, Y}, {X, Y}}])) - 8*\[Lambda][{y}]^3*
         (M*\[Omega][{{Y}, {X, X, Y}}]^3 + \[Omega][{{X}, {X, Y, Y}}]^2*
           (18 - M + M*\[Omega][{{Y}, {X, X, Y}}]) + 
          \[Omega][{{Y}, {X, X, Y}}]*(3*(6 + 5*M) + 2*(-9 + M)*
             \[Omega][{{X, Y}, {X, Y}}]) + \[Omega][{{Y}, {X, X, Y}}]^2*
           (18 + 5*M + M*\[Omega][{{X, Y}, {X, Y}}]) - 
          3*(-3*M + (6 + M)*\[Omega][{{X, Y}, {X, Y}}]) + 
          \[Omega][{{X}, {X, Y, Y}}]*(6*(3 + M) + 
            2*M*\[Omega][{{Y}, {X, X, Y}}]^2 - (18 + M)*
             \[Omega][{{X, Y}, {X, Y}}] + \[Omega][{{Y}, {X, X, Y}}]*
             (4*(9 + M) + M*\[Omega][{{X, Y}, {X, Y}}]))) + 
        \[Lambda][{y}]*(16*(-6 + M)*\[Omega][{{X}, {X, Y, Y}}]^4 + 
          8*(12 + 7*M + M^2)*\[Omega][{{Y}, {X, X, Y}}]^3 + 
          16*M*\[Omega][{{Y}, {X, X, Y}}]^4 + 8*\[Omega][{{X}, {X, Y, Y}}]^3*
           (18 - 7*M + M^2 + 4*(-9 + 2*M)*\[Omega][{{Y}, {X, X, Y}}] + 
            12*\[Omega][{{X, Y}, {X, Y}}]) + 
          3*M*(M^2 + (-10 + M)*M*\[Omega][{{X, Y}, {X, Y}}] - 
            4*(-5 + M)*\[Omega][{{X, Y}, {X, Y}}]^2) + 
          \[Omega][{{Y}, {X, X, Y}}]^2*(M*(180 + 16*M + M^2) + 
            4*(-48 - 2*M + M^2)*\[Omega][{{X, Y}, {X, Y}}] - 
            16*M*\[Omega][{{X, Y}, {X, Y}}]^2) + \[Omega][{{Y}, {X, X, Y}}]*
           (2*M^2*(21 + M) + M*(-240 + 16*M + M^2)*\[Omega][{{X, Y}, {X, 
                Y}}] - 4*(-24 + 12*M + M^2)*\[Omega][{{X, Y}, {X, Y}}]^2) + 
          \[Omega][{{X}, {X, Y, Y}}]^2*(M*(60 + M^2) + 96*(-3 + M)*
             \[Omega][{{Y}, {X, X, Y}}]^2 + 4*(-96 + 10*M + M^2)*
             \[Omega][{{X, Y}, {X, Y}}] - 16*M*\[Omega][{{X, Y}, {X, Y}}]^2 + 
            8*\[Omega][{{Y}, {X, X, Y}}]*(48 - 7*M + 3*M^2 + 
              24*\[Omega][{{X, Y}, {X, Y}}])) + \[Omega][{{X}, {X, Y, Y}}]*
           (2*M^2*(9 + M) + 32*(-3 + 2*M)*\[Omega][{{Y}, {X, X, Y}}]^3 + 
            M*(-216 + 16*M + M^2)*\[Omega][{{X, Y}, {X, Y}}] - 
            4*(-60 + 12*M + M^2)*\[Omega][{{X, Y}, {X, Y}}]^2 + 
            8*\[Omega][{{Y}, {X, X, Y}}]^2*(42 + 7*M + 3*M^2 + 
              12*\[Omega][{{X, Y}, {X, Y}}]) + 2*\[Omega][{{Y}, {X, X, Y}}]*
             (M*(120 + 8*M + M^2) + 4*(-72 + 4*M + M^2)*\[Omega][
                {{X, Y}, {X, Y}}] - 16*M*\[Omega][{{X, Y}, {X, Y}}]^2))) - 
        2*\[Lambda][{y}]^2*(2*(24 + 18*M + M^2)*\[Omega][{{Y}, {X, X, Y}}]^
            3 + 8*M*\[Omega][{{Y}, {X, X, Y}}]^4 + 
          4*\[Omega][{{X}, {X, Y, Y}}]^3*(30 - 3*M + 
            2*M*\[Omega][{{Y}, {X, X, Y}}]) + \[Omega][{{X}, {X, Y, Y}}]^2*
           (48 + 50*M - 3*M^2 + 2*(144 + 6*M + M^2)*\[Omega][{{Y}, {X, X, 
                Y}}] + 24*M*\[Omega][{{Y}, {X, X, Y}}]^2 - 
            4*(30 + M)*\[Omega][{{X, Y}, {X, Y}}]) + 
          3*(M^2 + (14 - 3*M)*M*\[Omega][{{X, Y}, {X, Y}}] + 
            8*(-3 + M)*\[Omega][{{X, Y}, {X, Y}}]^2) + 
          \[Omega][{{Y}, {X, X, Y}}]*(10*M*(-3 + 2*M) + 3*(32 - 34*M + M^2)*
             \[Omega][{{X, Y}, {X, Y}}] - 16*M*\[Omega][{{X, Y}, {X, Y}}]^
              2) + \[Omega][{{Y}, {X, X, Y}}]^2*(-24 + 74*M + 9*M^2 + 
            2*(-24 - 14*M + M^2)*\[Omega][{{X, Y}, {X, Y}}] - 
            8*M*\[Omega][{{X, Y}, {X, Y}}]^2) + \[Omega][{{X}, {X, Y, Y}}]*
           (2*M*(21 + M) + 4*(54 + 15*M + M^2)*\[Omega][{{Y}, {X, X, Y}}]^2 + 
            24*M*\[Omega][{{Y}, {X, X, Y}}]^3 - 3*(-8 + 10*M + M^2)*
             \[Omega][{{X, Y}, {X, Y}}] + 8*M*\[Omega][{{X, Y}, {X, Y}}]^2 + 
            2*\[Omega][{{Y}, {X, X, Y}}]*(12 + 62*M + 3*M^2 + 
              (-84 - 16*M + M^2)*\[Omega][{{X, Y}, {X, Y}}] - 
              4*M*\[Omega][{{X, Y}, {X, Y}}]^2)))))/
      (E^((T*(M + 6*\[Lambda][{y}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
           4*\[Omega][{{Y}, {X, X, Y}}]))/2)*(M - 6*\[Lambda][{y}])*
       (2 + M - 4*\[Lambda][{y}])*(M + 4*\[Lambda][{y}] + 
        4*\[Omega][{{X}, {X, Y, Y}}])*(M + 6*\[Lambda][{y}] + 
        4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
       (M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
        2*\[Omega][{{X, Y}, {X, Y}}])*(2*\[Lambda][{y}] + 
        \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] - 
        \[Omega][{{X, Y}, {X, Y}}])) + 
     (-((2 + M)*(1 + M + 2*\[Omega][{{X}, {X, Y, Y}}])*
         (1 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          2*\[Omega][{{Y}, {X, X, Y}}])*(2 + 3*M + M^2 + 
          2*\[Omega][{{X}, {X, Y, Y}}]^2 + 2*\[Omega][{{Y}, {X, X, Y}}]^2 + 
          \[Omega][{{Y}, {X, X, Y}}]*(4 + 3*M - 
            4*\[Omega][{{X, Y}, {X, Y}}]) + \[Omega][{{X}, {X, Y, Y}}]*
           (4 + 3*M + 4*\[Omega][{{Y}, {X, X, Y}}] - 
            4*\[Omega][{{X, Y}, {X, Y}}]) - 4*\[Omega][{{X, Y}, {X, Y}}] - 
          3*M*\[Omega][{{X, Y}, {X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]^2)) - 
       \[Lambda][{y}]*(1 + M + 2*\[Omega][{{X}, {X, Y, Y}}])*
        (-32 - 64*M - 38*M^2 - 5*M^3 + M^4 + 4*(-14 + 3*M)*
          \[Omega][{{X}, {X, Y, Y}}]^3 + 4*(-14 + 3*M)*
          \[Omega][{{Y}, {X, X, Y}}]^3 + 56*\[Omega][{{X, Y}, {X, Y}}] + 
         84*M*\[Omega][{{X, Y}, {X, Y}}] + 
         24*M^2*\[Omega][{{X, Y}, {X, Y}}] - 
         3*M^3*\[Omega][{{X, Y}, {X, Y}}] - 24*\[Omega][{{X, Y}, {X, Y}}]^2 - 
         24*M*\[Omega][{{X, Y}, {X, Y}}]^2 + 2*M^2*\[Omega][{{X, Y}, {X, Y}}]^
           2 + 4*\[Omega][{{X}, {X, Y, Y}}]^2*(-38 - 18*M + 4*M^2 + 
           (-42 + 9*M)*\[Omega][{{Y}, {X, X, Y}}] - 4*(-7 + M)*
            \[Omega][{{X, Y}, {X, Y}}]) + 8*\[Omega][{{Y}, {X, X, Y}}]^2*
          (-19 - 9*M + 2*M^2 - 2*(-7 + M)*\[Omega][{{X, Y}, {X, Y}}]) + 
         \[Omega][{{Y}, {X, X, Y}}]*(-128 - 152*M - 32*M^2 + 7*M^3 - 
           2*(-88 - 52*M + 7*M^2)*\[Omega][{{X, Y}, {X, Y}}] + 
           4*(-14 + M)*\[Omega][{{X, Y}, {X, Y}}]^2) + 
         \[Omega][{{X}, {X, Y, Y}}]*(-128 - 152*M - 32*M^2 + 7*M^3 + 
           12*(-14 + 3*M)*\[Omega][{{Y}, {X, X, Y}}]^2 - 
           2*(-88 - 52*M + 7*M^2)*\[Omega][{{X, Y}, {X, Y}}] + 
           4*(-14 + M)*\[Omega][{{X, Y}, {X, Y}}]^2 + 
           16*\[Omega][{{Y}, {X, X, Y}}]*(-19 - 9*M + 2*M^2 - 
             2*(-7 + M)*\[Omega][{{X, Y}, {X, Y}}]))) - 
       20*\[Lambda][{y}]^4*(M^2*\[Omega][{{Y}, {X, X, Y}}]^3 + 
         \[Omega][{{X}, {X, Y, Y}}]^2*(-8 + 4*M + 
           M^2*\[Omega][{{Y}, {X, X, Y}}]) + M^2*\[Omega][{{Y}, {X, X, Y}}]^2*
          (6 + \[Omega][{{X, Y}, {X, Y}}]) + 4*(1 + M)*
          (-1 + M + \[Omega][{{X, Y}, {X, Y}}]) + \[Omega][{{Y}, {X, X, Y}}]*
          (-4 - 2*M + 11*M^2 + 3*M^2*\[Omega][{{X, Y}, {X, Y}}]) + 
         \[Omega][{{X}, {X, Y, Y}}]*(2*M^2*\[Omega][{{Y}, {X, X, Y}}]^2 + 
           2*(-6 + 3*M + M^2 + 4*\[Omega][{{X, Y}, {X, Y}}]) + 
           \[Omega][{{Y}, {X, X, Y}}]*(-8 + 4*M + 6*M^2 + 
             M^2*\[Omega][{{X, Y}, {X, Y}}]))) + 2*\[Lambda][{y}]^3*
        (4*M^2*(10 + M)*\[Omega][{{Y}, {X, X, Y}}]^3 + 
         4*M^2*\[Omega][{{Y}, {X, X, Y}}]^4 + 2*\[Omega][{{X}, {X, Y, Y}}]^3*
          (5*(12 - 6*M + M^2) + 2*M^2*\[Omega][{{Y}, {X, X, Y}}]) + 
         \[Omega][{{X}, {X, Y, Y}}]^2*(164 - 58*M + 13*M^2 + 5*M^3 + 
           4*(60 - 30*M + 15*M^2 + M^3)*\[Omega][{{Y}, {X, X, Y}}] + 
           12*M^2*\[Omega][{{Y}, {X, X, Y}}]^2 + 10*(-8 - 2*M + M^2)*
            \[Omega][{{X, Y}, {X, Y}}]) + (1 + M)*(-8 + 23*M^2 + 
           (28 - 42*M + 15*M^2)*\[Omega][{{X, Y}, {X, Y}}] - 
           20*\[Omega][{{X, Y}, {X, Y}}]^2) + \[Omega][{{Y}, {X, X, Y}}]*
          (52 + 8*M + 119*M^2 + 55*M^3 + (-40 - 50*M + 7*M^2 + 17*M^3)*
            \[Omega][{{X, Y}, {X, Y}}] - 12*M^2*\[Omega][{{X, Y}, {X, Y}}]^
             2) + \[Omega][{{Y}, {X, X, Y}}]^2*(60 + 30*M + 107*M^2 + 
           29*M^3 + 4*M^2*(1 + M)*\[Omega][{{X, Y}, {X, Y}}] - 
           4*M^2*\[Omega][{{X, Y}, {X, Y}}]^2) + \[Omega][{{X}, {X, Y, Y}}]*
          (36 + 8*M + 21*M^2 + 19*M^3 + 2*(60 - 30*M + 45*M^2 + 4*M^3)*
            \[Omega][{{Y}, {X, X, Y}}]^2 + 12*M^2*\[Omega][{{Y}, {X, X, Y}}]^
             3 + (16 - 134*M + 25*M^2 + 5*M^3)*\[Omega][{{X, Y}, {X, Y}}] - 
           40*\[Omega][{{X, Y}, {X, Y}}]^2 + 2*\[Omega][{{Y}, {X, X, Y}}]*
            (112 - 14*M + 60*M^2 + 17*M^3 + (-40 - 10*M + 7*M^2 + 2*M^3)*
              \[Omega][{{X, Y}, {X, Y}}] - 2*M^2*\[Omega][{{X, Y}, {X, Y}}]^
               2))) - \[Lambda][{y}]^2*(8*(20 - 10*M + M^2)*
          \[Omega][{{X}, {X, Y, Y}}]^4 + 4*(20 + 10*M + 6*M^2 + 3*M^3)*
          \[Omega][{{Y}, {X, X, Y}}]^3 + 8*M^2*\[Omega][{{Y}, {X, X, Y}}]^4 + 
         4*\[Omega][{{X}, {X, Y, Y}}]^3*(162 - 9*M - 22*M^2 + 3*M^3 + 
           4*(30 - 15*M + 2*M^2)*\[Omega][{{Y}, {X, X, Y}}] + 
           20*(-4 + M)*\[Omega][{{X, Y}, {X, Y}}]) + 
         2*\[Omega][{{Y}, {X, X, Y}}]^2*(142 + 123*M + 40*M^2 + 13*M^3 + 
           2*M^4 + 2*(-40 - 30*M + 7*M^2 + 2*M^3)*\[Omega][
             {{X, Y}, {X, Y}}] - 4*M^2*\[Omega][{{X, Y}, {X, Y}}]^2) + 
         (1 + M)*(60 + 60*M + 11*M^2 + M^3 + (-72 - 102*M - 17*M^2 + 12*M^3)*
            \[Omega][{{X, Y}, {X, Y}}] - 6*(-2 - 7*M + 2*M^2)*
            \[Omega][{{X, Y}, {X, Y}}]^2) + \[Omega][{{Y}, {X, X, Y}}]*
          (3*(88 + 114*M + 47*M^2 + 11*M^3 + 2*M^4) + 
           (-296 - 356*M - 82*M^2 + 42*M^3 + 4*M^4)*\[Omega][
             {{X, Y}, {X, Y}}] - 4*(-20 - 20*M + 7*M^2 + M^3)*
            \[Omega][{{X, Y}, {X, Y}}]^2) + 2*\[Omega][{{X}, {X, Y, Y}}]^2*
          (406 + 201*M - 77*M^2 - 5*M^3 + 2*M^4 + 24*(10 - 5*M + M^2)*
            \[Omega][{{Y}, {X, X, Y}}]^2 + 2*(-188 - 60*M + 17*M^2 + 2*M^3)*
            \[Omega][{{X, Y}, {X, Y}}] - 4*(-20 + M^2)*
            \[Omega][{{X, Y}, {X, Y}}]^2 + 2*\[Omega][{{Y}, {X, X, Y}}]*
            (344 - 8*M - 38*M^2 + 9*M^3 + 40*(-4 + M)*\[Omega][{{X, Y}, 
                {X, Y}}])) + \[Omega][{{X}, {X, Y, Y}}]*(384 + 462*M + 
           55*M^2 - 37*M^3 + 6*M^4 + 16*(10 - 5*M + 2*M^2)*
            \[Omega][{{Y}, {X, X, Y}}]^3 + (-440 - 560*M - 80*M^2 + 42*M^3 + 
             4*M^4)*\[Omega][{{X, Y}, {X, Y}}] - 4*(-26 - 41*M + 7*M^2 + M^3)*
            \[Omega][{{X, Y}, {X, Y}}]^2 + 4*\[Omega][{{Y}, {X, X, Y}}]^2*
            (202 + 11*M - 10*M^2 + 9*M^3 + 20*(-4 + M)*\[Omega][{{X, Y}, 
                {X, Y}}]) + 2*\[Omega][{{Y}, {X, X, Y}}]*(548 + 324*M - 
             37*M^2 + 8*M^3 + 4*M^4 + 4*(-114 - 45*M + 12*M^2 + 2*M^3)*
              \[Omega][{{X, Y}, {X, Y}}] - 8*(-10 + M^2)*\[Omega][
                {{X, Y}, {X, Y}}]^2))))/
      (E^(T*(1 + M + \[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          2*\[Omega][{{Y}, {X, X, Y}}]))*(1 + M - 5*\[Lambda][{y}])*
       (2 + M - 4*\[Lambda][{y}])*(1 + M + 2*\[Omega][{{X}, {X, Y, Y}}])*
       (1 + M + \[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}])*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, Y}, {X, Y}}])*
       (1 + M - 2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}]))))/
   ((3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
    (1 + 2*\[Omega][{{Y}, {X, X, Y}}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])), 
  2*((-4*M^2*(-1 + \[Lambda][{y}])*\[Lambda][{y}]^2*(M + 5*\[Lambda][{y}] + 
       4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}] - 
       4*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}])/
     (E^(T*(\[Lambda][{y}] + 2*\[Omega][{{X, Y}, {X, Y}}]))*
      (5*\[Lambda][{y}] + 2*(\[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}]))*
      (M + 4*\[Lambda][{y}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
       4*\[Omega][{{Y}, {X, X, Y}}] - 4*\[Omega][{{X, Y}, {X, Y}}])*
      (M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
       2*\[Omega][{{X, Y}, {X, Y}}])*(1 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
       2*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, Y}, {X, Y}}])*
      (2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}])*
      (1 + 2*\[Omega][{{X, Y}, {X, Y}}])*(\[Lambda][{y}] + 
       2*\[Omega][{{X, Y}, {X, Y}}])) + 
    (M^2*\[Lambda][{y}]^3*(M + 8*\[Lambda][{y}] + 
       4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}] + 
       2*\[Omega][{{X, Y}, {X, Y}}]))/
     ((3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{Y}, {X, X, Y}}])*(1 + M + \[Lambda][{y}] + 
       2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
      (M + 6*\[Lambda][{y}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
       4*\[Omega][{{Y}, {X, X, Y}}])*(3*\[Lambda][{y}] + 
       \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
       \[Omega][{{X, Y}, {X, Y}}])*(\[Lambda][{y}] + 
       2*\[Omega][{{X, Y}, {X, Y}}])*(M + 2*\[Lambda][{y}] + 
       2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
       2*\[Omega][{{X, Y}, {X, Y}}])) + 
    (2*M^2*(-1 + \[Lambda][{y}])*\[Lambda][{y}]*(M - \[Lambda][{y}] + 
       \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] - 
       \[Omega][{{X, Y}, {X, Y}}])*(-\[Omega][{{X}, {X, Y, Y}}]^2 - 
       2*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] - 
       \[Omega][{{Y}, {X, X, Y}}]^2 + \[Omega][{{X, Y}, {X, Y}}]^2 + 
       2*\[Lambda][{y}]*(\[Omega][{{X}, {X, Y, Y}}]*
          (-1 + \[Omega][{{X, Y}, {X, Y}}]) + \[Omega][{{Y}, {X, X, Y}}]*
          (-1 + \[Omega][{{X, Y}, {X, Y}}]) + \[Omega][{{X, Y}, {X, Y}}]*
          (2 + \[Omega][{{X, Y}, {X, Y}}]))))/
     (E^(T*(3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
      (M - 4*\[Lambda][{y}])*(M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
       2*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, Y}, {X, Y}}])*
      (1 + M - 2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}])*
      (2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}])*
      (3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}])*
      (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
       \[Omega][{{X, Y}, {X, Y}}])*(3*\[Lambda][{y}] + 
       \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
       \[Omega][{{X, Y}, {X, Y}}])*(1 + 2*\[Omega][{{X, Y}, {X, Y}}])) + 
    (4*M*\[Lambda][{y}]^2*(M^2 + 2*(-4 + M)*\[Omega][{{X}, {X, Y, Y}}]^2 - 
       2*M*\[Omega][{{Y}, {X, X, Y}}] + M^2*\[Omega][{{Y}, {X, X, Y}}] - 
       8*\[Omega][{{Y}, {X, X, Y}}]^2 + 2*M*\[Omega][{{Y}, {X, X, Y}}]^2 + 
       6*M*\[Omega][{{X, Y}, {X, Y}}] + M^2*\[Omega][{{X, Y}, {X, Y}}] + 
       4*M*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}] + 
       8*\[Omega][{{X, Y}, {X, Y}}]^2 + 2*M*\[Omega][{{X, Y}, {X, Y}}]^2 + 
       \[Omega][{{X}, {X, Y, Y}}]*(4*(-4 + M)*\[Omega][{{Y}, {X, X, Y}}] + 
         M*(-2 + M + 4*\[Omega][{{X, Y}, {X, Y}}])) - 
       4*\[Lambda][{y}]*(M - 2*\[Omega][{{X, Y}, {X, Y}}] + 
         3*M*\[Omega][{{X, Y}, {X, Y}}] + M*\[Omega][{{X, Y}, {X, Y}}]^2 + 
         \[Omega][{{X}, {X, Y, Y}}]*(2 + M*\[Omega][{{X, Y}, {X, Y}}]) + 
         \[Omega][{{Y}, {X, X, Y}}]*(2 + M*\[Omega][{{X, Y}, {X, Y}}]))))/
     (E^((T*(M + 2*\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          2*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
      (M - 4*\[Lambda][{y}])*(M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
       2*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, Y}, {X, Y}}])*
      (2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
       2*\[Omega][{{X, Y}, {X, Y}}])*(2*\[Lambda][{y}] + 
       \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] - 
       \[Omega][{{X, Y}, {X, Y}}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])*
      (1 + 2*\[Omega][{{X, Y}, {X, Y}}])*(M + 2*\[Lambda][{y}] + 
       2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
       2*\[Omega][{{X, Y}, {X, Y}}])) - 
    (M^2*(-1 + \[Lambda][{y}])*(2*(\[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}])*(\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}])^2 + 
       \[Lambda][{y}]*(-2*\[Omega][{{X}, {X, Y, Y}}]^3 + 
         \[Omega][{{X}, {X, Y, Y}}]^2*(5 - 6*\[Omega][{{Y}, {X, X, Y}}]) + 
         5*\[Omega][{{Y}, {X, X, Y}}]^2 - 2*\[Omega][{{Y}, {X, X, Y}}]^3 + 
         6*\[Omega][{{X, Y}, {X, Y}}]^2 + \[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, Y}, {X, Y}}]*(-11 + 2*\[Omega][{{X, Y}, {X, Y}}]) + 
         \[Omega][{{X}, {X, Y, Y}}]*(10*\[Omega][{{Y}, {X, X, Y}}] - 
           6*\[Omega][{{Y}, {X, X, Y}}]^2 + \[Omega][{{X, Y}, {X, Y}}]*
            (-11 + 2*\[Omega][{{X, Y}, {X, Y}}]))) + 
       \[Lambda][{y}]^2*(\[Omega][{{X}, {X, Y, Y}}]^2*
          (-5 + 2*\[Omega][{{X, Y}, {X, Y}}]) + \[Omega][{{Y}, {X, X, Y}}]^2*
          (-5 + 2*\[Omega][{{X, Y}, {X, Y}}]) + 3*\[Omega][{{X, Y}, {X, Y}}]*
          (1 + 2*\[Omega][{{X, Y}, {X, Y}}]) + \[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, Y}, {X, Y}}]*(7 + 2*\[Omega][{{X, Y}, {X, Y}}]) + 
         \[Omega][{{X}, {X, Y, Y}}]*(2*\[Omega][{{Y}, {X, X, Y}}]*
            (-5 + 2*\[Omega][{{X, Y}, {X, Y}}]) + \[Omega][{{X, Y}, {X, Y}}]*
            (7 + 2*\[Omega][{{X, Y}, {X, Y}}])))))/
     (E^(2*T*(3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}]))*(M - 6*\[Lambda][{y}])*
      (1 + M - 5*\[Lambda][{y}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{Y}, {X, X, Y}}])*(3*\[Lambda][{y}] + 
       \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
      (5*\[Lambda][{y}] + 2*(\[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}]))*
      (3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}])*
      (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
       \[Omega][{{X, Y}, {X, Y}}])*(1 + 2*\[Omega][{{X, Y}, {X, Y}}])) - 
    (4*M*\[Lambda][{y}]*(-((M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}])*(\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}])*
         (4*(-6 + M)*\[Omega][{{X}, {X, Y, Y}}]^2 + 4*(-6 + M)*
           \[Omega][{{Y}, {X, X, Y}}]^2 + 
          6*(M - 4*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}]*((-6 + M)*M - 4*(-12 + M)*
             \[Omega][{{X, Y}, {X, Y}}]) + \[Omega][{{X}, {X, Y, Y}}]*
           ((-6 + M)*M + 8*(-6 + M)*\[Omega][{{Y}, {X, X, Y}}] - 
            4*(-12 + M)*\[Omega][{{X, Y}, {X, Y}}]))) - 
       8*\[Lambda][{y}]^3*(\[Omega][{{X}, {X, Y, Y}}]^2*
          (18 - M + M*\[Omega][{{X, Y}, {X, Y}}]) + 
         \[Omega][{{Y}, {X, X, Y}}]^2*(18 - M + 
           M*\[Omega][{{X, Y}, {X, Y}}]) + 
         3*(3*M + 2*(-3 + M)*\[Omega][{{X, Y}, {X, Y}}] + 
           M*\[Omega][{{X, Y}, {X, Y}}]^2) + \[Omega][{{Y}, {X, X, Y}}]*
          (6*(3 + M) + (-18 + 5*M)*\[Omega][{{X, Y}, {X, Y}}] + 
           M*\[Omega][{{X, Y}, {X, Y}}]^2) + \[Omega][{{X}, {X, Y, Y}}]*
          (6*(3 + M) + (-18 + 5*M)*\[Omega][{{X, Y}, {X, Y}}] + 
           M*\[Omega][{{X, Y}, {X, Y}}]^2 + 2*\[Omega][{{Y}, {X, X, Y}}]*
            (18 - M + M*\[Omega][{{X, Y}, {X, Y}}]))) - 
       2*\[Lambda][{y}]^2*(4*\[Omega][{{X}, {X, Y, Y}}]^3*
          (30 - 3*M + 2*M*\[Omega][{{X, Y}, {X, Y}}]) + 
         4*\[Omega][{{Y}, {X, X, Y}}]^3*(30 - 3*M + 
           2*M*\[Omega][{{X, Y}, {X, Y}}]) + \[Omega][{{Y}, {X, X, Y}}]^2*
          (48 + 50*M - 3*M^2 + 2*(-96 + 22*M + M^2)*\[Omega][
             {{X, Y}, {X, Y}}]) + 3*M*(M + (-10 + 3*M)*
            \[Omega][{{X, Y}, {X, Y}}] + 2*(-8 + M)*
            \[Omega][{{X, Y}, {X, Y}}]^2 - 8*\[Omega][{{X, Y}, {X, Y}}]^3) + 
         \[Omega][{{Y}, {X, X, Y}}]*(2*M*(21 + M) + (-48 - 6*M + 9*M^2)*
            \[Omega][{{X, Y}, {X, Y}}] + 2*(36 - 8*M + M^2)*
            \[Omega][{{X, Y}, {X, Y}}]^2 - 8*M*\[Omega][{{X, Y}, {X, Y}}]^
             3) + \[Omega][{{X}, {X, Y, Y}}]^2*(48 + 50*M - 3*M^2 + 
           2*(-96 + 22*M + M^2)*\[Omega][{{X, Y}, {X, Y}}] + 
           12*\[Omega][{{Y}, {X, X, Y}}]*(30 - 3*M + 2*M*\[Omega][{{X, Y}, 
                {X, Y}}])) + \[Omega][{{X}, {X, Y, Y}}]*(2*M*(21 + M) + 
           (-48 - 6*M + 9*M^2)*\[Omega][{{X, Y}, {X, Y}}] + 
           2*(36 - 8*M + M^2)*\[Omega][{{X, Y}, {X, Y}}]^2 - 
           8*M*\[Omega][{{X, Y}, {X, Y}}]^3 + 12*\[Omega][{{Y}, {X, X, Y}}]^2*
            (30 - 3*M + 2*M*\[Omega][{{X, Y}, {X, Y}}]) + 
           2*\[Omega][{{Y}, {X, X, Y}}]*(48 + 50*M - 3*M^2 + 
             2*(-96 + 22*M + M^2)*\[Omega][{{X, Y}, {X, Y}}]))) + 
       \[Lambda][{y}]*(16*(-6 + M)*\[Omega][{{X}, {X, Y, Y}}]^4 + 
         16*(-6 + M)*\[Omega][{{Y}, {X, X, Y}}]^4 + 
         8*\[Omega][{{Y}, {X, X, Y}}]^3*(18 - 7*M + M^2 + 
           24*\[Omega][{{X, Y}, {X, Y}}]) + 8*\[Omega][{{X}, {X, Y, Y}}]^3*
          (18 - 7*M + M^2 + 8*(-6 + M)*\[Omega][{{Y}, {X, X, Y}}] + 
           24*\[Omega][{{X, Y}, {X, Y}}]) + \[Omega][{{Y}, {X, X, Y}}]^2*
          (M*(60 + M^2) + 4*(-108 + 38*M + M^2)*\[Omega][{{X, Y}, {X, Y}}] - 
           16*(6 + M)*\[Omega][{{X, Y}, {X, Y}}]^2) + 
         \[Omega][{{Y}, {X, X, Y}}]*(2*M^2*(9 + M) + M*(-96 + 32*M + M^2)*
            \[Omega][{{X, Y}, {X, Y}}] - 4*(-108 + 24*M + M^2)*
            \[Omega][{{X, Y}, {X, Y}}]^2) + 
         3*(M^3 + (-2 + M)*M^2*\[Omega][{{X, Y}, {X, Y}}] - 
           4*(-3 + M)*M*\[Omega][{{X, Y}, {X, Y}}]^2 - 
           48*\[Omega][{{X, Y}, {X, Y}}]^3) + \[Omega][{{X}, {X, Y, Y}}]^2*
          (M*(60 + M^2) + 96*(-6 + M)*\[Omega][{{Y}, {X, X, Y}}]^2 + 
           4*(-108 + 38*M + M^2)*\[Omega][{{X, Y}, {X, Y}}] - 
           16*(6 + M)*\[Omega][{{X, Y}, {X, Y}}]^2 + 
           24*\[Omega][{{Y}, {X, X, Y}}]*(18 - 7*M + M^2 + 
             24*\[Omega][{{X, Y}, {X, Y}}])) + \[Omega][{{X}, {X, Y, Y}}]*
          (2*M^2*(9 + M) + 64*(-6 + M)*\[Omega][{{Y}, {X, X, Y}}]^3 + 
           M*(-96 + 32*M + M^2)*\[Omega][{{X, Y}, {X, Y}}] - 
           4*(-108 + 24*M + M^2)*\[Omega][{{X, Y}, {X, Y}}]^2 + 
           24*\[Omega][{{Y}, {X, X, Y}}]^2*(18 - 7*M + M^2 + 
             24*\[Omega][{{X, Y}, {X, Y}}]) + 2*\[Omega][{{Y}, {X, X, Y}}]*
            (M*(60 + M^2) + 4*(-108 + 38*M + M^2)*\[Omega][{{X, Y}, 
                {X, Y}}] - 16*(6 + M)*\[Omega][{{X, Y}, {X, Y}}]^2)))))/
     (E^((T*(M + 6*\[Lambda][{y}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}]))/2)*(M - 6*\[Lambda][{y}])*
      (2 + M - 4*\[Lambda][{y}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{Y}, {X, X, Y}}])*(M + 6*\[Lambda][{y}] + 
       4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
      (M + 4*\[Lambda][{y}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
       4*\[Omega][{{Y}, {X, X, Y}}] - 4*\[Omega][{{X, Y}, {X, Y}}])*
      (M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
       2*\[Omega][{{X, Y}, {X, Y}}])*(2*\[Lambda][{y}] + 
       \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] - 
       \[Omega][{{X, Y}, {X, Y}}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])*
      (1 + 2*\[Omega][{{X, Y}, {X, Y}}])) + 
    ((2 + M)*(1 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}])*(2 + 5*M + 4*M^2 + M^3 + 
        4*\[Omega][{{X}, {X, Y, Y}}]^3 + 4*\[Omega][{{Y}, {X, X, Y}}]^3 + 
        2*\[Omega][{{Y}, {X, X, Y}}]^2*(5 + 4*M - 
          6*\[Omega][{{X, Y}, {X, Y}}]) + 2*\[Omega][{{X}, {X, Y, Y}}]^2*
         (5 + 4*M + 6*\[Omega][{{Y}, {X, X, Y}}] - 
          6*\[Omega][{{X, Y}, {X, Y}}]) - 8*\[Omega][{{X, Y}, {X, Y}}] - 
        13*M*\[Omega][{{X, Y}, {X, Y}}] - 5*M^2*\[Omega][{{X, Y}, {X, Y}}] + 
        10*\[Omega][{{X, Y}, {X, Y}}]^2 + 8*M*\[Omega][{{X, Y}, {X, Y}}]^2 - 
        4*\[Omega][{{X, Y}, {X, Y}}]^3 + \[Omega][{{Y}, {X, X, Y}}]*
         (8 + 13*M + 5*M^2 - 4*(5 + 4*M)*\[Omega][{{X, Y}, {X, Y}}] + 
          12*\[Omega][{{X, Y}, {X, Y}}]^2) + \[Omega][{{X}, {X, Y, Y}}]*
         (8 + 13*M + 5*M^2 + 12*\[Omega][{{Y}, {X, X, Y}}]^2 + 
          4*\[Omega][{{Y}, {X, X, Y}}]*(5 + 4*M - 
            6*\[Omega][{{X, Y}, {X, Y}}]) - 4*(5 + 4*M)*
           \[Omega][{{X, Y}, {X, Y}}] + 12*\[Omega][{{X, Y}, {X, Y}}]^2)) + 
      20*\[Lambda][{y}]^4*(-4 + 4*M^2 + 12*\[Omega][{{X, Y}, {X, Y}}] - 
        4*M*\[Omega][{{X, Y}, {X, Y}}] + 9*M^2*\[Omega][{{X, Y}, {X, Y}}] - 
        8*\[Omega][{{X, Y}, {X, Y}}]^2 + 3*M^2*\[Omega][{{X, Y}, {X, Y}}]^2 + 
        \[Omega][{{X}, {X, Y, Y}}]^2*(-8 + 4*M + 
          M^2*\[Omega][{{X, Y}, {X, Y}}]) + \[Omega][{{Y}, {X, X, Y}}]^2*
         (-8 + 4*M + M^2*\[Omega][{{X, Y}, {X, Y}}]) + 
        \[Omega][{{Y}, {X, X, Y}}]*(2*(-6 + 3*M + M^2) + 2*(8 - 2*M + 3*M^2)*
           \[Omega][{{X, Y}, {X, Y}}] + M^2*\[Omega][{{X, Y}, {X, Y}}]^2) + 
        \[Omega][{{X}, {X, Y, Y}}]*(2*(-6 + 3*M + M^2) + 2*(8 - 2*M + 3*M^2)*
           \[Omega][{{X, Y}, {X, Y}}] + M^2*\[Omega][{{X, Y}, {X, Y}}]^2 + 
          2*\[Omega][{{Y}, {X, X, Y}}]*(-8 + 4*M + 
            M^2*\[Omega][{{X, Y}, {X, Y}}]))) - 2*\[Lambda][{y}]^3*
       (-8 - 8*M + 23*M^2 + 23*M^3 + 44*\[Omega][{{X, Y}, {X, Y}}] - 
        14*M*\[Omega][{{X, Y}, {X, Y}}] + 71*M^2*\[Omega][{{X, Y}, {X, Y}}] + 
        51*M^3*\[Omega][{{X, Y}, {X, Y}}] - 76*\[Omega][{{X, Y}, {X, Y}}]^2 + 
        64*M*\[Omega][{{X, Y}, {X, Y}}]^2 - 18*M^2*\[Omega][{{X, Y}, {X, Y}}]^
          2 + 12*M^3*\[Omega][{{X, Y}, {X, Y}}]^2 + 
        40*\[Omega][{{X, Y}, {X, Y}}]^3 - 12*M^2*\[Omega][{{X, Y}, {X, Y}}]^
          3 + 2*\[Omega][{{X}, {X, Y, Y}}]^3*(5*(12 - 6*M + M^2) + 
          2*M^2*\[Omega][{{X, Y}, {X, Y}}]) + 2*\[Omega][{{Y}, {X, X, Y}}]^3*
         (5*(12 - 6*M + M^2) + 2*M^2*\[Omega][{{X, Y}, {X, Y}}]) + 
        \[Omega][{{Y}, {X, X, Y}}]^2*(164 - 58*M + 13*M^2 + 5*M^3 + 
          4*(-50 + 10*M + 10*M^2 + M^3)*\[Omega][{{X, Y}, {X, Y}}]) + 
        \[Omega][{{Y}, {X, X, Y}}]*(36 + 8*M + 21*M^2 + 19*M^3 + 
          (-88 - 46*M + 119*M^2 + 29*M^3)*\[Omega][{{X, Y}, {X, Y}}] + 
          (40 + 20*M - 6*M^2 + 4*M^3)*\[Omega][{{X, Y}, {X, Y}}]^2 - 
          4*M^2*\[Omega][{{X, Y}, {X, Y}}]^3) + \[Omega][{{X}, {X, Y, Y}}]^2*
         (164 - 58*M + 13*M^2 + 5*M^3 + 4*(-50 + 10*M + 10*M^2 + M^3)*
           \[Omega][{{X, Y}, {X, Y}}] + 6*\[Omega][{{Y}, {X, X, Y}}]*
           (5*(12 - 6*M + M^2) + 2*M^2*\[Omega][{{X, Y}, {X, Y}}])) + 
        \[Omega][{{X}, {X, Y, Y}}]*(36 + 8*M + 21*M^2 + 19*M^3 + 
          (-88 - 46*M + 119*M^2 + 29*M^3)*\[Omega][{{X, Y}, {X, Y}}] + 
          (40 + 20*M - 6*M^2 + 4*M^3)*\[Omega][{{X, Y}, {X, Y}}]^2 - 
          4*M^2*\[Omega][{{X, Y}, {X, Y}}]^3 + 6*\[Omega][{{Y}, {X, X, Y}}]^2*
           (5*(12 - 6*M + M^2) + 2*M^2*\[Omega][{{X, Y}, {X, Y}}]) + 
          2*\[Omega][{{Y}, {X, X, Y}}]*(164 - 58*M + 13*M^2 + 5*M^3 + 
            4*(-50 + 10*M + 10*M^2 + M^3)*\[Omega][{{X, Y}, {X, Y}}]))) + 
      \[Lambda][{y}]*(-32 - 96*M - 102*M^2 - 43*M^3 - 4*M^4 + M^5 + 
        8*(-14 + 3*M)*\[Omega][{{X}, {X, Y, Y}}]^4 + 
        8*(-14 + 3*M)*\[Omega][{{Y}, {X, X, Y}}]^4 + 
        120*\[Omega][{{X, Y}, {X, Y}}] + 268*M*\[Omega][{{X, Y}, {X, Y}}] + 
        184*M^2*\[Omega][{{X, Y}, {X, Y}}] + 
        31*M^3*\[Omega][{{X, Y}, {X, Y}}] - 
        5*M^4*\[Omega][{{X, Y}, {X, Y}}] - 136*\[Omega][{{X, Y}, {X, Y}}]^2 - 
        216*M*\[Omega][{{X, Y}, {X, Y}}]^2 - 
        70*M^2*\[Omega][{{X, Y}, {X, Y}}]^2 + 
        8*M^3*\[Omega][{{X, Y}, {X, Y}}]^2 + 48*\[Omega][{{X, Y}, {X, Y}}]^
          3 + 48*M*\[Omega][{{X, Y}, {X, Y}}]^3 - 
        4*M^2*\[Omega][{{X, Y}, {X, Y}}]^3 + 4*\[Omega][{{Y}, {X, X, Y}}]^3*
         (-90 - 47*M + 11*M^2 - 14*(-6 + M)*\[Omega][{{X, Y}, {X, Y}}]) + 
        4*\[Omega][{{X}, {X, Y, Y}}]^3*(-90 - 47*M + 11*M^2 + 
          8*(-14 + 3*M)*\[Omega][{{Y}, {X, X, Y}}] - 14*(-6 + M)*
           \[Omega][{{X, Y}, {X, Y}}]) + 2*\[Omega][{{Y}, {X, X, Y}}]^2*
         (3*(-68 - 88*M - 20*M^2 + 5*M^3) + (384 + 224*M - 38*M^2)*
           \[Omega][{{X, Y}, {X, Y}}] + 4*(-42 + 5*M)*
           \[Omega][{{X, Y}, {X, Y}}]^2) + \[Omega][{{Y}, {X, X, Y}}]*
         (-192 - 408*M - 260*M^2 - 35*M^3 + 9*M^4 + 
          (544 + 752*M + 202*M^2 - 34*M^3)*\[Omega][{{X, Y}, {X, Y}}] + 
          4*(-114 - 77*M + 9*M^2)*\[Omega][{{X, Y}, {X, Y}}]^2 - 
          8*(-14 + M)*\[Omega][{{X, Y}, {X, Y}}]^3) + 
        2*\[Omega][{{X}, {X, Y, Y}}]^2*(3*(-68 - 88*M - 20*M^2 + 5*M^3) + 
          24*(-14 + 3*M)*\[Omega][{{Y}, {X, X, Y}}]^2 + 
          (384 + 224*M - 38*M^2)*\[Omega][{{X, Y}, {X, Y}}] + 
          4*(-42 + 5*M)*\[Omega][{{X, Y}, {X, Y}}]^2 + 
          6*\[Omega][{{Y}, {X, X, Y}}]*(-90 - 47*M + 11*M^2 - 
            14*(-6 + M)*\[Omega][{{X, Y}, {X, Y}}])) + 
        \[Omega][{{X}, {X, Y, Y}}]*(-192 - 408*M - 260*M^2 - 35*M^3 + 9*M^4 + 
          32*(-14 + 3*M)*\[Omega][{{Y}, {X, X, Y}}]^3 + 
          (544 + 752*M + 202*M^2 - 34*M^3)*\[Omega][{{X, Y}, {X, Y}}] + 
          4*(-114 - 77*M + 9*M^2)*\[Omega][{{X, Y}, {X, Y}}]^2 - 
          8*(-14 + M)*\[Omega][{{X, Y}, {X, Y}}]^3 + 
          12*\[Omega][{{Y}, {X, X, Y}}]^2*(-90 - 47*M + 11*M^2 - 
            14*(-6 + M)*\[Omega][{{X, Y}, {X, Y}}]) + 
          4*\[Omega][{{Y}, {X, X, Y}}]*(3*(-68 - 88*M - 20*M^2 + 5*M^3) + 
            (384 + 224*M - 38*M^2)*\[Omega][{{X, Y}, {X, Y}}] + 
            4*(-42 + 5*M)*\[Omega][{{X, Y}, {X, Y}}]^2))) + 
      \[Lambda][{y}]^2*(60 + 120*M + 71*M^2 + 12*M^3 + M^4 + 
        8*(20 - 10*M + M^2)*\[Omega][{{X}, {X, Y, Y}}]^4 + 
        8*(20 - 10*M + M^2)*\[Omega][{{Y}, {X, X, Y}}]^4 - 
        192*\[Omega][{{X, Y}, {X, Y}}] - 294*M*\[Omega][{{X, Y}, {X, Y}}] - 
        33*M^2*\[Omega][{{X, Y}, {X, Y}}] + 
        65*M^3*\[Omega][{{X, Y}, {X, Y}}] + 
        12*M^4*\[Omega][{{X, Y}, {X, Y}}] + 156*\[Omega][{{X, Y}, {X, Y}}]^
          2 + 258*M*\[Omega][{{X, Y}, {X, Y}}]^2 + 
        28*M^2*\[Omega][{{X, Y}, {X, Y}}]^2 - 
        12*M^3*\[Omega][{{X, Y}, {X, Y}}]^2 - 24*\[Omega][{{X, Y}, {X, Y}}]^
          3 - 84*M*\[Omega][{{X, Y}, {X, Y}}]^3 + 
        4*\[Omega][{{Y}, {X, X, Y}}]^3*(162 - 9*M - 22*M^2 + 3*M^3 + 
          40*(-3 + M)*\[Omega][{{X, Y}, {X, Y}}]) + 
        4*\[Omega][{{X}, {X, Y, Y}}]^3*(162 - 9*M - 22*M^2 + 3*M^3 + 
          8*(20 - 10*M + M^2)*\[Omega][{{Y}, {X, X, Y}}] + 
          40*(-3 + M)*\[Omega][{{X, Y}, {X, Y}}]) + 
        2*\[Omega][{{Y}, {X, X, Y}}]^2*(406 + 201*M - 77*M^2 - 5*M^3 + 
          2*M^4 + (-660 - 82*M + 90*M^2 + 4*M^3)*\[Omega][{{X, Y}, {X, Y}}] - 
          4*(-60 + 10*M + M^2)*\[Omega][{{X, Y}, {X, Y}}]^2) + 
        \[Omega][{{Y}, {X, X, Y}}]*(384 + 462*M + 55*M^2 - 37*M^3 + 6*M^4 + 
          2*(-484 - 358*M + 77*M^2 + 39*M^3 + 2*M^4)*
           \[Omega][{{X, Y}, {X, Y}}] - 4*(-174 - 71*M + 17*M^2 + M^3)*
           \[Omega][{{X, Y}, {X, Y}}]^2 - 160*\[Omega][{{X, Y}, {X, Y}}]^3) + 
        2*\[Omega][{{X}, {X, Y, Y}}]^2*(406 + 201*M - 77*M^2 - 5*M^3 + 
          2*M^4 + 24*(20 - 10*M + M^2)*\[Omega][{{Y}, {X, X, Y}}]^2 + 
          (-660 - 82*M + 90*M^2 + 4*M^3)*\[Omega][{{X, Y}, {X, Y}}] - 
          4*(-60 + 10*M + M^2)*\[Omega][{{X, Y}, {X, Y}}]^2 + 
          6*\[Omega][{{Y}, {X, X, Y}}]*(162 - 9*M - 22*M^2 + 3*M^3 + 
            40*(-3 + M)*\[Omega][{{X, Y}, {X, Y}}])) + 
        \[Omega][{{X}, {X, Y, Y}}]*(384 + 462*M + 55*M^2 - 37*M^3 + 6*M^4 + 
          32*(20 - 10*M + M^2)*\[Omega][{{Y}, {X, X, Y}}]^3 + 
          2*(-484 - 358*M + 77*M^2 + 39*M^3 + 2*M^4)*
           \[Omega][{{X, Y}, {X, Y}}] - 4*(-174 - 71*M + 17*M^2 + M^3)*
           \[Omega][{{X, Y}, {X, Y}}]^2 - 160*\[Omega][{{X, Y}, {X, Y}}]^3 + 
          12*\[Omega][{{Y}, {X, X, Y}}]^2*(162 - 9*M - 22*M^2 + 3*M^3 + 
            40*(-3 + M)*\[Omega][{{X, Y}, {X, Y}}]) + 
          4*\[Omega][{{Y}, {X, X, Y}}]*(406 + 201*M - 77*M^2 - 5*M^3 + 
            2*M^4 + (-660 - 82*M + 90*M^2 + 4*M^3)*\[Omega][{{X, Y}, {X, 
                Y}}] - 4*(-60 + 10*M + M^2)*\[Omega][{{X, Y}, {X, Y}}]^2))))/
     (E^(T*(1 + M + \[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}]))*(1 + M - 5*\[Lambda][{y}])*
      (2 + M - 4*\[Lambda][{y}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{Y}, {X, X, Y}}])*(1 + M + \[Lambda][{y}] + 
       2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
      (1 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
       2*\[Omega][{{X, Y}, {X, Y}}])*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
       2*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, Y}, {X, Y}}])*
      (1 + M - 2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}])*
      (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
       \[Omega][{{X, Y}, {X, Y}}])*(1 + 2*\[Omega][{{X, Y}, {X, Y}}])))}}
