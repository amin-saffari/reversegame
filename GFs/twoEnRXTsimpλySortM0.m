{\[Lambda][{y}]/(E^(2*T*\[Omega][{{X, X}, {Y, Y}}])*
    (-1 - 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
    (-\[Lambda][{y}] - 2*\[Omega][{{Y}, {X, X, Y}}] + 
     \[Omega][{{X, X}, {Y, Y}}])*(1 + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
  (2*E^(-T - 2*T*\[Omega][{{X}, {X, Y, Y}}] - T*\[Omega][{{X, X}, {Y, Y}}])*
    \[Lambda][{y}]*(1 + \[Omega][{{X, X}, {Y, Y}}]))/
   ((1 + 2*\[Omega][{{X}, {X, Y, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
    (3 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
    (-\[Lambda][{y}] - 2*\[Omega][{{Y}, {X, X, Y}}] + 
     \[Omega][{{X, X}, {Y, Y}}])*(1 + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
  (E^(-(T*\[Lambda][{y}]) - 2*T*\[Omega][{{Y}, {X, X, Y}}] - 
      T*\[Omega][{{X, X}, {Y, Y}}])*(2*\[Lambda][{y}] - 
     2*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Lambda][{y}]*
      \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}] + 
     \[Lambda][{y}]*\[Omega][{{X, X}, {Y, Y}}]))/
   ((\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
     \[Omega][{{X, X}, {Y, Y}}])*(-1 - 2*\[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{X, X}, {Y, Y}}])*(3 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
     \[Omega][{{X, X}, {Y, Y}}])*(1 + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
  ((-2*E^(-(T*\[Lambda][{y}]) - 2*T*\[Omega][{{Y}, {X, X, Y}}] - 
        T*\[Omega][{{X, X}, {Y, Y}}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{Y}, {X, X, Y}}]))/(-1 - 2*\[Omega][{{X}, {X, Y, Y}}] + 
      \[Omega][{{X, X}, {Y, Y}}]) - 
    (E^(-T - T*\[Lambda][{y}] - 2*T*\[Omega][{{X}, {X, Y, Y}}] - 
        2*T*\[Omega][{{Y}, {X, X, Y}}])*(5 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
       \[Omega][{{X, X}, {Y, Y}}]))/(1 + 2*\[Omega][{{X}, {X, Y, Y}}] - 
      \[Omega][{{X, X}, {Y, Y}}]))/((3 + \[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{Y}, {X, X, Y}}])*(1 + 2*\[Omega][{{Y}, {X, X, Y}}])*
    (3 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])) + 
  ((2*E^(-T - 2*T*\[Omega][{{X}, {X, Y, Y}}] - T*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{Y}, {X, X, Y}}]))/(\[Lambda][{y}] + 
      2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}]) - 
    (E^(-T - T*\[Lambda][{y}] - 2*T*\[Omega][{{X}, {X, Y, Y}}] - 
        2*T*\[Omega][{{Y}, {X, X, Y}}])*(5*\[Lambda][{y}] + 
       2*\[Lambda][{y}]*\[Omega][{{X}, {X, Y, Y}}] - 
       2*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Lambda][{y}]*
        \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))/
     (\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
      \[Omega][{{X, X}, {Y, Y}}]))/((1 + 2*\[Omega][{{X}, {X, Y, Y}}])*
    (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
    (3 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])) + 
  (E^(-T - T*\[Lambda][{y}] - 2*T*\[Omega][{{X}, {X, Y, Y}}] - 
      2*T*\[Omega][{{Y}, {X, X, Y}}])*
    (-2*(2 + 4*\[Omega][{{X}, {X, Y, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
      (2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
      (2 + 8*\[Omega][{{Y}, {X, X, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]^2 - 
       4*\[Omega][{{X, X}, {Y, Y}}] - 8*\[Omega][{{Y}, {X, X, Y}}]*
        \[Omega][{{X, X}, {Y, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]^2)*
      (12 + 16*\[Omega][{{X}, {X, Y, Y}}]^3 - 38*\[Omega][{{X, X}, {Y, Y}}] + 
       10*\[Omega][{{X, X}, {Y, Y}}]^2 + 32*\[Omega][{{X, X}, {Y, Y}}]^3 + 
       8*\[Omega][{{X, X}, {Y, Y}}]^4 + 16*\[Omega][{{Y}, {X, X, Y}}]^3*
        (1 + \[Omega][{{X, X}, {Y, Y}}]) + \[Omega][{{Y}, {X, X, Y}}]*
        (52 - 68*\[Omega][{{X, X}, {Y, Y}}] - 72*\[Omega][{{X, X}, {Y, Y}}]^
           2) + 2*\[Omega][{{Y}, {X, X, Y}}]^2*
        (32 + 12*\[Omega][{{X, X}, {Y, Y}}] - 12*\[Omega][{{X, X}, {Y, Y}}]^
           2) + 8*\[Omega][{{X}, {X, Y, Y}}]^2*
        (8 + 6*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
         \[Omega][{{X, X}, {Y, Y}}]^2) + \[Omega][{{X}, {X, Y, Y}}]*
        (52 + 128*\[Omega][{{Y}, {X, X, Y}}] + 48*\[Omega][{{Y}, {X, X, Y}}]^
           2 - 72*\[Omega][{{X, X}, {Y, Y}}] + 32*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, X}, {Y, Y}}] + 32*\[Omega][{{Y}, {X, X, Y}}]^2*
          \[Omega][{{X, X}, {Y, Y}}] - 56*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         16*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         16*\[Omega][{{X, X}, {Y, Y}}]^3)) - 8*\[Lambda][{y}]^6*
      (-960 + 1360*\[Omega][{{X, X}, {Y, Y}}] + 
       2240*\[Omega][{{X, X}, {Y, Y}}]^2 - 1840*\[Omega][{{X, X}, {Y, Y}}]^
         3 - 800*\[Omega][{{X, X}, {Y, Y}}]^4 - 
       1280*(\[Omega][{{Y}, {X, X, Y}}]^4 + \[Omega][{{Y}, {X, X, Y}}]^4*
          \[Omega][{{X, X}, {Y, Y}}]) + 1280*\[Omega][{{Y}, {X, X, Y}}]^3*
        (-5 - 4*\[Omega][{{X, X}, {Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}]^2) + 
       320*\[Omega][{{Y}, {X, X, Y}}]^2*
        (-29 - 11*\[Omega][{{X, X}, {Y, Y}}] + 21*\[Omega][{{X, X}, {Y, Y}}]^
           2 + \[Omega][{{X, X}, {Y, Y}}]^3) + 2*\[Omega][{{X}, {X, Y, Y}}]^2*
        (-320 - 960*\[Omega][{{Y}, {X, X, Y}}] - 
         640*\[Omega][{{Y}, {X, X, Y}}]^2 - 320*\[Omega][{{X, X}, {Y, Y}}] - 
         1280*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
         640*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
         480*\[Omega][{{X, X}, {Y, Y}}]^2 + 160*\[Omega][{{X, X}, {Y, Y}}]^
           3) + \[Omega][{{Y}, {X, X, Y}}]*(-5120 + 
         2080*\[Omega][{{X, X}, {Y, Y}}] + 9120*\[Omega][{{X, X}, {Y, Y}}]^
           2 - 320*\[Omega][{{X, X}, {Y, Y}}]^4) + \[Omega][{{X}, {X, Y, Y}}]*
        (-2240 - 800*\[Omega][{{X, X}, {Y, Y}}] + 
         3360*\[Omega][{{X, X}, {Y, Y}}]^2 - 320*\[Omega][{{X, X}, {Y, Y}}]^
           4 - 2560*\[Omega][{{Y}, {X, X, Y}}]^3*
          (1 + \[Omega][{{X, X}, {Y, Y}}]) + 4*\[Omega][{{Y}, {X, X, Y}}]^2*
          (-2080 - 1920*\[Omega][{{X, X}, {Y, Y}}] + 
           320*\[Omega][{{X, X}, {Y, Y}}]^2) + 4*\[Omega][{{Y}, {X, X, Y}}]*
          (-2000 - 1600*\[Omega][{{X, X}, {Y, Y}}] + 
           1120*\[Omega][{{X, X}, {Y, Y}}]^2 + 160*\[Omega][{{X, X}, {Y, Y}}]^
             3))) - 4*\[Lambda][{y}]^5*
      (1344 - 944*\[Omega][{{X, X}, {Y, Y}}] - 
       3776*\[Omega][{{X, X}, {Y, Y}}]^2 - 1584*\[Omega][{{X, X}, {Y, Y}}]^
         3 + 3680*\[Omega][{{X, X}, {Y, Y}}]^4 + 
       1280*\[Omega][{{X, X}, {Y, Y}}]^5 - 
       2560*(\[Omega][{{Y}, {X, X, Y}}]^5 + \[Omega][{{Y}, {X, X, Y}}]^5*
          \[Omega][{{X, X}, {Y, Y}}]) + 32*\[Omega][{{Y}, {X, X, Y}}]^4*
        (-264 - 104*\[Omega][{{X, X}, {Y, Y}}] + 
         160*\[Omega][{{X, X}, {Y, Y}}]^2) + 4*\[Omega][{{Y}, {X, X, Y}}]^3*
        (160 + 4192*\[Omega][{{X, X}, {Y, Y}}] + 
         4512*\[Omega][{{X, X}, {Y, Y}}]^2 - 480*\[Omega][{{X, X}, {Y, Y}}]^
           3) + 4*\[Omega][{{X}, {X, Y, Y}}]^3*
        (-320 - 960*\[Omega][{{Y}, {X, X, Y}}] - 
         640*\[Omega][{{Y}, {X, X, Y}}]^2 - 320*\[Omega][{{X, X}, {Y, Y}}] - 
         1280*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
         640*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
         480*\[Omega][{{X, X}, {Y, Y}}]^2 + 160*\[Omega][{{X, X}, {Y, Y}}]^
           3) + 2*\[Omega][{{Y}, {X, X, Y}}]^2*
        (5536 + 8064*\[Omega][{{X, X}, {Y, Y}}] + 
         2176*\[Omega][{{X, X}, {Y, Y}}]^2 - 6624*\[Omega][{{X, X}, {Y, Y}}]^
           3 - 640*\[Omega][{{X, X}, {Y, Y}}]^4) + \[Omega][{{Y}, {X, X, Y}}]*
        (7168 + 1728*\[Omega][{{X, X}, {Y, Y}}] - 
         9888*\[Omega][{{X, X}, {Y, Y}}]^2 - 15200*\[Omega][{{X, X}, {Y, Y}}]^
           3 - 192*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         640*\[Omega][{{X, X}, {Y, Y}}]^5) + 2*\[Omega][{{X}, {X, Y, Y}}]^2*
        (-1792 - 32*\[Omega][{{X, X}, {Y, Y}}] + 
         3648*\[Omega][{{X, X}, {Y, Y}}]^2 - 1184*\[Omega][{{X, X}, {Y, Y}}]^
           3 - 640*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         3840*\[Omega][{{Y}, {X, X, Y}}]^3*(1 + \[Omega][{{X, X}, {Y, Y}}]) + 
         8*\[Omega][{{Y}, {X, X, Y}}]^2*(-1008 - 
           1008*\[Omega][{{X, X}, {Y, Y}}] + 320*\[Omega][{{X, X}, {Y, Y}}]^
             2) + 2*\[Omega][{{Y}, {X, X, Y}}]*(-3328 - 
           2304*\[Omega][{{X, X}, {Y, Y}}] + 4000*\[Omega][{{X, X}, {Y, Y}}]^
             2 + 480*\[Omega][{{X, X}, {Y, Y}}]^3)) + 
       \[Omega][{{X}, {X, Y, Y}}]*(1216 + 6080*\[Omega][{{X, X}, {Y, Y}}] + 
         2656*\[Omega][{{X, X}, {Y, Y}}]^2 - 9760*\[Omega][{{X, X}, {Y, Y}}]^
           3 - 832*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         640*\[Omega][{{X, X}, {Y, Y}}]^5 - 7680*\[Omega][{{Y}, {X, X, Y}}]^4*
          (1 + \[Omega][{{X, X}, {Y, Y}}]) + 8*\[Omega][{{Y}, {X, X, Y}}]^3*
          (-2592 - 1792*\[Omega][{{X, X}, {Y, Y}}] + 
           1280*\[Omega][{{X, X}, {Y, Y}}]^2) + 4*\[Omega][{{Y}, {X, X, Y}}]^
           2*(-2848 + 1888*\[Omega][{{X, X}, {Y, Y}}] + 
           8672*\[Omega][{{X, X}, {Y, Y}}]^2 - 160*\[Omega][{{X, X}, {Y, Y}}]^
             3) + 4*\[Omega][{{Y}, {X, X, Y}}]*
          (240 + 4800*\[Omega][{{X, X}, {Y, Y}}] + 
           7232*\[Omega][{{X, X}, {Y, Y}}]^2 - 
           2624*\[Omega][{{X, X}, {Y, Y}}]^3 - 640*\[Omega][{{X, X}, {Y, Y}}]^
             4))) + \[Lambda][{y}]*(2 + 4*\[Omega][{{Y}, {X, X, Y}}] - 
       2*\[Omega][{{X, X}, {Y, Y}}])*(96 + 1664*\[Omega][{{Y}, {X, X, Y}}] + 
       7840*\[Omega][{{Y}, {X, X, Y}}]^2 + 15104*\[Omega][{{Y}, {X, X, Y}}]^
         3 + 11904*\[Omega][{{Y}, {X, X, Y}}]^4 + 
       2560*\[Omega][{{Y}, {X, X, Y}}]^5 - 904*\[Omega][{{X, X}, {Y, Y}}] - 
       9920*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
       29728*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
       30272*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
       6272*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
       512*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] + 
       2760*\[Omega][{{X, X}, {Y, Y}}]^2 + 17184*\[Omega][{{Y}, {X, X, Y}}]*
        \[Omega][{{X, X}, {Y, Y}}]^2 + 22144*\[Omega][{{Y}, {X, X, Y}}]^2*
        \[Omega][{{X, X}, {Y, Y}}]^2 - 4480*\[Omega][{{Y}, {X, X, Y}}]^3*
        \[Omega][{{X, X}, {Y, Y}}]^2 - 9088*\[Omega][{{Y}, {X, X, Y}}]^4*
        \[Omega][{{X, X}, {Y, Y}}]^2 - 2048*\[Omega][{{Y}, {X, X, Y}}]^5*
        \[Omega][{{X, X}, {Y, Y}}]^2 - 2840*\[Omega][{{X, X}, {Y, Y}}]^3 - 
       3856*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
       16320*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
       19136*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
       4992*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
       472*\[Omega][{{X, X}, {Y, Y}}]^4 - 10080*\[Omega][{{Y}, {X, X, Y}}]*
        \[Omega][{{X, X}, {Y, Y}}]^4 - 14048*\[Omega][{{Y}, {X, X, Y}}]^2*
        \[Omega][{{X, X}, {Y, Y}}]^4 - 3328*\[Omega][{{Y}, {X, X, Y}}]^3*
        \[Omega][{{X, X}, {Y, Y}}]^4 + 1760*\[Omega][{{X, X}, {Y, Y}}]^5 + 
       3664*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
       352*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 - 
       176*\[Omega][{{X, X}, {Y, Y}}]^6 + 960*\[Omega][{{Y}, {X, X, Y}}]*
        \[Omega][{{X, X}, {Y, Y}}]^6 - 224*\[Omega][{{X, X}, {Y, Y}}]^7 + 
       32*\[Omega][{{X}, {X, Y, Y}}]^4*(8 + 96*\[Omega][{{Y}, {X, X, Y}}] + 
         128*\[Omega][{{Y}, {X, X, Y}}]^2 - 28*\[Omega][{{X, X}, {Y, Y}}] - 
         80*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
         16*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
         16*\[Omega][{{X, X}, {Y, Y}}]^2 - 4*\[Omega][{{X, X}, {Y, Y}}]^3) + 
       8*\[Omega][{{X}, {X, Y, Y}}]^3*(144 - 632*\[Omega][{{X, X}, {Y, Y}}] + 
         248*\[Omega][{{X, X}, {Y, Y}}]^2 - 16*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         96*\[Omega][{{X, X}, {Y, Y}}]^4 + 32*\[Omega][{{Y}, {X, X, Y}}]^3*
          (48 + 20*\[Omega][{{X, X}, {Y, Y}}]) + 
         4*\[Omega][{{Y}, {X, X, Y}}]^2*(872 - 
           144*\[Omega][{{X, X}, {Y, Y}}] - 96*\[Omega][{{X, X}, {Y, Y}}]^
             2) + 2*\[Omega][{{Y}, {X, X, Y}}]*
          (920 - 1072*\[Omega][{{X, X}, {Y, Y}}] - 
           64*\[Omega][{{X, X}, {Y, Y}}]^2 - 80*\[Omega][{{X, X}, {Y, Y}}]^
             3)) + 4*\[Omega][{{X}, {X, Y, Y}}]^2*
        (336 - 2000*\[Omega][{{X, X}, {Y, Y}}] + 
         2320*\[Omega][{{X, X}, {Y, Y}}]^2 + 384*\[Omega][{{X, X}, {Y, Y}}]^
           3 - 328*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         344*\[Omega][{{X, X}, {Y, Y}}]^5 + 32*\[Omega][{{Y}, {X, X, Y}}]^4*
          (96 + 68*\[Omega][{{X, X}, {Y, Y}}]) + 
         16*\[Omega][{{Y}, {X, X, Y}}]^3*(768 + 
           36*\[Omega][{{X, X}, {Y, Y}}] - 240*\[Omega][{{X, X}, {Y, Y}}]^
             2) + 4*\[Omega][{{Y}, {X, X, Y}}]^2*
          (3312 - 3536*\[Omega][{{X, X}, {Y, Y}}] - 
           1848*\[Omega][{{X, X}, {Y, Y}}]^2 + 208*\[Omega][{{X, X}, {Y, Y}}]^
             3) + 2*\[Omega][{{Y}, {X, X, Y}}]*(2384 - 
           6064*\[Omega][{{X, X}, {Y, Y}}] + 1328*\[Omega][{{X, X}, {Y, Y}}]^
             2 + 1912*\[Omega][{{X, X}, {Y, Y}}]^3 + 
           480*\[Omega][{{X, X}, {Y, Y}}]^4)) + 2*\[Omega][{{X}, {X, Y, Y}}]*
        (304 - 2328*\[Omega][{{X, X}, {Y, Y}}] + 
         4864*\[Omega][{{X, X}, {Y, Y}}]^2 - 1888*\[Omega][{{X, X}, {Y, Y}}]^
           3 - 2176*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         552*\[Omega][{{X, X}, {Y, Y}}]^5 + 480*\[Omega][{{X, X}, {Y, Y}}]^
           6 + 2048*(\[Omega][{{Y}, {X, X, Y}}]^5 + 
           \[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]) + 
         16*\[Omega][{{Y}, {X, X, Y}}]^4*(840 + 
           152*\[Omega][{{X, X}, {Y, Y}}] - 448*\[Omega][{{X, X}, {Y, Y}}]^
             2) + 8*\[Omega][{{Y}, {X, X, Y}}]^3*
          (3152 - 2864*\[Omega][{{X, X}, {Y, Y}}] - 
           2528*\[Omega][{{X, X}, {Y, Y}}]^2 + 816*\[Omega][{{X, X}, {Y, Y}}]^
             3) + 4*\[Omega][{{Y}, {X, X, Y}}]^2*
          (4464 - 10376*\[Omega][{{X, X}, {Y, Y}}] + 
           856*\[Omega][{{X, X}, {Y, Y}}]^2 + 5600*\[Omega][{{X, X}, {Y, Y}}]^
             3 - 32*\[Omega][{{X, X}, {Y, Y}}]^4) + 
         2*\[Omega][{{Y}, {X, X, Y}}]*(2392 - 
           10032*\[Omega][{{X, X}, {Y, Y}}] + 9144*\[Omega][{{X, X}, {Y, Y}}]^
             2 + 3616*\[Omega][{{X, X}, {Y, Y}}]^3 - 
           3808*\[Omega][{{X, X}, {Y, Y}}]^4 - 880*\[Omega][{{X, X}, {Y, Y}}]^
             5))) - 2*\[Lambda][{y}]^4*
      (2496 + 22912*\[Omega][{{Y}, {X, X, Y}}] + 
       89536*\[Omega][{{Y}, {X, X, Y}}]^2 + 178688*\[Omega][{{Y}, {X, X, Y}}]^
         3 + 176384*\[Omega][{{Y}, {X, X, Y}}]^4 + 
       72704*\[Omega][{{Y}, {X, X, Y}}]^5 + 10240*\[Omega][{{Y}, {X, X, Y}}]^
         6 - 14480*\[Omega][{{X, X}, {Y, Y}}] - 
       95904*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
       235968*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
       219008*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
       9984*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
       44544*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] + 
       10240*\[Omega][{{Y}, {X, X, Y}}]^6*\[Omega][{{X, X}, {Y, Y}}] + 
       17312*\[Omega][{{X, X}, {Y, Y}}]^2 + 46336*\[Omega][{{Y}, {X, X, Y}}]*
        \[Omega][{{X, X}, {Y, Y}}]^2 - 58816*\[Omega][{{Y}, {X, X, Y}}]^2*
        \[Omega][{{X, X}, {Y, Y}}]^2 - 257664*\[Omega][{{Y}, {X, X, Y}}]^3*
        \[Omega][{{X, X}, {Y, Y}}]^2 - 162048*\[Omega][{{Y}, {X, X, Y}}]^4*
        \[Omega][{{X, X}, {Y, Y}}]^2 - 28160*\[Omega][{{Y}, {X, X, Y}}]^5*
        \[Omega][{{X, X}, {Y, Y}}]^2 + 19328*\[Omega][{{X, X}, {Y, Y}}]^3 + 
       143200*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
       287808*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
       151168*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
       24320*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
       36224*\[Omega][{{X, X}, {Y, Y}}]^4 - 95552*\[Omega][{{Y}, {X, X, Y}}]*
        \[Omega][{{X, X}, {Y, Y}}]^4 - 31808*\[Omega][{{Y}, {X, X, Y}}]^2*
        \[Omega][{{X, X}, {Y, Y}}]^4 - 1920*\[Omega][{{Y}, {X, X, Y}}]^3*
        \[Omega][{{X, X}, {Y, Y}}]^4 + 5328*\[Omega][{{X, X}, {Y, Y}}]^5 - 
       17856*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
       6720*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
       6240*\[Omega][{{X, X}, {Y, Y}}]^6 + 2240*\[Omega][{{Y}, {X, X, Y}}]*
        \[Omega][{{X, X}, {Y, Y}}]^6 - 16*\[Omega][{{X}, {X, Y, Y}}]^4*
        (-320 - 960*\[Omega][{{Y}, {X, X, Y}}] - 
         640*\[Omega][{{Y}, {X, X, Y}}]^2 - 320*\[Omega][{{X, X}, {Y, Y}}] - 
         1280*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
         640*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
         480*\[Omega][{{X, X}, {Y, Y}}]^2 + 160*\[Omega][{{X, X}, {Y, Y}}]^
           3) - 4*\[Omega][{{X}, {X, Y, Y}}]^3*
        (-6528 - 1088*\[Omega][{{X, X}, {Y, Y}}] + 
         11712*\[Omega][{{X, X}, {Y, Y}}]^2 - 2336*\[Omega][{{X, X}, {Y, Y}}]^
           3 - 1760*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         8960*\[Omega][{{Y}, {X, X, Y}}]^3*(1 + \[Omega][{{X, X}, {Y, Y}}]) + 
         16*\[Omega][{{Y}, {X, X, Y}}]^2*(-1736 - 
           1416*\[Omega][{{X, X}, {Y, Y}}] + 440*\[Omega][{{X, X}, {Y, Y}}]^
             2) + 2*\[Omega][{{Y}, {X, X, Y}}]*(-12352 - 
           7456*\[Omega][{{X, X}, {Y, Y}}] + 10400*\[Omega][{{X, X}, {Y, Y}}]^
             2 + 1120*\[Omega][{{X, X}, {Y, Y}}]^3)) + 
       2*\[Omega][{{X}, {X, Y, Y}}]^2*(19008 - 
         21056*\[Omega][{{X, X}, {Y, Y}}] - 37696*\[Omega][{{X, X}, {Y, Y}}]^
           2 + 38848*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         4256*\[Omega][{{X, X}, {Y, Y}}]^4 - 3360*\[Omega][{{X, X}, {Y, Y}}]^
           5 + 25600*\[Omega][{{Y}, {X, X, Y}}]^4*
          (1 + \[Omega][{{X, X}, {Y, Y}}]) + 16*\[Omega][{{Y}, {X, X, Y}}]^3*
          (7216 + 4816*\[Omega][{{X, X}, {Y, Y}}] - 
           2480*\[Omega][{{X, X}, {Y, Y}}]^2) + 8*\[Omega][{{Y}, {X, X, Y}}]^
           2*(20736 + 4656*\[Omega][{{X, X}, {Y, Y}}] - 
           17808*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           880*\[Omega][{{X, X}, {Y, Y}}]^3) + 2*\[Omega][{{Y}, {X, X, Y}}]*
          (47648 - 18176*\[Omega][{{X, X}, {Y, Y}}] - 
           74880*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           20128*\[Omega][{{X, X}, {Y, Y}}]^3 + 
           4960*\[Omega][{{X, X}, {Y, Y}}]^4)) + \[Omega][{{X}, {X, Y, Y}}]*
        (18112 - 56224*\[Omega][{{X, X}, {Y, Y}}] - 
         6080*\[Omega][{{X, X}, {Y, Y}}]^2 + 
         100704*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         42176*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         16576*\[Omega][{{X, X}, {Y, Y}}]^5 + 2240*\[Omega][{{X, X}, {Y, Y}}]^
           6 + 35840*\[Omega][{{Y}, {X, X, Y}}]^5*
          (1 + \[Omega][{{X, X}, {Y, Y}}]) + 64*\[Omega][{{Y}, {X, X, Y}}]^4*
          (3248 + 2008*\[Omega][{{X, X}, {Y, Y}}] - 
           1240*\[Omega][{{X, X}, {Y, Y}}]^2) + 8*\[Omega][{{Y}, {X, X, Y}}]^
           3*(50848 + 2368*\[Omega][{{X, X}, {Y, Y}}] - 
           44832*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           5600*\[Omega][{{X, X}, {Y, Y}}]^3) + 4*\[Omega][{{Y}, {X, X, Y}}]^
           2*(82912 - 70688*\[Omega][{{X, X}, {Y, Y}}] - 
           122080*\[Omega][{{X, X}, {Y, Y}}]^2 + 
           54304*\[Omega][{{X, X}, {Y, Y}}]^3 + 
           2720*\[Omega][{{X, X}, {Y, Y}}]^4) - 4*\[Omega][{{Y}, {X, X, Y}}]*
          (-31024 + 59984*\[Omega][{{X, X}, {Y, Y}}] + 
           44592*\[Omega][{{X, X}, {Y, Y}}]^2 - 
           79504*\[Omega][{{X, X}, {Y, Y}}]^3 + 
           384*\[Omega][{{X, X}, {Y, Y}}]^4 + 3360*\[Omega][{{X, X}, {Y, Y}}]^
             5))) - \[Lambda][{y}]^2*
      (1920 + 23488*\[Omega][{{Y}, {X, X, Y}}] + 
       112064*\[Omega][{{Y}, {X, X, Y}}]^2 + 
       265216*\[Omega][{{Y}, {X, X, Y}}]^3 + 
       322560*\[Omega][{{Y}, {X, X, Y}}]^4 + 
       183296*\[Omega][{{Y}, {X, X, Y}}]^5 + 33792*\[Omega][{{Y}, {X, X, Y}}]^
         6 - 12992*\[Omega][{{X, X}, {Y, Y}}] - 
       128160*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
       453120*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
       699648*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
       426496*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
       36352*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] + 
       16384*\[Omega][{{Y}, {X, X, Y}}]^6*\[Omega][{{X, X}, {Y, Y}}] + 
       31312*\[Omega][{{X, X}, {Y, Y}}]^2 + 218976*\[Omega][{{Y}, {X, X, Y}}]*
        \[Omega][{{X, X}, {Y, Y}}]^2 + 445120*\[Omega][{{Y}, {X, X, Y}}]^2*
        \[Omega][{{X, X}, {Y, Y}}]^2 + 176256*\[Omega][{{Y}, {X, X, Y}}]^3*
        \[Omega][{{X, X}, {Y, Y}}]^2 - 235776*\[Omega][{{Y}, {X, X, Y}}]^4*
        \[Omega][{{X, X}, {Y, Y}}]^2 - 136192*\[Omega][{{Y}, {X, X, Y}}]^5*
        \[Omega][{{X, X}, {Y, Y}}]^2 - 17408*\[Omega][{{Y}, {X, X, Y}}]^6*
        \[Omega][{{X, X}, {Y, Y}}]^2 - 25632*\[Omega][{{X, X}, {Y, Y}}]^3 - 
       60160*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
       188928*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
       507776*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
       278528*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
       48640*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^3 - 
       12000*\[Omega][{{X, X}, {Y, Y}}]^4 - 152064*\[Omega][{{Y}, {X, X, Y}}]*
        \[Omega][{{X, X}, {Y, Y}}]^4 - 359680*\[Omega][{{Y}, {X, X, Y}}]^2*
        \[Omega][{{X, X}, {Y, Y}}]^4 - 219008*\[Omega][{{Y}, {X, X, Y}}]^3*
        \[Omega][{{X, X}, {Y, Y}}]^4 - 43264*\[Omega][{{Y}, {X, X, Y}}]^4*
        \[Omega][{{X, X}, {Y, Y}}]^4 + 26432*\[Omega][{{X, X}, {Y, Y}}]^5 + 
       96032*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
       58368*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
       6784*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^5 - 
       6512*\[Omega][{{X, X}, {Y, Y}}]^6 + 5088*\[Omega][{{Y}, {X, X, Y}}]*
        \[Omega][{{X, X}, {Y, Y}}]^6 + 9344*\[Omega][{{Y}, {X, X, Y}}]^2*
        \[Omega][{{X, X}, {Y, Y}}]^6 - 3168*\[Omega][{{X, X}, {Y, Y}}]^7 - 
       4736*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^7 + 
       640*\[Omega][{{X, X}, {Y, Y}}]^8 - 8*\[Omega][{{X}, {X, Y, Y}}]^4*
        (-576 + 32*\[Omega][{{Y}, {X, X, Y}}]^3*
          (-136 - 56*\[Omega][{{X, X}, {Y, Y}}]) + 
         640*\[Omega][{{X, X}, {Y, Y}}] + 224*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         64*\[Omega][{{X, X}, {Y, Y}}]^3 - 224*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         16*\[Omega][{{Y}, {X, X, Y}}]^2*(-512 + 
           56*\[Omega][{{X, X}, {Y, Y}}]^2) + 2*\[Omega][{{Y}, {X, X, Y}}]*
          (-2080 + 1248*\[Omega][{{X, X}, {Y, Y}}] + 
           608*\[Omega][{{X, X}, {Y, Y}}]^2 + 224*\[Omega][{{X, X}, {Y, Y}}]^
             3)) - 4*\[Omega][{{X}, {X, Y, Y}}]^3*
        (-5248 + 64*\[Omega][{{Y}, {X, X, Y}}]^4*
          (-408 - 248*\[Omega][{{X, X}, {Y, Y}}]) + 
         10368*\[Omega][{{X, X}, {Y, Y}}] + 32*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         5600*\[Omega][{{X, X}, {Y, Y}}]^3 - 992*\[Omega][{{X, X}, {Y, Y}}]^
           4 + 1440*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         32*\[Omega][{{Y}, {X, X, Y}}]^3*(-2824 - 
           448*\[Omega][{{X, X}, {Y, Y}}] + 608*\[Omega][{{X, X}, {Y, Y}}]^
             2) + 4*\[Omega][{{Y}, {X, X, Y}}]^2*(-25312 + 
           9952*\[Omega][{{X, X}, {Y, Y}}] + 9536*\[Omega][{{X, X}, {Y, Y}}]^
             2 - 448*\[Omega][{{X, X}, {Y, Y}}]^3) + 
         2*\[Omega][{{Y}, {X, X, Y}}]*(-20896 + 
           23488*\[Omega][{{X, X}, {Y, Y}}] + 8096*\[Omega][{{X, X}, {Y, Y}}]^
             2 - 5120*\[Omega][{{X, X}, {Y, Y}}]^3 - 
           2432*\[Omega][{{X, X}, {Y, Y}}]^4)) + 
       2*\[Omega][{{X}, {X, Y, Y}}]^2*(12608 - 128*\[Omega][{{Y}, {X, X, Y}}]^
           5*(-408 - 328*\[Omega][{{X, X}, {Y, Y}}]) - 
         43520*\[Omega][{{X, X}, {Y, Y}}] + 30976*\[Omega][{{X, X}, {Y, Y}}]^
           2 + 24416*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         21760*\[Omega][{{X, X}, {Y, Y}}]^4 - 6112*\[Omega][{{X, X}, {Y, Y}}]^
           5 + 3392*\[Omega][{{X, X}, {Y, Y}}]^6 - 
         64*\[Omega][{{Y}, {X, X, Y}}]^4*(-4376 - 
           1128*\[Omega][{{X, X}, {Y, Y}}] + 1504*\[Omega][{{X, X}, {Y, Y}}]^
             2) - 8*\[Omega][{{Y}, {X, X, Y}}]^3*(-61120 + 
           28384*\[Omega][{{X, X}, {Y, Y}}] + 
           36992*\[Omega][{{X, X}, {Y, Y}}]^2 - 
           6784*\[Omega][{{X, X}, {Y, Y}}]^3) - 4*\[Omega][{{Y}, {X, X, Y}}]^
           2*(-91040 + 123168*\[Omega][{{X, X}, {Y, Y}}] + 
           43552*\[Omega][{{X, X}, {Y, Y}}]^2 - 
           47808*\[Omega][{{X, X}, {Y, Y}}]^3 - 
           2624*\[Omega][{{X, X}, {Y, Y}}]^4) - 2*\[Omega][{{Y}, {X, X, Y}}]*
          (-58400 + 137280*\[Omega][{{X, X}, {Y, Y}}] - 
           27200*\[Omega][{{X, X}, {Y, Y}}]^2 - 
           76192*\[Omega][{{X, X}, {Y, Y}}]^3 + 
           10848*\[Omega][{{X, X}, {Y, Y}}]^4 + 
           8096*\[Omega][{{X, X}, {Y, Y}}]^5)) + \[Omega][{{X}, {X, Y, Y}}]*
        (11776 - 59776*\[Omega][{{X, X}, {Y, Y}}] + 
         92128*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         11904*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         69248*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         29568*\[Omega][{{X, X}, {Y, Y}}]^5 + 
         10912*\[Omega][{{X, X}, {Y, Y}}]^6 - 3456*\[Omega][{{X, X}, {Y, Y}}]^
           7 + 34816*(\[Omega][{{Y}, {X, X, Y}}]^6 + 
           \[Omega][{{Y}, {X, X, Y}}]^6*\[Omega][{{X, X}, {Y, Y}}]) - 
         128*\[Omega][{{Y}, {X, X, Y}}]^5*(-2328 - 
           808*\[Omega][{{X, X}, {Y, Y}}] + 1088*\[Omega][{{X, X}, {Y, Y}}]^
             2) - 16*\[Omega][{{Y}, {X, X, Y}}]^4*(-49344 + 
           21856*\[Omega][{{X, X}, {Y, Y}}] + 
           37408*\[Omega][{{X, X}, {Y, Y}}]^2 - 
           10432*\[Omega][{{X, X}, {Y, Y}}]^3) - 
         8*\[Omega][{{Y}, {X, X, Y}}]^3*(-112768 + 
           162080*\[Omega][{{X, X}, {Y, Y}}] + 
           61344*\[Omega][{{X, X}, {Y, Y}}]^2 - 
           90560*\[Omega][{{X, X}, {Y, Y}}]^3 + 
           6272*\[Omega][{{X, X}, {Y, Y}}]^4) - 4*\[Omega][{{Y}, {X, X, Y}}]^
           2*(-123488 + 320000*\[Omega][{{X, X}, {Y, Y}}] - 
           90624*\[Omega][{{X, X}, {Y, Y}}]^2 - 
           202464*\[Omega][{{X, X}, {Y, Y}}]^3 + 
           71584*\[Omega][{{X, X}, {Y, Y}}]^4 + 
           7520*\[Omega][{{X, X}, {Y, Y}}]^5) - 2*\[Omega][{{Y}, {X, X, Y}}]*
          (-63072 + 241920*\[Omega][{{X, X}, {Y, Y}}] - 
           219488*\[Omega][{{X, X}, {Y, Y}}]^2 - 
           101088*\[Omega][{{X, X}, {Y, Y}}]^3 + 
           156992*\[Omega][{{X, X}, {Y, Y}}]^4 - 
           3744*\[Omega][{{X, X}, {Y, Y}}]^5 - 
           10624*\[Omega][{{X, X}, {Y, Y}}]^6))) - 
     2*\[Lambda][{y}]^3*(-2976 - 29312*\[Omega][{{Y}, {X, X, Y}}] - 
       119456*\[Omega][{{Y}, {X, X, Y}}]^2 - 
       249024*\[Omega][{{Y}, {X, X, Y}}]^3 - 
       268160*\[Omega][{{Y}, {X, X, Y}}]^4 - 
       133376*\[Omega][{{Y}, {X, X, Y}}]^5 - 22528*\[Omega][{{Y}, {X, X, Y}}]^
         6 + 16408*\[Omega][{{X, X}, {Y, Y}}] + 
       128576*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
       375616*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
       466944*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
       188544*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
       26624*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] - 
       17408*\[Omega][{{Y}, {X, X, Y}}]^6*\[Omega][{{X, X}, {Y, Y}}] - 
       26912*\[Omega][{{X, X}, {Y, Y}}]^2 - 136608*\[Omega][{{Y}, {X, X, Y}}]*
        \[Omega][{{X, X}, {Y, Y}}]^2 - 153600*\[Omega][{{Y}, {X, X, Y}}]^2*
        \[Omega][{{X, X}, {Y, Y}}]^2 + 145408*\[Omega][{{Y}, {X, X, Y}}]^3*
        \[Omega][{{X, X}, {Y, Y}}]^2 + 263040*\[Omega][{{Y}, {X, X, Y}}]^4*
        \[Omega][{{X, X}, {Y, Y}}]^2 + 83712*\[Omega][{{Y}, {X, X, Y}}]^5*
        \[Omega][{{X, X}, {Y, Y}}]^2 + 5120*\[Omega][{{Y}, {X, X, Y}}]^6*
        \[Omega][{{X, X}, {Y, Y}}]^2 + 400*\[Omega][{{X, X}, {Y, Y}}]^3 - 
       81024*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
       347232*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
       389120*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
       128384*\[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
       12800*\[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^3 + 
       34176*\[Omega][{{X, X}, {Y, Y}}]^4 + 168096*\[Omega][{{Y}, {X, X, Y}}]*
        \[Omega][{{X, X}, {Y, Y}}]^4 + 210464*\[Omega][{{Y}, {X, X, Y}}]^2*
        \[Omega][{{X, X}, {Y, Y}}]^4 + 67904*\[Omega][{{Y}, {X, X, Y}}]^3*
        \[Omega][{{X, X}, {Y, Y}}]^4 + 8960*\[Omega][{{Y}, {X, X, Y}}]^4*
        \[Omega][{{X, X}, {Y, Y}}]^4 - 21448*\[Omega][{{X, X}, {Y, Y}}]^5 - 
       32960*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
       5024*\[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
       640*\[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^5 - 
       2368*\[Omega][{{X, X}, {Y, Y}}]^6 - 13568*\[Omega][{{Y}, {X, X, Y}}]*
        \[Omega][{{X, X}, {Y, Y}}]^6 - 2560*\[Omega][{{Y}, {X, X, Y}}]^2*
        \[Omega][{{X, X}, {Y, Y}}]^6 + 2720*\[Omega][{{X, X}, {Y, Y}}]^7 + 
       640*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^7 + 
       8*\[Omega][{{X}, {X, Y, Y}}]^4*(-768 - 
         128*\[Omega][{{X, X}, {Y, Y}}] + 1152*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         96*\[Omega][{{X, X}, {Y, Y}}]^3 - 160*\[Omega][{{X, X}, {Y, Y}}]^4 - 
         1280*\[Omega][{{Y}, {X, X, Y}}]^3*(1 + \[Omega][{{X, X}, {Y, Y}}]) + 
         8*\[Omega][{{Y}, {X, X, Y}}]^2*(-592 - 
           432*\[Omega][{{X, X}, {Y, Y}}] + 80*\[Omega][{{X, X}, {Y, Y}}]^
             2) + 2*\[Omega][{{Y}, {X, X, Y}}]*(-1792 - 
           736*\[Omega][{{X, X}, {Y, Y}}] + 1120*\[Omega][{{X, X}, {Y, Y}}]^
             2 + 160*\[Omega][{{X, X}, {Y, Y}}]^3)) + 
       2*\[Omega][{{X}, {X, Y, Y}}]^3*(-14400 + 
         9856*\[Omega][{{X, X}, {Y, Y}}] + 20768*\[Omega][{{X, X}, {Y, Y}}]^
           2 - 15360*\[Omega][{{X, X}, {Y, Y}}]^3 - 
         2464*\[Omega][{{X, X}, {Y, Y}}]^4 + 1600*\[Omega][{{X, X}, {Y, Y}}]^
           5 - 15360*\[Omega][{{Y}, {X, X, Y}}]^4*
          (1 + \[Omega][{{X, X}, {Y, Y}}]) + 64*\[Omega][{{Y}, {X, X, Y}}]^3*
          (-1316 - 876*\[Omega][{{X, X}, {Y, Y}}] + 
           320*\[Omega][{{X, X}, {Y, Y}}]^2) + 8*\[Omega][{{Y}, {X, X, Y}}]^2*
          (-17056 - 4640*\[Omega][{{X, X}, {Y, Y}}] + 
           9392*\[Omega][{{X, X}, {Y, Y}}]^2 - 320*\[Omega][{{X, X}, {Y, Y}}]^
             3) + 2*\[Omega][{{Y}, {X, X, Y}}]*(-38944 + 
           8160*\[Omega][{{X, X}, {Y, Y}}] + 38240*\[Omega][{{X, X}, {Y, Y}}]^
             2 - 8352*\[Omega][{{X, X}, {Y, Y}}]^3 - 
           2560*\[Omega][{{X, X}, {Y, Y}}]^4)) + \[Omega][{{X}, {X, Y, Y}}]^2*
        (-36736 + 76448*\[Omega][{{X, X}, {Y, Y}}] + 
         23584*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         103040*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         28000*\[Omega][{{X, X}, {Y, Y}}]^4 + 
         14304*\[Omega][{{X, X}, {Y, Y}}]^5 - 2560*\[Omega][{{X, X}, {Y, Y}}]^
           6 - 30720*\[Omega][{{Y}, {X, X, Y}}]^5*
          (1 + \[Omega][{{X, X}, {Y, Y}}]) + 64*\[Omega][{{Y}, {X, X, Y}}]^4*
          (-3840 - 2400*\[Omega][{{X, X}, {Y, Y}}] + 
           1120*\[Omega][{{X, X}, {Y, Y}}]^2) + 16*\[Omega][{{Y}, {X, X, Y}}]^
           3*(-37248 - 4928*\[Omega][{{X, X}, {Y, Y}}] + 
           23472*\[Omega][{{X, X}, {Y, Y}}]^2 - 
           2560*\[Omega][{{X, X}, {Y, Y}}]^3) + 8*\[Omega][{{Y}, {X, X, Y}}]^
           2*(-72928 + 40624*\[Omega][{{X, X}, {Y, Y}}] + 
           72752*\[Omega][{{X, X}, {Y, Y}}]^2 - 
           23472*\[Omega][{{X, X}, {Y, Y}}]^3 - 
           960*\[Omega][{{X, X}, {Y, Y}}]^4) + 2*\[Omega][{{Y}, {X, X, Y}}]*
          (-122496 + 161280*\[Omega][{{X, X}, {Y, Y}}] + 
           124512*\[Omega][{{X, X}, {Y, Y}}]^2 - 
           148288*\[Omega][{{X, X}, {Y, Y}}]^3 - 
           3744*\[Omega][{{X, X}, {Y, Y}}]^4 + 
           6080*\[Omega][{{X, X}, {Y, Y}}]^5)) + \[Omega][{{X}, {X, Y, Y}}]*
        (-17888 + 66624*\[Omega][{{X, X}, {Y, Y}}] - 
         45824*\[Omega][{{X, X}, {Y, Y}}]^2 - 
         71200*\[Omega][{{X, X}, {Y, Y}}]^3 + 
         83232*\[Omega][{{X, X}, {Y, Y}}]^4 - 4256*\[Omega][{{X, X}, {Y, Y}}]^
           5 - 11328*\[Omega][{{X, X}, {Y, Y}}]^6 + 
         640*\[Omega][{{X, X}, {Y, Y}}]^7 - 10240*\[Omega][{{Y}, {X, X, Y}}]^
           6*(1 + \[Omega][{{X, X}, {Y, Y}}]) + 64*\[Omega][{{Y}, {X, X, Y}}]^
           5*(-2152 - 1352*\[Omega][{{X, X}, {Y, Y}}] + 
           640*\[Omega][{{X, X}, {Y, Y}}]^2) + 16*\[Omega][{{Y}, {X, X, Y}}]^
           4*(-30320 - 2528*\[Omega][{{X, X}, {Y, Y}}] + 
           20112*\[Omega][{{X, X}, {Y, Y}}]^2 - 
           2880*\[Omega][{{X, X}, {Y, Y}}]^3) + 4*\[Omega][{{Y}, {X, X, Y}}]^
           3*(-174144 + 122560*\[Omega][{{X, X}, {Y, Y}}] + 
           167648*\[Omega][{{X, X}, {Y, Y}}]^2 - 
           76640*\[Omega][{{X, X}, {Y, Y}}]^3 + 
           2560*\[Omega][{{X, X}, {Y, Y}}]^4) + 2*\[Omega][{{Y}, {X, X, Y}}]^
           2*(-232416 + 392896*\[Omega][{{X, X}, {Y, Y}}] + 
           163904*\[Omega][{{X, X}, {Y, Y}}]^2 - 
           334016*\[Omega][{{X, X}, {Y, Y}}]^3 + 
           28832*\[Omega][{{X, X}, {Y, Y}}]^4 + 
           4800*\[Omega][{{X, X}, {Y, Y}}]^5) + 2*\[Omega][{{Y}, {X, X, Y}}]*
          (-73200 + 200528*\[Omega][{{X, X}, {Y, Y}}] - 
           36112*\[Omega][{{X, X}, {Y, Y}}]^2 - 
           208912*\[Omega][{{X, X}, {Y, Y}}]^3 + 
           92736*\[Omega][{{X, X}, {Y, Y}}]^4 + 
           17984*\[Omega][{{X, X}, {Y, Y}}]^5 - 
           2560*\[Omega][{{X, X}, {Y, Y}}]^6)))))/
   ((1 - 5*\[Lambda][{y}])*(2 - 4*\[Lambda][{y}])*
    (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
    (2 + 2*\[Lambda][{y}] + 4*\[Omega][{{X}, {X, Y, Y}}] + 
     4*\[Omega][{{Y}, {X, X, Y}}] - 4*\[Omega][{{X, X}, {Y, Y}}])*
    (2 + 4*\[Omega][{{X}, {X, Y, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
    (1 + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
     2*\[Omega][{{X, X}, {Y, Y}}])*(2 + 4*\[Omega][{{Y}, {X, X, Y}}] - 
     2*\[Omega][{{X, X}, {Y, Y}}])*(1 - 2*\[Lambda][{y}] + 
     2*\[Omega][{{X}, {X, Y, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
    (1 - 2*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
     \[Omega][{{X, X}, {Y, Y}}])*(\[Lambda][{y}] + 
     2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
    (3 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
    (3 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
    (1 + 2*\[Omega][{{X, X}, {Y, Y}}])), 
 (6 + 8*\[Omega][{{X, Y}, {X, Y}}] + 8*\[Omega][{{Y}, {X, X, Y}}]*
    (1 + \[Omega][{{X, Y}, {X, Y}}]) + 8*\[Omega][{{X}, {X, Y, Y}}]*
    (1 + \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))/
  (E^(T*(1 + \[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
      2*\[Omega][{{Y}, {X, X, Y}}]))*(1 + 2*\[Omega][{{X}, {X, Y, Y}}])*
   (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
   (1 + 2*\[Omega][{{Y}, {X, X, Y}}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
    \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])*
   (1 + 2*\[Omega][{{X, Y}, {X, Y}}]))}
