{\[Lambda][{x}]/(E^(2*T*\[Omega][{{X, X}, {Y, Y}}])*
    (-\[Lambda][{x}] - 2*\[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{X, X}, {Y, Y}}])*(-1 - 2*\[Omega][{{Y}, {X, X, Y}}] + 
     \[Omega][{{X, X}, {Y, Y}}])*(1 + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
  (2*E^(-T - 2*T*\[Omega][{{Y}, {X, X, Y}}] - T*\[Omega][{{X, X}, {Y, Y}}])*
    \[Lambda][{x}]*(1 + \[Omega][{{X, X}, {Y, Y}}]))/
   ((1 + 2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
    (-\[Lambda][{x}] - 2*\[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{X, X}, {Y, Y}}])*(3 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
     \[Omega][{{X, X}, {Y, Y}}])*(1 + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
  (E^(-(T*\[Lambda][{x}]) - 2*T*\[Omega][{{X}, {X, Y, Y}}] - 
      T*\[Omega][{{X, X}, {Y, Y}}])*(2*\[Lambda][{x}] - 
     2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Lambda][{x}]*
      \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}] + 
     \[Lambda][{x}]*\[Omega][{{X, X}, {Y, Y}}]))/
   ((\[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] - 
     \[Omega][{{X, X}, {Y, Y}}])*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{X, X}, {Y, Y}}])*(-1 - 2*\[Omega][{{Y}, {X, X, Y}}] + 
     \[Omega][{{X, X}, {Y, Y}}])*(1 + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
  (E^(-T - T*\[Lambda][{x}] - 2*T*\[Omega][{{X}, {X, Y, Y}}] - 
      2*T*\[Omega][{{Y}, {X, X, Y}}])*(12*\[Lambda][{x}] - 
     12*\[Omega][{{X}, {X, Y, Y}}] + 16*\[Lambda][{x}]*
      \[Omega][{{X}, {X, Y, Y}}] - 4*\[Omega][{{X}, {X, Y, Y}}]^2 + 
     4*\[Lambda][{x}]*\[Omega][{{X}, {X, Y, Y}}]^2 + 
     4*\[Lambda][{x}]*\[Omega][{{Y}, {X, X, Y}}] - 
     4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     4*\[Lambda][{x}]*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     6*\[Omega][{{X, X}, {Y, Y}}] + 19*\[Lambda][{x}]*
      \[Omega][{{X, X}, {Y, Y}}] - 8*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 18*\[Lambda][{x}]*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     4*\[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     4*\[Lambda][{x}]*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 8*\[Lambda][{x}]*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     4*\[Lambda][{x}]*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 5*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     5*\[Lambda][{x}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     2*\[Lambda][{x}]*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       2 + 2*\[Lambda][{x}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + \[Omega][{{X, X}, {Y, Y}}]^3))/
   ((3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
    (\[Lambda][{x}] + 2*\[Omega][{{X}, {X, Y, Y}}] - 
     \[Omega][{{X, X}, {Y, Y}}])*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] - 
     \[Omega][{{X, X}, {Y, Y}}])*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{X, X}, {Y, Y}}])*(3 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
     \[Omega][{{X, X}, {Y, Y}}])*(1 + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
  ((-2*E^(-(T*\[Lambda][{x}]) - 2*T*\[Omega][{{X}, {X, Y, Y}}] - 
        T*\[Omega][{{X, X}, {Y, Y}}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{Y}, {X, X, Y}}]))/(-1 - 2*\[Omega][{{Y}, {X, X, Y}}] + 
      \[Omega][{{X, X}, {Y, Y}}]) + 
    (E^(-T - T*\[Lambda][{x}] - 2*T*\[Omega][{{X}, {X, Y, Y}}] - 
        2*T*\[Omega][{{Y}, {X, X, Y}}])*(5 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{X, X}, {Y, Y}}]))/(-1 - 2*\[Omega][{{Y}, {X, X, Y}}] + 
      \[Omega][{{X, X}, {Y, Y}}]))/((1 + 2*\[Omega][{{X}, {X, Y, Y}}])*
    (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
    (3 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])) + 
  (E^(-5*T - T*\[Lambda][{x}] - 4*T*\[Omega][{{X}, {X, Y, Y}}] - 
      6*T*\[Omega][{{Y}, {X, X, Y}}] - T*\[Omega][{{X, X}, {Y, Y}}])*
    (8*E^(4*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
        4*T*\[Omega][{{Y}, {X, X, Y}}])*\[Lambda][{x}]^3*
      (6 + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
      (6*E^(T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}]) - 
       5*E^(T*\[Omega][{{X, X}, {Y, Y}}]) + 
       2*E^(T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}])*
        \[Omega][{{X}, {X, Y, Y}}] - 2*E^(T*\[Omega][{{X, X}, {Y, Y}}])*
        \[Omega][{{X}, {X, Y, Y}}] + 
       2*E^(T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}])*
        \[Omega][{{Y}, {X, X, Y}}] - 2*E^(T*\[Omega][{{X, X}, {Y, Y}}])*
        \[Omega][{{Y}, {X, X, Y}}])*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
       \[Omega][{{X, X}, {Y, Y}}]) - 
     32*E^(4*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
        4*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, X}, {Y, Y}}])*
      (3 + 8*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{X}, {X, Y, Y}}]^2 + 
       8*\[Omega][{{Y}, {X, X, Y}}] + 8*\[Omega][{{X}, {X, Y, Y}}]*
        \[Omega][{{Y}, {X, X, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}]^2)*
      (2*\[Omega][{{X}, {X, Y, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
      (1 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]) + 
     4*\[Lambda][{x}]^2*(-72*E^(4*T + T*\[Lambda][{x}] + 
          4*T*\[Omega][{{X}, {X, Y, Y}}] + 4*T*\[Omega][{{Y}, {X, X, Y}}]) + 
       60*E^(4*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
          4*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, X}, {Y, Y}}]) + 
       64*E^(4*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
          4*T*\[Omega][{{Y}, {X, X, Y}}])*
        (E^(T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}]) - 
         E^(T*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^4 - 
       72*E^(4*T + T*\[Lambda][{x}] + 4*T*\[Omega][{{X}, {X, Y, Y}}] + 
          4*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{X, X}, {Y, Y}}] + 
       48*E^(4*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
          4*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, X}, {Y, Y}}])*
        \[Omega][{{X, X}, {Y, Y}}] - 
       12*E^(4*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
          4*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, X}, {Y, Y}}])*
        \[Omega][{{X, X}, {Y, Y}}]^2 + 
       32*E^(4*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
          4*T*\[Omega][{{Y}, {X, X, Y}}])*
        (E^(T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}]) - 
         E^(T*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^3*
        (1 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]) + 
       32*E^(4*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
          4*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{X}, {X, Y, Y}}]^2*
        (4*E^(T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}]) - 
         3*E^(T*\[Omega][{{X, X}, {Y, Y}}]) + 
         3*E^(T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}])*
          \[Omega][{{Y}, {X, X, Y}}] - 3*E^(T*\[Omega][{{X, X}, {Y, Y}}])*
          \[Omega][{{Y}, {X, X, Y}}])*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
         \[Omega][{{X, X}, {Y, Y}}]) + 
       32*E^(4*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
          4*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^3*
        (9*E^(T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}]) - 
         8*E^(T*\[Omega][{{X, X}, {Y, Y}}]) + 
         E^(T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}])*
          \[Omega][{{X, X}, {Y, Y}}] - E^(T*\[Omega][{{X, X}, {Y, Y}}])*
          \[Omega][{{X, X}, {Y, Y}}]) + 
       16*E^(2*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
          4*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^2*
        (17*E^(2*T + T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}]) - 
         14*E^(T*(2 + \[Omega][{{X, X}, {Y, Y}}])) + 
         8*E^(2*T + T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}])*
          \[Omega][{{X, X}, {Y, Y}}] - 
         8*E^(2*T + T*\[Omega][{{X, X}, {Y, Y}}])*
          \[Omega][{{X, X}, {Y, Y}}]) + 
       8*E^(2*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
          4*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]*
        (-9*E^(2*T + T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}]) + 
         8*E^(2*T + T*\[Omega][{{X, X}, {Y, Y}}]) + 
         9*E^(2*T + T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}])*
          \[Omega][{{X, X}, {Y, Y}}] - 
         11*E^(T*(2 + \[Omega][{{X, X}, {Y, Y}}]))*
          \[Omega][{{X, X}, {Y, Y}}] - E^(2*T + T*\[Omega][{{X, X}, {Y, Y}}])*
          \[Omega][{{X, X}, {Y, Y}}]^2) + 2*\[Omega][{{X}, {X, Y, Y}}]*
        (36*E^(4*T + T*\[Lambda][{x}] + 4*T*\[Omega][{{X}, {X, Y, Y}}] + 
            4*T*\[Omega][{{Y}, {X, X, Y}}]) - 
         16*E^(4*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
            4*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, X}, {Y, Y}}]) + 
         96*E^(4*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
            4*T*\[Omega][{{Y}, {X, X, Y}}])*
          (E^(T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}]) - 
           E^(T*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^3 + 
         E^(2*(T + T*\[Omega][{{X}, {X, Y, Y}}] + 2*T*\[Omega][{{Y}, 
                {X, X, Y}}]))*(36*E^(2*T + T*\[Lambda][{x}] + 
              2*T*\[Omega][{{X}, {X, Y, Y}}]) - 
           20*E^(T*(2 + \[Omega][{{X, X}, {Y, Y}}])))*
          \[Omega][{{X, X}, {Y, Y}}] - 
         4*E^(4*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
            4*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, X}, {Y, Y}}])*
          \[Omega][{{X, X}, {Y, Y}}]^2 + 
         8*E^(2*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
            4*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]*
          (25*E^(2*T + T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}]) - 
           17*E^(T*(2 + \[Omega][{{X, X}, {Y, Y}}])) + 
           16*E^(2*T + T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}])*
            \[Omega][{{X, X}, {Y, Y}}] - 
           14*E^(2*T + T*\[Omega][{{X, X}, {Y, Y}}])*\[Omega][
             {{X, X}, {Y, Y}}]) + 8*E^(4*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
            4*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^2*
          (38*E^(T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}]) - 
           32*E^(T*\[Omega][{{X, X}, {Y, Y}}]) + 
           6*(E^(T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}]) - 
             E^(T*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][
             {{X, X}, {Y, Y}}]))) + \[Lambda][{x}]*
      (-576*E^(4*T + T*\[Lambda][{x}] + 4*T*\[Omega][{{X}, {X, Y, Y}}] + 
          4*T*\[Omega][{{Y}, {X, X, Y}}]) + 
       480*E^(4*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
          4*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, X}, {Y, Y}}]) - 
       512*E^(4*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
          4*T*\[Omega][{{Y}, {X, X, Y}}])*
        (E^(T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}]) - 
         E^(T*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^4 - 
       576*E^(4*T + T*\[Lambda][{x}] + 4*T*\[Omega][{{X}, {X, Y, Y}}] + 
          4*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{X, X}, {Y, Y}}] + 
       528*E^(4*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
          4*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, X}, {Y, Y}}])*
        \[Omega][{{X, X}, {Y, Y}}] + 
       48*E^(4*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
          4*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, X}, {Y, Y}}])*
        \[Omega][{{X, X}, {Y, Y}}]^2 - 
       128*E^(4*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
          4*T*\[Omega][{{Y}, {X, X, Y}}])*
        (2*E^(T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}]) - 
         3*E^(T*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^3*
        (1 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]) + 
       32*E^(2*(T + T*\[Omega][{{X}, {X, Y, Y}}] + 
           2*T*\[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^3*
        (-88*E^(2*T + T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}]) + 
         80*E^(T*(2 + \[Omega][{{X, X}, {Y, Y}}])) + 
         2*(-4*E^(2*T + T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}]) + 
           2*E^(T*(2 + \[Omega][{{X, X}, {Y, Y}}])))*
          \[Omega][{{X, X}, {Y, Y}}]) - 
       64*E^(4*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
          4*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^2*
        (74*E^(T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}]) - 
         64*E^(T*\[Omega][{{X, X}, {Y, Y}}]) + 
         20*E^(T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}])*
          \[Omega][{{X, X}, {Y, Y}}] - 15*E^(T*\[Omega][{{X, X}, {Y, Y}}])*
          \[Omega][{{X, X}, {Y, Y}}] + E^(T*\[Omega][{{X, X}, {Y, Y}}])*
          \[Omega][{{X, X}, {Y, Y}}]^2) - 
       32*E^(4*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
          4*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]*
        (90*E^(T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}]) - 
         76*E^(T*\[Omega][{{X, X}, {Y, Y}}]) + 
         54*E^(T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}])*
          \[Omega][{{X, X}, {Y, Y}}] - 47*E^(T*\[Omega][{{X, X}, {Y, Y}}])*
          \[Omega][{{X, X}, {Y, Y}}] + 2*E^(T*\[Omega][{{X, X}, {Y, Y}}])*
          \[Omega][{{X, X}, {Y, Y}}]^2) - 
       64*E^(2*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
          4*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{X}, {X, Y, Y}}]^2*
        (20*E^(2*T + T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}]) - 
         20*E^(2*T + T*\[Omega][{{X, X}, {Y, Y}}]) + 
         52*E^(2*T + T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}])*
          \[Omega][{{Y}, {X, X, Y}}] - 
         56*E^(T*(2 + \[Omega][{{X, X}, {Y, Y}}]))*
          \[Omega][{{Y}, {X, X, Y}}] + 24*E^(2*T + T*\[Lambda][{x}] + 
            2*T*\[Omega][{{X}, {X, Y, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^2 - 
         32*E^(2*T + T*\[Omega][{{X, X}, {Y, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^
           2 + 20*E^(2*T + T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}])*
          \[Omega][{{X, X}, {Y, Y}}] - 
         19*E^(T*(2 + \[Omega][{{X, X}, {Y, Y}}]))*
          \[Omega][{{X, X}, {Y, Y}}] + 12*E^(2*T + T*\[Lambda][{x}] + 
            2*T*\[Omega][{{X}, {X, Y, Y}}])*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, X}, {Y, Y}}] - 
         14*E^(2*T + T*\[Omega][{{X, X}, {Y, Y}}])*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, X}, {Y, Y}}] + E^(2*T + T*\[Omega][{{X, X}, {Y, Y}}])*
          \[Omega][{{X, X}, {Y, Y}}]^2) + 2*\[Omega][{{X}, {X, Y, Y}}]*
        (-864*E^(4*T + T*\[Lambda][{x}] + 4*T*\[Omega][{{X}, {X, Y, Y}}] + 
            4*T*\[Omega][{{Y}, {X, X, Y}}]) + 
         688*E^(4*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
            4*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, X}, {Y, Y}}]) + 
         64*E^(4*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
            4*T*\[Omega][{{Y}, {X, X, Y}}])*
          (-12*E^(T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}]) + 
           14*E^(T*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^
           3 - 16*E^(4*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
            4*T*\[Omega][{{Y}, {X, X, Y}}])*
          (54*E^(T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}]) - 
           41*E^(T*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}] - 
         32*E^(4*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
            4*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, X}, {Y, Y}}])*
          \[Omega][{{X, X}, {Y, Y}}]^2 - 
         64*E^(2*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
            4*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^2*
          (46*E^(2*T + T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}]) - 
           45*E^(T*(2 + \[Omega][{{X, X}, {Y, Y}}])) + 
           6*E^(2*T + T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}])*
            \[Omega][{{X, X}, {Y, Y}}] - 
           5*E^(2*T + T*\[Omega][{{X, X}, {Y, Y}}])*\[Omega][
             {{X, X}, {Y, Y}}]) - 
         32*E^(2*T + 2*T*\[Omega][{{X}, {X, Y, Y}}] + 
            4*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]*
          (94*E^(2*T + T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}]) - 
           81*E^(2*T + T*\[Omega][{{X, X}, {Y, Y}}]) + 
           40*E^(2*T + T*\[Lambda][{x}] + 2*T*\[Omega][{{X}, {X, Y, Y}}])*
            \[Omega][{{X, X}, {Y, Y}}] - 
           34*E^(T*(2 + \[Omega][{{X, X}, {Y, Y}}]))*\[Omega][
             {{X, X}, {Y, Y}}] + 2*E^(2*T + T*\[Omega][{{X, X}, {Y, Y}}])*
            \[Omega][{{X, X}, {Y, Y}}]^2)))))/((-4 + 2*\[Lambda][{x}])*
    (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
    (1 + 2*\[Omega][{{Y}, {X, X, Y}}])*(1 + \[Lambda][{x}] + 
     2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
    (6 + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
    (2*\[Lambda][{x}] + 4*\[Omega][{{X}, {X, Y, Y}}] - 
     2*\[Omega][{{X, X}, {Y, Y}}])*(3 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
     \[Omega][{{X, X}, {Y, Y}}])*(2 + 4*\[Omega][{{Y}, {X, X, Y}}] + 
     2*\[Omega][{{X, X}, {Y, Y}}])), 
 (2*E^(-T - T*\[Lambda][{x}] - 2*T*\[Omega][{{X}, {X, Y, Y}}] - 
      2*T*\[Omega][{{Y}, {X, X, Y}}]))/((1 + 2*\[Omega][{{X}, {X, Y, Y}}])*
    (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
    (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
     \[Omega][{{X, Y}, {X, Y}}])) + 
  (2*E^(-T - T*\[Lambda][{x}] - 2*T*\[Omega][{{X}, {X, Y, Y}}] - 
      2*T*\[Omega][{{Y}, {X, X, Y}}]))/
   ((3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
    (1 + 2*\[Omega][{{Y}, {X, X, Y}}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])) + 
  (E^(-5*T - T*\[Lambda][{x}] - 5*T*\[Omega][{{X}, {X, Y, Y}}] - 
      5*T*\[Omega][{{Y}, {X, X, Y}}] - T*\[Omega][{{X, Y}, {X, Y}}])*
    (-128*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
        3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
      \[Omega][{{X}, {X, Y, Y}}]^5 - 
     192*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
        3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
      \[Omega][{{Y}, {X, X, Y}}] - 
     800*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
        3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 
     1120*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
        3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
      \[Omega][{{Y}, {X, X, Y}}]^3 - 
     640*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
        3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
      \[Omega][{{Y}, {X, X, Y}}]^4 - 
     128*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
        3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
      \[Omega][{{Y}, {X, X, Y}}]^5 + 
     192*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
        3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
      \[Omega][{{X, Y}, {X, Y}}] + 
     704*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
        3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}] + 
     864*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
        3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, Y}, {X, Y}}] + 
     512*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
        3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, Y}, {X, Y}}] + 
     128*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
        3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, Y}, {X, Y}}] + 
     96*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
        3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
      \[Omega][{{X, Y}, {X, Y}}]^2 + 
     352*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
        3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}]^2 + 
     384*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
        3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, Y}, {X, Y}}]^2 + 
     128*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
        3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, Y}, {X, Y}}]^2 - 
     96*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
        3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
      \[Omega][{{X, Y}, {X, Y}}]^3 - 
     256*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
        3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}]^3 - 
     128*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
        3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, Y}, {X, Y}}]^3 + 
     8*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
        3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
      \[Lambda][{x}]^3*(6 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
       4*\[Omega][{{Y}, {X, X, Y}}])*(2 + \[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}])*
      (1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
       \[Omega][{{X, Y}, {X, Y}}]) - 
     128*E^(2*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
        3*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{X}, {X, Y, Y}}]^4*
      (5*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}])) + 
       5*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{Y}, {X, X, Y}}] - 
       E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}]) - 
     32*E^(2*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
        3*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{X}, {X, Y, Y}}]^3*
      (35*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}]) + 
       80*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}] + 
       40*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^
         2 - 16*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*
        \[Omega][{{X, Y}, {X, Y}}] - 
       16*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]*
        \[Omega][{{X, Y}, {X, Y}}] - 4*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*
        \[Omega][{{X, Y}, {X, Y}}]^2) - 
     32*E^(2*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
        3*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{X}, {X, Y, Y}}]^2*
      (25*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}]) + 
       105*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*
        \[Omega][{{Y}, {X, X, Y}}] + 
       120*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^
         2 + 40*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*
        \[Omega][{{Y}, {X, X, Y}}]^3 - 
       27*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}] - 
       48*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]*
        \[Omega][{{X, Y}, {X, Y}}] - 
       24*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^2*
        \[Omega][{{X, Y}, {X, Y}}] - 
       12*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}]^
         2 - 12*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*
        \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}]^2 + 
       4*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}]^
         3) + \[Omega][{{X}, {X, Y, Y}}]*
      (-192*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}]) - 
       640*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
        \[Omega][{{Y}, {X, X, Y}}]^4 + 
       704*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
        \[Omega][{{X, Y}, {X, Y}}] + 
       352*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
        \[Omega][{{X, Y}, {X, Y}}]^2 - 
       256*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
        \[Omega][{{X, Y}, {X, Y}}]^3 + 
       512*E^(2*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^3*
        (-5*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}])) + 
         E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}]) + 
       96*E^(2*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^2*
        (-35*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}]) + 
         16*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*
          \[Omega][{{X, Y}, {X, Y}}] + 
         4*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}]^
           2) - 64*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
        \[Omega][{{Y}, {X, X, Y}}]*(25 - 27*\[Omega][{{X, Y}, {X, Y}}] - 
         12*\[Omega][{{X, Y}, {X, Y}}]^2 + 4*\[Omega][{{X, Y}, {X, Y}}]^3)) + 
     4*\[Lambda][{x}]^2*(-24*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}]) + 
       24*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
        \[Omega][{{X}, {X, Y, Y}}]^4 + 
       24*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
        \[Omega][{{Y}, {X, X, Y}}]^4 - 
       36*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
        \[Omega][{{X, Y}, {X, Y}}] + 
       12*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
        \[Omega][{{X, Y}, {X, Y}}]^3 - 
       4*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
        \[Omega][{{Y}, {X, X, Y}}]^3*(-25 + 2*\[Omega][{{X, Y}, {X, Y}}]) - 
       4*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
        \[Omega][{{X}, {X, Y, Y}}]^3*(-25 - 24*\[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, Y}, {X, Y}}]) - 
       12*E^(2*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^2*
        (-10*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}])) + 
         E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*\[Omega][{{X, Y}, {X, Y}}] + 
         2*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}]^
           2) - 12*E^(2*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{X}, {X, Y, Y}}]^2*
        (-10*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}])) - 
         25*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*
          \[Omega][{{Y}, {X, X, Y}}] - 
         12*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^
           2 + E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*
          \[Omega][{{X, Y}, {X, Y}}] + 
         2*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, Y}, {X, Y}}] + 
         2*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}]^
           2) + 4*E^(2*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]*
        (5*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}]) - 
         6*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*
          \[Omega][{{X, Y}, {X, Y}}] - 
         9*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}]^
           2 + 2*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*
          \[Omega][{{X, Y}, {X, Y}}]^3) + 
       4*E^(2*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{X}, {X, Y, Y}}]*
        (5*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}]) + 
         60*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*
          \[Omega][{{Y}, {X, X, Y}}] + 
         75*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^
           2 + 24*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*
          \[Omega][{{Y}, {X, X, Y}}]^3 - 
         6*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*
          \[Omega][{{X, Y}, {X, Y}}] - 
         6*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, Y}, {X, Y}}] - 
         6*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^
           2*\[Omega][{{X, Y}, {X, Y}}] - 
         9*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}]^
           2 - 12*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*
          \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}]^2 + 
         2*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}]^
           3)) + \[Lambda][{x}]*
      (-192*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}]) + 
       64*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
        \[Omega][{{X}, {X, Y, Y}}]^5 + 
       64*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
        \[Omega][{{Y}, {X, X, Y}}]^5 - 
       64*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
        \[Omega][{{Y}, {X, X, Y}}]^4*(-2 + \[Omega][{{X, Y}, {X, Y}}]) + 
       144*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
        \[Omega][{{X, Y}, {X, Y}}]^2 - 
       48*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
        \[Omega][{{X, Y}, {X, Y}}]^3 - 
       64*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
        \[Omega][{{X}, {X, Y, Y}}]^4*(-2 - 5*\[Omega][{{Y}, {X, X, Y}}] + 
         \[Omega][{{X, Y}, {X, Y}}]) - 
       16*E^(2*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^3*
        (23*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}])) + 
         12*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*
          \[Omega][{{X, Y}, {X, Y}}] + 
         4*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}]^
           2) - 16*E^(2*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{X}, {X, Y, Y}}]^3*
        (23*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}])) - 
         32*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*
          \[Omega][{{Y}, {X, X, Y}}] - 
         40*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^
           2 + 12*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*
          \[Omega][{{X, Y}, {X, Y}}] + 
         16*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, Y}, {X, Y}}] + 
         4*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}]^
           2) + 16*E^(4*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}] + T*\[Omega][{{X, Y}, {X, Y}}])*
        \[Omega][{{Y}, {X, X, Y}}]*(-56 - 18*\[Omega][{{X, Y}, {X, Y}}] + 
         15*\[Omega][{{X, Y}, {X, Y}}]^2 + 4*\[Omega][{{X, Y}, {X, Y}}]^3) + 
       16*E^(2*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^2*
        (-71*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}]) - 
         21*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*
          \[Omega][{{X, Y}, {X, Y}}] + 
         4*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}]^
           3) + 16*E^(2*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{X}, {X, Y, Y}}]^2*
        (-71*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}]) - 
         69*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*
          \[Omega][{{Y}, {X, X, Y}}] + 
         48*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^
           2 + 40*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*
          \[Omega][{{Y}, {X, X, Y}}]^3 - 
         21*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*
          \[Omega][{{X, Y}, {X, Y}}] - 
         36*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, Y}, {X, Y}}] - 
         24*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^
           2*\[Omega][{{X, Y}, {X, Y}}] - 
         12*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, Y}, {X, Y}}]^2 + 
         4*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}]^
           3) + 16*E^(2*T + 3*T*\[Omega][{{X}, {X, Y, Y}}] + 
          3*T*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{X}, {X, Y, Y}}]*
        (-56*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}]) - 
         142*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*
          \[Omega][{{Y}, {X, X, Y}}] - 
         69*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^
           2 + 32*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*
          \[Omega][{{Y}, {X, X, Y}}]^3 + 
         20*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^
           4 - 18*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*
          \[Omega][{{X, Y}, {X, Y}}] - 
         42*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, Y}, {X, Y}}] - 
         36*E^(T*(2 + \[Omega][{{X, Y}, {X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^
           2*\[Omega][{{X, Y}, {X, Y}}] - 
         16*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^
           3*\[Omega][{{X, Y}, {X, Y}}] + 
         15*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}]^
           2 - 12*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*
          \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, Y}, {X, Y}}]^2 + 
         4*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*\[Omega][{{X, Y}, {X, Y}}]^
           3 + 8*E^(2*T + T*\[Omega][{{X, Y}, {X, Y}}])*
          \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, Y}, {X, Y}}]^3))))/
   (2*(-4 + 2*\[Lambda][{x}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{Y}, {X, X, Y}}])*(1 + \[Lambda][{x}] + 
     2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
    (6 + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
    (2 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] - 
     \[Omega][{{X, Y}, {X, Y}}])*(\[Lambda][{x}] + 
     \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] - 
     \[Omega][{{X, Y}, {X, Y}}])*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])*
    (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
     \[Omega][{{X, Y}, {X, Y}}])*(1 + 2*\[Omega][{{X, Y}, {X, Y}}]))}
