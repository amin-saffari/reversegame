{{-(((-8*M*\[Omega][{{X}, {X, Y, Y}}]*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}])*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}])*(8 + M + 4*\[Omega][{{Y}, {X, X, Y}}] + 
        2*\[Omega][{{X, X}, {Y, Y}}]))/
      (E^((T*(M + 4*\[Omega][{{X}, {X, Y, Y}}]))/2)*
       (M + 4*\[Omega][{{X}, {X, Y, Y}}])*(3 + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (4 + M + 4*\[Omega][{{Y}, {X, X, Y}}])*
       (1 + \[Omega][{{X, X}, {Y, Y}}])*(2 + M + 
        2*\[Omega][{{X, X}, {Y, Y}}])) - 
     (4*(3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (4 + M + 4*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
       (1 + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{X}, {X, Y, Y}}]^2 - 
        \[Omega][{{X, X}, {Y, Y}}]^2))/
      (E^((T*(2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
           2*\[Omega][{{X, X}, {Y, Y}}]))/2)*
       (2 + M + 4*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
       (1 + \[Omega][{{X, X}, {Y, Y}}])*(-2 - 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}])*(2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{X, X}, {Y, Y}}])) + 
     (4*(3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (2 + M + 4*\[Omega][{{X}, {X, Y, Y}}]^2 + 
        2*\[Omega][{{X, X}, {Y, Y}}] + 2*\[Omega][{{X}, {X, Y, Y}}]*
         (3 + \[Omega][{{X, X}, {Y, Y}}])))/
      (E^(T*(1 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}]))*(1 + M + 
        2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
       (-1 - 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
       (2 + M + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
     (4*M*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (3 - 2*\[Omega][{{X}, {X, Y, Y}}]^2 + \[Omega][{{X}, {X, Y, Y}}]*
         (-3 + 2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}]) + 
        \[Omega][{{Y}, {X, X, Y}}]*(5 + \[Omega][{{X, X}, {Y, Y}}])))/
      (E^((T*(6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
           4*\[Omega][{{Y}, {X, X, Y}}]))/2)*(-2 + M)*
       (3 + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
       (2 + 2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])) - 
     (M^2*(144 + 104*M + 21*M^2 + M^3 + 128*\[Omega][{{X}, {X, Y, Y}}]^3 + 
        84*\[Omega][{{X, X}, {Y, Y}}] + 34*M*\[Omega][{{X, X}, {Y, Y}}] + 
        3*M^2*\[Omega][{{X, X}, {Y, Y}}] + 12*\[Omega][{{X, X}, {Y, Y}}]^2 + 
        2*M*\[Omega][{{X, X}, {Y, Y}}]^2 + 8*\[Omega][{{Y}, {X, X, Y}}]^2*
         (3 + M + \[Omega][{{X, X}, {Y, Y}}]) + 
        4*\[Omega][{{X}, {X, Y, Y}}]^2*(116 + 21*M + 
          36*\[Omega][{{Y}, {X, X, Y}}] + 18*\[Omega][{{X, X}, {Y, Y}}]) + 
        2*\[Omega][{{Y}, {X, X, Y}}]*(60 + 29*M + 3*M^2 + 
          (26 + 5*M)*\[Omega][{{X, X}, {Y, Y}}] + 
          2*\[Omega][{{X, X}, {Y, Y}}]^2) + 2*\[Omega][{{X}, {X, Y, Y}}]*
         (244 + 105*M + 8*M^2 + 24*\[Omega][{{Y}, {X, X, Y}}]^2 + 
          (82 + 15*M)*\[Omega][{{X, X}, {Y, Y}}] + 
          6*\[Omega][{{X, X}, {Y, Y}}]^2 + 6*\[Omega][{{Y}, {X, X, Y}}]*
           (26 + 5*M + 4*\[Omega][{{X, X}, {Y, Y}}]))))/
      ((M + 4*\[Omega][{{X}, {X, Y, Y}}])*
       (2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
       (1 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
       (2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{X, X}, {Y, Y}}])) + 
     (2*(8*M*\[Omega][{{X}, {X, Y, Y}}]^3*(M + 4*\[Omega][{{Y}, {X, X, Y}}] - 
          2*\[Omega][{{X, X}, {Y, Y}}]) + 4*\[Omega][{{X}, {X, Y, Y}}]^2*
         (-8 - 2*M + 9*M^2 + 8*(-2 + M)*\[Omega][{{Y}, {X, X, Y}}]^2 + 
          (8 - 16*M + M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
          2*M*\[Omega][{{X, X}, {Y, Y}}]^2 + 2*\[Omega][{{Y}, {X, X, Y}}]*
           (-12 + 16*M + M^2 + 4*\[Omega][{{X, X}, {Y, Y}}])) + 
        (4 + M + 4*\[Omega][{{Y}, {X, X, Y}}])*(-20 - 6*M + 5*M^2 + 
          (16 - 4*M + M^2)*\[Omega][{{X, X}, {Y, Y}}] - 
          2*(-2 + M)*\[Omega][{{X, X}, {Y, Y}}]^2 - 
          8*\[Omega][{{Y}, {X, X, Y}}]^2*(5 + \[Omega][{{X, X}, {Y, Y}}]) + 
          4*\[Omega][{{Y}, {X, X, Y}}]*(-15 + 2*M + 
            (2 + M)*\[Omega][{{X, X}, {Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}]^
             2)) + 2*\[Omega][{{X}, {X, Y, Y}}]*(-56 - 14*M + 22*M^2 + M^3 - 
          32*\[Omega][{{Y}, {X, X, Y}}]^3 + 4*(12 - 8*M + M^2)*
           \[Omega][{{X, X}, {Y, Y}}] + (8 - 10*M)*\[Omega][{{X, X}, {Y, Y}}]^
            2 + 8*\[Omega][{{Y}, {X, X, Y}}]^2*(4*(-5 + M) + 
            M*\[Omega][{{X, X}, {Y, Y}}]) + 2*\[Omega][{{Y}, {X, X, Y}}]*
           (-92 + 28*M + 7*M^2 + (24 + 2*M + M^2)*\[Omega][{{X, X}, {Y, 
                Y}}] - 2*(-2 + M)*\[Omega][{{X, X}, {Y, Y}}]^2))))/
      (E^(T*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          2*\[Omega][{{Y}, {X, X, Y}}]))*(-2 + M)*
       (2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (4 + M + 4*\[Omega][{{Y}, {X, X, Y}}])*
       (2 + M + 4*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
       (1 + 2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])))/
    ((1 + 2*\[Omega][{{X}, {X, Y, Y}}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
      \[Omega][{{Y}, {X, X, Y}}])*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
      \[Omega][{{X, X}, {Y, Y}}]))), 
  -(((-4*M*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}]))/
      (E^((T*(6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
           4*\[Omega][{{Y}, {X, X, Y}}]))/2)*(-2 + M)*
       (6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
        4*\[Omega][{{Y}, {X, X, Y}}])) - 
     (8*(3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (1 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))/
      (E^((T*(2 + M + 4*\[Omega][{{Y}, {X, X, Y}}] + 
           2*\[Omega][{{X, X}, {Y, Y}}]))/2)*
       (2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
       (2 + M + 4*\[Omega][{{Y}, {X, X, Y}}] + 
        2*\[Omega][{{X, X}, {Y, Y}}])) - 
     (M*(36 + 8*M + M^2 + 8*\[Omega][{{X}, {X, Y, Y}}]^2 + 
        6*(6 + M)*\[Omega][{{Y}, {X, X, Y}}] + 
        8*\[Omega][{{Y}, {X, X, Y}}]^2 + 2*\[Omega][{{X}, {X, Y, Y}}]*
         (18 + M + 8*\[Omega][{{Y}, {X, X, Y}}]) + 
        2*M*\[Omega][{{X, X}, {Y, Y}}]))/
      ((2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
       (2 + M + 4*\[Omega][{{Y}, {X, X, Y}}] + 
        2*\[Omega][{{X, X}, {Y, Y}}])) + 
     (2*(-20 + 12*M + 5*M^2 + 8*M*\[Omega][{{X}, {X, Y, Y}}]^2 + 
        4*(-2 + M)*\[Omega][{{Y}, {X, X, Y}}]^2 - 
        4*\[Omega][{{X, X}, {Y, Y}}] - 4*M*\[Omega][{{X, X}, {Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}]*(-14 + 9*M + M^2 - 
          (2 + M)*\[Omega][{{X, X}, {Y, Y}}]) + 2*\[Omega][{{X}, {X, Y, Y}}]*
         (-10 + 13*M + M^2 + (-4 + 6*M)*\[Omega][{{Y}, {X, X, Y}}] - 
          (2 + M)*\[Omega][{{X, X}, {Y, Y}}])))/
      (E^(T*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          2*\[Omega][{{Y}, {X, X, Y}}]))*(-2 + M)*
       (2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] - 
        2*\[Omega][{{X, X}, {Y, Y}}])))/
    ((3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
     (1 + 2*\[Omega][{{Y}, {X, X, Y}}])*(3 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
      \[Omega][{{X, X}, {Y, Y}}]))), 
  -(((4*(3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (4 + M + 4*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
       (1 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
       (3 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))/
      (E^((T*(2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
           2*\[Omega][{{X, X}, {Y, Y}}]))/2)*
       (2 + M + 4*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
       (-2 - 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
       (2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{X, X}, {Y, Y}}])) + 
     (8*(3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (1 + \[Omega][{{X, X}, {Y, Y}}])*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}])*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}]))/
      (E^((T*(2 + M + 4*\[Omega][{{Y}, {X, X, Y}}] + 
           2*\[Omega][{{X, X}, {Y, Y}}]))/2)*
       (2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
       (1 + 2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
       (2 + M + 4*\[Omega][{{Y}, {X, X, Y}}] + 
        2*\[Omega][{{X, X}, {Y, Y}}])) + 
     (8*(3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       \[Omega][{{X, X}, {Y, Y}}]*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}])*(3 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}]))/
      (E^((T*(M + 4*\[Omega][{{X, X}, {Y, Y}}]))/2)*
       (2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
       (-1 - 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
       (M + 4*\[Omega][{{X, X}, {Y, Y}}])) - 
     (2*(3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (3 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
       (2*(2 + M) + 8*\[Omega][{{X}, {X, Y, Y}}]^2 + 
        (4 + M)*\[Omega][{{X, X}, {Y, Y}}] + 2*\[Omega][{{X}, {X, Y, Y}}]*
         (6 + M + 2*\[Omega][{{X, X}, {Y, Y}}])))/
      (E^(T*(1 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}]))*(2 + M + 
        4*\[Omega][{{X}, {X, Y, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
       (1 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
       (-1 - 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])) + 
     (4*M*(2*\[Omega][{{Y}, {X, X, Y}}]^2 + \[Omega][{{X}, {X, Y, Y}}]^2*
         (2 + 4*\[Omega][{{X, X}, {Y, Y}}]) + 
        3*(3 + 4*\[Omega][{{X, X}, {Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}]^2) + 
        \[Omega][{{Y}, {X, X, Y}}]*(9 + 8*\[Omega][{{X, X}, {Y, Y}}] + 
          2*\[Omega][{{X, X}, {Y, Y}}]^2) + \[Omega][{{X}, {X, Y, Y}}]*
         (9 + 14*\[Omega][{{X, X}, {Y, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]^
            2 + 4*\[Omega][{{Y}, {X, X, Y}}]*
           (1 + \[Omega][{{X, X}, {Y, Y}}]))))/
      (E^((T*(6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
           4*\[Omega][{{Y}, {X, X, Y}}]))/2)*(-2 + M)*
       (6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
       (2 + 2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])) - 
     (2*(16*\[Omega][{{Y}, {X, X, Y}}]^3*(4*(-1 + M) + 2*(-5 + 2*M)*
           \[Omega][{{X, X}, {Y, Y}}] + (-2 + M)*\[Omega][{{X, X}, {Y, Y}}]^
            2) + 4*\[Omega][{{Y}, {X, X, Y}}]^2*(-72 + 62*M + 7*M^2 + 
          (-152 + 62*M + 11*M^2)*\[Omega][{{X, X}, {Y, Y}}] + 
          (-32 + 4*M + 3*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 - 
          4*M*\[Omega][{{X, X}, {Y, Y}}]^3) + 
        (3 + \[Omega][{{X, X}, {Y, Y}}])*(-32 - 4*M + 14*M^2 + 4*M^3 + 
          (-24 - 32*M + 6*M^2 + 5*M^3)*\[Omega][{{X, X}, {Y, Y}}] - 
          2*(-24 + 10*M + 7*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          8*(1 + M)*\[Omega][{{X, X}, {Y, Y}}]^3) + 
        2*\[Omega][{{Y}, {X, X, Y}}]*(-160 + 82*M + 49*M^2 + 2*M^3 + 
          4*(-69 + 6*M + 17*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
          (12 - 72*M + 8*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          (36 - 20*M - 3*M^2)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
          2*(2 + M)*\[Omega][{{X, X}, {Y, Y}}]^4) + 
        16*M*\[Omega][{{X}, {X, Y, Y}}]^3*(-1 + M + 
          (-1 + M)*\[Omega][{{X, X}, {Y, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}]^
            2 + \[Omega][{{Y}, {X, X, Y}}]*
           (2 + 4*\[Omega][{{X, X}, {Y, Y}}])) + 
        4*\[Omega][{{X}, {X, Y, Y}}]^2*(-8 - 14*M + 18*M^2 + M^3 + 
          (-12 - 24*M + 19*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}] - 
          (-16 + 40*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 - 
          2*(-2 + M)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
          8*\[Omega][{{Y}, {X, X, Y}}]^2*(2*M + (-2 + 3*M)*
             \[Omega][{{X, X}, {Y, Y}}]) + 2*\[Omega][{{Y}, {X, X, Y}}]*
           (-8 + 22*M + 4*M^2 + (-24 + 36*M + 5*M^2)*\[Omega][
              {{X, X}, {Y, Y}}] - 4*M*\[Omega][{{X, X}, {Y, Y}}]^2)) + 
        2*\[Omega][{{X}, {X, Y, Y}}]*(-64 - 14*M + 43*M^2 + 8*M^3 + 
          (-76 - 80*M + 48*M^2 + 9*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
          (92 - 120*M - 12*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          (44 - 12*M - 3*M^2)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
          2*(2 + M)*\[Omega][{{X, X}, {Y, Y}}]^4 + 
          16*\[Omega][{{Y}, {X, X, Y}}]^3*(M + (-2 + M)*
             \[Omega][{{X, X}, {Y, Y}}]) + 4*\[Omega][{{Y}, {X, X, Y}}]^2*
           (2*(-8 + 16*M + M^2) + (-52 + 38*M + 3*M^2)*\[Omega][
              {{X, X}, {Y, Y}}] + 2*(-2 + M)*\[Omega][{{X, X}, {Y, Y}}]^2) + 
          2*\[Omega][{{Y}, {X, X, Y}}]*(-80 + 84*M + 25*M^2 + M^3 + 
            (-172 + 90*M + 36*M^2 + M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
            2*(-4 - 14*M + M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
            (4 - 6*M)*\[Omega][{{X, X}, {Y, Y}}]^3))))/
      (E^(T*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          2*\[Omega][{{Y}, {X, X, Y}}]))*(-2 + M)*
       (2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
       (2 + M + 4*\[Omega][{{Y}, {X, X, Y}}] - 2*\[Omega][{{X, X}, {Y, Y}}])*
       (1 + 2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])) - 
     (M*(64*\[Omega][{{X}, {X, Y, Y}}]^5*(6 + M + 
          4*\[Omega][{{Y}, {X, X, Y}}] + 6*\[Omega][{{X, X}, {Y, Y}}]) + 
        32*\[Omega][{{Y}, {X, X, Y}}]^4*(6 + 3*M + M^2 + 
          (8 + 5*M)*\[Omega][{{X, X}, {Y, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]^
            2) + 16*\[Omega][{{Y}, {X, X, Y}}]^3*
         (2*(42 + 27*M + 10*M^2 + M^3) + (130 + 95*M + 13*M^2)*
           \[Omega][{{X, X}, {Y, Y}}] + (52 + 19*M)*
           \[Omega][{{X, X}, {Y, Y}}]^2 + 6*\[Omega][{{X, X}, {Y, Y}}]^3) + 
        2*\[Omega][{{Y}, {X, X, Y}}]^2*(1656 + 1404*M + 590*M^2 + 107*M^3 + 
          5*M^4 + (3096 + 2708*M + 714*M^2 + 47*M^3)*
           \[Omega][{{X, X}, {Y, Y}}] + 4*(458 + 285*M + 34*M^2)*
           \[Omega][{{X, X}, {Y, Y}}]^2 + 4*(106 + 31*M)*
           \[Omega][{{X, X}, {Y, Y}}]^3 + 32*\[Omega][{{X, X}, {Y, Y}}]^4) + 
        (3 + \[Omega][{{X, X}, {Y, Y}}])*(432 + 648*M + 364*M^2 + 104*M^3 + 
          17*M^4 + M^5 + (1152 + 1408*M + 620*M^2 + 126*M^3 + 9*M^4)*
           \[Omega][{{X, X}, {Y, Y}}] + 4*(252 + 222*M + 64*M^2 + 7*M^3)*
           \[Omega][{{X, X}, {Y, Y}}]^2 + 36*(8 + 4*M + M^2)*
           \[Omega][{{X, X}, {Y, Y}}]^3 + 16*M*\[Omega][{{X, X}, {Y, Y}}]^
            4) + \[Omega][{{Y}, {X, X, Y}}]*(3456 + 3888*M + 1888*M^2 + 
          458*M^3 + 47*M^4 + M^5 + (8064 + 8512*M + 3160*M^2 + 452*M^3 + 
            17*M^4)*\[Omega][{{X, X}, {Y, Y}}] + 2*(3312 + 2744*M + 664*M^2 + 
            43*M^3)*\[Omega][{{X, X}, {Y, Y}}]^2 + 8*(288 + 154*M + 19*M^2)*
           \[Omega][{{X, X}, {Y, Y}}]^3 + 16*(18 + 5*M)*
           \[Omega][{{X, X}, {Y, Y}}]^4) + 32*\[Omega][{{X}, {X, Y, Y}}]^4*
         (90 + 29*M + 2*M^2 + 24*\[Omega][{{Y}, {X, X, Y}}]^2 + 
          2*(55 + 8*M)*\[Omega][{{X, X}, {Y, Y}}] + 
          20*\[Omega][{{X, X}, {Y, Y}}]^2 + 2*\[Omega][{{Y}, {X, X, Y}}]*
           (48 + 7*M + 22*\[Omega][{{X, X}, {Y, Y}}])) + 
        4*\[Omega][{{X}, {X, Y, Y}}]^3*(1992 + 1060*M + 146*M^2 + 7*M^3 + 
          192*\[Omega][{{Y}, {X, X, Y}}]^3 + 4*(758 + 245*M + 16*M^2)*
           \[Omega][{{X, X}, {Y, Y}}] + 8*(141 + 19*M)*
           \[Omega][{{X, X}, {Y, Y}}]^2 + 88*\[Omega][{{X, X}, {Y, Y}}]^3 + 
          32*\[Omega][{{Y}, {X, X, Y}}]^2*(43 + 6*M + 
            15*\[Omega][{{X, X}, {Y, Y}}]) + 8*\[Omega][{{Y}, {X, X, Y}}]*
           (370 + 117*M + 8*M^2 + 4*(77 + 12*M)*\[Omega][{{X, X}, {Y, Y}}] + 
            54*\[Omega][{{X, X}, {Y, Y}}]^2)) + 
        2*\[Omega][{{X}, {X, Y, Y}}]^2*(5112 + 4068*M + 934*M^2 + 97*M^3 + 
          4*M^4 + 128*\[Omega][{{Y}, {X, X, Y}}]^4 + 
          (9720 + 5332*M + 846*M^2 + 45*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
          4*(1394 + 471*M + 41*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          4*(250 + 47*M)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
          32*\[Omega][{{X, X}, {Y, Y}}]^4 + 32*\[Omega][{{Y}, {X, X, Y}}]^3*
           (52 + 7*M + 18*\[Omega][{{X, X}, {Y, Y}}]) + 
          16*\[Omega][{{Y}, {X, X, Y}}]^2*(2*(202 + 62*M + 5*M^2) + 
            (294 + 53*M)*\[Omega][{{X, X}, {Y, Y}}] + 
            50*\[Omega][{{X, X}, {Y, Y}}]^2) + 4*\[Omega][{{Y}, {X, X, Y}}]*
           (2448 + 1280*M + 200*M^2 + 11*M^3 + 2*(1502 + 541*M + 47*M^2)*
             \[Omega][{{X, X}, {Y, Y}}] + 2*(544 + 107*M)*
             \[Omega][{{X, X}, {Y, Y}}]^2 + 100*\[Omega][{{X, X}, {Y, Y}}]^
              3)) + \[Omega][{{X}, {X, Y, Y}}]*(6048 + 6768*M + 2440*M^2 + 
          434*M^3 + 41*M^4 + M^5 + (14400 + 12160*M + 3480*M^2 + 424*M^3 + 
            19*M^4)*\[Omega][{{X, X}, {Y, Y}}] + 2*(5760 + 3528*M + 700*M^2 + 
            51*M^3)*\[Omega][{{X, X}, {Y, Y}}]^2 + 48*(72 + 31*M + 4*M^2)*
           \[Omega][{{X, X}, {Y, Y}}]^3 + 16*(18 + 7*M)*
           \[Omega][{{X, X}, {Y, Y}}]^4 + 64*\[Omega][{{Y}, {X, X, Y}}]^4*
           (8 + M + 4*\[Omega][{{X, X}, {Y, Y}}]) + 
          32*\[Omega][{{Y}, {X, X, Y}}]^3*(130 + 39*M + 5*M^2 + 
            26*(4 + M)*\[Omega][{{X, X}, {Y, Y}}] + 
            18*\[Omega][{{X, X}, {Y, Y}}]^2) + 4*\[Omega][{{Y}, {X, X, Y}}]^2*
           (2952 + 1524*M + 310*M^2 + 23*M^3 + 4*(874 + 391*M + 42*M^2)*
             \[Omega][{{X, X}, {Y, Y}}] + 8*(157 + 40*M)*
             \[Omega][{{X, X}, {Y, Y}}]^2 + 136*\[Omega][{{X, X}, {Y, Y}}]^
              3) + 2*\[Omega][{{Y}, {X, X, Y}}]*
           (3*(2352 + 1840*M + 516*M^2 + 68*M^3 + 3*M^4) + 
            4*(2988 + 1926*M + 392*M^2 + 25*M^3)*\[Omega][{{X, X}, {Y, Y}}] + 
            4*(1708 + 788*M + 85*M^2)*\[Omega][{{X, X}, {Y, Y}}]^2 + 
            8*(178 + 43*M)*\[Omega][{{X, X}, {Y, Y}}]^3 + 
            64*\[Omega][{{X, X}, {Y, Y}}]^4))))/
      ((2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
       (1 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
       (2 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}])*
       (2 + M + 4*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}])*
       (M + 4*\[Omega][{{X, X}, {Y, Y}}])))/
    ((3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
     (3 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
     (3 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
     (1 + 2*\[Omega][{{X, X}, {Y, Y}}])))}, 
 {(-2*((-8*M*\[Omega][{{X}, {X, Y, Y}}]*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))/
      (E^((T*(M + 4*\[Omega][{{X}, {X, Y, Y}}]))/2)*
       (M + 4*\[Omega][{{X}, {X, Y, Y}}])*(3 + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (4 + M + 4*\[Omega][{{Y}, {X, X, Y}}])*
       (1 - \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, Y}, {X, Y}}])) + (8*M*(1 + \[Omega][{{X}, {X, Y, Y}}])*
       (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, Y}, {X, Y}}]))/
      (E^((T*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
           2*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
       (2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
        2*\[Omega][{{X, Y}, {X, Y}}])*(2 + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}])*
       (1 - \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, Y}, {X, Y}}])*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}])) - 
     (4*M*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (6 + \[Omega][{{X}, {X, Y, Y}}]^2 + \[Omega][{{Y}, {X, X, Y}}]^2 - 
        \[Omega][{{Y}, {X, X, Y}}]*(-4 + \[Omega][{{X, Y}, {X, Y}}]) + 
        \[Omega][{{X}, {X, Y, Y}}]*(6 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
          \[Omega][{{X, Y}, {X, Y}}])))/
      (E^((T*(6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
           4*\[Omega][{{Y}, {X, X, Y}}]))/2)*(-2 + M)*
       (3 + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
       (2 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] - 
        \[Omega][{{X, Y}, {X, Y}}])) - 
     (M^2*(36 + 8*M + M^2 + 20*\[Omega][{{X}, {X, Y, Y}}]^2 + 
        4*\[Omega][{{Y}, {X, X, Y}}]^2 + 12*\[Omega][{{X, Y}, {X, Y}}] + 
        2*M*\[Omega][{{X, Y}, {X, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}]*
         (6 + M + \[Omega][{{X, Y}, {X, Y}}]) + 4*\[Omega][{{X}, {X, Y, Y}}]*
         (14 + 2*M + 6*\[Omega][{{Y}, {X, X, Y}}] + 
          3*\[Omega][{{X, Y}, {X, Y}}])))/((M + 4*\[Omega][{{X}, {X, Y, Y}}])*
       (2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
       (2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        2*\[Omega][{{X, Y}, {X, Y}}])) + 
     (2*(8*M*\[Omega][{{X}, {X, Y, Y}}]^3 - 
        (4 + M + 4*\[Omega][{{Y}, {X, X, Y}}])*(-4 - 12*M + M^2 - 
          4*\[Omega][{{Y}, {X, X, Y}}]^2 - 4*\[Omega][{{Y}, {X, X, Y}}]*
           (2 + M - \[Omega][{{X, Y}, {X, Y}}]) - 2*(-2 + M)*
           \[Omega][{{X, Y}, {X, Y}}]) + 4*\[Omega][{{X}, {X, Y, Y}}]^2*
         (4 + 13*M + 4*(1 + M)*\[Omega][{{Y}, {X, X, Y}}] + 
          2*M*\[Omega][{{X, Y}, {X, Y}}]) + 4*\[Omega][{{X}, {X, Y, Y}}]*
         (8 + 24*M + M^2 + 2*(4 + M)*\[Omega][{{Y}, {X, X, Y}}]^2 + 
          (-4 + 5*M)*\[Omega][{{X, Y}, {X, Y}}] + 
          2*\[Omega][{{Y}, {X, X, Y}}]*(8 + 9*M + (-2 + M)*
             \[Omega][{{X, Y}, {X, Y}}]))))/
      (E^(T*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          2*\[Omega][{{Y}, {X, X, Y}}]))*(-2 + M)*
       (2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (4 + M + 4*\[Omega][{{Y}, {X, X, Y}}])*
       (2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
        2*\[Omega][{{X, Y}, {X, Y}}]))))/((1 + 2*\[Omega][{{X}, {X, Y, Y}}])*
    (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
    (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
     \[Omega][{{X, Y}, {X, Y}}])), 
  (-2*((4*M*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (1 + \[Omega][{{X, Y}, {X, Y}}]))/
      (E^((T*(6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
           4*\[Omega][{{Y}, {X, X, Y}}]))/2)*(-2 + M)*
       (6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
       (2 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] - 
        \[Omega][{{X, Y}, {X, Y}}])) - 
     (4*M*(3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, Y}, {X, Y}}]))/
      (E^((T*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
           2*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
       (2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
        2*\[Omega][{{X, Y}, {X, Y}}])*(2 + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}])*
       (2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        2*\[Omega][{{X, Y}, {X, Y}}])) - 
     (M^2*(8 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
        4*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/
      ((2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
       (2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        2*\[Omega][{{X, Y}, {X, Y}}])) - 
     (2*(-4 + 6*M + M^2 + 2*(-2 + M)*\[Omega][{{X}, {X, Y, Y}}]^2 + 
        2*(-2 + M)*\[Omega][{{Y}, {X, X, Y}}]^2 + 
        4*\[Omega][{{X, Y}, {X, Y}}] + 4*M*\[Omega][{{X, Y}, {X, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}]*(4*(-1 + M) + 
          (2 + M)*\[Omega][{{X, Y}, {X, Y}}]) + 2*\[Omega][{{X}, {X, Y, Y}}]*
         (4*(-1 + M) + 2*(-2 + M)*\[Omega][{{Y}, {X, X, Y}}] + 
          (2 + M)*\[Omega][{{X, Y}, {X, Y}}])))/
      (E^(T*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          2*\[Omega][{{Y}, {X, X, Y}}]))*(-2 + M)*
       (2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
        2*\[Omega][{{X, Y}, {X, Y}}]))))/
   ((3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
    (1 + 2*\[Omega][{{Y}, {X, X, Y}}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])), 
  -(((8*M*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (1 + \[Omega][{{X, Y}, {X, Y}}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))/
      (E^((T*(6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
           4*\[Omega][{{Y}, {X, X, Y}}]))/2)*(-2 + M)*
       (6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
       (2 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] - 
        \[Omega][{{X, Y}, {X, Y}}])) - 
     (2*M^2*(3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, Y}, {X, Y}}])*(8 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 
        4*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/
      ((2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (6 + M + 4*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}])*
       (2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        2*\[Omega][{{X, Y}, {X, Y}}])) - 
     (8*M*(3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (3 + \[Omega][{{X}, {X, Y, Y}}]^2 + \[Omega][{{Y}, {X, X, Y}}]^2 + 
        4*\[Omega][{{X, Y}, {X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]^2 + 
        2*\[Omega][{{Y}, {X, X, Y}}]*(2 + \[Omega][{{X, Y}, {X, Y}}]) + 
        2*\[Omega][{{X}, {X, Y, Y}}]*(2 + \[Omega][{{Y}, {X, X, Y}}] + 
          \[Omega][{{X, Y}, {X, Y}}])))/
      (E^((T*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
           2*\[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, Y}, {X, Y}}]))/2)*
       (2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
        2*\[Omega][{{X, Y}, {X, Y}}])*(2 + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}])*
       (2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        2*\[Omega][{{X, Y}, {X, Y}}])) - 
     (4*(3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, Y}, {X, Y}}])*(-4 + 6*M + M^2 + 
        2*(-2 + M)*\[Omega][{{X}, {X, Y, Y}}]^2 + 
        2*(-2 + M)*\[Omega][{{Y}, {X, X, Y}}]^2 + 
        4*\[Omega][{{X, Y}, {X, Y}}] + 4*M*\[Omega][{{X, Y}, {X, Y}}] + 
        2*\[Omega][{{Y}, {X, X, Y}}]*(4*(-1 + M) + 
          (2 + M)*\[Omega][{{X, Y}, {X, Y}}]) + 2*\[Omega][{{X}, {X, Y, Y}}]*
         (4*(-1 + M) + 2*(-2 + M)*\[Omega][{{Y}, {X, X, Y}}] + 
          (2 + M)*\[Omega][{{X, Y}, {X, Y}}])))/
      (E^(T*(2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          2*\[Omega][{{Y}, {X, X, Y}}]))*(-2 + M)*
       (2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
       (2 + M + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
        2*\[Omega][{{X, Y}, {X, Y}}])))/
    ((3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
     (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
       \[Omega][{{X, Y}, {X, Y}}])^2*(1 + 2*\[Omega][{{X, Y}, {X, Y}}])))}}
