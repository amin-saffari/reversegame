{{(32*a1^2*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}])*
     (E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
      E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^5 + 
    16*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}])*\[Omega][{{X}, {X, Y, Y}}]^4*
     (12*a1^2*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) + 
      A2*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) + 
      a1*A2*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) + 
      2*a1^2*A2*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
      11*a1^2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) - 
      A2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) - 
      a1*A2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) - 
      2*a1^2*A2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) + 
      2*a1^2*(3*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
        2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}] + 
      a1^2*(E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
        2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]) + 
    8*\[Omega][{{X}, {X, Y, Y}}]^3*(E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}])*
       (A2*((11 + 3*A2)*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
          3*(3 + A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 
        a1*A2*(2*(7 + A2)*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
          (13 + 2*A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 
        a1^2*((47 + 17*A2 + A2^2)*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
          (41 + 16*A2 + A2^2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))) + 
      4*a1^2*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}])*
       (3*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
        E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^2 + 
      E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}])*
       (-2*A2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) + 
        a1*A2*(E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
          2*E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 
        a1^2*((11 + 2*A2)*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
          (16 + 3*A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])))*
       \[Omega][{{X, X}, {Y, Y}}] - 
      a1^2*E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^2 + 
      2*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}])*\[Omega][{{Y}, {X, X, Y}}]*
       (a1*A2*(3*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
          2*E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 
        A2*(3*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
          E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 
        a1^2*((25 + 4*A2)*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
          (17 + 3*A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 
        a1^2*(3*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
          4*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}])) + 
    4*\[Omega][{{X}, {X, Y, Y}}]^2*
     (72*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}])) + 
      36*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}])) + 
      69*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}])) + 25*a1^2*A2*
       E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{Y}, {X, X, Y}}])) + 28*A2^2*
       E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{Y}, {X, X, Y}}])) + 23*a1*A2^2*
       E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{Y}, {X, X, Y}}])) + 3*a1^2*A2^2*
       E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{Y}, {X, X, Y}}])) + 
      3*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}])) + 
      a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}])) - 
      61*a1^2*E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}])) - 
      23*A2*E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}])) - 
      62*a1*A2*E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}])) - 27*a1^2*A2*
       E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{X, X}, {Y, Y}}])) - 23*A2^2*
       E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{X, X}, {Y, Y}}])) - 22*a1*A2^2*
       E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{X, X}, {Y, Y}}])) - 3*a1^2*A2^2*
       E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{X, X}, {Y, Y}}])) - 
      3*A2^3*E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}])) - 
      a1*A2^3*E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}])) + 
      8*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^3 + 
      E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}])*
       (-(A2*(12 + 5*A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 
        a1*A2*((11 + 2*A2)*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
          (17 + 3*A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 
        a1^2*((36 + 17*A2 + A2^2)*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
          (42 + 14*A2 + A2^2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])))*
       \[Omega][{{X, X}, {Y, Y}}] - (A2 + a1*A2 + a1^2*(5 + A2))*
       E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
       \[Omega][{{X, X}, {Y, Y}}]^2 + 
      4*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^2*
       (3*A2*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) + 
        a1*A2*(3*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
          E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 
        a1^2*(2*(7 + A2)*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
          (6 + A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 
        a1^2*(3*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
          2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]) + 
      2*\[Omega][{{Y}, {X, X, Y}}]*(E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}])*
         (A2*((22 + 7*A2)*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
            2*(3 + A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 
          a1*A2*((31 + 4*A2)*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
            (22 + 3*A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 
          a1^2*((58 + 15*A2 + A2^2)*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, 
                   Y}}]) - (40 + 16*A2 + A2^2)*E^(T1*\[Omega][{{X, X}, 
                 {Y, Y}}]))) + E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}])*
         (-2*A2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) + 
          a1*A2*(3*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
            4*E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 2*a1^2*(11 + 2*A2)*
           (E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
            E^(T1*\[Omega][{{X, X}, {Y, Y}}])))*\[Omega][{{X, X}, {Y, Y}}] - 
        2*a1^2*E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            \[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^2)) + 
    2*\[Omega][{{X}, {X, Y, Y}}]*
     (36*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}])) + 
      36*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}])) + 144*a1*A2*
       E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{Y}, {X, X, Y}}])) - 42*a1^2*A2*
       E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{Y}, {X, X, Y}}])) + 66*A2^2*
       E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{Y}, {X, X, Y}}])) + 81*a1*A2^2*
       E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{Y}, {X, X, Y}}])) - 28*a1^2*A2^2*
       E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{Y}, {X, X, Y}}])) + 23*A2^3*
       E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{Y}, {X, X, Y}}])) + 9*a1*A2^3*
       E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{Y}, {X, X, Y}}])) - 2*a1^2*A2^3*
       E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{Y}, {X, X, Y}}])) + 
      A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}])) - 
      30*a1^2*E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}])) - 
      15*A2*E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}])) - 122*a1*A2*
       E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{X, X}, {Y, Y}}])) + 21*a1^2*A2*
       E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{X, X}, {Y, Y}}])) - 43*A2^2*
       E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{X, X}, {Y, Y}}])) - 78*a1*A2^2*
       E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{X, X}, {Y, Y}}])) + 20*a1^2*A2^2*
       E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{X, X}, {Y, Y}}])) - 19*A2^3*
       E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{X, X}, {Y, Y}}])) - 9*a1*A2^3*
       E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{X, X}, {Y, Y}}])) + 2*a1^2*A2^3*
       E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{X, X}, {Y, Y}}])) - 
      A2^4*E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}])) + 
      E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}])*
       (-(A2*(18 + 21*A2 + 4*A2^2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 
        2*a1^2*(3*(6 + 5*A2 + A2^2)*E^(T1 + 2*T1*\[Omega][{{Y}, 
                {X, X, Y}}]) + (-18 - 4*A2 + A2^2)*
           E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 
        a1*A2*((36 + 17*A2 + A2^2)*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
          (52 + 19*A2 + A2^2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])))*
       \[Omega][{{X, X}, {Y, Y}}] - (a1^2*(6 + A2) + a1*A2*(6 + A2) + 
        A2*(3 + 2*A2))*E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^2 + 
      8*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^3*
       (a1^2 + A2 + a1*A2 + a1^2*\[Omega][{{X, X}, {Y, Y}}]) + 
      4*\[Omega][{{Y}, {X, X, Y}}]^2*
       (E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}])*
         (A2*(11 + 5*A2)*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) + 
          a1*A2*(2*(10 + A2)*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
            (7 + A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])) - 
          a1^2*((-11 + 4*A2)*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) + 
            (5 + 4*A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))) + 
        a1*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}])*
         (A2*(3*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
            2*E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 
          a1*((11 + 2*A2)*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
            (6 + A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])))*
         \[Omega][{{X, X}, {Y, Y}}] - 
        a1^2*E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            \[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^2) + 
      2*\[Omega][{{Y}, {X, X, Y}}]*(E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}])*
         (-(a1^2*((-36 + 27*A2 + 7*A2^2)*E^(T1 + 2*T1*\[Omega][{{Y}, 
                   {X, X, Y}}]) + (25 + 2*A2 + A2^2)*E^(T1*\[Omega][
                 {{X, X}, {Y, Y}}]))) + A2*((36 + 39*A2 + 5*A2^2)*
             E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - (5 + 11*A2 + A2^2)*
             E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 
          a1*A2*((102 + 29*A2 + A2^2)*E^(T1 + 2*T1*\[Omega][{{Y}, 
                  {X, X, Y}}]) - (68 + 25*A2 + A2^2)*
             E^(T1*\[Omega][{{X, X}, {Y, Y}}]))) + 
        E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}])*
         (-3*A2*(2 + A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) + 
          2*a1*A2*((11 + 2*A2)*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
            (13 + 2*A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 
          a1^2*((36 + 17*A2 + A2^2)*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, 
                   Y}}]) - (30 + 7*A2 + A2^2)*E^(T1*\[Omega][{{X, X}, 
                 {Y, Y}}])))*\[Omega][{{X, X}, {Y, Y}}] - 
        (A2 + 2*a1*A2 + a1^2*(5 + A2))*
         E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][
             {{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^2)) + 
    A2*(108*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}])) - 
      72*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}])) + 
      36*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}])) + 
      90*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}])) - 60*a1^2*A2*
       E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{Y}, {X, X, Y}}])) + 30*A2^2*
       E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{Y}, {X, X, Y}}])) + 18*a1*A2^2*
       E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{Y}, {X, X, Y}}])) - 12*a1^2*A2^2*
       E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{Y}, {X, X, Y}}])) + 
      6*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}])) - 
      72*a1*E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}])) + 
      42*a1^2*E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}])) - 
      15*A2*E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}])) - 
      86*a1*A2*E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}])) + 46*a1^2*A2*
       E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{X, X}, {Y, Y}}])) - 20*A2^2*
       E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{X, X}, {Y, Y}}])) - 20*a1*A2^2*
       E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{X, X}, {Y, Y}}])) + 10*a1^2*A2^2*
       E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][
           {{X, X}, {Y, Y}}])) - 
      5*A2^3*E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}])) + 
      E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}])*
       (2*a1^2*(12 + 6*A2 + A2^2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) - 
        A2*(18 + 9*A2 + A2^2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) + 
        a1*(6*(6 + 5*A2 + A2^2)*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
          2*(30 + 15*A2 + 2*A2^2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])))*
       \[Omega][{{X, X}, {Y, Y}}] + (-4*a1 + 2*a1^2 - A2)*(3 + A2)*
       E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
       \[Omega][{{X, X}, {Y, Y}}]^2 - 
      8*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^3*
       (-3*a1 + 2*a1^2 - A2 - a1*\[Omega][{{X, X}, {Y, Y}}]) - 
      4*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^2*
       (-(A2*(11 + 2*A2)*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}])) + 
        a1^2*((22 + 4*A2)*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
          E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 
        a1*(-3*(11 + 2*A2)*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) + 
          (6 + 5*A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])) - 
        a1*((11 + 2*A2)*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) - 
          (5 + a1 + A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*
         \[Omega][{{X, X}, {Y, Y}}] + a1*E^(T1*\[Omega][{{X, X}, {Y, Y}}])*
         \[Omega][{{X, X}, {Y, Y}}]^2) - 2*\[Omega][{{Y}, {X, X, Y}}]*
       (E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}])*
         (-(A2*((36 + 17*A2 + A2^2)*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, 
                    Y}}]) - 5*(1 + A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))) + 
          a1^2*(2*(36 + 17*A2 + A2^2)*E^(T1 + 2*T1*\[Omega][{{Y}, 
                  {X, X, Y}}]) - (23 + 16*A2)*E^(T1*\[Omega][{{X, X}, 
                 {Y, Y}}])) + a1*(-3*(36 + 17*A2 + A2^2)*
             E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}]) + (48 + 46*A2 + 5*A2^2)*
             E^(T1*\[Omega][{{X, X}, {Y, Y}}]))) + 
        E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}])*
         (-2*a1^2*(5 + A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) + 
          A2*(6 + A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) + 
          a1*(-((36 + 17*A2 + A2^2)*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, 
                    Y}}])) + (40 + 13*A2 + A2^2)*E^(T1*\[Omega][
                {{X, X}, {Y, Y}}])))*\[Omega][{{X, X}, {Y, Y}}] - 
        (3*a1^2 - A2 - a1*(8 + A2))*E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            \[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^2)))/
   (E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
       \[Omega][{{X, X}, {Y, Y}}]))*(1 + 2*\[Omega][{{X}, {X, Y, Y}}])*
    (A2 + 2*\[Omega][{{X}, {X, Y, Y}}])*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{Y}, {X, X, Y}}])*(2 + A2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
     2*\[Omega][{{Y}, {X, X, Y}}])*(3 + A2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
     2*\[Omega][{{Y}, {X, X, Y}}])*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] - 
     \[Omega][{{X, X}, {Y, Y}}])*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{X, X}, {Y, Y}}])*(1 + A2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{X, X}, {Y, Y}}])), 
  (36*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}])) + 
    36*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}])) + 
    30*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}])) + 
    30*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}])) + 
    6*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}])) + 
    6*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}])) - 
    36*a1*E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}])) + 
    6*a1^2*E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}])) - 
    15*A2*E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}])) - 
    68*a1*A2*E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}])) + 
    28*a1^2*A2*E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}])) - 
    20*A2^2*E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}])) - 
    20*a1*A2^2*E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}])) + 10*a1^2*A2^2*
     E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])) - 
    5*A2^3*E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}])) + 
    16*a1*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}])*
     (E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) - 
      E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^4 + 
    36*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{X, X}, {Y, Y}}] + 
    30*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{X, X}, {Y, Y}}] + 
    6*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{X, X}, {Y, Y}}] - 
    36*a1*E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}] - 
    18*A2*E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}] + 
    6*a1*A2*E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}] - 
    24*a1^2*A2*E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}] - 
    9*A2^2*E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}] + 
    8*a1*A2^2*E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}] - 
    10*a1^2*A2^2*E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}] - 
    A2^3*E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}] - 
    6*a1^2*E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^2 - 
    3*A2*E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^2 + 
    2*a1*A2*E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^2 - 
    4*a1^2*A2*E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^2 - 
    A2^2*E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^2 + 
    8*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{X}, {X, Y, Y}}]^3*
     (a1*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) + 
      A2*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) - 
      a1*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) + 
      a1^2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) - 
      2*a1*A2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) + 
      2*a1^2*A2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) + 
      2*a1*(E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) + 
        (-1 + a1)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*
       \[Omega][{{Y}, {X, X, Y}}] + 
      a1*(E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) + 
        (-1 + a1)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*
       \[Omega][{{X, X}, {Y, Y}}]) + 
    8*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^3*
     (a1^2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) + 
      A2*(E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) - 
        E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 2*a1*(6 + A2)*
       (E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) - 
        E^(T1*\[Omega][{{X, X}, {Y, Y}}])) - 
      a1*(-E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) + 
        (1 + a1)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*
       \[Omega][{{X, X}, {Y, Y}}]) + 4*\[Omega][{{Y}, {X, X, Y}}]^2*
     (E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}])*
       (3*a1^2*(2 + A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) + 
        A2*((11 + 2*A2)*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) - 
          (9 + 2*A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 
        a1*((47 + 19*A2 + A2^2)*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) - 
          (47 + 21*A2 + A2^2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))) + 
      E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}])*
       (-2*A2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) - a1^2*(5 + 3*A2)*
         E^(T1*\[Omega][{{X, X}, {Y, Y}}]) + 
        a1*((11 + 2*A2)*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) - 
          11*E^(T1*\[Omega][{{X, X}, {Y, Y}}])))*\[Omega][{{X, X}, {Y, Y}}] - 
      a1^2*E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^2) + 
    2*\[Omega][{{Y}, {X, X, Y}}]*(E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}])*
       (a1^2*(11 + 19*A2 + 2*A2^2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) + 
        A2*((36 + 17*A2 + A2^2)*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) - 
          (23 + 14*A2 + A2^2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 
        a1*((72 + 47*A2 + 7*A2^2)*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) - 
          (72 + 67*A2 + 9*A2^2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))) + 
      E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}])*
       (-3*A2*(4 + A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) - 
        2*a1^2*(3 + 9*A2 + A2^2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) + 
        a1*((36 + 17*A2 + A2^2)*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) + 
          (-36 + 3*A2 + A2^2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])))*
       \[Omega][{{X, X}, {Y, Y}}] - (A2 + a1^2*(5 + A2))*
       E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
       \[Omega][{{X, X}, {Y, Y}}]^2) + 
    4*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{X}, {X, Y, Y}}]^2*
     (11*a1*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) + 
      11*A2*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) + 
      2*a1*A2*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) + 
      2*A2^2*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) - 
      11*a1*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) + 
      6*a1^2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) - 
      22*a1*A2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) + 
      17*a1^2*A2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) - 
      2*a1*A2^2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) + 
      2*a1^2*A2^2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) + 
      4*a1*(3*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) + 
        (-3 + 2*a1)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*
       \[Omega][{{Y}, {X, X, Y}}]^2 + 
      a1*((11 + 2*A2)*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) - 
        (11 + a1*(-5 + A2))*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*
       \[Omega][{{X, X}, {Y, Y}}] - a1^2*E^(T1*\[Omega][{{X, X}, {Y, Y}}])*
       \[Omega][{{X, X}, {Y, Y}}]^2 + 2*\[Omega][{{Y}, {X, X, Y}}]*
       (3*A2*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) + 
        a1^2*(8 + 5*A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) + 
        2*a1*((7 + A2)*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) - 
          (7 + 3*A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 
        a1*(3*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) + 
          (-3 + a1)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*
         \[Omega][{{X, X}, {Y, Y}}])) + 2*\[Omega][{{X}, {X, Y, Y}}]*
     (E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}])*
       (a1^2*(11 + 43*A2 + 12*A2^2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) + 
        A2*((36 + 17*A2 + A2^2)*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) - 
          5*(1 + A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 
        a1*((36 + 17*A2 + A2^2)*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) - 
          (36 + 73*A2 + 17*A2^2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))) + 
      8*a1*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}])*
       (3*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) + 
        (-3 + a1)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*
       \[Omega][{{Y}, {X, X, Y}}]^3 + 
      E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}])*
       (-(A2*(6 + A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])) - 
        2*a1^2*(-3 + 5*A2 + A2^2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) + 
        a1*((36 + 17*A2 + A2^2)*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) + 
          (-36 - A2 + A2^2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])))*
       \[Omega][{{X, X}, {Y, Y}}] - (A2 + a1^2*(5 + A2))*
       E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
       \[Omega][{{X, X}, {Y, Y}}]^2 + 
      4*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{Y}, {X, X, Y}}]^2*
       (a1^2*(8 + 3*A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) + 
        A2*(3*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) - 
          E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 
        a1*((25 + 4*A2)*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) - 
          (25 + 6*A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])) - 
        a1*(-3*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) + 
          (3 + a1)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*
         \[Omega][{{X, X}, {Y, Y}}]) + 2*\[Omega][{{Y}, {X, X, Y}}]*
       (E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}])*
         (2*a1^2*(9 + 12*A2 + A2^2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) + 
          A2*((22 + 4*A2)*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) - 
            (6 + A2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}])) + 
          a1*((58 + 21*A2 + A2^2)*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) - 
            (58 + 45*A2 + 3*A2^2)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))) + 
        2*E^(T1 + 2*T1*\[Omega][{{Y}, {X, X, Y}}])*
         (-(A2*E^(T1*\[Omega][{{X, X}, {Y, Y}}])) - 
          2*a1^2*A2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]) + 
          a1*((11 + 2*A2)*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) - 
            11*E^(T1*\[Omega][{{X, X}, {Y, Y}}])))*
         \[Omega][{{X, X}, {Y, Y}}] - 
        2*a1^2*E^(T1*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
            \[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^2)))/
   (E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}] + 
       \[Omega][{{X, X}, {Y, Y}}]))*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{Y}, {X, X, Y}}])*(1 + 2*\[Omega][{{Y}, {X, X, Y}}])*
    (2 + A2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
    (3 + A2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
    (1 + 2*\[Omega][{{X}, {X, Y, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
    (3 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
    (1 + A2 + 2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])), 
  -((-162*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])) - 
     459*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])) - 
     459*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])) - 
     189*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])) - 
     27*A2^5*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])) - 
     108*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
     36*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}])) - 
     36*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}])) - 
     282*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
     114*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}])) - 
     84*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}])) - 
     222*a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
     102*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}])) - 
     60*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}])) - 
     48*a1*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
     24*a1^2*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}])) - 
     12*A2^5*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}])) + 
     108*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}])) + 
     108*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}])) + 
     198*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}])) + 
     198*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}])) + 
     108*a1*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}])) + 
     108*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}])) + 
     18*a1*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}])) + 
     18*A2^5*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}])) + 
     324*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}])) - 
     216*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}])) + 
     108*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}])) + 
     594*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}])) - 
     396*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}])) + 
     198*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}])) + 
     324*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}])) - 
     216*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}])) + 
     108*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}])) + 
     54*a1*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}])) - 
     36*a1^2*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}])) + 
     18*A2^5*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}])) - 
     756*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] - 1701*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] - 1269*A2^3*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] - 351*A2^4*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] - 
     27*A2^5*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] - 504*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}] + 
     216*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] - 108*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}] - 
     1174*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] + 662*a1^2*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}] - 
     172*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] - 710*a1*A2^3*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}] + 
     454*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] - 
     68*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] - 112*a1*A2^4*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}] + 
     80*a1^2*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] - 
     4*A2^5*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] + 504*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] + 504*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] + 630*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] + 630*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] + 216*a1*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] + 216*A2^4*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] + 18*a1*A2^4*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] + 
     18*A2^5*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] + 1512*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] - 1080*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] + 216*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] + 2538*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] - 1824*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] + 318*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] + 1188*a1*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] - 864*a1^2*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] + 108*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] + 162*a1*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] - 120*a1^2*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] + 
     6*A2^5*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}] - 
     1278*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 - 2316*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 - 1236*A2^3*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 - 
     204*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 - 
     6*A2^5*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 - 852*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2 + 
     476*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 - 
     80*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 - 1896*a1*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2 + 
     1360*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 - 
     96*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 - 836*a1*A2^3*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2 + 
     668*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 - 
     16*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 - 80*a1*A2^4*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2 + 
     72*a1^2*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 + 852*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 + 
     852*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 + 692*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 + 
     692*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 + 132*a1*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 + 
     132*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 + 4*a1*A2^4*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 + 
     4*A2^5*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 + 2556*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 - 1992*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 + 
     132*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 + 3804*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 - 3012*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 + 
     156*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 + 1368*a1*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 - 1128*a1^2*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 + 
     24*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 + 120*a1*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 - 108*a1^2*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2 - 
     996*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 - 1464*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 - 
     504*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 - 
     36*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 - 664*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^3 + 
     472*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 - 
     16*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 - 1424*a1*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^3 + 
     1200*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 - 
     16*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 - 400*a1*A2^3*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^3 + 
     368*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 - 16*a1*A2^4*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^3 + 
     16*a1^2*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 + 664*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 + 
     664*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 + 312*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 + 
     312*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 + 24*a1*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 + 
     24*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 + 1992*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 - 1704*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 + 
     24*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 + 2592*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 - 2256*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 + 
     24*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 + 624*a1*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 - 576*a1^2*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 + 24*a1*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 - 24*a1^2*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3 - 
     360*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4 - 
     432*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4 - 
     72*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4 - 240*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^4 + 
     208*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4 - 496*a1*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^4 + 
     464*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4 - 64*a1*A2^3*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^4 + 
     64*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4 + 240*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4 + 
     240*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4 + 48*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4 + 
     48*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4 + 720*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4 - 672*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4 + 816*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4 - 768*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4 + 96*a1*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4 - 96*a1^2*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4 - 
     48*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5 - 
     48*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5 - 
     32*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5 + 32*a1^2*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^5 - 
     64*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5 + 64*a1^2*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^5 + 
     32*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5 + 
     32*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5 + 
     96*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5 - 96*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5 + 96*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5 - 96*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5 - 
     756*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] - 1701*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] - 1269*A2^3*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] - 351*A2^4*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] - 
     27*A2^5*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] - 432*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}] + 
     144*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] - 108*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}] - 
     706*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] + 194*a1^2*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}] - 
     172*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] - 350*a1*A2^3*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}] + 
     94*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] - 
     68*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] - 40*a1*A2^4*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}] + 
     8*a1^2*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] - 
     4*A2^5*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] + 432*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] + 216*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] + 714*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] + 318*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] + 324*a1*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] + 108*A2^4*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] + 42*a1*A2^4*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] + 
     6*A2^5*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] + 1512*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] - 1008*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] + 504*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] + 1890*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] - 1260*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] + 630*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] + 648*a1*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] - 432*a1^2*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] + 216*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] + 54*a1*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] - 36*a1^2*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] + 
     18*A2^5*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] - 
     3276*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] - 
     5376*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] - 
     2784*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] - 
     492*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] - 
     24*A2^5*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] - 
     1848*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     808*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] - 
     304*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] - 
     2220*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     1052*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] - 
     240*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] - 
     712*a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     376*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] - 
     32*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] - 
     40*a1*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     24*a1^2*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     1848*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     840*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     1988*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     728*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     564*a1*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     132*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     40*a1*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     4*A2^5*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     6552*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] - 
     4704*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     840*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     7224*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] - 
     5236*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     728*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     2124*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] - 
     1560*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     132*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     156*a1*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] - 
     116*a1^2*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     4*A2^5*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] - 
     5004*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] - 
     5984*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] - 
     2048*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] - 
     208*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] - 
     4*A2^5*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] - 
     2768*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] + 
     1616*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] - 
     192*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] - 
     2648*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] + 
     1848*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] - 
     64*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] - 
     536*a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] + 
     440*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] - 
     16*a1*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] + 
     16*a1^2*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] + 
     2768*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] + 
     1064*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] + 
     1896*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] + 
     512*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] + 
     304*a1*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] + 
     40*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] + 
     8*a1*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] + 
     10008*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] - 
     7904*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] + 
     424*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] + 
     9016*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] - 
     7336*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] + 
     224*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] + 
     1968*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] - 
     1688*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] + 
     16*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] + 
     104*a1*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] - 
     96*a1^2*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] - 
     3472*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] - 
     2896*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] - 
     576*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] - 
     24*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] - 
     1872*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] + 
     1392*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] - 
     32*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] - 
     1312*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] + 
     1152*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] - 
     128*a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] + 
     128*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] + 
     1872*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] + 
     544*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] + 
     736*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] + 
     112*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] + 
     48*a1*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] + 
     6944*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] - 
     6032*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] + 
     64*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] + 
     4752*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] - 
     4288*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] + 
     16*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] + 
     656*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] - 
     624*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] + 
     16*a1*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] - 
     16*a1^2*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}] - 
     1104*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}] - 
     576*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}] - 
     48*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}] - 
     576*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}] + 
     512*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}] - 
     224*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}] + 
     224*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}] + 
     576*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}] + 
     96*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}] + 
     96*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}] + 
     2208*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}] - 
     2080*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}] + 
     1024*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}] - 
     992*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}] + 
     64*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}] - 
     64*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}] - 
     128*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}] - 
     32*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}] - 
     64*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}] + 
     64*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}] + 
     64*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}] + 
     256*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}] - 
     256*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}] + 
     64*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}] - 
     64*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}] - 
     1278*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 2316*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 1236*A2^3*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 
     204*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 
     6*A2^5*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 564*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     188*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 
     80*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 636*a1*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     100*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 
     96*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 188*a1*A2^3*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     20*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 
     16*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 8*a1*A2^4*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     564*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 + 
     132*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 + 792*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 + 
     156*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 + 240*a1*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 + 
     24*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 + 12*a1*A2^4*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 + 2556*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 1704*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 + 
     852*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 + 2076*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 1384*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 + 
     692*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 + 396*a1*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 264*a1^2*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 + 
     132*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 + 12*a1*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 8*a1^2*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 + 
     4*A2^5*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 
     5004*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     5984*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     2048*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     208*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     4*A2^5*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     2104*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     952*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     192*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     1320*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     520*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     64*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     168*a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     72*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     2104*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     424*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     1680*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     224*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     280*a1*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     16*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     8*a1*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     10008*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     7240*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     1064*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     7072*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     5176*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     512*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     1176*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     872*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     40*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     32*a1*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     24*a1^2*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     6512*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     5168*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     1056*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     48*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     2496*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     1600*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     64*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     1024*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     768*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     64*a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     64*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     2496*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     368*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     1104*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     80*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     80*a1*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     13024*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     10528*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     368*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     7104*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     6000*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     80*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     848*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     768*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     16*a1*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     16*a1^2*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     3648*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     1760*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     160*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     1184*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     992*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     256*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     256*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     1184*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     96*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     224*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     7296*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     6528*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     32*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     2688*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     2528*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     160*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     160*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     864*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     192*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     192*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     192*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     192*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     1728*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     1664*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     320*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     320*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     64*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     128*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     128*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     996*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3 - 1464*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3 - 
     504*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3 - 
     36*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3 - 288*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     96*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3 - 
     16*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3 - 240*a1*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     16*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3 - 
     16*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3 - 32*a1*A2^3*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     288*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3 + 
     24*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3 + 336*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3 + 
     24*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3 + 48*a1*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3 + 1992*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3 - 1328*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3 + 
     664*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3 + 936*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3 - 624*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3 + 
     312*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3 + 72*a1*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3 - 48*a1^2*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3 + 
     24*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3 - 
     3472*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     2896*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     576*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     24*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     912*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     432*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     32*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     240*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     80*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     912*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     64*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     464*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     16*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     32*a1*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     6944*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     5072*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     544*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     2832*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     2096*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     112*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     192*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     144*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     3648*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     1760*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     160*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     768*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     576*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     96*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     96*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     768*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     32*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     160*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     7296*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     6112*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     96*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     2144*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     1920*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     96*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     96*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     1472*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     320*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     192*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     192*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     192*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     2944*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     2752*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     448*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     448*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     192*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     384*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     384*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     360*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4 - 
     432*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4 - 
     72*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4 - 
     48*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4 + 16*a1^2*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, Y}}]^4 - 
     32*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4 + 
     48*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4 + 48*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4 + 720*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4 - 480*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4 + 
     240*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4 + 144*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4 - 96*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4 + 
     48*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4 - 
     1104*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4 - 
     576*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4 - 
     48*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4 - 
     128*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4 + 
     64*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4 + 
     128*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4 + 
     32*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4 + 
     2208*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4 - 
     1632*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4 + 
     96*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4 + 
     384*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4 - 
     288*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4 - 
     864*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^4 - 
     192*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^4 - 
     64*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^4 + 
     64*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^4 + 
     64*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^4 + 
     1728*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^4 - 
     1536*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^4 + 
     192*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^4 - 
     192*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^4 - 
     192*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^4 + 
     384*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^4 - 
     384*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^4 - 
     48*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^5 - 
     48*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^5 + 
     96*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^5 - 64*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^5 + 
     32*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^5 - 
     128*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^5 - 
     32*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^5 + 
     256*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^5 - 
     192*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^5 - 
     64*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^5 + 
     128*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^5 - 
     128*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^5 - 
     324*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] - 
     432*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] - 918*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] - 900*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] - 918*a1*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] - 630*A2^3*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] - 378*a1*A2^3*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] - 180*A2^4*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] - 54*a1*A2^4*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] - 
     18*A2^5*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] - 
     216*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] + 
     72*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] - 
     72*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] - 924*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}] + 
     294*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] - 315*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}] - 
     1150*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] + 368*a1^2*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}] - 
     391*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] - 488*a1*A2^3*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}] + 
     142*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] - 173*A2^4*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}] - 
     64*a1*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] + 14*a1^2*A2^4*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}] - 
     25*A2^5*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] + 
     216*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] + 
     216*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] + 756*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] + 648*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] + 768*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] + 570*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] + 306*a1*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] + 198*A2^4*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] + 42*a1*A2^4*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] + 
     24*A2^5*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] + 
     324*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] - 108*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] + 
     216*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] + 1134*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] - 378*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] + 648*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] + 990*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] - 222*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] + 570*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] + 324*a1*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] - 18*a1^2*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] + 198*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] + 36*a1*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] + 6*a1^2*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] + 
     24*A2^5*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}] - 
     1512*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     1656*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     3402*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     2652*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     2538*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     1338*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     702*a1*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     246*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     54*a1*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     12*A2^5*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     1008*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     432*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     216*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     3788*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     1722*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     718*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     3784*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     1782*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     610*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     1328*a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     650*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     166*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     150*a1*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     80*a1^2*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     10*A2^5*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     1008*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     1008*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     2700*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     2196*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     2020*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     1390*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     534*a1*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     318*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     38*a1*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     20*A2^5*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     1512*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     648*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     432*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     4986*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     2334*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     1140*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     3774*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     1574*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     742*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     1044*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     378*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     162*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     96*a1*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     34*a1^2*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     8*A2^5*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     2556*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     2400*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     4632*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     2824*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     2472*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     952*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     408*a1*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     98*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     12*a1*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     2*A2^5*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     1704*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     952*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     160*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     5960*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     3604*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     460*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     4800*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     3028*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     276*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     1272*a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     860*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     40*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     92*a1*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     72*a1^2*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     1704*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     1704*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     3552*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     2700*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     1860*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     1168*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     288*a1*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     156*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     8*a1*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     4*A2^5*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     2556*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     1428*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     264*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     7776*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     4744*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     620*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     4676*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     2756*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     280*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     936*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     556*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     32*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     52*a1*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     36*a1^2*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     1992*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     1640*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     2928*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     1328*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     1008*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     264*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     72*a1*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     12*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     1328*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     944*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     32*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     4384*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     3288*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     88*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     2760*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     2128*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     40*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     488*a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     408*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     16*a1*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     16*a1^2*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     1328*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     1328*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     2160*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     1496*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     712*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     400*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     48*a1*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     24*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     1992*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     1416*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     48*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     5536*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     4168*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     104*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     2424*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     1808*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     32*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     304*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     240*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     8*a1*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     8*a1^2*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     720*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     528*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     864*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     264*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     144*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     24*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     480*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     416*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     1504*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     1328*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     704*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     624*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     64*a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     64*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     480*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     480*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     608*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     368*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     96*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     48*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     720*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     624*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     1824*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     1616*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     512*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     448*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     32*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     32*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     96*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] - 
     64*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] - 
     96*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] - 
     16*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] - 
     64*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] + 
     64*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] - 
     192*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] + 
     192*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] - 
     64*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] + 
     64*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] + 
     64*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] + 
     64*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] + 
     64*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] + 
     32*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] + 
     96*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] - 
     96*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] + 
     224*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] - 
     224*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] + 
     32*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] - 
     32*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] - 
     1512*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     1656*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     3402*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     2652*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     2538*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     1338*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     702*a1*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     246*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     54*a1*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     12*A2^5*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     864*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     288*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     216*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     2636*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     570*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     718*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     2452*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     450*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     610*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     788*a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     110*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     166*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     78*a1*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     8*a1^2*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     10*A2^5*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     864*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     432*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     2652*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     1140*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     2200*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     742*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     666*a1*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     162*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     62*a1*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     8*A2^5*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     1512*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     504*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     1008*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     3690*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     990*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     2196*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     2262*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     242*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     1390*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     468*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     66*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     318*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     24*a1*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     14*a1^2*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     20*A2^5*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     6552*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     5376*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 10752*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 6272*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 5568*a1*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 2120*A2^3*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 984*a1*A2^3*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 220*A2^4*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 48*a1*A2^4*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     4*A2^5*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     3696*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 1616*a1^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     608*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 8632*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     3104*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 1328*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     5732*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 1932*a1^2*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     680*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 1316*a1*A2^3*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     452*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 100*A2^4*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     80*a1*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 32*a1^2*A2^4*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     4*A2^5*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     3696*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     1680*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 8168*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 2936*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 4788*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 1280*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 932*a1*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 164*A2^4*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 48*a1*A2^4*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     4*A2^5*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     6552*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 2856*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     1680*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 14448*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 6280*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 2936*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 7600*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 2812*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 1280*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 1320*a1*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 388*a1^2*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 164*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 56*a1*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 8*a1^2*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     4*A2^5*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 10008*a1*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     6360*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 11968*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 5056*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 4096*a1*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 1008*A2^3*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 416*a1*A2^3*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     44*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 8*a1*A2^4*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     5536*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 3232*a1^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     384*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 10504*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     5632*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 648*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     5096*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 2768*a1^2*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     208*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 768*a1*A2^3*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     456*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     16*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 24*a1*A2^4*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     16*a1^2*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     5536*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     2128*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 9000*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 2536*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 3528*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 680*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 392*a1*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     40*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 8*a1*A2^4*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 10008*a1*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 5800*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     848*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 18784*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 11344*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 1144*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 7432*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 4336*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 312*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 840*a1*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 480*a1^2*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     16*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 16*a1*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 8*a1^2*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     6944*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     3392*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 5792*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 1632*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 1152*a1*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 144*A2^3*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 48*a1*A2^3*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     3744*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 2784*a1^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^3*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     64*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 5552*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^3*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     3888*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     96*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 1856*a1*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^3*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     1328*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     16*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 144*a1*A2^3*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^3*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     112*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     3744*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     1088*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 4400*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 864*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 1024*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 112*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 48*a1*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     6944*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 5120*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     128*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 10496*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 8016*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 128*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 2720*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 2064*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     16*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 144*a1*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 112*a1^2*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     2208*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     800*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 1152*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 176*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 96*a1*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     1152*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 1024*a1^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^4*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     1184*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 992*a1^2*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^4*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     224*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 192*a1^2*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^4*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     1152*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     192*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 928*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     96*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 96*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     2208*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 1952*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 2496*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 2240*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 320*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 288*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     256*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     64*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 64*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     128*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 128*a1^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^5*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     64*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 64*a1^2*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^5*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     128*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 64*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     256*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 256*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 192*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 192*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     2556*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     2400*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     4632*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     2824*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     2472*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     952*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     408*a1*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     98*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     12*a1*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     2*A2^5*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     1128*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     376*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     160*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     2720*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     364*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     460*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     1956*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     184*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     276*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     444*a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     32*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     40*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     20*a1*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     1128*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     264*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     3032*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     620*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     1920*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     280*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     380*a1*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     32*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     16*a1*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     2556*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     852*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     1704*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     4320*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     768*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     2700*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     1724*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     136*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     1168*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     192*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     96*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     156*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     4*a1*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     4*a1^2*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     4*A2^5*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     10008*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 
     6360*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 11968*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 5056*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 4096*a1*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 1008*A2^3*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 416*a1*A2^3*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 
     44*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 8*a1*A2^4*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 
     4208*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 1904*a1^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     384*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 6720*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     1848*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 648*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     2952*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 624*a1^2*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     208*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 408*a1*A2^3*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     96*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 
     16*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 8*a1*A2^4*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     4208*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     848*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 7440*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 1144*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 3096*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 312*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 360*a1*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     16*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 8*a1*A2^4*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 10008*a1*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 4472*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     2128*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 14896*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 5896*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 2536*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 5032*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 1504*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 680*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 464*a1*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 72*a1^2*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     40*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 8*a1*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 13024*a1*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 
     5728*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 10336*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 2736*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 2112*a1*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 240*A2^3*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 96*a1*A2^3*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 
     4992*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 3200*a1^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     128*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 5904*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     2832*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 240*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     1632*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 704*a1^2*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     48*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 112*a1*A2^3*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     48*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     4992*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     736*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 6064*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 624*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 1520*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     80*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 80*a1*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 13024*a1*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 8032*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     736*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 15328*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 9264*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 624*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 3504*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 1984*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     80*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 160*a1*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 80*a1^2*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 
     7296*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 
     2080*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 3520*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 448*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 320*a1*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 
     2368*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 1984*a1^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^3*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     1984*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 1248*a1^2*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^3*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     32*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 288*a1*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^3*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     160*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     2368*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     192*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 1920*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     96*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 224*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     7296*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 5760*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     64*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 6016*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 4704*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     32*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 672*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 512*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 
     1728*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 
     256*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 384*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 
     384*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 384*a1^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^4*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     192*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 128*a1^2*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^4*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     384*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 192*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     1728*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 1600*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 768*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 704*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 
     128*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     128*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 128*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 
     1992*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     1640*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     2928*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     1328*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     1008*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     264*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     72*a1*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     12*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     576*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     192*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     32*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     1176*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     80*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     88*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     664*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     32*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     40*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     80*a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     576*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     48*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     1368*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     104*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     616*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     32*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     64*a1*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     1992*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     664*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     1328*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     2224*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     64*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     1496*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     504*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     208*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     400*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     24*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     24*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     24*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     6944*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 
     3392*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 5792*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 1632*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 1152*a1*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 144*A2^3*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 48*a1*A2^3*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 
     1824*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 864*a1^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     64*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 2032*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     368*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 
     96*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 592*a1*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     64*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 
     16*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 32*a1*A2^3*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     1824*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 
     128*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 2480*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 128*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 656*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 
     16*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 32*a1*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 
     6944*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 3200*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 
     1088*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 6656*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 2256*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 864*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 1248*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 224*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 112*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 48*a1*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 
     7296*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 
     2080*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 3520*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 448*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 320*a1*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 
     1536*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 1152*a1^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     1184*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 448*a1^2*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     32*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 160*a1*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     32*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 
     1536*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 
     64*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 1312*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 
     32*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 160*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 
     7296*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 4928*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 
     192*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 4928*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 3008*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 
     96*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 480*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 256*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 
     2944*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 
     384*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 640*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 
     384*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 384*a1^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^3*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     192*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 64*a1^2*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^3*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     384*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 192*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 
     2944*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 2560*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 1024*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 832*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 
     384*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 
     384*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 384*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 
     720*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     528*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     864*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     264*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     144*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     24*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     96*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     32*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     176*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     80*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     96*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     208*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     64*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     720*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     240*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     480*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     480*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     128*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     368*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     48*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     48*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     48*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     2208*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] - 
     800*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] - 1152*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] - 176*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] - 96*a1*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] - 
     256*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] + 128*a1^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     192*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] - 32*a1*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] + 
     256*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] + 256*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] + 32*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] + 
     2208*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] - 1056*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] + 
     192*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] + 1216*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] - 288*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] + 
     96*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] + 96*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] - 
     1728*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] - 
     256*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] - 384*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] - 
     128*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] + 128*a1^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}] - 
     64*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] + 
     128*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] + 64*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] + 
     1728*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] - 1344*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] + 512*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] - 320*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] - 
     384*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] + 
     384*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] - 384*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}] - 
     96*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] - 
     64*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] - 
     96*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] - 
     16*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] + 
     96*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] - 
     32*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] + 
     64*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] + 
     32*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] + 
     32*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] + 
     32*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}] - 
     256*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^5*
      \[Omega][{{X, X}, {Y, Y}}] - 
     64*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^5*
      \[Omega][{{X, X}, {Y, Y}}] - 64*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^5*
      \[Omega][{{X, X}, {Y, Y}}] + 
     256*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^5*
      \[Omega][{{X, X}, {Y, Y}}] - 128*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^5*
      \[Omega][{{X, X}, {Y, Y}}] + 64*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^5*
      \[Omega][{{X, X}, {Y, Y}}] - 
     128*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^5*
      \[Omega][{{X, X}, {Y, Y}}] + 
     128*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^5*
      \[Omega][{{X, X}, {Y, Y}}] - 128*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^5*
      \[Omega][{{X, X}, {Y, Y}}] - 
     864*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     396*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 1800*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     582*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 1260*a1*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     294*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 360*a1*A2^3*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     57*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 36*a1*A2^4*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     3*A2^5*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     720*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     132*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     294*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 1844*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     328*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     758*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 1288*a1*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     128*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     580*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 228*a1*A2^3*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     72*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     150*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 8*a1*A2^4*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     28*a1^2*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     10*A2^5*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     720*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     504*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 1536*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     888*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 1080*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     510*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 306*a1*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     108*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 30*a1*A2^4*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     6*A2^5*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     864*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     144*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     504*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 1512*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 24*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     888*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 840*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 240*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     510*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 162*a1*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 144*a1^2*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     108*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 6*a1*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 24*a1^2*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     6*A2^5*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     3312*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1224*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     5304*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1346*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     2676*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     458*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     492*a1*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     49*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     24*a1*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     A2^5*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     2880*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     796*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     748*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     6168*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1892*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1380*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     3600*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1072*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     684*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     592*a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     112*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     90*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     12*a1*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     12*a1^2*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     2*A2^5*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     2880*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1872*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     4616*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     2420*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     2316*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     926*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     426*a1*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     108*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     22*a1*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     2*A2^5*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     3312*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     864*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1008*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     5016*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     688*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1412*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     2284*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     20*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     542*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     354*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     84*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     60*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     14*a1*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     8*a1^2*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     2*A2^5*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     4800*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1420*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     5648*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1080*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1904*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     216*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     196*a1*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     10*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     4*a1*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     4336*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1768*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     536*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     8080*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     3816*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     752*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     3592*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1816*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     204*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     396*a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     200*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     8*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     4*a1*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     4336*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     2632*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     5016*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     2316*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1684*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     516*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     180*a1*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     24*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     4*a1*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     4800*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1904*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     616*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     6016*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1888*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     684*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     2012*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     500*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     148*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     196*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     36*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     8*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     4*a1*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     3280*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     760*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     2656*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     352*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     528*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     32*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     24*a1*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     3072*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1776*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     112*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     5008*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     3248*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     128*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1472*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1048*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     8*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     72*a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     56*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     3072*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1744*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     2416*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     920*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     488*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     88*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     24*a1*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     3280*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1888*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     112*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     3232*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1656*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     104*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     688*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     352*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     8*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     32*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     16*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1056*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     184*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     528*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     40*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     48*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1024*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     800*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1456*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1200*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     208*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     192*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1024*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     544*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     496*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     128*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     48*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1056*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     832*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     768*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     560*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     80*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     64*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     128*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     16*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     32*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     128*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     128*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     160*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     160*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     128*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     64*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     32*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     128*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     128*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     64*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     64*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     3312*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1224*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     5304*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1346*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     2676*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     458*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     492*a1*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     49*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     24*a1*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     A2^5*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     2448*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     364*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     748*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     4728*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     452*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1380*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     2556*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     28*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     684*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     376*a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     104*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     90*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     12*a1*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     12*a1^2*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     2*A2^5*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     2448*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1008*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     4328*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1412*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     2304*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     542*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     438*a1*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     60*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     22*a1*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     2*A2^5*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     3312*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     432*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1872*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     4152*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     464*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     2420*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1492*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     824*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     926*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     150*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     276*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     108*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     2*a1*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     20*a1^2*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     2*A2^5*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     10752*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     2984*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 12544*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 2280*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 4240*a1*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     456*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 440*a1*A2^3*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     20*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 8*a1*A2^4*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     8384*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 2000*a1^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1696*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 11832*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     2504*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 1904*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     4620*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 844*a1^2*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     520*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 448*a1*A2^3*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     24*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     32*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 8*a1*A2^4*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     8384*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     2960*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 10536*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 2760*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 3748*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     616*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 412*a1*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     32*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 8*a1*A2^4*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     10752*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 2368*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     2960*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 11264*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 728*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 2760*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 3256*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 492*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     616*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 276*a1*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 136*a1^2*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     32*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 4*a1*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 4*a1^2*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     12720*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     2568*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 10112*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 1192*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 2016*a1*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     104*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 88*a1*A2^3*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     10416*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 3872*a1^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1040*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 11048*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     4328*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     704*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 2792*a1*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1168*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     88*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 136*a1*A2^3*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     56*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     10416*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     3024*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 8840*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 1672*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 1880*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     168*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 88*a1*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     12720*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 4560*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     1392*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 10208*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 2688*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     856*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 1944*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 312*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     88*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 80*a1*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     6784*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     912*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 3264*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     192*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 288*a1*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     5856*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 3168*a1^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^3*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     192*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 4368*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^3*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     2576*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     64*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 544*a1*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^3*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     352*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     5856*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     1280*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 3024*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     320*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 288*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     6784*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 3680*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     192*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 3680*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 1776*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     64*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 336*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 144*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     1600*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     112*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 352*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     1472*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 1088*a1^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^4*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     608*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 480*a1^2*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^4*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1472*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     192*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 352*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     1600*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 1216*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 448*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 320*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     128*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     128*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     128*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     128*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     128*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     128*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     4800*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1420*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     5648*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1080*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1904*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     216*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     196*a1*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     10*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     4*a1*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     2896*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     328*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     536*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     4408*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     144*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     752*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1708*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     68*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     204*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     156*a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     40*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     8*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     4*a1*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     2896*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     616*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     4128*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     684*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1512*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     148*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     160*a1*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     8*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     4*a1*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     4800*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     464*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     2632*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     4000*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1016*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     2316*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     812*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     872*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     516*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     32*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     148*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     24*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     4*a1^2*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     12720*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     2568*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 10112*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 1192*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 2016*a1*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     104*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 88*a1*A2^3*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     8160*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 1616*a1^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1040*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 7680*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     960*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     704*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 1800*a1*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     176*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     88*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 80*a1*A2^3*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     8160*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     1392*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 7520*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     856*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 1632*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     88*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 80*a1*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     12720*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 2304*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     3024*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 8576*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 264*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 1672*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 1392*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 488*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     168*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 48*a1*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 40*a1^2*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     11456*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     1456*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 5472*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     304*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 480*a1*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     7712*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 2592*a1^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     480*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 4512*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1376*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     160*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 512*a1*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     160*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     7712*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     928*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 4224*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     256*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 416*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     11456*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 3744*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     928*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 5280*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 1056*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     256*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 432*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 16*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     4160*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     256*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 896*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     2944*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 1472*a1^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^3*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     64*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 864*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^3*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     416*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     2944*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     192*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 736*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     4160*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 2176*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     64*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 960*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 416*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     512*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     384*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     256*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     384*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     512*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     384*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     3280*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     760*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     2656*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     352*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     528*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     32*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     24*a1*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1392*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     96*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     112*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1736*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     24*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     128*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     384*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     40*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     8*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     16*a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1392*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     112*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1576*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     104*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     336*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     8*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     16*a1*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     3280*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     208*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1744*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1600*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     816*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     920*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     136*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     352*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     88*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     24*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     6784*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     912*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 3264*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     192*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 288*a1*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     3104*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     416*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     192*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 1904*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     112*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     64*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 192*a1*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     3104*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     192*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 1904*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     64*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 192*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     6784*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     928*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     1280*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 2592*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 432*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     320*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 176*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 112*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     4160*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     256*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 896*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     1984*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     512*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     64*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 544*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     96*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     1984*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     64*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 544*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     4160*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 1216*a1^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     192*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 832*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 96*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     768*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     384*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     128*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     384*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     768*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     384*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     1056*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     184*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     528*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     40*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     48*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     224*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     240*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     16*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     16*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     224*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     208*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     16*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     1056*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     32*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     544*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     224*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     272*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     128*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     48*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     1600*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     112*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 352*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     384*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 128*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     384*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 128*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     1600*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     128*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     192*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 256*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 96*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     512*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     128*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     128*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     512*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     128*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     128*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     16*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     32*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     128*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     64*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     32*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     128*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^5*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     128*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^5*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     792*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     144*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 1164*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     156*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 588*a1*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     54*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 114*a1*A2^3*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     6*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 6*a1*A2^4*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     864*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     432*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 1224*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     180*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     702*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 264*a1*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     360*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     312*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 48*a1*A2^3*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     138*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     45*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 8*a1*A2^4*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     10*a1^2*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     A2^5*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     864*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     360*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 1296*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     408*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 660*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     150*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 126*a1*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     18*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 6*a1*A2^4*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     792*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     72*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     360*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 948*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 348*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     408*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 372*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 288*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     150*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 48*a1*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 78*a1^2*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     18*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 6*a1^2*A2^4*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     2448*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     360*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     2692*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     268*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     916*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     54*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     98*a1*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     2*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     2*a1*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     2880*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     288*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     864*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     3360*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     84*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     936*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     780*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     320*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     238*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     16*a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     98*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     12*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     2*a1*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     4*a1^2*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     2880*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     1008*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     3216*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     796*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     1096*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     170*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     114*a1*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     6*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     2*a1*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     2448*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     720*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     2260*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     524*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     556*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     640*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     324*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     122*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     52*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     50*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     6*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     2*a1^2*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     2840*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     320*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     2160*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     144*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     432*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     12*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     20*a1*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     3648*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     864*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     528*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     3288*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     752*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     332*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     560*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     20*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     32*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     8*a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     16*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     3648*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     1016*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     2800*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     484*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     556*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     40*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     24*a1*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     2840*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     344*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     440*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     1896*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     112*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     212*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     340*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     76*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     24*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     12*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     8*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     1520*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     120*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     704*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     24*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     64*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     2176*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     928*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     96*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     1344*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     632*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     24*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     104*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     40*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     2176*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     432*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     1008*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     88*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     88*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     1520*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     448*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     80*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     656*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     104*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     24*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     56*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     368*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     16*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     80*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     608*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     416*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     192*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     144*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     608*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     64*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     128*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     368*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     208*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     80*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     32*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     32*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     64*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     64*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     64*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     32*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     32*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     2448*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     360*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     2692*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     268*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     916*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     54*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     98*a1*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     2*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     2*a1*A2^4*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     2448*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     144*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     864*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     2784*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     492*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     936*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     624*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     476*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     238*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     28*a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     86*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     12*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     2*a1*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     4*a1^2*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     2448*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     720*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     2784*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     556*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     964*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     122*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     102*a1*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     6*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     2*a1*A2^4*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     2448*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     432*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     1008*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     2116*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     1100*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     796*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     520*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     576*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     170*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     28*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     86*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     6*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     2*a1^2*A2^4*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     5968*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     640*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 4560*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     288*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 912*a1*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     24*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 40*a1*A2^3*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     6432*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     288*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     1344*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 5256*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     152*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     832*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 828*a1*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     300*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     88*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 20*a1*A2^3*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     28*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     6432*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     1456*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 5048*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     696*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 1020*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     64*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 44*a1*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     5968*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     464*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     1456*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 3744*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 1304*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     696*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 592*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 428*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     64*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 16*a1*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 28*a1^2*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     5136*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     360*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 2384*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     72*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 208*a1*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     6000*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 1104*a1^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     576*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 3224*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     592*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     152*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 248*a1*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     8*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     6000*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     944*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 2840*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     200*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 248*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     5136*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     240*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     592*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 2064*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 320*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     136*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 152*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 64*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     1824*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     64*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 384*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     2336*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     864*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     64*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 624*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^3*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     272*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     2336*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     192*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 496*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     1824*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     384*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     64*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 352*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 16*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     224*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     320*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     192*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     320*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     224*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     96*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     2840*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     320*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     2160*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     144*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     432*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     12*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     20*a1*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     2496*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     288*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     528*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     2112*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     424*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     332*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     364*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     176*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     32*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     12*a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     12*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     2496*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     440*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     2008*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     212*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     416*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     24*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     20*a1*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     2840*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     808*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     1016*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     1608*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     1192*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     484*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     204*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     352*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     40*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     4*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     20*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     5136*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     360*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 2384*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     72*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 208*a1*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     4896*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     576*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 2464*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     168*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     152*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 200*a1*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     40*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     4896*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     592*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 2384*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     136*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 216*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     5136*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     864*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     944*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 1888*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 952*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     200*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 120*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 128*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     2912*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     96*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 608*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     3008*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     448*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     128*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 720*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     112*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     3008*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     192*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 656*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     2912*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     96*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     192*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 512*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 144*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     512*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     576*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     192*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     576*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     512*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     64*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     1520*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     120*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     704*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     24*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     64*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     1072*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     176*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     96*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     584*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     128*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     24*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     56*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     8*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     1072*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     80*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     552*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     24*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     56*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     1520*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     656*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     432*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     480*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     528*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     88*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     24*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     64*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     1824*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     64*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 384*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     1440*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     32*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     64*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 336*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     16*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     1440*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     64*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 336*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     1824*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     512*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     192*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 288*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 208*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     512*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     448*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     64*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     448*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     512*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     64*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     368*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     16*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     80*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     160*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     32*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     48*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     160*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     48*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     368*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     240*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     64*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     48*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     80*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     224*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     128*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     128*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     224*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}]^3 + 
     96*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^4*
      \[Omega][{{X, X}, {Y, Y}}]^3 - 
     32*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     32*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     32*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^5*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     288*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     18*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 312*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     15*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 108*a1*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     3*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 12*a1*A2^3*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     432*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     120*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     276*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 164*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     356*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     260*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 82*a1*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     218*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     68*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 26*a1*A2^3*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     34*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     4*A2^4*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     432*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     72*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 468*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     60*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 162*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     12*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 18*a1*A2^3*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     288*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     144*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     72*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 276*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 192*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     60*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 78*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 84*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     12*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 6*a1*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 12*a1^2*A2^3*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     720*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     36*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     536*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     17*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     108*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     4*a1*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     1152*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     200*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     400*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     464*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     520*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     232*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     22*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     206*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     24*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     6*a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     14*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     1152*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     144*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     864*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     68*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     174*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     4*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     6*a1*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     720*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     288*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     144*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     464*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     280*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     68*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     74*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     76*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     4*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     2*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     4*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     640*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     22*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     288*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     4*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     24*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     1104*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     32*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     168*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     316*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     228*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     40*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     48*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     1104*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     88*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     500*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     16*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     40*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     640*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     176*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     88*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     244*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     120*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     16*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     16*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     16*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     240*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     4*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     48*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     448*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     80*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     16*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     56*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     24*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     448*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     16*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     88*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     240*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     32*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     16*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     40*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     16*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     32*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     64*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     32*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     64*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     32*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     720*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     36*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     536*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     17*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     108*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     4*a1*A2^3*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     1008*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     344*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     400*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     440*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     544*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     232*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     2*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     182*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     24*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     6*a1*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     14*a1^2*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     1008*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     144*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     744*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     68*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     150*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     4*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     6*a1*A2^3*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     720*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     432*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     144*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     464*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     400*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     68*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     74*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     100*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     4*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     2*a1*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     4*a1^2*A2^3*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     1280*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     44*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 576*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     8*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 48*a1*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     1920*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     448*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     384*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 592*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     528*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     96*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 8*a1*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     88*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     1920*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     176*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 864*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     32*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 72*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     1280*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     640*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     176*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 488*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 376*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     32*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 32*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 40*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     720*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     12*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 144*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     1168*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     96*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     80*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 168*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     104*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     1168*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     48*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 232*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     720*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     272*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     48*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 120*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 80*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     128*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     224*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     32*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     224*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     128*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     32*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     640*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     22*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     288*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     4*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     24*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     816*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     320*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     168*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     308*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     236*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     40*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     8*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     40*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     816*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     88*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     364*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     16*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     32*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     640*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     464*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     88*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     244*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     256*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     16*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     16*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     24*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     720*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     12*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 144*a1*A2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     992*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     272*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     80*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 168*a1*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     104*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     992*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     48*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 200*a1*A2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     720*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     448*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     48*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 120*a1*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 112*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     192*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     288*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     32*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     288*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     192*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     96*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     240*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     4*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     48*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     272*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     96*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     16*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     56*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     24*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     272*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     16*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     56*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     240*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     208*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     16*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     40*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     48*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     128*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     160*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     32*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     160*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     128*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^4 + 
     96*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}]^4 - 
     32*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     32*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     32*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     32*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^4 + 
     32*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^4*\[Omega][{{X, X}, {Y, Y}}]^4 - 
     36*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^5 - 
     30*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^5 - 6*a1*A2^2*
      E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^5 - 
     72*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^5 - 
     72*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^5 - 
     72*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^5 + 
     36*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^5 - 114*a1^2*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     39*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^5 + 22*a1*A2^2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     32*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^5 - 
     5*A2^3*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^5 + 
     72*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^5 + 
     60*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^5 + 12*a1*A2^2*
      E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^5 + 
     36*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^5 + 
     36*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^5 + 
     30*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^5 + 30*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^5 + 6*a1*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^5 + 6*a1^2*A2^2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^5 - 
     72*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     34*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     2*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     144*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     144*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     72*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     12*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     126*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     18*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     4*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     14*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     144*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     68*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     4*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     72*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     72*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     34*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     34*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     2*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     2*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     44*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     8*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     88*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     88*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     16*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     36*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     88*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     16*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     44*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     44*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     8*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     8*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     8*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     16*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     16*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     16*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     8*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     8*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     72*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     34*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     2*a1*A2^2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     144*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     144*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     72*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     12*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     126*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     18*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     4*a1*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     14*a1^2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     144*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     68*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     4*a1*A2^2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     72*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     72*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     34*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     34*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     2*a1*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     2*a1^2*A2^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     88*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^5 - 
     16*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^5 - 
     176*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^5 - 
     176*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^5 - 
     32*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^5 - 72*a1^2*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     176*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^5 + 
     32*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^5 + 
     88*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^5 + 
     88*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^5 + 
     16*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^5 + 16*a1^2*A2*
      E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^5 - 
     24*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^5 - 
     48*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^5 - 
     48*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^5 + 
     48*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^5 + 
     24*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^5 + 
     24*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^5 - 
     44*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     8*a1*A2*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     88*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     88*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     16*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     36*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     88*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     16*a1*A2*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     44*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     44*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     8*a1*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     8*a1^2*A2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     24*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^5 - 
     48*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^5 - 
     48*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^5 + 
     48*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^5 + 
     24*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^5 + 
     24*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^5 - 
     8*a1*E^(2*T1*(2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     16*a1*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     16*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     16*a1*E^(T1*(3 + 4*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     8*a1*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^5 + 
     8*a1^2*E^(T1*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         4*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}]^5 - 
     12*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^6 - 
     6*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^6 + 
     4*a1*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^6 - 8*a1^2*A2*
      E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
         2*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}]^6 - 
     2*A2^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X, X}, {Y, Y}}]^6 - 
     20*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^6 - 
     4*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^6 - 
     4*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^6 - 
     8*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^6 - 
     20*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^6 - 
     4*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^6 - 
     4*a1^2*A2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^6 - 
     16*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^6 - 
     8*a1^2*E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}] + 2*\[Omega][{{X, X}, {Y, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^6)/
    (E^(4*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}]))*(3 + \[Omega][{{X}, {X, Y, Y}}] + 
      \[Omega][{{Y}, {X, X, Y}}])*(2 + A2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
      2*\[Omega][{{Y}, {X, X, Y}}])*(3 + A2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
      2*\[Omega][{{Y}, {X, X, Y}}])*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] - 
      \[Omega][{{X, X}, {Y, Y}}])*(1 + 2*\[Omega][{{Y}, {X, X, Y}}] - 
      \[Omega][{{X, X}, {Y, Y}}])*(3 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
      \[Omega][{{X, X}, {Y, Y}}])*(1 + A2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
      \[Omega][{{X, X}, {Y, Y}}])*(3 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
      \[Omega][{{X, X}, {Y, Y}}])*(1 + A2 + 2*\[Omega][{{Y}, {X, X, Y}}] + 
      \[Omega][{{X, X}, {Y, Y}}])*(1 + 2*\[Omega][{{X, X}, {Y, Y}}])*
     (A2 + 2*\[Omega][{{X, X}, {Y, Y}}])))}, 
 {(2*(8*a1^2*\[Omega][{{X}, {X, Y, Y}}]^4 + 4*\[Omega][{{X}, {X, Y, Y}}]^3*
      (7*a1^2 + A2 + 3*a1*A2 + a1^2*A2 + 6*a1^2*\[Omega][{{Y}, {X, X, Y}}] + 
       2*a1^2*\[Omega][{{X, Y}, {X, Y}}]) + 2*\[Omega][{{X}, {X, Y, Y}}]^2*
      (16*a1^2 + 5*A2 + 28*a1*A2 - 7*a1^2*A2 + 4*A2^2 + 5*a1*A2^2 + 
       12*a1^2*\[Omega][{{Y}, {X, X, Y}}]^2 + 2*(A2 + a1*A2 + a1^2*(5 + A2))*
        \[Omega][{{X, Y}, {X, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}]*
        (2*A2 + 7*a1*A2 + 2*a1^2*(7 + A2) + 
         4*a1^2*\[Omega][{{X, Y}, {X, Y}}])) + \[Omega][{{X}, {X, Y, Y}}]*
      (12*a1^2 + 6*A2 + 80*a1*A2 - 48*a1^2*A2 + 13*A2^2 + 34*a1*A2^2 - 
       16*a1^2*A2^2 + 5*A2^3 + 2*a1*A2^3 + 8*a1^2*\[Omega][{{Y}, {X, X, Y}}]^
         3 + 2*(a1^2*(6 + A2) + a1*A2*(6 + A2) + A2*(3 + 2*A2))*
        \[Omega][{{X, Y}, {X, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}]^2*
        (A2 + 5*a1*A2 + a1^2*(7 + A2) + 2*a1^2*\[Omega][{{X, Y}, {X, Y}}]) + 
       2*\[Omega][{{Y}, {X, X, Y}}]*(5*A2*(1 + A2) + 2*a1*A2*(21 + 4*A2) - 
         2*a1^2*(-8 + 7*A2) + 2*(A2 + 2*a1*A2 + a1^2*(5 + A2))*
          \[Omega][{{X, Y}, {X, Y}}])) + 
     A2*(36*a1 - 30*a1^2 + 3*A2 + 28*a1*A2 - 20*a1^2*A2 + 4*A2^2 + 
       4*a1*A2^2 - 2*a1^2*A2^2 + A2^3 + 4*a1*\[Omega][{{Y}, {X, X, Y}}]^3 - 
       (-4*a1 + 2*a1^2 - A2)*(3 + A2)*\[Omega][{{X, Y}, {X, Y}}] + 
       2*\[Omega][{{Y}, {X, X, Y}}]^2*(-7*a1^2 + A2 + a1*(14 + 3*A2) + 
         2*a1*\[Omega][{{X, Y}, {X, Y}}]) + \[Omega][{{Y}, {X, X, Y}}]*
        (A2*(5 + 3*A2) - 4*a1^2*(11 + 3*A2) + 2*a1*(30 + 13*A2 + A2^2) + 
         2*(-3*a1^2 + A2 + a1*(8 + A2))*\[Omega][{{X, Y}, {X, Y}}]))))/
   (E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}]))*
    (1 + 2*\[Omega][{{X}, {X, Y, Y}}])*(A2 + 2*\[Omega][{{X}, {X, Y, Y}}])*
    (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
    (2 + A2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
    (3 + A2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
    (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
     \[Omega][{{X, Y}, {X, Y}}])*(1 + A2 + \[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])), 
  (2*(6*a1^2 + 3*A2 + 10*a1*A2 - 2*a1^2*A2 + 4*A2^2 + 4*a1*A2^2 - 
     2*a1^2*A2^2 + A2^3 + 4*a1^2*\[Omega][{{X}, {X, Y, Y}}]^3 + 
     4*a1^2*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     6*a1^2*\[Omega][{{X, Y}, {X, Y}}] + 3*A2*\[Omega][{{X, Y}, {X, Y}}] - 
     2*a1*A2*\[Omega][{{X, Y}, {X, Y}}] + 
     4*a1^2*A2*\[Omega][{{X, Y}, {X, Y}}] + A2^2*\[Omega][{{X, Y}, {X, Y}}] + 
     2*\[Omega][{{Y}, {X, X, Y}}]^2*(A2 + 2*a1*A2 + a1^2*(7 + A2) + 
       2*a1^2*\[Omega][{{X, Y}, {X, Y}}]) + 2*\[Omega][{{X}, {X, Y, Y}}]^2*
      (7*a1^2 + A2 + 2*a1*A2 + a1^2*A2 + 6*a1^2*\[Omega][{{Y}, {X, X, Y}}] + 
       2*a1^2*\[Omega][{{X, Y}, {X, Y}}]) + \[Omega][{{Y}, {X, X, Y}}]*
      (16*a1^2 + 2*a1*A2*(7 + A2) + A2*(5 + 3*A2) + 2*(A2 + a1^2*(5 + A2))*
        \[Omega][{{X, Y}, {X, Y}}]) + \[Omega][{{X}, {X, Y, Y}}]*
      (16*a1^2 + 5*A2 + 14*a1*A2 + 3*A2^2 + 2*a1*A2^2 + 
       12*a1^2*\[Omega][{{Y}, {X, X, Y}}]^2 + 2*(A2 + a1^2*(5 + A2))*
        \[Omega][{{X, Y}, {X, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}]*
        (A2 + 2*a1*A2 + a1^2*(7 + A2) + 2*a1^2*\[Omega][{{X, Y}, {X, Y}}]))))/
   (E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}]))*
    (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
    (1 + 2*\[Omega][{{Y}, {X, X, Y}}])*
    (2 + A2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
    (3 + A2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
    (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
     \[Omega][{{X, Y}, {X, Y}}])*(1 + A2 + \[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])), 
  (2*(6*a1^2 + 3*A2 + 10*a1*A2 - 2*a1^2*A2 + 4*A2^2 + 4*a1*A2^2 - 
     2*a1^2*A2^2 + A2^3 + 4*a1^2*\[Omega][{{X}, {X, Y, Y}}]^3 + 
     4*a1^2*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     6*a1^2*\[Omega][{{X, Y}, {X, Y}}] + 3*A2*\[Omega][{{X, Y}, {X, Y}}] - 
     2*a1*A2*\[Omega][{{X, Y}, {X, Y}}] + 
     4*a1^2*A2*\[Omega][{{X, Y}, {X, Y}}] + A2^2*\[Omega][{{X, Y}, {X, Y}}] + 
     2*\[Omega][{{Y}, {X, X, Y}}]^2*(A2 + 2*a1*A2 + a1^2*(7 + A2) + 
       2*a1^2*\[Omega][{{X, Y}, {X, Y}}]) + 2*\[Omega][{{X}, {X, Y, Y}}]^2*
      (7*a1^2 + A2 + 2*a1*A2 + a1^2*A2 + 6*a1^2*\[Omega][{{Y}, {X, X, Y}}] + 
       2*a1^2*\[Omega][{{X, Y}, {X, Y}}]) + \[Omega][{{Y}, {X, X, Y}}]*
      (16*a1^2 + 2*a1*A2*(7 + A2) + A2*(5 + 3*A2) + 2*(A2 + a1^2*(5 + A2))*
        \[Omega][{{X, Y}, {X, Y}}]) + \[Omega][{{X}, {X, Y, Y}}]*
      (16*a1^2 + 5*A2 + 14*a1*A2 + 3*A2^2 + 2*a1*A2^2 + 
       12*a1^2*\[Omega][{{Y}, {X, X, Y}}]^2 + 2*(A2 + a1^2*(5 + A2))*
        \[Omega][{{X, Y}, {X, Y}}] + 4*\[Omega][{{Y}, {X, X, Y}}]*
        (A2 + 2*a1*A2 + a1^2*(7 + A2) + 2*a1^2*\[Omega][{{X, Y}, {X, Y}}]))))/
   (E^(2*T1*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}]))*
    (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
    (2 + A2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
    (3 + A2 + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
    (3 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
     \[Omega][{{X, Y}, {X, Y}}])*(1 + A2 + \[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])*
    (1 + 2*\[Omega][{{X, Y}, {X, Y}}]))}}
