{(E^(-T1 - T2 - (2*T1 + 5*T2)*\[Lambda][{y}] - 2*(T1 + 2*T2)*
       \[Omega][{{X}, {X, Y, Y}}] - 4*T1*\[Omega][{{Y}, {X, X, Y}}] - 
      6*T2*\[Omega][{{Y}, {X, X, Y}}] - T1*\[Omega][{{X, X}, {Y, Y}}] - 
      T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^2*
    ((-6*(-1 + a1)*E^(T1 + T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
          2*(T1 + 2*T2)*(\[Omega][{{X}, {X, Y, Y}}] + 
            \[Omega][{{Y}, {X, X, Y}}])) + 6*(-1 + a1)*a1*
        E^(T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
          4*T2*\[Omega][{{X}, {X, Y, Y}}] + 2*T1*\[Omega][{{Y}, {X, X, Y}}] + 
          4*T2*\[Omega][{{Y}, {X, X, Y}}] + T1*\[Omega][{{X, X}, {Y, Y}}]) + 
       6*a1*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 2*(T1 + 2*T2)*
           \[Omega][{{X}, {X, Y, Y}}] + 2*T1*\[Omega][{{Y}, {X, X, Y}}] + 
          6*T2*\[Omega][{{Y}, {X, X, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}]) - 
       2*(-1 + a1)*a1*E^(T2 + (T1 + 2*T2)*\[Lambda][{y}] + 
          2*T2*\[Omega][{{X}, {X, Y, Y}}] + 2*T1*\[Omega][{{Y}, {X, X, Y}}] + 
          4*T2*\[Omega][{{Y}, {X, X, Y}}] + T1*\[Omega][{{X, X}, {Y, Y}}] + 
          T2*\[Omega][{{X, X}, {Y, Y}}]) - 5*(-1 + a1)^2*
        E^((T1 + 4*T2)*\[Lambda][{y}] + 2*T2*\[Omega][{{X}, {X, Y, Y}}] + 
          2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
          T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}]) + 
       (-6 + a1)*a1*E^(T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
          4*T2*\[Omega][{{X}, {X, Y, Y}}] + 2*T1*\[Omega][{{Y}, {X, X, Y}}] + 
          6*T2*\[Omega][{{Y}, {X, X, Y}}] + T1*\[Omega][{{X, X}, {Y, Y}}] + 
          T2*\[Omega][{{X, X}, {Y, Y}}]))*\[Lambda][{y}] - 
     2*E^((T1 + 4*T2)*\[Lambda][{y}] + 2*T2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*(T1 + 2*T2)*\[Omega][{{Y}, {X, X, Y}}])*
      ((-1 + a1)*E^((T1 + T2)*(1 + 2*\[Omega][{{X}, {X, Y, Y}}])) + 
       (-1 + a1)^2*E^((T1 + T2)*\[Omega][{{X, X}, {Y, Y}}]) - 
       (-1 + a1)*a1*E^(T2 + 2*T2*\[Omega][{{X}, {X, Y, Y}}] + 
          T1*\[Omega][{{X, X}, {Y, Y}}]) - 
       a1*E^(T1 + T2 + T2*\[Lambda][{y}] + 2*(T1 + T2)*
           \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
          T2*\[Omega][{{X, X}, {Y, Y}}]) + 
       a1*E^(T2 + T2*\[Lambda][{y}] + 2*T2*\[Omega][{{X}, {X, Y, Y}}] + 
          2*T2*\[Omega][{{Y}, {X, X, Y}}] + T1*\[Omega][{{X, X}, {Y, Y}}] + 
          T2*\[Omega][{{X, X}, {Y, Y}}]))*(\[Omega][{{X}, {X, Y, Y}}] + 
       \[Omega][{{Y}, {X, X, Y}}]) + 
     E^((T1 + 2*T2)*\[Lambda][{y}] + 2*T2*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      (E^(2*T2*\[Lambda][{y}]) + 2*a1*(E^T2 - E^(2*T2*\[Lambda][{y}])) + 
       a1^2*(-2*E^T2 + E^(2*T2*\[Lambda][{y}]) + 
         E^(T2*(1 + 3*\[Lambda][{y}] + 2*(\[Omega][{{X}, {X, Y, Y}}] + 
              \[Omega][{{Y}, {X, X, Y}}])))))*\[Lambda][{y}]*
      (2*\[Omega][{{X}, {X, Y, Y}}] - \[Omega][{{X, X}, {Y, Y}}])))/
   ((3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{Y}, {X, X, Y}}])*(\[Lambda][{y}] + 
     2*\[Omega][{{Y}, {X, X, Y}}])*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] - 
     \[Omega][{{X, X}, {Y, Y}}])*(3*\[Lambda][{y}] + 
     2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])) + 
  (E^(-2*T1 - 2*T2 - (T1 + 5*T2)*\[Lambda][{y}] - 2*(2*T1 + 5*T2)*
       \[Omega][{{X}, {X, Y, Y}}] - 2*T1*\[Omega][{{Y}, {X, X, Y}}] - 
      4*T2*\[Omega][{{Y}, {X, X, Y}}] - T1*\[Omega][{{X, X}, {Y, Y}}] - 
      2*T2*\[Omega][{{X, X}, {Y, Y}}])*
    (-36*a1*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^7 + 
     36*a1^2*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^7 + 
     36*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T1*\[Omega][{{Y}, {X, X, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^7 - 72*a1*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^7 + 
     36*a1^2*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^7 + 
     18*a1*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^7 - 18*a1^2*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T1*\[Omega][{{X, X}, {Y, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^7 + 
     108*a1*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^7 - 108*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^7 + 36*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 5*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^7 - 6*a1*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T2*\[Omega][{{Y}, {X, X, Y}}] + T1*\[Omega][{{X, X}, {Y, Y}}] + 
        2*T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^7 + 
     6*a1^2*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^7 - 30*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T2*\[Omega][{{Y}, {X, X, Y}}] + T1*\[Omega][{{X, X}, {Y, Y}}] + 
        2*T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^7 + 
     60*a1*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^7 - 30*a1^2*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T2*\[Omega][{{Y}, {X, X, Y}}] + T1*\[Omega][{{X, X}, {Y, Y}}] + 
        2*T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^7 - 
     72*a1*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^7 + 72*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T1*\[Omega][{{X, X}, {Y, Y}}] + 
        2*T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^7 - 
     30*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 5*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^7 - 84*a1*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^6*
      \[Omega][{{X}, {X, Y, Y}}] + 
     84*a1^2*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^6*
      \[Omega][{{X}, {X, Y, Y}}] + 
     12*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T1*\[Omega][{{Y}, {X, X, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X}, {X, Y, Y}}] - 
     24*a1*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^6*
      \[Omega][{{X}, {X, Y, Y}}] + 
     12*a1^2*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^6*
      \[Omega][{{X}, {X, Y, Y}}] + 
     42*a1*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X}, {X, Y, Y}}] - 
     42*a1^2*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X}, {X, Y, Y}}] + 
     108*a1*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X}, {X, Y, Y}}] - 
     108*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X}, {X, Y, Y}}] + 
     12*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 5*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X}, {X, Y, Y}}] + 
     6*a1*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X}, {X, Y, Y}}] - 
     6*a1^2*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X}, {X, Y, Y}}] - 
     12*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X}, {X, Y, Y}}] + 
     24*a1*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X}, {X, Y, Y}}] - 
     12*a1^2*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X}, {X, Y, Y}}] - 
     72*a1*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X}, {X, Y, Y}}] + 
     72*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X}, {X, Y, Y}}] - 
     12*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 5*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X}, {X, Y, Y}}] - 
     24*a1*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^5*
      \[Omega][{{X}, {X, Y, Y}}]^2 + 
     24*a1^2*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^5*
      \[Omega][{{X}, {X, Y, Y}}]^2 + 
     12*a1*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]^2 - 
     12*a1^2*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]^2 + 
     24*a1*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]^2 - 
     24*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]^2 + 
     4*a1*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]^2 - 
     4*a1^2*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]^2 - 
     16*a1*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]^2 + 
     16*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]^2 - 
     72*a1*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^6*
      \[Omega][{{Y}, {X, X, Y}}] + 
     72*a1^2*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^6*
      \[Omega][{{Y}, {X, X, Y}}] + 
     72*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T1*\[Omega][{{Y}, {X, X, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{Y}, {X, X, Y}}] - 
     144*a1*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^6*
      \[Omega][{{Y}, {X, X, Y}}] + 
     72*a1^2*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^6*
      \[Omega][{{Y}, {X, X, Y}}] + 
     18*a1*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{Y}, {X, X, Y}}] - 
     18*a1^2*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{Y}, {X, X, Y}}] + 
     216*a1*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{Y}, {X, X, Y}}] - 
     216*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{Y}, {X, X, Y}}] + 
     72*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 5*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{Y}, {X, X, Y}}] - 
     22*a1*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{Y}, {X, X, Y}}] + 
     22*a1^2*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{Y}, {X, X, Y}}] - 
     50*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{Y}, {X, X, Y}}] + 
     100*a1*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{Y}, {X, X, Y}}] - 
     50*a1^2*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{Y}, {X, X, Y}}] - 
     96*a1*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{Y}, {X, X, Y}}] + 
     96*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{Y}, {X, X, Y}}] - 
     50*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 5*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{Y}, {X, X, Y}}] - 
     164*a1*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^5*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     164*a1^2*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^5*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     20*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T1*\[Omega][{{Y}, {X, X, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}] - 
     40*a1*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^5*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     20*a1^2*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^5*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}] + 
     40*a1*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}] - 
     40*a1^2*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}] + 
     204*a1*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}] - 204*a1^2*
      E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}] + 
     20*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 5*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}] + 
     8*a1*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}] - 
     8*a1^2*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}] - 
     20*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}] + 
     40*a1*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}] - 
     20*a1^2*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}] - 
     88*a1*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}] + 
     88*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}] - 
     20*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 5*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{Y}, {X, X, Y}}] - 
     40*a1*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] + 
     40*a1^2*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] + 
     8*a1*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}] - 
     8*a1^2*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}] + 
     40*a1*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}] - 
     40*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}] + 
     8*a1*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}] - 
     8*a1^2*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}] - 
     16*a1*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}] + 
     16*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}] - 
     44*a1*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^5*
      \[Omega][{{Y}, {X, X, Y}}]^2 + 
     44*a1^2*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^5*
      \[Omega][{{Y}, {X, X, Y}}]^2 + 
     44*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T1*\[Omega][{{Y}, {X, X, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     88*a1*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^5*
      \[Omega][{{Y}, {X, X, Y}}]^2 + 
     44*a1^2*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^5*
      \[Omega][{{Y}, {X, X, Y}}]^2 + 
     4*a1*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     4*a1^2*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     132*a1*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     132*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     44*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 5*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     20*a1*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     20*a1^2*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     20*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     40*a1*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     20*a1^2*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     24*a1*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     24*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     20*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 5*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     96*a1*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     96*a1^2*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     8*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T1*\[Omega][{{Y}, {X, X, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^
       2 - 16*a1*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     8*a1^2*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     8*a1*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^
       2 - 8*a1^2*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T1*\[Omega][{{X, X}, {Y, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     112*a1*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^
       2 - 112*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^
       2 + 8*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 5*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^
       2 - 8*a1*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T2*\[Omega][{{Y}, {X, X, Y}}] + T1*\[Omega][{{X, X}, {Y, Y}}] + 
        2*T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     8*a1^2*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^
       2 - 8*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^
       2 + 16*a1*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^
       2 - 8*a1^2*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T2*\[Omega][{{Y}, {X, X, Y}}] + T1*\[Omega][{{X, X}, {Y, Y}}] + 
        2*T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     16*a1*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^
       2 + 16*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T1*\[Omega][{{X, X}, {Y, Y}}] + 
        2*T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2 - 
     8*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 5*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^
       2 - 16*a1*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     16*a1^2*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]^2 + 
     16*a1*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 
     16*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]^2 - 
     8*a1*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{Y}, {X, X, Y}}]^3 + 
     8*a1^2*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{Y}, {X, X, Y}}]^3 + 
     8*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T1*\[Omega][{{Y}, {X, X, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     16*a1*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{Y}, {X, X, Y}}]^3 + 
     8*a1^2*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{Y}, {X, X, Y}}]^3 + 
     24*a1*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     24*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     8*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 5*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]^3 - 
     16*a1*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     16*a1^2*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^3 + 
     16*a1*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^
       3 - 16*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^
       3 + 54*a1*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^6*
      \[Omega][{{X, X}, {Y, Y}}] - 
     54*a1^2*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^6*
      \[Omega][{{X, X}, {Y, Y}}] + 
     18*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T1*\[Omega][{{Y}, {X, X, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X, X}, {Y, Y}}] - 
     36*a1*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^6*
      \[Omega][{{X, X}, {Y, Y}}] + 
     18*a1^2*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^6*
      \[Omega][{{X, X}, {Y, Y}}] - 
     18*a1*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X, X}, {Y, Y}}] + 
     18*a1^2*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X, X}, {Y, Y}}] - 
     18*a1*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X, X}, {Y, Y}}] + 
     18*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X, X}, {Y, Y}}] + 
     18*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 5*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X, X}, {Y, Y}}] - 
     21*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X, X}, {Y, Y}}] + 
     42*a1*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X, X}, {Y, Y}}] - 
     21*a1^2*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X, X}, {Y, Y}}] - 
     24*a1*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X, X}, {Y, Y}}] + 
     24*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X, X}, {Y, Y}}] - 
     21*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 5*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^6*\[Omega][{{X, X}, {Y, Y}}] + 
     54*a1*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^5*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     54*a1^2*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^5*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     6*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T1*\[Omega][{{Y}, {X, X, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     12*a1*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^5*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     6*a1^2*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^5*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     6*a1*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     6*a1^2*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     42*a1*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     42*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     6*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 5*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     2*a1*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     2*a1^2*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     6*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     12*a1*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     6*a1^2*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     8*a1*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     8*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     6*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 5*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X}, {X, Y, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     12*a1*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     12*a1^2*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     12*a1*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     12*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     90*a1*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^5*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     90*a1^2*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^5*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     54*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T1*\[Omega][{{Y}, {X, X, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     108*a1*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^5*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     54*a1^2*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^5*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     18*a1*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     18*a1^2*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     18*a1*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     18*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     54*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 5*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     14*a1*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     14*a1^2*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     50*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     100*a1*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     50*a1^2*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     68*a1*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     68*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     50*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 5*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     60*a1*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     60*a1^2*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     16*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T1*\[Omega][{{Y}, {X, X, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     32*a1*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     16*a1^2*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     4*a1*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     4*a1^2*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     28*a1*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     28*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     16*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 5*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     16*a1*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     16*a1^2*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     16*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     32*a1*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     16*a1^2*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     44*a1*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     44*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     16*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 5*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] + 
     8*a1*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     8*a1^2*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}] - 
     8*a1*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     8*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     8*a1*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     8*a1^2*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
     8*a1*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     8*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
     48*a1*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     48*a1^2*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     40*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T1*\[Omega][{{Y}, {X, X, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 
     80*a1*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] + 
     40*a1^2*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}] - 
     4*a1*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     4*a1^2*E^(T1 + 2*T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     32*a1*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 
     32*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     40*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 5*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 
     24*a1*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     24*a1^2*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 
     24*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     48*a1*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 
     24*a1^2*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 
     20*a1*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     20*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 
     24*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 5*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     16*a1*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] - 
     16*a1^2*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     8*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T1*\[Omega][{{Y}, {X, X, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^
       2*\[Omega][{{X, X}, {Y, Y}}] - 
     16*a1*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     8*a1^2*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}] + 
     8*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 5*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^
       2*\[Omega][{{X, X}, {Y, Y}}] - 
     8*a1*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^
       2*\[Omega][{{X, X}, {Y, Y}}] + 
     8*a1^2*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^
       2*\[Omega][{{X, X}, {Y, Y}}] - 
     8*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^
       2*\[Omega][{{X, X}, {Y, Y}}] + 
     16*a1*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^
       2*\[Omega][{{X, X}, {Y, Y}}] - 
     8*a1^2*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^
       2*\[Omega][{{X, X}, {Y, Y}}] - 
     8*a1*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^
       2*\[Omega][{{X, X}, {Y, Y}}] + 
     8*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^
       2*\[Omega][{{X, X}, {Y, Y}}] - 
     8*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 5*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]^
       2*\[Omega][{{X, X}, {Y, Y}}] + 
     8*a1*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] - 
     8*a1^2*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     8*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T1*\[Omega][{{Y}, {X, X, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 
     16*a1*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     8*a1^2*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{Y}, {X, X, Y}}]^3*\[Omega][{{X, X}, {Y, Y}}] + 
     8*a1*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 
     8*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] + 
     8*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 5*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{Y}, {X, X, Y}}]^3*
      \[Omega][{{X, X}, {Y, Y}}] - 
     18*a1*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^5*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     18*a1^2*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^5*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     18*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T1*\[Omega][{{Y}, {X, X, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     36*a1*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^5*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     18*a1^2*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^5*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     18*a1*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     18*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     18*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 5*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     6*a1*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     6*a1^2*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     12*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     24*a1*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     12*a1^2*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     18*a1*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     18*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     12*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 5*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^5*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     6*a1*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     6*a1^2*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     6*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T1*\[Omega][{{Y}, {X, X, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       2 + 12*a1*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     6*a1^2*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     6*a1*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       2 + 6*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       2 - 6*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 5*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       2 - 6*a1*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T2*\[Omega][{{Y}, {X, X, Y}}] + T1*\[Omega][{{X, X}, {Y, Y}}] + 
        2*T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     6*a1^2*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       2 + 6*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       2 - 12*a1*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       2 + 6*a1^2*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T2*\[Omega][{{Y}, {X, X, Y}}] + T1*\[Omega][{{X, X}, {Y, Y}}] + 
        2*T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     18*a1*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       2 - 18*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T1*\[Omega][{{X, X}, {Y, Y}}] + 
        2*T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     6*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 5*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       2 - 4*a1*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T2*\[Omega][{{Y}, {X, X, Y}}] + T1*\[Omega][{{X, X}, {Y, Y}}] + 
        2*T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     4*a1^2*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     4*a1*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     4*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     18*a1*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     18*a1^2*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     18*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T1*\[Omega][{{Y}, {X, X, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       2 + 36*a1*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     18*a1^2*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     18*a1*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       2 + 18*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       2 - 18*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 5*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       2 + 10*a1*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T2*\[Omega][{{Y}, {X, X, Y}}] + T1*\[Omega][{{X, X}, {Y, Y}}] + 
        2*T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     10*a1^2*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       2 + 2*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       2 - 4*a1*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       2 + 2*a1^2*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T2*\[Omega][{{Y}, {X, X, Y}}] + T1*\[Omega][{{X, X}, {Y, Y}}] + 
        2*T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     6*a1*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       2 + 6*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T1*\[Omega][{{X, X}, {Y, Y}}] + 
        2*T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^4*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     2*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 5*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       2 - 4*a1*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     4*a1^2*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     4*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T1*\[Omega][{{Y}, {X, X, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     8*a1*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T1*\[Omega][{{Y}, {X, X, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     4*a1^2*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     4*a1*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     4*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     4*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 5*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     8*a1*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     8*a1^2*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     4*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     8*a1*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     4*a1^2*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     4*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 5*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{Y}, {X, X, Y}}]*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     4*a1*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 + 
     4*a1^2*E^(T1 + 2*T2 + (T1 + 4*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     4*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T1*\[Omega][{{Y}, {X, X, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     8*a1*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T1*\[Omega][{{Y}, {X, X, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     4*a1^2*E^(T1 + T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T1*\[Omega][{{Y}, {X, X, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{Y}, {X, X, Y}}]^2*\[Omega][{{X, X}, {Y, Y}}]^2 - 
     4*a1*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     4*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     4*a1^2*E^(T1 + 2*T2 + (T1 + 5*T2)*\[Lambda][{y}] + 
        2*(T1 + 5*T2)*\[Omega][{{X}, {X, Y, Y}}] + 2*(T1 + 2*T2)*
         \[Omega][{{Y}, {X, X, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     4*a1*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     4*a1^2*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     4*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     8*a1*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     4*a1^2*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     4*a1*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     4*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 - 
     4*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 5*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{Y}, {X, X, Y}}]^2*
      \[Omega][{{X, X}, {Y, Y}}]^2 + 
     3*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     6*a1*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     3*a1^2*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     6*a1*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     6*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     3*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 5*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^4*\[Omega][{{X, X}, {Y, Y}}]^3 - 
     2*a1*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       3 + 2*a1^2*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T2*\[Omega][{{Y}, {X, X, Y}}] + T1*\[Omega][{{X, X}, {Y, Y}}] + 
        2*T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     2*a1*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       3 - 2*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T1*\[Omega][{{X, X}, {Y, Y}}] + 
        2*T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{X}, {X, Y, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     2*a1*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       3 - 2*a1^2*E^(T1 + 2*T2 + 2*T2*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T2*\[Omega][{{Y}, {X, X, Y}}] + T1*\[Omega][{{X, X}, {Y, Y}}] + 
        2*T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     2*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       3 - 4*a1*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       3 + 2*a1^2*E^(T1 + T2 + 4*T2*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        2*T2*\[Omega][{{Y}, {X, X, Y}}] + T1*\[Omega][{{X, X}, {Y, Y}}] + 
        2*T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     2*a1*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 4*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       3 - 2*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 
        2*(T1 + 4*T2)*\[Omega][{{X}, {X, Y, Y}}] + 
        4*T2*\[Omega][{{Y}, {X, X, Y}}] + T1*\[Omega][{{X, X}, {Y, Y}}] + 
        2*T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]^3*
      \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^3 + 
     2*a1^2*E^(T1 + 2*T2 + 5*T2*\[Lambda][{y}] + 2*(T1 + 5*T2)*
         \[Omega][{{X}, {X, Y, Y}}] + 4*T2*\[Omega][{{Y}, {X, X, Y}}] + 
        T1*\[Omega][{{X, X}, {Y, Y}}] + 2*T2*\[Omega][{{X, X}, {Y, Y}}])*
      \[Lambda][{y}]^3*\[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}]^
       3))/((\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}])*
    (3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{Y}, {X, X, Y}}])*(3*\[Lambda][{y}] + 
     2*\[Omega][{{Y}, {X, X, Y}}])*(\[Lambda][{y}] + 
     2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
    (2*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
     \[Omega][{{X, X}, {Y, Y}}])*(\[Lambda][{y}] + 
     \[Omega][{{X, X}, {Y, Y}}])*(3*\[Lambda][{y}] + 
     2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])) + 
  (\[Lambda][{y}]^2*(((-1 + a1)*E^(-T1 - 2*T1*\[Omega][{{X}, {X, Y, Y}}] - 
         2*(T1 + T2)*\[Omega][{{X, X}, {Y, Y}}])*
       (E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) - 
        a1*E^(T1*\[Omega][{{X, X}, {Y, Y}}])))/
      ((1 + 2*\[Omega][{{X}, {X, Y, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
       (-\[Lambda][{y}] - 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}])) + 
     (2*(-1 + a1)*E^(-(T2*(\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
            \[Omega][{{X, X}, {Y, Y}}])) - T1*(1 + \[Lambda][{y}] + 
           2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
           \[Omega][{{X, X}, {Y, Y}}]))*
       (-E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) + 
        a1*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*(\[Lambda][{y}] + 
        \[Omega][{{X, X}, {Y, Y}}]))/((\[Lambda][{y}] + 
        2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
       (-1 - 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
       (3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}])) + 
     ((-1 + a1)^2*(2*\[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}] + \[Lambda][{y}]*
         (2 - 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])))/
      (E^((T1 + T2)*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}]))*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] - 
        \[Omega][{{X, X}, {Y, Y}}])*(3*\[Lambda][{y}] + 
        2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
       (-\[Lambda][{y}] - 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}])) + 
     (2*(-1 + a1)*a1*E^(-T1 - (T1 + T2)*\[Lambda][{y}] - 
         2*(T1 + T2)*\[Omega][{{X}, {X, Y, Y}}] - 
         2*T1*\[Omega][{{Y}, {X, X, Y}}] - T1*\[Omega][{{X, X}, {Y, Y}}] - 
         T2*\[Omega][{{X, X}, {Y, Y}}])*\[Lambda][{y}]*
       ((-2*E^(T1*(\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}])) + 
          E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*\[Lambda][{y}] + 
        E^(T1*(\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}]))*
         (-2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])))/
      ((\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
        \[Omega][{{X, X}, {Y, Y}}])*(2*\[Lambda][{y}] + 
        2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
       (3*\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}])) + 
     (2*(-1 + a1)*a1*E^(-T1 - (T1 + 3*T2)*\[Lambda][{y}] - 
         2*(T1 + T2)*(\[Omega][{{X}, {X, Y, Y}}] + \[Omega][
            {{Y}, {X, X, Y}}]))*\[Lambda][{y}]*(3*\[Lambda][{y}]^2 + 
        \[Omega][{{X, X}, {Y, Y}}]*(2*\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}]) + \[Lambda][{y}]*
         (\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
          4*\[Omega][{{X, X}, {Y, Y}}])))/
      ((3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}])*(2*\[Lambda][{y}] + 
        2*\[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
       (3*\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}])*(3*\[Lambda][{y}] + 
        2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])) + 
     ((-1 + a1)^2*(12*\[Lambda][{y}]^3 + 2*(\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}])*\[Omega][{{X, X}, {Y, Y}}]*
         (2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}]) + 
        \[Lambda][{y}]^2*(16*\[Omega][{{X}, {X, Y, Y}}] + 
          4*\[Omega][{{Y}, {X, X, Y}}] + 19*\[Omega][{{X, X}, {Y, Y}}]) - 
        \[Lambda][{y}]*(2*\[Omega][{{X}, {X, Y, Y}}] - 
          \[Omega][{{X, X}, {Y, Y}}])*(6*\[Lambda][{y}]^2 + 
          \[Omega][{{X, X}, {Y, Y}}]*(2*\[Omega][{{X}, {X, Y, Y}}] + 
            \[Omega][{{X, X}, {Y, Y}}]) + \[Lambda][{y}]*
           (2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
            5*\[Omega][{{X, X}, {Y, Y}}])) + \[Lambda][{y}]*
         (4*\[Omega][{{X}, {X, Y, Y}}]^2 + \[Omega][{{X, X}, {Y, Y}}]*
           (8*\[Omega][{{Y}, {X, X, Y}}] + 5*\[Omega][{{X, X}, {Y, Y}}]) + 
          2*\[Omega][{{X}, {X, Y, Y}}]*(2*\[Omega][{{Y}, {X, X, Y}}] + 
            9*\[Omega][{{X, X}, {Y, Y}}]))))/
      (E^((T1 + T2)*(1 + \[Lambda][{y}] + 2*(\[Omega][{{X}, {X, Y, Y}}] + 
            \[Omega][{{Y}, {X, X, Y}}])))*(3*\[Lambda][{y}] + 
        \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (1 + 2*\[Omega][{{X}, {X, Y, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
       (\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
        \[Omega][{{X, X}, {Y, Y}}])*(3*\[Lambda][{y}] + 
        2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
       (3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}])) - 
     (a1*(-3*E^(T1*(1 + \[Lambda][{y}] + 2*(\[Omega][{{X}, {X, Y, Y}}] + 
              \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])))*
         (3*E^(T1*(\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}])) - 
          2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*
         (3*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) + 
          (-3 + a1)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*\[Lambda][{y}]^3 - 
        E^(T1*(1 + \[Lambda][{y}] + 2*(\[Omega][{{X}, {X, Y, Y}}] + 
              \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])))*
         (-E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) + 
          E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*(\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}])*(2*\[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{X, X}, {Y, Y}}])*
         (-2*E^(T1*(\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}]))*
           \[Omega][{{Y}, {X, X, Y}}] - 
          (E^(T1*(\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}])) - 
            2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][
            {{X, X}, {Y, Y}}]) - 
        E^(T1*(1 + \[Lambda][{y}] + 2*(\[Omega][{{X}, {X, Y, Y}}] + 
              \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])))*
         \[Lambda][{y}]^2*
         ((3*E^(T1*(\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}])) - 
            2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*
           (9*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) + 
            (-9 + a1)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*
           \[Omega][{{X}, {X, Y, Y}}] + 
          (9*E^(T1*(\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}])) - 
            2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*
           (3*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) + 
            (-3 + a1)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*
           \[Omega][{{Y}, {X, X, Y}}] + 
          (18*E^(T1*(1 + \[Lambda][{y}] + 2*(\[Omega][{{X}, {X, Y, Y}}] + 
                  \[Omega][{{Y}, {X, X, Y}}]))) + (24 - 5*a1)*
             E^(2*T1*\[Omega][{{X, X}, {Y, Y}}]) - 
            24*E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][
                 {{X, X}, {Y, Y}}])) + 3*(-6 + a1)*E^(T1*(\[Lambda][{y}] + 
                2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])))*
           \[Omega][{{X, X}, {Y, Y}}]) - 
        a1*E^(T1*(1 + \[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            2*\[Omega][{{Y}, {X, X, Y}}] + 3*\[Omega][{{X, X}, {Y, Y}}]))*
         \[Lambda][{y}]*(2*\[Omega][{{X}, {X, Y, Y}}] - 
          \[Omega][{{X, X}, {Y, Y}}])*
         ((9*E^(T1*(\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}])) - 
            6*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*\[Lambda][{y}]^2 + 
          2*E^(T1*(\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}]))*
           \[Omega][{{Y}, {X, X, Y}}]^2 + 
          E^(T1*(\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}]))*
           \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] - 
          E^(T1*\[Omega][{{X, X}, {Y, Y}}])*\[Omega][{{X, X}, {Y, Y}}]^2 + 
          \[Lambda][{y}]*((3*E^(T1*(\[Lambda][{y}] + 2*\[Omega][{{Y}, 
                     {X, X, Y}}])) - 2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*
             \[Omega][{{X}, {X, Y, Y}}] + 
            (9*E^(T1*(\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}])) - 
              2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{Y}, {X, X, 
                Y}}] + (3*E^(T1*(\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, 
                      Y}}])) - 5*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*
             \[Omega][{{X, X}, {Y, Y}}]) + \[Omega][{{X}, {X, Y, Y}}]*
           (2*E^(T1*(\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}]))*
             \[Omega][{{Y}, {X, X, Y}}] + (E^(T1*(\[Lambda][{y}] + 
                 2*\[Omega][{{Y}, {X, X, Y}}])) - 2*E^(T1*\[Omega][
                  {{X, X}, {Y, Y}}]))*\[Omega][{{X, X}, {Y, Y}}])) - 
        \[Lambda][{y}]*(2*E^(T1*(1 + \[Lambda][{y}] + 
              2*(\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
                \[Omega][{{X, X}, {Y, Y}}])))*
           (3*E^(T1*(\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}])) - 
            2*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*
           (E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) - 
            E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^2 + 
          2*E^(T1*(1 + 2*(\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
                2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])))*
           (3*E^(T1 + 2*T1*\[Omega][{{X}, {X, Y, Y}}]) + 
            (-3 + a1)*E^(T1*\[Omega][{{X, X}, {Y, Y}}]))*
           \[Omega][{{Y}, {X, X, Y}}]^2 + 
          E^(T1*(1 + \[Lambda][{y}] + 2*(\[Omega][{{X}, {X, Y, Y}}] + 
                \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])))*
           (12*E^(T1*(1 + \[Lambda][{y}] + 2*(\[Omega][{{X}, {X, Y, Y}}] + 
                  \[Omega][{{Y}, {X, X, Y}}]))) + 
            8*E^(2*T1*\[Omega][{{X, X}, {Y, Y}}]) - 
            8*E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][
                 {{X, X}, {Y, Y}}])) + (-12 + a1)*E^(T1*(\[Lambda][{y}] + 
                2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])))*
           \[Omega][{{Y}, {X, X, Y}}]*\[Omega][{{X, X}, {Y, Y}}] + 
          E^(T1*(1 + \[Lambda][{y}] + 2*(\[Omega][{{X}, {X, Y, Y}}] + 
                \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])))*
           (3*E^(T1*(1 + \[Lambda][{y}] + 2*(\[Omega][{{X}, {X, Y, Y}}] + 
                  \[Omega][{{Y}, {X, X, Y}}]))) - (-6 + a1)*
             E^(2*T1*\[Omega][{{X, X}, {Y, Y}}]) - 
            6*E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][
                 {{X, X}, {Y, Y}}])) - 3*E^(T1*(\[Lambda][{y}] + 
                2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])))*
           \[Omega][{{X, X}, {Y, Y}}]^2 + 
          E^(T1*(1 + \[Lambda][{y}] + 2*(\[Omega][{{X}, {X, Y, Y}}] + 
                \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, X}, {Y, Y}}])))*
           \[Omega][{{X}, {X, Y, Y}}]*
           (2*(12*E^(T1*(1 + \[Lambda][{y}] + 2*(\[Omega][{{X}, {X, Y, Y}}] + 
                    \[Omega][{{Y}, {X, X, Y}}]))) + 2*E^(2*T1*\[Omega][
                  {{X, X}, {Y, Y}}]) - 2*E^(T1*(1 + 2*\[Omega][{{X}, 
                     {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])) + 
              (-12 + a1)*E^(T1*(\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, 
                      Y}}] + \[Omega][{{X, X}, {Y, Y}}])))*
             \[Omega][{{Y}, {X, X, Y}}] + (12*E^(T1*(1 + \[Lambda][{y}] + 
                  2*(\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, 
                       Y}}]))) - 2*(-10 + a1)*E^(2*T1*\[Omega][{{X, X}, 
                   {Y, Y}}]) - 20*E^(T1*(1 + 2*\[Omega][{{X}, {X, Y, Y}}] + 
                  \[Omega][{{X, X}, {Y, Y}}])) + (-12 + a1)*E^
                (T1*(\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
                  \[Omega][{{X, X}, {Y, Y}}])))*\[Omega][{{X, X}, {Y, 
                Y}}]))))/(E^(2*T1*(1 + \[Lambda][{y}] + 
          2*(\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
            \[Omega][{{X, X}, {Y, Y}}])))*(3*\[Lambda][{y}] + 
        \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
       (1 + 2*\[Omega][{{X}, {X, Y, Y}}] - \[Omega][{{X, X}, {Y, Y}}])*
       (\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] - 
        \[Omega][{{X, X}, {Y, Y}}])*(3*\[Lambda][{y}] + 
        2*\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{X, X}, {Y, Y}}])*
       (3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
        \[Omega][{{X, X}, {Y, Y}}]))))/(\[Lambda][{y}] + 
    2*\[Omega][{{X, X}, {Y, Y}}]), 
 (2*E^(-T1 - T2 - (T1 + 3*T2)*\[Lambda][{y}] - 
      2*(T1 + T2)*\[Omega][{{X}, {X, Y, Y}}] - 
      2*T1*\[Omega][{{Y}, {X, X, Y}}] - 2*T2*\[Omega][{{Y}, {X, X, Y}}] - 
      T2*\[Omega][{{X, Y}, {X, Y}}])*\[Lambda][{y}]^3*
    (2*(E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}])) + 
       a1*(3*E^(T2*(1 + 2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
             \[Omega][{{Y}, {X, X, Y}}])) - 
         E^(T2*(1 + \[Omega][{{X, Y}, {X, Y}}])) - 
         2*E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}]))) + 
       a1^2*(-3*E^(T2*(1 + 2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
             \[Omega][{{Y}, {X, X, Y}}])) + 
         E^(T2*(1 + \[Omega][{{X, Y}, {X, Y}}])) + 
         E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}])) + 
         E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))))*
      \[Lambda][{y}] + E^(2*T2*\[Lambda][{y}])*
      (E^(T2*\[Omega][{{X, Y}, {X, Y}}]) + 
       2*a1*(E^(T2*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][
             {{Y}, {X, X, Y}}])) - E^(T2*\[Omega][{{X, Y}, {X, Y}}])) + 
       a1^2*(-2*E^(T2*(1 + \[Omega][{{X}, {X, Y, Y}}] + 
             \[Omega][{{Y}, {X, X, Y}}])) + 
         E^(T2*\[Omega][{{X, Y}, {X, Y}}]) + 
         E^(T2*(1 + \[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))))*
      \[Omega][{{X}, {X, Y, Y}}] + 
     2*a1*E^(T2*(1 + 2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}] - 
     2*a1^2*E^(T2*(1 + 2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{Y}, {X, X, Y}}] + 
     E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] - 
     2*a1*E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] + 
     a1^2*E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] + 
     a1^2*E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{Y}, {X, X, Y}}] - 
     2*a1*E^(T2*(1 + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{X, Y}, {X, Y}}] + 
     2*a1^2*E^(T2*(1 + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{X, Y}, {X, Y}}] - 
     E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{X, Y}, {X, Y}}] + 
     2*a1*E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{X, Y}, {X, Y}}] - 
     a1^2*E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{X, Y}, {X, Y}}] - 
     a1^2*E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
         2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
      \[Omega][{{X, Y}, {X, Y}}]))/
   ((3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{Y}, {X, X, Y}}])*(2*\[Lambda][{y}] + 
     \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] - 
     \[Omega][{{X, Y}, {X, Y}}])*(3*\[Lambda][{y}] + 
     \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
     \[Omega][{{X, Y}, {X, Y}}])*(\[Lambda][{y}] + 
     2*\[Omega][{{X, Y}, {X, Y}}])) + 
  (2*\[Lambda][{y}]^3*(a1^2 + (-1 + a1)^2/
      E^(T2*(1 + \[Lambda][{y}] + 2*(\[Omega][{{X}, {X, Y, Y}}] + 
           \[Omega][{{Y}, {X, X, Y}}]))) - 
     (2*(-1 + a1)*a1*(3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}]))/
      (E^(T2*(\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
          \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
       (2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
        \[Omega][{{Y}, {X, X, Y}}] - \[Omega][{{X, Y}, {X, Y}}])) + 
     (2*(-1 + a1)*a1*(\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}]))/
      (E^(T2*(3*\[Lambda][{y}] + 2*(\[Omega][{{X}, {X, Y, Y}}] + 
            \[Omega][{{Y}, {X, X, Y}}])))*(2*\[Lambda][{y}] + 
        \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] - 
        \[Omega][{{X, Y}, {X, Y}}]))))/
   (E^(T1*(1 + \[Lambda][{y}] + 2*(\[Omega][{{X}, {X, Y, Y}}] + 
         \[Omega][{{Y}, {X, X, Y}}])))*(3*\[Lambda][{y}] + 
     \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])*
    (\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}])*
    (3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}])) + 
  (2*E^(-T1 - T2 - (T1 + 5*T2)*\[Lambda][{y}] - (2*T1 + 7*T2)*
       \[Omega][{{X}, {X, Y, Y}}] - 2*T1*\[Omega][{{Y}, {X, X, Y}}] - 
      5*T2*\[Omega][{{Y}, {X, X, Y}}] - T2*\[Omega][{{X, Y}, {X, Y}}])*
    \[Lambda][{y}]^3*
    (6*E^(T2*(2*\[Lambda][{y}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
         3*\[Omega][{{Y}, {X, X, Y}}]))*
      (E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}])) - 
       2*a1*(3*E^(T2*(1 + 2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
             \[Omega][{{Y}, {X, X, Y}}])) - 
         E^(T2*(1 + \[Omega][{{X, Y}, {X, Y}}])) + 
         E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}])) - 
         3*E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
             \[Omega][{{X, Y}, {X, Y}}]))) + 
       a1^2*(6*E^(T2*(1 + 2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
             \[Omega][{{Y}, {X, X, Y}}])) - 
         2*E^(T2*(1 + \[Omega][{{X, Y}, {X, Y}}])) + 
         E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}])) - 
         6*E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
             \[Omega][{{X, Y}, {X, Y}}])) + 
         E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
            2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))))*
      \[Lambda][{y}]^3 + \[Lambda][{y}]^2*
      (-3*E^(T2*(4*\[Lambda][{y}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}]))*
        (E^(T2*\[Omega][{{X, Y}, {X, Y}}]) + 
         2*a1*(8*E^(T2*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][
                {{Y}, {X, X, Y}}])) - E^(T2*\[Omega][{{X, Y}, {X, Y}}]) - 
           7*E^(T2*(1 + \[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
               \[Omega][{{X, Y}, {X, Y}}]))) + 
         a1^2*(-16*E^(T2*(1 + \[Omega][{{X}, {X, Y, Y}}] + \[Omega][
                {{Y}, {X, X, Y}}])) + E^(T2*\[Omega][{{X, Y}, {X, Y}}]) + 
           14*E^(T2*(1 + \[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
               \[Omega][{{X, Y}, {X, Y}}])) + 
           E^(T2*(1 + \[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
              2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))))*
        \[Omega][{{X}, {X, Y, Y}}] + 
       E^(T2*(2*\[Lambda][{y}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}]))*
        (13*E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}])) + 
         a1*(-36*E^(T2*(1 + 2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
               \[Omega][{{Y}, {X, X, Y}}])) + 
           20*E^(T2*(1 + \[Omega][{{X, Y}, {X, Y}}])) - 
           26*E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}])) + 
           42*E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
               \[Omega][{{X, Y}, {X, Y}}]))) + 
         a1^2*(36*E^(T2*(1 + 2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
               \[Omega][{{Y}, {X, X, Y}}])) - 
           20*E^(T2*(1 + \[Omega][{{X, Y}, {X, Y}}])) + 
           13*E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}])) - 
           42*E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
               \[Omega][{{X, Y}, {X, Y}}])) + 
           13*E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*
                \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))))*
        \[Omega][{{Y}, {X, X, Y}}] + 
       3*E^(T2*(2*\[Lambda][{y}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
        (E^(2*T2*\[Lambda][{y}]) - 2*a1*(-2*E^T2 + E^(2*T2*\[Lambda][{y}]) + 
           E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}]))) + 
         a1^2*(-4*E^T2 + E^(2*T2*\[Lambda][{y}]) + 
           2*E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}])) + 
           E^(T2*(1 + 3*\[Lambda][{y}] + 2*(\[Omega][{{X}, {X, Y, Y}}] + 
                \[Omega][{{Y}, {X, X, Y}}])))))*\[Omega][{{X, Y}, {X, Y}}]) + 
     \[Lambda][{y}]*
      (-(E^(T2*(2*\[Lambda][{y}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
            3*\[Omega][{{Y}, {X, X, Y}}]))*
         (3*E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}])) + 
          2*a1*(6*E^(T2*(1 + 2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
                \[Omega][{{Y}, {X, X, Y}}])) + 
            5*E^(T2*(1 + \[Omega][{{X, Y}, {X, Y}}])) - 
            3*E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}])) - 
            8*E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
                \[Omega][{{X, Y}, {X, Y}}]))) + 
          a1^2*(-12*E^(T2*(1 + 2*\[Lambda][{y}] + \[Omega][{{X}, 
                  {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])) - 
            10*E^(T2*(1 + \[Omega][{{X, Y}, {X, Y}}])) + 
            3*E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}])) + 
            16*E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
                \[Omega][{{X, Y}, {X, Y}}])) + 
            3*E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
                2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))))*
         \[Omega][{{X}, {X, Y, Y}}]^2) + 
       E^(T2*(2*\[Lambda][{y}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}]))*
        (9*E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}])) - 
         2*a1*(4*E^(T2*(1 + 2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
               \[Omega][{{Y}, {X, X, Y}}])) - 
           5*E^(T2*(1 + \[Omega][{{X, Y}, {X, Y}}])) + 
           9*E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}])) - 
           8*E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
               \[Omega][{{X, Y}, {X, Y}}]))) + 
         a1^2*(8*E^(T2*(1 + 2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
               \[Omega][{{Y}, {X, X, Y}}])) - 
           10*E^(T2*(1 + \[Omega][{{X, Y}, {X, Y}}])) + 
           9*E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}])) - 
           16*E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
               \[Omega][{{X, Y}, {X, Y}}])) + 
           9*E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 2*
                \[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))))*
        \[Omega][{{Y}, {X, X, Y}}]^2 + 
       2*E^(T2*(2*\[Lambda][{y}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
        (E^(2*T2*\[Lambda][{y}]) + a1*(3*E^T2 - 2*E^(2*T2*\[Lambda][{y}]) - 
           E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}]))) + 
         a1^2*(-3*E^T2 + E^(2*T2*\[Lambda][{y}]) + 
           E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}])) + 
           E^(T2*(1 + 3*\[Lambda][{y}] + 2*(\[Omega][{{X}, {X, Y, Y}}] + 
                \[Omega][{{Y}, {X, X, Y}}])))))*\[Omega][{{Y}, {X, X, Y}}]*
        \[Omega][{{X, Y}, {X, Y}}] - 
       3*E^(T2*(4*\[Lambda][{y}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
        (1 + 2*a1*(-1 + E^(T2*(1 + \[Lambda][{y}] + 2*\[Omega][
                {{Y}, {X, X, Y}}]))) + 
         a1^2*(1 - 2*E^(T2*(1 + \[Lambda][{y}] + 2*\[Omega][{{Y}, 
                  {X, X, Y}}])) + E^(T2*(1 + \[Lambda][{y}] + 
              2*(\[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])))))*
        \[Omega][{{X, Y}, {X, Y}}]^2 - 2*\[Omega][{{X}, {X, Y, Y}}]*
        (E^(T2*(2*\[Lambda][{y}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}]))*
          (E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}])) + 
           2*a1*(11*E^(T2*(1 + 2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, 
                    Y}}] + \[Omega][{{Y}, {X, X, Y}}])) - 
             2*E^(T2*(1 + \[Omega][{{X, Y}, {X, Y}}])) - 
             E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}])) - 
             8*E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
                 \[Omega][{{X, Y}, {X, Y}}]))) + 
           a1^2*(-22*E^(T2*(1 + 2*\[Lambda][{y}] + \[Omega][{{X}, 
                   {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}])) + 
             4*E^(T2*(1 + \[Omega][{{X, Y}, {X, Y}}])) + 
             E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}])) + 
             16*E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
                 \[Omega][{{X, Y}, {X, Y}}])) + E^(T2*(1 + 3*\[Lambda][{y}] + 
                2*\[Omega][{{X}, {X, Y, Y}}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
                \[Omega][{{X, Y}, {X, Y}}]))))*\[Omega][{{Y}, {X, X, Y}}] - 
         E^(T2*(2*\[Lambda][{y}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (3*E^(2*T2*\[Lambda][{y}]) + a1*(7*E^T2 - 
             6*E^(2*T2*\[Lambda][{y}]) - E^(T2*(1 + 3*\[Lambda][{y}] + 
                2*\[Omega][{{Y}, {X, X, Y}}]))) + 
           a1^2*(-7*E^T2 + 3*E^(2*T2*\[Lambda][{y}]) + 
             E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}])) + 
             3*E^(T2*(1 + 3*\[Lambda][{y}] + 2*(\[Omega][{{X}, {X, Y, Y}}] + 
                   \[Omega][{{Y}, {X, X, Y}}])))))*
          \[Omega][{{X, Y}, {X, Y}}])) + 
     2*(-((-1 + a1)*a1*E^(T2*(1 + 2*\[Lambda][{y}] + 
            5*\[Omega][{{X}, {X, Y, Y}}] + 3*\[Omega][{{Y}, {X, X, Y}}] + 
            \[Omega][{{X, Y}, {X, Y}}]))*(-1 + E^(3*T2*\[Lambda][{y}] + 
            2*T2*\[Omega][{{Y}, {X, X, Y}}]))*\[Omega][{{X}, {X, Y, Y}}]^3) - 
       E^(T2*(2*\[Lambda][{y}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}]))*
        (E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}])) + 
         a1*(4*E^(T2*(1 + 2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
               \[Omega][{{Y}, {X, X, Y}}])) + 
           E^(T2*(1 + \[Omega][{{X, Y}, {X, Y}}])) - 
           2*E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}])) - 
           3*E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
               \[Omega][{{X, Y}, {X, Y}}]))) + 
         a1^2*(-4*E^(T2*(1 + 2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
               \[Omega][{{Y}, {X, X, Y}}])) - 
           E^(T2*(1 + \[Omega][{{X, Y}, {X, Y}}])) + 
           E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X, Y}, {X, Y}}])) + 
           3*E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
               \[Omega][{{X, Y}, {X, Y}}])) + 
           E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}] + 
              2*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))))*
        \[Omega][{{X}, {X, Y, Y}}]^2*\[Omega][{{Y}, {X, X, Y}}] + 
       E^(T2*(2*\[Lambda][{y}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
           3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
        (E^(2*T2*\[Lambda][{y}]) + a1*(E^T2 - 2*E^(2*T2*\[Lambda][{y}]) + 
           E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}]))) + 
         a1^2*(-E^T2 + E^(2*T2*\[Lambda][{y}]) - 
           E^(T2*(1 + 3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}])) + 
           E^(T2*(1 + 3*\[Lambda][{y}] + 2*(\[Omega][{{X}, {X, Y, Y}}] + 
                \[Omega][{{Y}, {X, X, Y}}])))))*\[Omega][{{Y}, {X, X, Y}}]*
        (\[Omega][{{Y}, {X, X, Y}}]^2 - \[Omega][{{X, Y}, {X, Y}}]^2) + 
       \[Omega][{{X}, {X, Y, Y}}]*
        (-((-1 + a1)*a1*E^(T2*(1 + 2*\[Lambda][{y}] + 5*\[Omega][
                {{X}, {X, Y, Y}}] + 3*\[Omega][{{Y}, {X, X, Y}}]))*
           (-4*E^(T2*(2*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
                \[Omega][{{Y}, {X, X, Y}}])) + 
            E^(T2*\[Omega][{{X, Y}, {X, Y}}]) + 
            3*E^(T2*(3*\[Lambda][{y}] + 2*\[Omega][{{Y}, {X, X, Y}}] + 
                \[Omega][{{X, Y}, {X, Y}}])))*\[Omega][{{Y}, {X, X, Y}}]^2) + 
         2*E^(T2*(2*\[Lambda][{y}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (E^(2*T2*\[Lambda][{y}]) + 2*a1*(E^T2 - E^(2*T2*\[Lambda][{y}])) + 
           a1^2*(-2*E^T2 + E^(2*T2*\[Lambda][{y}]) + 
             E^(T2*(1 + 3*\[Lambda][{y}] + 2*(\[Omega][{{X}, {X, Y, Y}}] + 
                  \[Omega][{{Y}, {X, X, Y}}])))))*\[Omega][{{Y}, {X, X, Y}}]*
          \[Omega][{{X, Y}, {X, Y}}] + (-1 + a1)*a1*
          E^(T2*(1 + 2*\[Lambda][{y}] + 5*\[Omega][{{X}, {X, Y, Y}}] + 
             3*\[Omega][{{Y}, {X, X, Y}}] + \[Omega][{{X, Y}, {X, Y}}]))*
          (-1 + E^(3*T2*\[Lambda][{y}] + 2*T2*\[Omega][{{Y}, {X, X, Y}}]))*
          \[Omega][{{X, Y}, {X, Y}}]^2))))/
   ((\[Lambda][{y}] + 2*\[Omega][{{X}, {X, Y, Y}}])*
    (3*\[Lambda][{y}] + \[Omega][{{X}, {X, Y, Y}}] + 
     \[Omega][{{Y}, {X, X, Y}}])*(3*\[Lambda][{y}] + 
     2*\[Omega][{{Y}, {X, X, Y}}])*(2*\[Lambda][{y}] + 
     \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] - 
     \[Omega][{{X, Y}, {X, Y}}])*(\[Lambda][{y}] - 
     \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
     \[Omega][{{X, Y}, {X, Y}}])*(3*\[Lambda][{y}] + 
     \[Omega][{{X}, {X, Y, Y}}] + \[Omega][{{Y}, {X, X, Y}}] + 
     \[Omega][{{X, Y}, {X, Y}}]))}
